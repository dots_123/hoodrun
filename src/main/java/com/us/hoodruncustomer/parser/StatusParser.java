package com.us.hoodruncustomer.parser;

import org.json.JSONException;
import org.json.JSONObject;

public class StatusParser {
    private static String status;
    private static String message = "message";
    private static String customer_id;
    private static String pickup_latitude;
    private static String pickup_longitude;
    private static String pickup_location_name;
    private static String destination_latitude;
    private static String destination_longitude;
    private static String destination_location_name;
    private static String ride_id;
    private static String driver_id;
    private static String payment_id;
    private static String total_passengers;
    private static String driver_latitude;
    private static String driver_longitude;
    private static String trip_distance_miles;
    private static String trip_duration;
    private static String payment_mode;
    private static String total_cost;
    private static String discount_applied;
    private static String discount_total;
    private static String request_id;
    private static String customer_name;
    private static String tip;
    private static String ride_rating;
    private static String estimated_fare;
    private static String driver_name;
    private static String customer_image;
    private static String vehicle_type;
    private static String vehicle_model;
    private static String fare;
    private static String driver_list_car_image;
    private static String driver_map_car_image;
    private static String driver_future_request_id;
    private static String customer_mobile_number;

    private static String start_location_name;
    private static String end_location_name;
    private static String no_of_seats;
    private static String list_car_image;
    private static String driver_profile_pic;
    private static String total;

    public static String getTotal() {
        return total;
    }

    public static void setTotal(String total) {
        StatusParser.total = total;
    }

    public static String getStart_location_name() {
        return start_location_name;
    }

    public static void setStart_location_name(String start_location_name) {
        StatusParser.start_location_name = start_location_name;
    }

    public static String getEnd_location_name() {
        return end_location_name;
    }

    public static void setEnd_location_name(String end_location_name) {
        StatusParser.end_location_name = end_location_name;
    }

    public static String getNo_of_seats() {
        return no_of_seats;
    }

    public static void setNo_of_seats(String no_of_seats) {
        StatusParser.no_of_seats = no_of_seats;
    }

    public static String getList_car_image() {
        return list_car_image;
    }

    public static void setList_car_image(String list_car_image) {
        StatusParser.list_car_image = list_car_image;
    }

    public static String getDriver_profile_pic() {
        return driver_profile_pic;
    }

    public static void setDriver_profile_pic(String driver_profile_pic) {
        StatusParser.driver_profile_pic = driver_profile_pic;
    }

    public static String getFare() {
        return fare;
    }

    public static String getTotal_passengers() {
        return total_passengers;
    }

    public static String getEstimated_fare() {
        return estimated_fare;
    }

    public static String getDriver_name() {
        return driver_name;
    }

    public static String getCustomer_image() {
        return customer_image;
    }

    public static String getVehicle_type() {
        return vehicle_type;
    }

    public static String getVehicle_model() {
        return vehicle_model;
    }


    public static String getCustomer_name() {
        return customer_name;
    }

    public static String getTip() {
        return tip;
    }

    public static String getRide_rating() {
        return ride_rating;
    }

    public static String getCheck_fav() {
        return check_fav;
    }

    private static String check_fav;

    public static String getRequest_id() {
        return request_id;
    }


    public static String getRideStatus(String data, String user) {


        try {

            JSONObject mObject = new JSONObject(data);
            status = mObject.optString("status");
            message = mObject.optString("message");


            if (status.equals("TRUE")) {
                if (user.equals("customer_id")) {
                    switch (message) {
                        case "Driver is not arrived":
                            ride_id = mObject.optString("ride_id");
                            request_id = mObject.optString("request_id");
                            driver_id = mObject.optString("driver_id");
                            driver_latitude = mObject.optString("driver_latitude");
                            driver_longitude = mObject.optString("driver_longitude");

                            break;
                        case "Driver arrived at the the pick up location":
                            ride_id = mObject.optString("ride_id");
                            request_id = mObject.optString("request_id");
                            driver_id = mObject.optString("driver_id");
                            driver_latitude = mObject.optString("driver_latitude");
                            driver_longitude = mObject.optString("driver_longitude");

                            break;
                        case "Journey started":
                            ride_id = mObject.optString("ride_id");
                            driver_id = mObject.optString("driver_id");
                            request_id = mObject.optString("request_id");
                            driver_latitude = mObject.optString("driver_latitude");
                            driver_longitude = mObject.optString("driver_longitude");

                            break;
                        case "Journey completed":
                            ride_id = mObject.optString("ride_id");
                            driver_id = mObject.optString("driver_id");
                            request_id = mObject.optString("request_id");
                            payment_id = mObject.optString("payment_id");
                            driver_latitude = mObject.optString("driver_latitude");
                            driver_longitude = mObject.optString("driver_longitude");

                            break;
                        case "Incomplete payment":
                            ride_id = mObject.optString("ride_id");
                            driver_id = mObject.optString("driver_id");
                            total_cost = mObject.optString("total_cost");
                            trip_distance_miles = mObject.optString("trip_distance");
                            trip_duration = mObject.optString("trip_duration");
                            payment_mode = mObject.optString("payment_mode");
                            driver_latitude = mObject.optString("driver_latitude");
                            driver_longitude = mObject.optString("driver_longitude");

                            break;
                        case "Incomplete ride rating":
                            ride_id = mObject.optString("ride_id");
                            driver_id = mObject.optString("driver_id");
                            driver_latitude = mObject.optString("driver_latitude");
                            driver_longitude = mObject.optString("driver_longitude");

                            break;
                        case "No response from the driver":

                            request_id = mObject.optString("request_id");
                            driver_id = mObject.optString("driver_id");

                            break;
                        case "Tip is successfully added for this ride":

                            ride_id = mObject.optString("ride_id");
                            driver_id = mObject.optString("driver_id");
                            total_cost = mObject.optString("total_cost");
                            trip_distance_miles = mObject.optString("trip_distance");
                            trip_duration = mObject.optString("trip_duration");
                            tip = mObject.optString("tip_the_driver");
                            fare = mObject.optString("fare");

                            break;
                        case "Tip is not given for this ride":
                            ride_id = mObject.optString("ride_id");
                            driver_id = mObject.optString("driver_id");
                            total_cost = mObject.optString("total_cost");
                            trip_distance_miles = mObject.optString("trip_distance");
                            trip_duration = mObject.optString("trip_duration");
                            driver_latitude = mObject.optString("driver_latitude");
                            driver_longitude = mObject.optString("driver_longitude");
                            tip = mObject.optString("tip_the_driver");
                            discount_applied = mObject.optString("discount_applied");
                            discount_total = mObject.optString("discount_total");
                            break;
                    }

                } else {

                    request_id = mObject.optString("request_id");
                    ride_id = mObject.optString("ride_id");
                    customer_id = mObject.optString("customer_id");
                    pickup_latitude = mObject.optString("pickup_latitude");
                    pickup_longitude = mObject.optString("pickup_longitude");
                    pickup_location_name = mObject.optString("pickup_location_name");
                    destination_latitude = mObject.optString("destination_latitude");
                    destination_longitude = mObject.optString("destination_longitude");
                    destination_location_name = mObject.optString("destination_location_name");
                    driver_latitude = mObject.optString("driver_latitude");
                    driver_longitude = mObject.optString("driver_longitude");
                    trip_distance_miles = mObject.optString("trip_distance_miles");
                    trip_duration = mObject.optString("trip_duration");
                    payment_mode = mObject.optString("payment_mode");
                    total_cost = mObject.optString("total_cost");
                    discount_applied = mObject.optString("discount_applied");
                    discount_total = mObject.optString("discount_total");
                    driver_list_car_image = mObject.optString("list_car_image");
                    driver_map_car_image = mObject.optString("map_car_image");
                    driver_future_request_id = mObject.optString("future_request_id");
                    driver_future_request_id = mObject.optString("future_request_id");
                    driver_future_request_id = mObject.optString("future_request_id");
                    driver_future_request_id = mObject.optString("future_request_id");
                    driver_future_request_id = mObject.optString("future_request_id");
                    customer_mobile_number = mObject.optString("mobile_no");
                    start_location_name = mObject.optString("start_location_name");
                    end_location_name = mObject.optString("end_location_name");
                    no_of_seats = mObject.optString("no_of_seats");
                    vehicle_type = mObject.optString("vehicle_type");
                    list_car_image = mObject.optString("list_car_image");
                    driver_profile_pic = mObject.optString("driver_profile_pic");
                    total = mObject.optString("total");

                }
            } else {

                driver_list_car_image = mObject.optString("list_car_image");
                driver_map_car_image = mObject.optString("map_car_image");

            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return status;

    }

    public static void setMessage(String message) {
        StatusParser.message = message;
    }

    public static String fav_status;

    public static String getFav_status() {
        return fav_status;
    }

    public static String getDriverResponse(String data) {
        try {

            JSONObject mObject = new JSONObject(data);
            message = mObject.optString("message");
            ride_id = mObject.optString("ride_id");
            request_id = mObject.optString("request_id");
            driver_id = mObject.optString("driver_id");
            driver_latitude = mObject.optString("driver_latitude");
            driver_longitude = mObject.optString("driver_longitude");

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return message;

    }

    public static String share_resp;

    public static String getShare_resp() {
        return share_resp;
    }

    public static String getShareResponseSec(String data) {
        try {
            JSONObject mObject = new JSONObject(data);
            message = mObject.optString("message");
            customer_name = mObject.optString("customer_name");
            share_resp = mObject.optString("share_status");


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return message;

    }

    public static String getDriverJourney(String data) {
        try {
            JSONObject mObject = new JSONObject(data);
            message = mObject.optString("message");
            ride_id = mObject.optString("ride_id");
            driver_id = mObject.optString("driver_id");
            fav_status = mObject.optString("favourite_status");
            trip_distance_miles = mObject.optString("trip_distance");
            trip_duration = mObject.optString("trip_duration");
            total_cost = mObject.optString("total");
            discount_applied = mObject.optString("discount_applied");
            discount_total = mObject.optString("discount_total");


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return message;

    }


    public static String getCustomerCancelRide(String data) {
        try {
            JSONObject mObject = new JSONObject(data);
            message = mObject.optString("message");
            customer_id = mObject.optString("customer_id");
            customer_name = mObject.optString("customer_name");

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return message;

    }

    public static String getShareCancelByPrimaryCustomer(String data) {
        try {
            JSONObject mObject = new JSONObject(data);
            message = mObject.optString("message");
            customer_id = mObject.optString("customer_id");
            customer_name = mObject.optString("customer_name");

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return message;

    }


    public static String getShareExpiredToSecondaryCustomer(String data) {
        try {
            JSONObject mObject = new JSONObject(data);
            message = mObject.optString("message");
            estimated_fare = mObject.optString("total_passengers");
            total_passengers = mObject.optString("approximate_fare");

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return message;

    }


    public static String getCustomerTipToDriver(String data) {
        try {
            JSONObject mObject = new JSONObject(data);
            message = mObject.optString("message");
            customer_id = mObject.optString("customer_id");
            tip = mObject.optString("tip");
            trip_distance_miles = mObject.optString("trip_distance");
            trip_duration = mObject.optString("trip_duration");
            total_cost = mObject.optString("total");
            fare = mObject.optString("fare");
            payment_mode = mObject.optString("payment_type");

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return message;

    }


    public static String getCustomerFeedback(String data) {
        try {
            JSONObject mObject = new JSONObject(data);


            message = mObject.optString("message");
            customer_id = mObject.optString("customer_id");
            customer_name = mObject.optString("customer_name");
            check_fav = mObject.optString("check_fav");
            fav_status = mObject.optString("fav_status");
            tip = mObject.optString("tip");
            ride_rating = mObject.optString("ride_ratting");


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return message;

    }


    /**
     * Hopoon Shared
     */

    public static String SharedOut_ride_id;
    public static String SharedOut_discount_total;
    public static String SharedOut_no_Seats;
    public static String SharedOut_payment_mode;
    public static String SharedOut_your_amount;
    public static String SharedOut_push_Status_Flag;
    public static String SharedOut_no_of_seats;
    public static String SharedOut_customer_pick_up_point_name;
    public static String SharedOut_driver_profile_pic;
    public static String SharedOut_vehicle_type;
    public static String SharedOut_promo_code;
    public static String SharedOut_list_car_image;
    public static String SharedOut_message;
    public static String SharedOut_title;
    public static String SharedOut_trip_distance;
    public static String SharedOut_total;
    public static String SharedOut_payment_type;
    public static String SharedOut_discount_applied;
    public static String SharedOut_customer_destination_point_name;
    public static String SharedOut_fare_multiLier;
    public static String SharedOut_trip_duration;
    public static String SharedOut_damage_charge;


    public static void setSharedDetails(String response) {

        JSONObject mObject = null;
        try {
            mObject = new JSONObject(response);

            SharedOut_ride_id = mObject.optString("ride_id");
            SharedOut_discount_total = mObject.optString("discount_total");
            SharedOut_no_Seats = mObject.optString("no_Seats");
            SharedOut_payment_mode = mObject.optString("payment_mode");
            SharedOut_your_amount = mObject.optString("your_amount");
            SharedOut_push_Status_Flag = mObject.optString("push_Status_Flag");
            SharedOut_no_of_seats = mObject.optString("no_of_seats");
            SharedOut_customer_pick_up_point_name = mObject.optString("customer_pick_up_point_name");
            SharedOut_driver_profile_pic = mObject.optString("driver_profile_pic");
            SharedOut_vehicle_type = mObject.optString("vehicle_type");
            SharedOut_promo_code = mObject.optString("promo_code");
            SharedOut_list_car_image = mObject.optString("list_car_image");
            SharedOut_message = mObject.optString("message");
            SharedOut_title = mObject.optString("title");
            SharedOut_trip_distance = mObject.optString("trip_distance");
            SharedOut_total = mObject.optString("total");
            SharedOut_payment_type = mObject.optString("payment_type");
            SharedOut_discount_applied = mObject.optString("discount_applied");
            SharedOut_customer_destination_point_name = mObject.optString("customer_destination_point_name");
            SharedOut_fare_multiLier = mObject.optString("fare_multiplier");
            SharedOut_trip_duration = mObject.optString("trip_duration");
            SharedOut_damage_charge = mObject.optString("damage_charge");

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    public static String getSharedOut_ride_id() {
        return SharedOut_ride_id;
    }

    public static void setSharedOut_ride_id(String sharedOut_ride_id) {
        SharedOut_ride_id = sharedOut_ride_id;
    }

    public static String getSharedOut_discount_total() {
        return SharedOut_discount_total;
    }

    public static void setSharedOut_discount_total(String sharedOut_discount_total) {
        SharedOut_discount_total = sharedOut_discount_total;
    }

    public static String getSharedOut_no_Seats() {
        return SharedOut_no_Seats;
    }

    public static void setSharedOut_no_Seats(String sharedOut_no_Seats) {
        SharedOut_no_Seats = sharedOut_no_Seats;
    }

    public static String getSharedOut_payment_mode() {
        return SharedOut_payment_mode;
    }

    public static void setSharedOut_payment_mode(String sharedOut_payment_mode) {
        SharedOut_payment_mode = sharedOut_payment_mode;
    }

    public static String getSharedOut_your_amount() {
        return SharedOut_your_amount;
    }

    public static void setSharedOut_your_amount(String sharedOut_your_amount) {
        SharedOut_your_amount = sharedOut_your_amount;
    }

    public static String getSharedOut_push_Status_Flag() {
        return SharedOut_push_Status_Flag;
    }

    public static void setSharedOut_push_Status_Flag(String sharedOut_push_Status_Flag) {
        SharedOut_push_Status_Flag = sharedOut_push_Status_Flag;
    }

    public static String getSharedOut_no_of_seats() {
        return SharedOut_no_of_seats;
    }

    public static void setSharedOut_no_of_seats(String sharedOut_no_of_seats) {
        SharedOut_no_of_seats = sharedOut_no_of_seats;
    }

    public static String getSharedOut_customer_pick_up_point_name() {
        return SharedOut_customer_pick_up_point_name;
    }

    public static void setSharedOut_customer_pick_up_point_name(String sharedOut_customer_pick_up_point_name) {
        SharedOut_customer_pick_up_point_name = sharedOut_customer_pick_up_point_name;
    }

    public static String getSharedOut_driver_profile_pic() {
        return SharedOut_driver_profile_pic;
    }

    public static void setSharedOut_driver_profile_pic(String sharedOut_driver_profile_pic) {
        SharedOut_driver_profile_pic = sharedOut_driver_profile_pic;
    }

    public static String getSharedOut_vehicle_type() {
        return SharedOut_vehicle_type;
    }

    public static void setSharedOut_vehicle_type(String sharedOut_vehicle_type) {
        SharedOut_vehicle_type = sharedOut_vehicle_type;
    }

    public static String getSharedOut_promo_code() {
        return SharedOut_promo_code;
    }

    public static void setSharedOut_promo_code(String sharedOut_promo_code) {
        SharedOut_promo_code = sharedOut_promo_code;
    }

    public static String getSharedOut_list_car_image() {
        return SharedOut_list_car_image;
    }

    public static void setSharedOut_list_car_image(String sharedOut_list_car_image) {
        SharedOut_list_car_image = sharedOut_list_car_image;
    }

    public static String getSharedOut_message() {
        return SharedOut_message;
    }

    public static void setSharedOut_message(String sharedOut_message) {
        SharedOut_message = sharedOut_message;
    }

    public static String getSharedOut_title() {
        return SharedOut_title;
    }

    public static void setSharedOut_title(String sharedOut_title) {
        SharedOut_title = sharedOut_title;
    }

    public static String getSharedOut_trip_distance() {
        return SharedOut_trip_distance;
    }

    public static void setSharedOut_trip_distance(String sharedOut_trip_distance) {
        SharedOut_trip_distance = sharedOut_trip_distance;
    }

    public static String getSharedOut_total() {
        return SharedOut_total;
    }

    public static void setSharedOut_total(String sharedOut_total) {
        SharedOut_total = sharedOut_total;
    }

    public static String getSharedOut_payment_type() {
        return SharedOut_payment_type;
    }

    public static void setSharedOut_payment_type(String sharedOut_payment_type) {
        SharedOut_payment_type = sharedOut_payment_type;
    }

    public static String getSharedOut_discount_applied() {
        return SharedOut_discount_applied;
    }

    public static void setSharedOut_discount_applied(String sharedOut_discount_applied) {
        SharedOut_discount_applied = sharedOut_discount_applied;
    }

    public static String getSharedOut_customer_destination_point_name() {
        return SharedOut_customer_destination_point_name;
    }

    public static void setSharedOut_customer_destination_point_name(String sharedOut_customer_destination_point_name) {
        SharedOut_customer_destination_point_name = sharedOut_customer_destination_point_name;
    }

    public static String getSharedOut_fare_multiLier() {
        return SharedOut_fare_multiLier;
    }

    public static void setSharedOut_fare_multiLier(String sharedOut_fare_multiLier) {
        SharedOut_fare_multiLier = sharedOut_fare_multiLier;
    }

    public static String getSharedOut_trip_duration() {
        return SharedOut_trip_duration;
    }

    public static void setSharedOut_trip_duration(String sharedOut_trip_duration) {
        SharedOut_trip_duration = sharedOut_trip_duration;
    }

    public static String getSharedOut_damage_charge() {
        return SharedOut_damage_charge;
    }

    public static void setSharedOut_damage_charge(String sharedOut_damage_charge) {
        SharedOut_damage_charge = sharedOut_damage_charge;
    }


	/* Parser for share push notification */


    /**
     * Firebase Messaging Shared
     *
     * @param data
     * @return
     */


    public static String Shared_destinationLocationName;
    public static String Shared_pushStatusFlag;
    public static String Shared_customerMobileNo;
    public static String Shared_pickingLocationName;
    public static String Shared_driverEmail;
    public static String Shared_customerDestinationPointLong;
    public static String Shared_vehicleType;
    public static String Shared_customerDestinationPointLat;
    public static String Shared_message;
    public static String Shared_title;
    public static String Shared_vehicleModel;
    public static String Shared_shareId;
    public static String Shared_driverName;
    public static String Shared_customerPickUpPointLat;
    public static String Shared_customerPickUpPointLong;
    public static String Shared_estimatedFare;
    public static String Shared_customerName;
    public static String Shared_requestId;
    public static String Shared_driverImage;
    public static String Shared_Shared_ustomerImage;


    public static void setShared(String response) {

        JSONObject mObject = null;
        try {
            mObject = new JSONObject(response);
            Shared_destinationLocationName = mObject.optString("destination_Location_Name");
            Shared_pushStatusFlag = mObject.optString("push_Status_Flag");
            Shared_customerMobileNo = mObject.optString("customer_mobile_no");
            Shared_pickingLocationName = mObject.optString("picking_Location_Name");
            Shared_driverEmail = mObject.optString("driver_email");
            Shared_customerDestinationPointLong = mObject.optString("customer_destination_point_long");
            Shared_vehicleType = mObject.optString("vehicle_type");
            Shared_customerDestinationPointLat = mObject.optString("customer_destination_point_lat");
            Shared_message = mObject.optString("message");
            Shared_title = mObject.optString("title");
            Shared_vehicleModel = mObject.optString("vehicle_Model");
            Shared_shareId = mObject.optString("share_id");
            Shared_driverName = mObject.optString("driver_name");
            Shared_customerPickUpPointLat = mObject.optString("customer_pick_up_point_lat");
            Shared_customerPickUpPointLong = mObject.optString("customer_pick_up_point_long");
            Shared_estimatedFare = mObject.optString("estimated_fare");
            Shared_customerName = mObject.optString("customer_name");
            Shared_requestId = mObject.optString("request_id");
            Shared_driverImage = mObject.optString("driver_image");
            Shared_Shared_ustomerImage = mObject.optString("customer_image");

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public static String getShared_destinationLocationName() {
        return Shared_destinationLocationName;
    }

    public static void setShared_destinationLocationName(String shared_destinationLocationName) {
        Shared_destinationLocationName = shared_destinationLocationName;
    }

    public static String getShared_pushStatusFlag() {
        return Shared_pushStatusFlag;
    }

    public static void setShared_pushStatusFlag(String shared_pushStatusFlag) {
        Shared_pushStatusFlag = shared_pushStatusFlag;
    }

    public static String getShared_customerMobileNo() {
        return Shared_customerMobileNo;
    }

    public static void setShared_customerMobileNo(String shared_customerMobileNo) {
        Shared_customerMobileNo = shared_customerMobileNo;
    }

    public static String getShared_pickingLocationName() {
        return Shared_pickingLocationName;
    }

    public static void setShared_pickingLocationName(String shared_pickingLocationName) {
        Shared_pickingLocationName = shared_pickingLocationName;
    }

    public static String getShared_driverEmail() {
        return Shared_driverEmail;
    }

    public static void setShared_driverEmail(String shared_driverEmail) {
        Shared_driverEmail = shared_driverEmail;
    }

    public static String getShared_customerDestinationPointLong() {
        return Shared_customerDestinationPointLong;
    }

    public static void setShared_customerDestinationPointLong(String shared_customerDestinationPointLong) {
        Shared_customerDestinationPointLong = shared_customerDestinationPointLong;
    }

    public static String getShared_vehicleType() {
        return Shared_vehicleType;
    }

    public static void setShared_vehicleType(String shared_vehicleType) {
        Shared_vehicleType = shared_vehicleType;
    }

    public static String getShared_customerDestinationPointLat() {
        return Shared_customerDestinationPointLat;
    }

    public static void setShared_customerDestinationPointLat(String shared_customerDestinationPointLat) {
        Shared_customerDestinationPointLat = shared_customerDestinationPointLat;
    }

    public static String getShared_message() {
        return Shared_message;
    }

    public static void setShared_message(String shared_message) {
        Shared_message = shared_message;
    }

    public static String getShared_title() {
        return Shared_title;
    }

    public static void setShared_title(String shared_title) {
        Shared_title = shared_title;
    }

    public static String getShared_vehicleModel() {
        return Shared_vehicleModel;
    }

    public static void setShared_vehicleModel(String shared_vehicleModel) {
        Shared_vehicleModel = shared_vehicleModel;
    }

    public static String getShared_shareId() {
        return Shared_shareId;
    }

    public static void setShared_shareId(String shared_shareId) {
        Shared_shareId = shared_shareId;
    }

    public static String getShared_driverName() {
        return Shared_driverName;
    }

    public static void setShared_driverName(String shared_driverName) {
        Shared_driverName = shared_driverName;
    }

    public static String getShared_customerPickUpPointLat() {
        return Shared_customerPickUpPointLat;
    }

    public static void setShared_customerPickUpPointLat(String shared_customerPickUpPointLat) {
        Shared_customerPickUpPointLat = shared_customerPickUpPointLat;
    }

    public static String getShared_customerPickUpPointLong() {
        return Shared_customerPickUpPointLong;
    }

    public static void setShared_customerPickUpPointLong(String shared_customerPickUpPointLong) {
        Shared_customerPickUpPointLong = shared_customerPickUpPointLong;
    }

    public static String getShared_estimatedFare() {
        return Shared_estimatedFare;
    }

    public static void setShared_estimatedFare(String shared_estimatedFare) {
        Shared_estimatedFare = shared_estimatedFare;
    }

    public static String getShared_customerName() {
        return Shared_customerName;
    }

    public static void setShared_customerName(String shared_customerName) {
        Shared_customerName = shared_customerName;
    }

    public static String getShared_requestId() {
        return Shared_requestId;
    }

    public static void setShared_requestId(String shared_requestId) {
        Shared_requestId = shared_requestId;
    }

    public static String getShared_driverImage() {
        return Shared_driverImage;
    }

    public static void setShared_driverImage(String shared_driverImage) {
        Shared_driverImage = shared_driverImage;
    }

    public static String getShared_Shared_ustomerImage() {
        return Shared_Shared_ustomerImage;
    }

    public static void setShared_Shared_ustomerImage(String shared_Shared_ustomerImage) {
        Shared_Shared_ustomerImage = shared_Shared_ustomerImage;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    public static String getSharePushDetails(String data) {
        try {
            JSONObject mObject = new JSONObject(data);
            message = mObject.optString("message");
            pickup_latitude = mObject.optString("customer_pick_up_point_lat");
            pickup_longitude = mObject.optString("customer_pick_up_point_long");
            pickup_location_name = mObject.optString("picking_Location_Name");
            destination_latitude = mObject.optString("customer_destination_point_lat");
            destination_longitude = mObject.optString("customer_destination_point_long");
            destination_location_name = mObject.optString("destination_Location_Name");
            estimated_fare = mObject.optString("estimated_fare");
            vehicle_type = mObject.getString("vehicle_type");
            vehicle_model = mObject.optString("vehicle_Model");
            customer_name = mObject.optString("customer_name");
            customer_image = mObject.optString("customer_image");
            payment_mode = mObject.optString("payment_type");
            request_id = mObject.optString("request_id");
            share_amount = mObject.optString("your_amout");

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return message;

    }

    private static String share_amount;

    public static String getShare_amount() {
        return share_amount;
    }

    public static String getSharePushAccepted(String data) {
        try {
            JSONObject mObject = new JSONObject(data);
            message = mObject.optString("message");
            share_amount = mObject.optString("your_amount");
            ride_id = mObject.optString("ride_id");
            trip_distance_miles = mObject.optString("trip_distance");
            trip_duration = mObject.optString("trip_duration");
            total_cost = mObject.optString("total");
            discount_applied = mObject.optString("discount_applied");
            discount_total = mObject.optString("discount_total");

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return message;

    }


    public static String getShareCancelledByDriver(String data) {
        try {
            JSONObject mObject = new JSONObject(data);
            message = mObject.optString("message");
            ride_id = mObject.optString("ride_id");
            driver_id = mObject.optString("driver_id");

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return message;

    }


    public static String getPayment_id() {
        return payment_id;
    }

    public static String getDriver_id() {
        return driver_id;
    }

    public static String getStatus() {
        return status;
    }

    public static String getMessage() {

        return message;
    }

    public static String getCustomer_id() {
        return customer_id;
    }

    public static String getPickup_latitude() {
        return pickup_latitude;
    }

    public static String getPickup_longitude() {
        return pickup_longitude;
    }

    public static String getPickup_location_name() {
        return pickup_location_name;
    }

    public static String getDestination_latitude() {
        return destination_latitude;
    }

    public static String getDestination_longitude() {
        return destination_longitude;
    }

    public static String getDestination_location_name() {
        return destination_location_name;
    }

    public static String getRide_id() {
        return ride_id;
    }

    public static String getDriver_latitude() {
        return driver_latitude;
    }

    public static String getDriver_longitude() {
        return driver_longitude;
    }

    public static String getTrip_distance_miles() {
        return trip_distance_miles;
    }

    public static String getTrip_duration() {
        return trip_duration;
    }

    public static String getPayment_mode() {
        return payment_mode;
    }

    public static String getTotal_cost() {
        return total_cost;
    }

    public static String getDiscount_applied() {
        return discount_applied;
    }

    public static void setDiscount_applied(String discount_applied) {
        StatusParser.discount_applied = discount_applied;
    }

    public static String getDiscount_total() {
        return discount_total;
    }

    public static void setDiscount_total(String discount_total) {
        StatusParser.discount_total = discount_total;
    }

    public static String getDriver_list_car_image() {
        return driver_list_car_image;
    }

    public static void setDriver_list_car_image(String driver_list_car_image) {
        StatusParser.driver_list_car_image = driver_list_car_image;
    }

    public static String getDriver_map_car_image() {
        return driver_map_car_image;
    }

    public static void setDriver_map_car_image(String driver_map_car_image) {
        StatusParser.driver_map_car_image = driver_map_car_image;
    }

    public static String getDriver_future_request_id() {
        return driver_future_request_id;
    }

    public static void setDriver_future_request_id(String driver_future_request_id) {
        StatusParser.driver_future_request_id = driver_future_request_id;
    }

    public static String getCustomer_mobile_number() {
        return customer_mobile_number;
    }

    public static void setCustomer_mobile_number(String customer_mobile_number) {
        StatusParser.customer_mobile_number = customer_mobile_number;
    }
}
