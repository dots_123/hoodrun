package com.us.hoodruncustomer.model;

/**
 * Created by admin on 25-Jan-17.
 */

public class WalletInfoDao {

    public String driver_earning;
    public String base_fare;
    public String hoponn_earning;
    public String total_cost;

    public WalletInfoDao(String driver_earning, String base_fare, String hoponn_earning, String total_cost) {
        this.driver_earning = driver_earning;
        this.base_fare = base_fare;
        this.hoponn_earning = hoponn_earning;
        this.total_cost = total_cost;
    }

    public WalletInfoDao() {

    }

    public String getDriver_earning() {
        return driver_earning;
    }

    public void setDriver_earning(String driver_earning) {
        this.driver_earning = driver_earning;
    }

    public String getBase_fare() {
        return base_fare;
    }

    public void setBase_fare(String base_fare) {
        this.base_fare = base_fare;
    }

    public String getHoponn_earning() {
        return hoponn_earning;
    }

    public void setHoponn_earning(String hoponn_earning) {
        this.hoponn_earning = hoponn_earning;
    }

    public String getTotal_cost() {
        return total_cost;
    }

    public void setTotal_cost(String total_cost) {
        this.total_cost = total_cost;
    }
}
