package com.us.hoodruncustomer.ridehistory;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.commonwork.BaseActivity;
import com.us.hoodruncustomer.commonwork.HistoryDetailsActivity;
import com.us.hoodruncustomer.commonwork.Injection;
import com.us.hoodruncustomer.configure.Configure;
import com.us.hoodruncustomer.exceptionhandler.ExceptionHandler;
import com.us.hoodruncustomer.fragment.InvalidTokenDialog;
import com.us.hoodruncustomer.fragment.PushDialog;
import com.us.hoodruncustomer.model.History;
import com.us.hoodruncustomer.model.HistoryListAdapter;
import com.us.hoodruncustomer.parser.ConnectionDetector;
import com.us.hoodruncustomer.parser.Parser;
import com.us.hoodruncustomer.home.CustomerHomeActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RideHistoryActivity extends BaseActivity implements Historycontract.View {

    private Historycontract.Presenter presenter;
    ListView mHistoryListView;
    Toolbar toolbar;
    TextView headertext;
    String font_path;
    Typeface tf;

    String mURL;
    String parsresp;

    public static ArrayList<History> getmHistoryList() {
        return mHistoryList;
    }

    public static ArrayList<History> mHistoryList = new ArrayList<History>();
    ConnectionDetector cd;
    String user_type = "";
    HistoryListAdapter historyListAdapter;
    int offset = 0;
    int historySize;
    boolean scroll_flag = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_history);
        new HistoryPresenter(Injection.remoteDataRepository(getApplicationContext()), this);

        /**
         * Important to catch the exceptions
         */

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        /*****************************************************************************************/

        MyApplication.activity = RideHistoryActivity.this;
        font_path = "fonts/opensans.ttf";
        tf = Typeface.createFromAsset(this.getAssets(), font_path);

        cd = new ConnectionDetector(RideHistoryActivity.this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);


        if (toolbar != null) {

            int color = Color.parseColor("#ffffff");
            toolbar.setTitleTextColor(color);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            headertext = (TextView) findViewById(android.R.id.text1);


            headertext.setText("Ride History");


        }


        String[] navMenuTitles;
        TypedArray navMenuIcons;



            navMenuTitles = getResources().getStringArray(
                    R.array.nav_drawer_user_items);
            navMenuIcons = getResources().obtainTypedArray(
                    R.array.nav_drawer_icons_user);
            mURL = Configure.URL_ride_history_customer;
            user_type = "009";


        set(navMenuTitles, navMenuIcons);


        headertext.setTypeface(tf, Typeface.BOLD);

        mHistoryListView = (ListView) findViewById(R.id.list_history);
        historyListAdapter = new HistoryListAdapter(RideHistoryActivity.this, mHistoryList, mAppManager.getuser());
        mHistoryListView.setAdapter(historyListAdapter);

        if (!mAppManager.getPrevActivity("HIS").equals("HISD")) {

            callAsync();
        }


        mHistoryListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                // TODO Auto-generated method stub
                startActivity(new Intent(RideHistoryActivity.this, HistoryDetailsActivity.class).putExtra("position", position));


            }
        });


        mHistoryListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {

                int threshold = 1;
                int count = mHistoryListView.getCount();

                if (scrollState == SCROLL_STATE_IDLE && scroll_flag) {
                    if (mHistoryListView.getLastVisiblePosition() >= count
                            - threshold) {
                        // Execute LoadMoreDataTask AsyncTask
                        offset = offset + 10;
                        callAsync();
                        ;
                    }


                }
            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

    }


    private void callAsync() {
        // TODO Auto-generated method stub

        if (cd.isConnectingToInternet()) {
          //  showProgress();
            historyViewer(mURL, offset);
        } else {
            Toast.makeText(getApplicationContext(), "No internet connection avilable.", Toast.LENGTH_LONG).show();
            System.exit(0);
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {


        if (keyCode == KeyEvent.KEYCODE_BACK) {


            onBackPressed();

        }
        return false;
    }


    @Override
    public void onBackPressed() {

        if (mAppManager.getuser().equals("customer")) {
            startActivity(new Intent(RideHistoryActivity.this, CustomerHomeActivity.class));
            finish();
        }

    }


    Dialog dialog2;

    private void Dialog() {
        TextView mDialogBody, mOK;


        dialog2 = new Dialog(RideHistoryActivity.this, R.style.Theme_Dialog);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog2.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog2.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);
        dialog2.setContentView(R.layout.dialog_no_title);
        dialog2.setCancelable(false);
        dialog2.setCanceledOnTouchOutside(false);
        dialog2.show();
        mDialogBody = (TextView) dialog2.findViewById(R.id.txt_dialog_body);
        mOK = (TextView) dialog2.findViewById(R.id.txt_dialog_ok);

        mDialogBody.setText("Ride history not available.");
        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (mAppManager.getuser().equals("customer")) {
                    startActivity(new Intent(RideHistoryActivity.this, CustomerHomeActivity.class));
                    finish();
                }

            }
        });
    }


    private void historyViewer(String url, final int offset) {
        Map<String, String> params = new HashMap<String, String>();


        if (mAppManager.getuser().equals("customer")) {
            params.put("customer_id", mAppManager.getCustomerID());

        } else {

            params.put("driver_id", mAppManager.getDriverID_Number());

        }
        params.put("token_code", mAppManager.getTokenCode());
        params.put("offset", "" + offset);

        presenter.getHistoty(params, this);

    }


    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.activityPaused();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mHistoryList.clear();
    }


    @Override
    protected void onStop() {
        super.onStop();
        mAppManager.savePrevActivity("HIS", "");
    }


    Dialog retry;

    private void retryDialog(final int i) {

        retry = new Dialog(RideHistoryActivity.this, R.style.Theme_Dialog);
        retry.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = retry.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        retry.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        retry.setContentView(R.layout.thaks_dialog_twobutton);
        retry.setCancelable(false);
        retry.setCanceledOnTouchOutside(false);
        retry.show();

        TextView mTitle = (TextView) retry.findViewById(R.id.txt_title);
        mTitle.setText("Error!");
        mTitle.setTypeface(tf, Typeface.BOLD);
        TextView mDialogBody = (TextView) retry.findViewById(R.id.txt_dialog_body);
        mDialogBody.setText("Internet connection is slow or disconnected. Try connecting again.");
        LinearLayout mOK = (LinearLayout) retry.findViewById(R.id.lin_Ok);
        LinearLayout mCancel = (LinearLayout) retry.findViewById(R.id.lin_cancel);
        TextView mRetry = (TextView) retry.findViewById(R.id.txt_dialog_ok);
        TextView mExit = (TextView) retry.findViewById(R.id.txt_dialog_cancel);
        mRetry.setText("RETRY");
        mExit.setText("EXIT");

        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


                retry.dismiss();

                switch (i) {
                    case 1:
                        callAsync();
                        break;

                    default:
                        break;

                }


            }
        });

        mCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                retry.dismiss();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });

    }

    @Override
    public void getHistoryData(String response) {

        hideProgress();
        System.out.println("HISTORYVIEWER===" + response);

        try {
            parsresp = Parser.getHistory(response, mAppManager.getuser());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!TextUtils.isEmpty(parsresp)) {

            switch (parsresp) {
                case "TRUE":
                    historySize = mHistoryList.size();
                    Log.i("historySize", "" + historySize);

                    for (int i = 0; i < Parser.getHistoryList().size(); i++) {

                        mHistoryList.add(historySize, Parser.getHistoryList().get(i));
                        Log.i("historySizeforloop", "" + historySize);
                        historySize++;
                    }

                    historyListAdapter.notifyDataSetChanged();

                    break;
                case "FALSE":
                    switch (Parser.getHistory_status_message()) {
                        case "Invalid token code":
                            new InvalidTokenDialog(RideHistoryActivity.this).show();
                            break;

                        case "Ride history not available":
                            if (offset == 0) {
                                Dialog();
                            } else {
                                scroll_flag = false;
                                //Toast.makeText(getApplicationContext(), Parser.getHistory_status_message(), Toast.LENGTH_SHORT).show();

                            }
                            break;
                        default:
                            PushDialog pushDialog = new PushDialog(RideHistoryActivity.this, Parser.getHistory_status_message(), "008");
                            pushDialog.show();
                            break;
                    }

                    break;
            }


        } else {

            PushDialog pushDialog = new PushDialog(RideHistoryActivity.this, "You have not completed any rides.", user_type);
            pushDialog.show();
        }

    }

    @Override
    public void showProgreass() {
        loader = new Dialog(RideHistoryActivity.this);
        loader.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loader.setContentView(R.layout.dialog_progress);
        loader_wheel = (ProgressWheel) loader.findViewById(R.id.progress_wheel);
        loader_wheel.setCircleRadius(50);
        loader.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        loader.setCancelable(false);
        loader.show();
    }

    @Override
    public void onFail(Throwable t) {

        hideProgress();
        retryDialog(1);

    }

    @Override
    public void setPresenter(Historycontract.Presenter presenter) {

        this.presenter = presenter;

    }

    @Override
    public void hideProgress() {
        if (loader != null) {
            loader.dismiss();
        }

    }

}
