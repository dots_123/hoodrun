package com.us.hoodruncustomer.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.us.hoodruncustomer.nodeservice.ConnectorService;

import io.socket.client.Socket;

/**
 * Created by dell pc on 28-05-2017.
 */

public class ConnectivityReceiver extends BroadcastReceiver {

    MyApplication app ;
    private Socket mSocket;

    @Override
    public void onReceive(Context context, Intent intent) {

        app = (MyApplication) context.getApplicationContext();
        mSocket = app.getSocket();


        if (MyApplication.isActivityVisible()) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager
                    .getActiveNetworkInfo();

            // Check internet connection and accrding to state change the
            // text of activity by calling method
            if (networkInfo != null && networkInfo.isConnected()) {

                if (mSocket != null) {

                    if (!mSocket.connected()) {
                        Log.d("NodeConnection","ConnectivityReceiver-onReceive");
                        Intent start_Serv = new Intent(context.getApplicationContext(), ConnectorService.class);
                        context.startService(start_Serv);
                    }

                }
              /*  Intent i = new Intent(DriverHomeActivity.RECEIVER_CONNECTIVITY_STATUS);
                i.putExtra("status", true);
                context.sendBroadcast(i);*/
            }else {
              /*  Intent i = new Intent(DriverHomeActivity.RECEIVER_CONNECTIVITY_STATUS);
                i.putExtra("status", false);
                context.sendBroadcast(i);*/

            }
        }
    }


}
