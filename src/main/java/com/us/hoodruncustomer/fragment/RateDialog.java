package com.us.hoodruncustomer.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.RatingBar;

import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.custom.MyTextView;
import com.us.hoodruncustomer.db.AppManager;
import com.us.hoodruncustomer.parser.StatusParser;

public class RateDialog extends Dialog implements OnClickListener{
	Context context;
	MyTextView title, body,ok;
	RatingBar rate;
	AppManager mAppManager;
	String font_path;
	Typeface tf;

	public RateDialog(Context context) {
		super(context,  R.style.Theme_Dialog);
		// TODO Auto-generated constructor stub
		this.context=context;
		mAppManager=AppManager.getInstance(context);
		font_path="fonts/opensans.ttf";
		tf = Typeface.createFromAsset(context.getAssets(), font_path);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		getWindow().setGravity(Gravity.CENTER);
		getWindow().setBackgroundDrawable(new ColorDrawable(0));
		setContentView(R.layout.rating_dialog_onebutton);
		setCancelable(false);
		setCanceledOnTouchOutside(false);
		rate=(RatingBar)findViewById(R.id.rating);
		title=(MyTextView)findViewById(R.id.text_title);
		title.setTypeface(tf, Typeface.BOLD);
		body=(MyTextView)findViewById(R.id.txt_dialog_body);
		ok=(MyTextView)findViewById(R.id.txt_dialog_ok);

		ok.setOnClickListener(this);

		rate.setStepSize((int) 0.5f);
		rate.setRating(Float.parseFloat(StatusParser.getRide_rating()));
		rate.setEnabled(false);

		if (StatusParser.getFav_status().equals("1")&&StatusParser.getCheck_fav().equals("1")){
			title.setText("Congratulations!");
			body.setText(StatusParser.getCustomer_name()
					+ " has added you to favourite drivers.");
		}else{
			title.setText("Customer Feedback");
			body.setText(StatusParser.getCustomer_name()
					+ " has rated you with "+Double.valueOf(StatusParser.getRide_rating()).intValue()+" stars.");
		}
		body.setTextColor(Color.parseColor("#000000"));

		ok.setText("DISMISS");


	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==R.id.txt_dialog_ok){

			if(MyApplication.mPlayer.isPlaying()){
				MyApplication.stopPlayer();
			}
			dismiss();
			MyApplication.feedback_status = 2;
			MyApplication.dialogDismissed();
		}

	}


}
