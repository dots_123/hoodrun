package com.us.hoodruncustomer.app;


import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.support.multidex.MultiDex;
import android.util.Base64;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.db.AppManager;
import com.us.hoodruncustomer.nodeservice.ConnectorService;
import com.us.hoodruncustomer.nodeservice.CustomerLocationUpdate;

import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import io.fabric.sdk.android.Fabric;
import io.socket.client.IO;
import io.socket.client.Socket;
import okhttp3.OkHttpClient;

public class MyApplication extends Application {
    public static Bitmap bitmap = null;
    public RequestQueue requestQueue;
    public static boolean activityVisible;
    public static boolean dialogVisible;
    public static boolean jobdialogVisible;
    public static boolean offferstatus = false;
    public static String currentActivity;
    public static int citystatus = 1;
    public static HashMap<String, Bitmap> carImages;
    public static Bitmap Mycar;
    public static Context context;
    public static Activity activity;
    public static int apprunning = 0;
    public static MediaPlayer mPlayer = null;
    public static int feedback_status = 1;
    private static String TAG = "MyApplication-Node";
    AppManager mAppManager;
    private Socket mSocket;
    private Intent startCustomeLocation;

    public static String last_request_id = "0";
    private IO.Options opts;

    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }

    public static Context getAppContext() {
        return MyApplication.context;
    }

    public static Activity getAppActivity() {
        return MyApplication.activity;
    }

    public static boolean isjobDialogVisible() {
        return jobdialogVisible;
    }

    public static void jobdialogShown() {
        jobdialogVisible = true;
    }

    public static boolean isDialogVisible() {
        return dialogVisible;
    }

    public static void dialogShown() {
        dialogVisible = true;
    }

    public static boolean isOfferVisible() {
        return offferstatus;
    }

    public static int iscityVisible() {
        return citystatus;
    }

    public static void OfferShown() {
        offferstatus = true;
    }

    public static void cityShown() {
        citystatus = 0;
    }

    public static void cityReset() {
        citystatus = 1;
    }

    public static void dialogDismissed() {
        dialogVisible = false;
    }

    public static void startPlayer(int id) {
        mPlayer = MediaPlayer.create(context, id);

        mPlayer.start();

    }

    public static void stopPlayer() {
        if (mPlayer.isPlaying()) {
            mPlayer.pause();

        }
    }

    public void onCreate() {
        super.onCreate();
        requestQueue = Volley.newRequestQueue(this);

        printKeyHash();
        mAppManager = AppManager.getInstance(this);
        Fabric.with(this, new Crashlytics());
        MyApplication.context = getApplicationContext();
        startCustomeLocation = new Intent(this, CustomerLocationUpdate.class);
        carImages = new HashMap<>();
        FacebookSdk.setApplicationId(getString(R.string.facebook_app_id));
        FacebookSdk.sdkInitialize(this);

        SSLContext mySSLContext = null;
        try {
            mySSLContext = SSLContext.getInstance("TLS");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        TrustManager[] trustAllCerts= new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[] {};
            }

            public void checkClientTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }
        } };

        try {
            mySSLContext.init(null, trustAllCerts, null);
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        HostnameVerifier myHostnameVerifier = new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };


        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .hostnameVerifier(myHostnameVerifier)
                .sslSocketFactory(mySSLContext.getSocketFactory(), (X509TrustManager) trustAllCerts[0])
                .build();

        // default settings for all sockets
        IO.setDefaultOkHttpWebSocketFactory(okHttpClient);
        IO.setDefaultOkHttpCallFactory(okHttpClient);

        opts = new IO.Options();
        opts.callFactory = okHttpClient;
        opts.webSocketFactory = okHttpClient;

        try {

            // mSocket = IO.socket("http://beta.hoponn.co.uk:3001");
            // mSocket = IO.socket("http://hoponn.co.uk:3003");
            // mSocket = IO.socket("http://192.168.8.64:3002");
            // mSocket = IO.socket("http://192.168.4.137:3000");
            // mSocket = IO.socket("http://192.168.4.137:3000");
            // mSocket = IO.socket("http://111.93.41.194:3002");
            // mSocket = IO.socket("http://demo.hoponn.co.uk:3000");
            // mSocket = IO.socket("http://demo.hoponn.co.uk:3000");
//             mSocket = IO.socket("http://192.168.4.139:3000");

            mSocket = IO.socket("https://www.hoodrunnow.com:3000");

            Log.v(TAG, "Fine!");

        } catch (URISyntaxException e) {
            Log.v(TAG, "Error Connecting to IP!" + e.getMessage());
        }

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public void StartNode() {
        if (mAppManager.getLogin_status()) {
            Log.d("NodeConnection", "Application-StartNode");
            startService(new Intent(getApplicationContext(), ConnectorService.class));
        }
    }

    public Socket getSocket() {
        return mSocket;
    }

    public void customerLocaitonUpdate(boolean status) {

        if (status) {
            startService(startCustomeLocation);
        } else {
            stopService(startCustomeLocation);
        }

    }


    void printKeyHash(){
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.us.hoodruncustomer",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {

        }
    }
}