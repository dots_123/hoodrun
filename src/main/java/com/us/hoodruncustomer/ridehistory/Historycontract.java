package com.us.hoodruncustomer.ridehistory;

import android.content.Context;

import com.us.hoodruncustomer.commonwork.BasePresenter;
import com.us.hoodruncustomer.commonwork.BaseView;

import java.util.Map;

/**
 * Created by admin on 11/15/2017.
 */

public interface Historycontract {

    interface View extends BaseView<Presenter> {
        void getHistoryData(String response);

        void showProgreass();

        void hideProgress();

        void onFail(Throwable t);


    }


    interface Presenter extends BasePresenter {

        void getHistoty(Map<String, String> params, Context context);


    }

}
