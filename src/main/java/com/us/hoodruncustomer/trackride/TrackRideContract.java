package com.us.hoodruncustomer.trackride;

import android.content.Context;

import com.us.hoodruncustomer.commonwork.BasePresenter;
import com.us.hoodruncustomer.commonwork.BaseView;

/**
 * Created by admin on 11/15/2017.
 */

public interface TrackRideContract {

    interface View extends BaseView<Presenter> {
        void customerCancellationChargeResponse(String response);

        void customerCancelRideResponse(String response);

        void userCheckIsRideEndResponse(String response);

        void showProgreass();

        void hideProgress();

        void onFail(Throwable t);

        void onFailRideEnd(Throwable t);

        void rideRatinResponse(String response);

        void rideRatingFail(Throwable t);


    }


    interface Presenter extends BasePresenter {

        void cusCancelCharge(String tokencode, String customerId, Context context);

        void customerCancelRide(int cancelStart, Context context);

        void checkUserEndRide(String tokencode, String rideId, Context context);

        void rideRating(String tokencode, String rateValue, String rideId, Context context);


    }
}
