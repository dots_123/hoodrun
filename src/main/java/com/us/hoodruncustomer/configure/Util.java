package com.us.hoodruncustomer.configure;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by admin on 20-Dec-16.
 */

public class Util {

    public static final int REQUEST_CODE_ASK_PERMISSIONS = 10001;
    static  Snackbar actionSnackBar;
    public static void hideKeyboard(Context context) {
        View view =((Activity) context).getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private static String[] getListOfPermissions(final Context context) {
        ArrayList<String> permissions = new ArrayList<>();

        try
        {
            final AssetManager assetManager = context.createPackageContext(context.getPackageName(), 0).getAssets();
            final XmlResourceParser xmlParser = assetManager.openXmlResourceParser(0, "AndroidManifest.xml");
            int eventType = xmlParser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT)
            {
                if ((eventType == XmlPullParser.START_TAG) && "uses-permission".equals(xmlParser.getName()))
                {
                    for (byte i = 0; i < xmlParser.getAttributeCount(); i ++)
                    {
                        if (xmlParser.getAttributeName(i).equals("name"))
                        {
                            permissions.add(xmlParser.getAttributeValue(i));
                        }
                    }
                }
                eventType = xmlParser.nextToken();
            }
            xmlParser.close(); // Pervents memory leak.
        }
        catch (Exception e){
            e.printStackTrace();
        }
        if (permissions.size() > 0)
            return Arrays.copyOf(permissions.toArray(), permissions.size(), String[].class);
        else return null;

    }

    public static void snackBarWithAction(Context context, View root, String errorMsg, int color)
    {
        actionSnackBar = Snackbar.make(root, errorMsg, Snackbar.LENGTH_INDEFINITE);
        View snackbarView = actionSnackBar.getView();
        snackbarView.setBackgroundColor(context.getResources().getColor(color));
        actionSnackBar.setActionTextColor(context.getResources().getColor(android.R.color.white));
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(context.getResources().getColor(android.R.color.white));
//        actionSnackBar.setAction("OK", new View.OnClickListener() {
//            @Override
//            public void onClick(View v)
//            {
//                actionSnackBar.dismiss();
//
//            }
//        });
        if(!actionSnackBar.isShown()){
            actionSnackBar.show();
        }


    }


    public static void requestAllRuntimePermissions(Activity activity)
    {
        String[] permissions= getListOfPermissions(activity);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions != null && permissions.length > 0) {
            activity.requestPermissions(permissions, REQUEST_CODE_ASK_PERMISSIONS);
        }
    }

    public static void hideSnackBar()
    {
        if(actionSnackBar != null)
            actionSnackBar.dismiss();
    }

    public static int pxToDp(Context context, int pixel){
        Resources r = context.getResources();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, pixel, r.getDisplayMetrics());
    }


}
