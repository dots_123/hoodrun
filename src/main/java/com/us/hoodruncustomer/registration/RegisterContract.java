package com.us.hoodruncustomer.registration;

import android.content.Context;

import com.us.hoodruncustomer.commonwork.BasePresenter;
import com.us.hoodruncustomer.commonwork.BaseView;

/**
 * Created by admin on 11/14/2017.
 */

public interface RegisterContract {


    interface View extends BaseView<RegisterContract.Presenter> {
        void registerSuccessResponse(String response);

        void showProgreass();

        void hideProgress();

        void onLoginFail(Throwable t);



        void onSaveResponseOtp(String response);

        void onSuccessResendOtp(String response);

        void onFailResend(Throwable t);


    }

    interface Presenter extends BasePresenter {

        void register(String fb_id, String google_id, String name, String email, String password, String mobile_number, String profile_picture, String device_id, String os, Context context);

        void saveOtp(String customerId, String otp, String email, String mobilenumber,String url, Context context);

        void resendOtp(String customerId, String tokencode, String email, String mobilenumber,String url, Context context);


    }
}
