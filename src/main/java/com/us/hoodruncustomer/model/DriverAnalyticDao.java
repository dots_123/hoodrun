package com.us.hoodruncustomer.model;

import java.util.ArrayList;

/**
 * Created by admin on 26-Jan-17.
 */

public class DriverAnalyticDao {

    public String date;
    public ArrayList<AnalyticsData> analytics;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<AnalyticsData> getAnalytics() {
        return analytics;
    }

    public void setAnalytics(ArrayList<AnalyticsData> analytics) {
        this.analytics = analytics;
    }
}
