package com.us.hoodruncustomer.payment;

import android.content.Context;

import com.us.hoodruncustomer.commonwork.BasePresenter;
import com.us.hoodruncustomer.commonwork.BaseView;

import java.util.Map;

/**
 * Created by admin on 11/15/2017.
 */

public interface PaymentContract {

    interface View extends BaseView<Presenter> {
        void getSaveCards(String response);

        void showProgreass();

        void hideProgress();

        void onFail(Throwable t);

        void getTokenResponse(String response);

        void onTokenFail(Throwable t);

        void onPaymentResponse(String response);


    }


    interface Presenter extends BasePresenter {

        void getSavecards(String tokencode, String customerId, Context context);

        void getToken(String customerId, String userType, Context context);

        void savePaymentData(Map<String, String> params, String url, boolean skipped, Context context);



    }

}
