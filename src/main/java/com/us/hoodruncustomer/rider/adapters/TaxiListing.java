package com.us.hoodruncustomer.rider.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.model.Cars;

import java.util.ArrayList;

/**
 * Created by admin on 16-Nov-16.
 */

public class TaxiListing extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context context;
    private ArrayList<Cars> availableTaxis;
    private LayoutInflater inflater = null;
    private int selectedPosition = -1;
    private double userLatitude;
    private double userLongitude;
    private IupdateCars iupdateCars;
    public static boolean isdefaultfound;
    public static boolean isdefaultfoundmakechange;
    private int deviceWidth;
    private LinearLayout.LayoutParams params;
    int margin;

    public TaxiListing(Context context, int deviceWidth) {
        this.context = context;
        availableTaxis = new ArrayList<Cars>();
        this.deviceWidth = deviceWidth;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        iupdateCars = (IupdateCars) context;
        params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View rootview = inflater.inflate(R.layout.taxi_listing, parent, false);
        return new MyViewHolder(rootview);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof MyViewHolder) {
            final MyViewHolder myHolder = ((MyViewHolder) holder);

            margin = deviceWidth / availableTaxis.size();
            myHolder.parentofCar.setLayoutParams(new RecyclerView.LayoutParams(margin, RecyclerView.LayoutParams.WRAP_CONTENT));

            myHolder.parentofCar.setOnClickListener(myHolder);
            myHolder.parentofCar.setTag(position);
            myHolder.name.setText(Html.fromHtml(availableTaxis.get(position).getName()));

            Log.d("TotalCars", availableTaxis.get(position).getAvg_time());

            myHolder.time.setText("(" + availableTaxis.get(position).getAvg_time() + ")");

            Glide.with(context).load(availableTaxis.get(position).getList_car_image()).into(myHolder.listImage);


            if (selectedPosition == -1) {

                if (availableTaxis.get(position).isDefault_car()) {
                    isdefaultfound = true;
                    myHolder.carSelector.setBackgroundResource(R.drawable.circle);
                    int color = Color.parseColor("#cd0102"); //The color u want
                    myHolder.listImage.setColorFilter(color);
                    myHolder.name.setTypeface(null, Typeface.BOLD);

                    myHolder.name.setTextColor(Color.parseColor("#000000"));
                    iupdateCars.specificCarClicked(availableTaxis.get(position), false);

                } else {
                    myHolder.carSelector.setBackgroundResource(R.drawable.blankcircle);
                    int color = Color.parseColor("#aaaaaa"); //The color u want
                    myHolder.listImage.setColorFilter(color);
                    myHolder.name.setTypeface(null, Typeface.NORMAL);
                    myHolder.name.setTextColor(Color.parseColor("#757575"));
                }

            } else {
                if (position == selectedPosition) {
                    myHolder.carSelector.setBackgroundResource(R.drawable.circle);
                    myHolder.name.setTypeface(null, Typeface.BOLD);
                    int color = Color.parseColor("#cd0102"); //The color u want
                    myHolder.listImage.setColorFilter(color);
                    myHolder.name.setTextColor(Color.parseColor("#000000"));
                    iupdateCars.specificCarClicked(availableTaxis.get(position), false);

                } else {
                    myHolder.carSelector.setBackgroundResource(R.drawable.blankcircle);
                    int color = Color.parseColor("#aaaaaa"); //The color u want
                    myHolder.listImage.setColorFilter(color);
                    myHolder.name.setTypeface(null, Typeface.NORMAL);
                    myHolder.name.setTextColor(Color.parseColor("#757575"));
                }
            }
        }
    }

    public void updateTaxisListing(ArrayList<Cars> availableTaxis, double userLatitude, double userLongitude) {
        this.availableTaxis = availableTaxis;
        this.userLatitude = userLatitude;
        this.userLongitude = userLongitude;
        for (Cars car : availableTaxis) {
            if (car.isDefault_car()) {
                isdefaultfound = true;
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {

        return availableTaxis.size();
    }

    /**
     * ViewHolder For Taxi Listing
     */

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView listImage;
        TextView name, time;
        LinearLayout parentofCar, carSelector;

        public MyViewHolder(View itemView) {
            super(itemView);
            listImage = (ImageView) itemView.findViewById(R.id.car_photo);
            name = (TextView) itemView.findViewById(R.id.car_type);
            time = (TextView) itemView.findViewById(R.id.time_away);
            parentofCar = (LinearLayout) itemView.findViewById(R.id.parent_ofCar);
            carSelector = (LinearLayout) itemView.findViewById(R.id.car_selector);

        }

        @Override
        public void onClick(View v) {

            int position = (int) v.getTag();
            selectedPosition = position;
            iupdateCars.specificCar(availableTaxis.get(position), true);
            notifyDataSetChanged();

        }

    }

    public interface IupdateCars {

        void specificCarClicked(Cars car, boolean userClicked);

        void specificCar(Cars car, boolean userClicked);

    }

}
