package com.us.hoodruncustomer.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.splash.SplashActivity;
import com.us.hoodruncustomer.custom.MyTextView;
import com.us.hoodruncustomer.db.AppManager;
import com.us.hoodruncustomer.fcm.MyFirebaseMessagingService;
import com.us.hoodruncustomer.home.CustomerHomeActivity;
import com.us.hoodruncustomer.trackride.RideReceipt;
import com.us.hoodruncustomer.share.RideSharingActivity;


public class PushDialog extends Dialog implements OnClickListener{
String push_message,push_flag;
Context context;
MyTextView body,ok;
AppManager mAppManager;
NotificationManager manager;
	
public PushDialog(Context context, String push_message, String push_flag) {
		super(context,  R.style.Theme_Dialog);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.push_message=push_message;
		this.push_flag=push_flag;
		mAppManager=AppManager.getInstance(context);
		manager=(NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		getWindow().setGravity(Gravity.CENTER);
		getWindow().setBackgroundDrawable(new ColorDrawable(0));
		setContentView(R.layout.dialog_no_title);
		setCancelable(false);
		setCanceledOnTouchOutside(false);
	
		body=(MyTextView)findViewById(R.id.txt_dialog_body);
		ok=(MyTextView)findViewById(R.id.txt_dialog_ok);
	
		ok.setOnClickListener(this);
		

		if(push_message.contains("."))
		body.setText(push_message);
		else
			body.setText(push_message.replaceAll("[-+.^:,]","")+".");
	
		switch (push_flag) {
		case "11":
			ok.setText("VIEW INVITATION");
			break;
        case "12":
        	ok.setText("View Details");
			break;

		default:
			ok.setText("OK");
			break;
		}

	

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	if(v.getId()==R.id.txt_dialog_ok){


		MyFirebaseMessagingService.push_Status_Flag="";
		dismiss();
		switch (push_flag) {
		case "6":
             mAppManager.saveCurrentActivity("");
			 context.startActivity(new Intent(context, RideReceipt.class));
				((Activity) context).finish();

			break;


		
         case "11":

			 MyFirebaseMessagingService.push_Status_Flag="";
			Intent myintent = new Intent(context,
					RideSharingActivity.class);
			context.startActivity(myintent);
			((Activity) context).finish();
			break;
		
		case "12":

			MyFirebaseMessagingService.push_Status_Flag="";
			mAppManager.savePrevActivity( "REC","REQ");
			mAppManager.setShared(true);
			Intent myintent1 = new Intent(context,
					RideReceipt.class);
			context.startActivity(myintent1);
			((Activity) context).finish();
			break;

			case "finish":

				context.startActivity(new Intent(context,SplashActivity.class));
				((Activity) context).finish();
				break;

			case "009":
				if(push_message.equals("You can't cancel the ride now")){
					context.startActivity(new Intent(context, SplashActivity.class));
					((Activity) context).finish();
					manager.cancelAll();
				}else {
					context.startActivity(new Intent(context, CustomerHomeActivity.class));
					((Activity) context).finish();
				}
				break;
		default:
			break;
		}
		MyApplication.dialogDismissed();
	}
		
	}


	
}
