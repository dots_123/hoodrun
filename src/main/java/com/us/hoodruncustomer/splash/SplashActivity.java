package com.us.hoodruncustomer.splash;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.gson.Gson;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.authentication.LoginActivity;
import com.us.hoodruncustomer.authentication.LoginContract;
import com.us.hoodruncustomer.authentication.LoginPresenter;
import com.us.hoodruncustomer.commonwork.Injection;
import com.us.hoodruncustomer.configure.Configure;
import com.us.hoodruncustomer.custom.SocialResponse;
import com.us.hoodruncustomer.db.AppManager;
import com.us.hoodruncustomer.home.ConfirmRideActivity;
import com.us.hoodruncustomer.home.CustomerHomeActivity;
import com.us.hoodruncustomer.nodeservice.ConnectorService;
import com.us.hoodruncustomer.parser.ConnectionDetector;
import com.us.hoodruncustomer.parser.Parser;
import com.us.hoodruncustomer.parser.StatusParser;
import com.us.hoodruncustomer.payment.AddPaymentActivity;
import com.us.hoodruncustomer.registration.RegisterActivity;
import com.us.hoodruncustomer.registration.Verification;
import com.us.hoodruncustomer.trackride.RideReceipt;
import com.us.hoodruncustomer.trackride.TrackRide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import studios.codelight.smartloginlibrary.LoginType;
import studios.codelight.smartloginlibrary.SmartLogin;
import studios.codelight.smartloginlibrary.SmartLoginCallbacks;
import studios.codelight.smartloginlibrary.SmartLoginConfig;
import studios.codelight.smartloginlibrary.SmartLoginFactory;
import studios.codelight.smartloginlibrary.UserSessionManager;
import studios.codelight.smartloginlibrary.users.SmartUser;
import studios.codelight.smartloginlibrary.util.SmartLoginException;

public class SplashActivity extends AppCompatActivity implements LoginContract.View, GoogleApiClient.OnConnectionFailedListener {

    String userEmail, userName, fbId, googleId;

    // 7A:C4:8D:3E:C4:A2:03:0E:03:30:E3:39:5F:40:86:BA:FE:6F:3C:94     - live sha1
    // 89:2F:54:92:EC:C0:37:5B:73:C7:24:E2:69:40:D5:73:AF:60:74:DD     - debug sha1

//    esSNPsSiAw4DMOM5X0CGuv5vPJQ=                     release key hashes fb
//    RemoveiS9UkuzAN1tzxyTiaUDVc69gdN0=               debug key hashes fb

    String email, password, parsresp = "";

    //google api client
    private GoogleApiClient mGoogleApiClient;

    //Signin constant to check the activity result
    private int RC_SIGN_IN = 100;

    private FirebaseAuth mAuth;

    LoginButton fbLoginButton;

    LinearLayout mImgLogin;
    ImageView mRegister;
    Handler handler;
    AppManager mAppManager;
    boolean isGPSEnabled;
    LocationManager myLocManager;
    int req_code;
    AlertDialog.Builder alertDialog;
    AlertDialog ad;
    ConnectionDetector cd;
    String id_type, id;
    String font_path;
    Typeface tf;
    String packageName = "com.us.hoodruncustomer";
    Activity a;
    MyApplication app;
    private static int RequestPermissionCode = 121;

    private LoginContract.Presenter presenter;
    CallbackManager mCallbackManager;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);
//        ButterKnife.bind(this);

        loader = new Dialog(SplashActivity.this);
        context = this;

        FacebookSdk.sdkInitialize(getApplicationContext());
        mCallbackManager = CallbackManager.Factory.create();

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        printKeyHash();

        new LoginPresenter(Injection.remoteDataRepository(getApplicationContext()), this);
        app = (MyApplication) getApplicationContext();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            if (!checkPermission()) {
                requestPermission();
            } else {
                createView();
            }
        else {
            createView();
        }

    }

    void printKeyHash() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.us.hoodruncustomer",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {

        }
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(SplashActivity.this, new String[]
                {
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                }, RequestPermissionCode);

    }

    @Override
    protected void onResume() {
        super.onResume();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
    }

    public boolean checkPermission() {

        int CoarsePermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION);
        int FinePermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        int NetworkStatePermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_NETWORK_STATE);

        return CoarsePermissionResult == PackageManager.PERMISSION_GRANTED &&
                NetworkStatePermissionResult == PackageManager.PERMISSION_GRANTED &&
                FinePermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    private void createView() {
        // TODO Auto-generated method stub

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(SplashActivity.this.getResources()
                    .getColor(R.color.black));
        }
        mAppManager = AppManager.getInstance(this);
        mAppManager.saveRequest_ID("");
        mAppManager.saveRide_ID("");
//		MyFirebaseMessagingService.messages = "";
//		MyFirebaseMessagingService.push_Status_Flag = "";
        handler = new Handler();
        cd = new ConnectionDetector(SplashActivity.this);
        myLocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        isGPSEnabled = myLocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        a = this;
        font_path = "fonts/opensans.ttf";

        tf = Typeface.createFromAsset(this.getAssets(), font_path);

        if (!mAppManager.getLogin_status()) {

            mAppManager.saveTokenCode(null);

            Intent start_Serv = new Intent(getApplicationContext(), ConnectorService.class);
            stopService(start_Serv);

            if (!isGPSEnabled || (getLocationMode(this) != Settings.Secure.LOCATION_MODE_HIGH_ACCURACY && getLocationMode(this) != Settings.Secure.LOCATION_MODE_SENSORS_ONLY)) {
                req_code = 1;
                showSettingAlert();
            } else {
                if (NotificationManagerCompat.from(this).areNotificationsEnabled()) {
                    splashShow();

                } else {
                    showNotificationAlert();
                }
            }

        }


        /**
         * Already Login
         */
        else if (mAppManager.getLogin_status()) {


            if (mAppManager.getTokenCode() != null && !mAppManager.getTokenCode().isEmpty()) {
                ((MyApplication) getApplication()).StartNode();
            }
            if (!isGPSEnabled || (getLocationMode(this) != Settings.Secure.LOCATION_MODE_HIGH_ACCURACY && getLocationMode(this) != Settings.Secure.LOCATION_MODE_SENSORS_ONLY)) {
                req_code = 2;
                showSettingAlert();
            } else {
                if (!NotificationManagerCompat.from(this).areNotificationsEnabled()) {
                    showNotificationAlert();

                } else {


                    app.customerLocaitonUpdate(true);
                    id_type = "customer_id";
                    id = mAppManager.getCustomerID();

                    if (cd.isConnectingToInternet()) {

                        callSplashRequest();

                    } else {
                        DialogAlert("No Internet !",
                                "Check your connection and try again.");
                    }


                }
            }

        } else {
            setContentView(R.layout.activity_splash);

            if (!isGPSEnabled || (getLocationMode(this) != Settings.Secure.LOCATION_MODE_HIGH_ACCURACY && getLocationMode(this) != Settings.Secure.LOCATION_MODE_SENSORS_ONLY)) {
                req_code = 3;
                showSettingAlert();
            } else if (!NotificationManagerCompat.from(this).areNotificationsEnabled()) {
                showNotificationAlert();

            } else {
                setupView();
            }
        }


    }


    private void setupView() {
        // TODO Auto-generated method stub
        mImgLogin = (LinearLayout) findViewById(R.id.img_login);
        mRegister = (ImageView) findViewById(R.id.btn_register);
        fbLoginButton = (LoginButton) findViewById(R.id.fb_login);

        try {

            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();

            mGoogleApiClient = new GoogleApiClient.Builder(context)
                    .enableAutoManage((FragmentActivity) context, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();

            fbLoginButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday", "user_friends"));

        } catch (Exception e) {
            e.printStackTrace();
        }

        fbLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d("fb_login", "facebook:onCancel");
                hideProgress();
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("fb_login", "facebook:onError", error);
                hideProgress();
            }
        });

        mRegister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent reg = new Intent(SplashActivity.this, RegisterActivity.class);
                startActivity(reg);
            }
        });

        mImgLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent reg = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(reg);
                finish();
            }
        });

    }

    public void splashShow() {

        new SplashShow().execute();

    }

    private void showSettingAlert() {
        // TODO Auto-generated method stub

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub

                alertDialog = new AlertDialog.Builder(SplashActivity.this);

                alertDialog.setTitle("LOCATION");
                alertDialog.setMessage("Please enable location services");

                alertDialog.setPositiveButton("ENABLE",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {

                                startActivityForResult(
                                        new Intent(
                                                Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                        req_code);

                            }
                        });
                if (a != null && !a.isFinishing()) {
                    ad = alertDialog.show();
                    ad.setCancelable(false);
                    ad.setCanceledOnTouchOutside(false);
                }

            }
        }, 3000);

    }


    private void showNotificationAlert() {
        // TODO Auto-generated method stub

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub

                alertDialog = new AlertDialog.Builder(SplashActivity.this);
                alertDialog.setTitle("NOTIFICATIONS");
                alertDialog.setMessage("Please enable push notifications in your settings to recieve ride alerts");

                alertDialog.setPositiveButton("ENABLE",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                try {
                                    //Open the specific App Info page:
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    intent.setData(Uri.parse("package:" + packageName));
                                    startActivity(intent);

                                } catch (ActivityNotFoundException e) {
                                    //e.printStackTrace();

                                    //Open the generic Apps page:
                                    Intent intent = new Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
                                    startActivity(intent);

                                }


                            }
                        });
                if (a != null && !a.isFinishing()) {
                    ad = alertDialog.show();
                    ad.setCancelable(false);
                    ad.setCanceledOnTouchOutside(false);
                }

            }
        }, 3000);

    }


    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();

    }

    @Override
    protected void onRestart() {
        // TODO Auto-generated method stub
        super.onRestart();

        myLocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        isGPSEnabled = myLocManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (isGPSEnabled || getLocationMode(this) == Settings.Secure.LOCATION_MODE_HIGH_ACCURACY || getLocationMode(this) == Settings.Secure.LOCATION_MODE_SENSORS_ONLY) {
            if (ad != null) {
                ad.cancel();
                createView();
            }
        } else showSettingAlert();

    }

    @Override
    public void nevigateToHeme(String response) {
//        hideProgress();
//
//        if(response!=null && !TextUtils.isEmpty(response)){
//
//            SocialResponse socialResponse = new Gson().fromJson(response, SocialResponse.class);
//            Toast.makeText(context, socialResponse.getMessage(), Toast.LENGTH_SHORT).show();
//        }

        hideProgress();


        System.out.println("LOGIN===" + response);
        parsresp = Parser.login(response);

        String status = null;
        try {
            status = new JSONObject(response).getString("status");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (status != null && status.equalsIgnoreCase("TRUE")) {


            if (!TextUtils.isEmpty(parsresp)) {

                if (parsresp.equalsIgnoreCase("true")) {
                    mAppManager.saveMobileNumber(Parser.getMob_no());
                    if (Parser.getUser_type().equals("1")) {

                        mAppManager.saveUser("customer");
                        mAppManager.saveCustomerName(Parser.getName());
                        switch (Parser.getProfileStatus()) {
                            case "1":
                                loader.dismiss();
                                mAppManager.saveCustomerID(Parser.customer_id);
                                mAppManager.saveTokenCode(Parser.getToken_code());
                                Toast.makeText(getApplicationContext(), "" + Parser.getLoginStatus(), Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(SplashActivity.this, AddPaymentActivity.class));

                                break;
                            case "2": {
                                loader.dismiss();
                                mAppManager.saveCustomerID(Parser.customer_id);
                                mAppManager.saveTokenCode(Parser.getToken_code());
                                mAppManager.saveOtpDestiantion("mobile");
                                // mAppManager.setLogin_status(true);
                                Toast.makeText(getApplicationContext(), "" + Parser.getLoginStatus(), Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(SplashActivity.this,
                                        Verification.class);
                                startActivity(i);

                                break;
                            }
                            case "3": {

                                mAppManager.saveCustomerID(Parser.customer_id);
                                mAppManager.saveTokenCode(Parser.getToken_code());
                                System.out.println("787tokencode787=== " + Parser.getToken_code() + "===" + Parser.customer_id);
                                mAppManager.setLogin_status(true);
//												if(mAppManager.getTokenCode() != null && !mAppManager.getTokenCode().isEmpty()) {
//													app.StartNode();
//												}
                                if (Parser.getIsride_status() == null) {
                                    loader.dismiss();
                                    Intent i = new Intent(SplashActivity.this, CustomerHomeActivity.class);
                                    i.putExtra("suspension", false);
                                    startActivity(i);
                                    finish();
                                } else {
                                    if (Parser.getIsride_status().equalsIgnoreCase("true")) {
                                        splashTask(Configure.URL_ride_completeness_customer);
                                    } else {
                                        loader.dismiss();

                                        Intent i = new Intent(SplashActivity.this, CustomerHomeActivity.class);
                                        i.putExtra("suspension", false);
                                        startActivity(i);
                                        finish();
                                    }
                                }
                                break;
                            }

                            case "4":
                                mAppManager.saveCustomerID(Parser.customer_id);
                                mAppManager.saveTokenCode(Parser.getToken_code());
                                System.out.println("787tokencode787=== " + Parser.getToken_code() + "===" + Parser.customer_id);
                                mAppManager.setLogin_status(true);
//												if(mAppManager.getTokenCode() != null && !mAppManager.getTokenCode().isEmpty()) {
//													app.StartNode();
//												}

                                if (Parser.getIsride_status() == null) {
                                    loader.dismiss();
                                    Intent i = new Intent(SplashActivity.this, CustomerHomeActivity.class);
                                    i.putExtra("suspension", true);
                                    i.putExtra("reason", Parser.getLoginStatus());
                                    startActivity(i);
                                    finish();
                                } else {
                                    if (Parser.getIsride_status().equalsIgnoreCase("true")) {
                                        splashTask(Configure.URL_ride_completeness_customer);
                                    } else {
                                        loader.dismiss();
                                        Intent i = new Intent(SplashActivity.this, CustomerHomeActivity.class);
                                        i.putExtra("suspension", true);
                                        i.putExtra("reason", Parser.getLoginStatus());
                                        startActivity(i);
                                        finish();
                                    }
                                }

                                break;

                        }

                        id_type = "customer_id";
                        //splashTask(Configure.URL_ride_completeness_customer);

                    }
                } else if (parsresp.equalsIgnoreCase("false")) {
                    String message = Parser.getLoginStatus();
                    iDialog(message);
                }


            } else {
                Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
                System.exit(0);
            }


        } else {
            Intent intent = new Intent(SplashActivity.this, RegisterActivity.class);
            intent.putExtra("from", "social");
            intent.putExtra("name", userName);
            intent.putExtra("email", userEmail);
            intent.putExtra("fb_id", fbId);
            intent.putExtra("google_id", googleId);
            context.startActivity(intent);
        }


    }


    private void splashTask(String url) {
        String id = "";

        Map<String, String> params = new HashMap<String, String>();
        params.put("token_code", mAppManager.getTokenCode());
        if (mAppManager.getuser().equals("customer")) {
            id = mAppManager.getCustomerID();
            params.put(id_type, mAppManager.getCustomerID());
        }
        presenter.updateData(mAppManager.getTokenCode(), id, SplashActivity.this);


    }


    Dialog login_dialog;

    private void iDialog(String message) {
        // TODO Auto-generated method stub

        login_dialog = new Dialog(SplashActivity.this, R.style.Theme_Dialog);
        login_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = login_dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        login_dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        login_dialog.setContentView(R.layout.dialog_no_title);
        login_dialog.setCancelable(false);
        login_dialog.setCanceledOnTouchOutside(false);
        login_dialog.show();

        TextView mDialogBody = (TextView) login_dialog.findViewById(R.id.txt_dialog_body);
        TextView mOK = (TextView) login_dialog.findViewById(R.id.txt_dialog_ok);

        mDialogBody.setText(message);

        mOK.setText("OK");

        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                login_dialog.dismiss();

                if (loader != null && loader.isShowing())
                    loader.dismiss();
            }
        });


    }

    @Override
    public void updateDataResponse(String response) {

        System.out.println("COMPLETENESS===" + response);

        parsresp = StatusParser.getRideStatus(response, id_type);

        if (!TextUtils.isEmpty(parsresp)) {
            if (parsresp.equals("TRUE")) {

                if (mAppManager.getuser().equals("customer")) {
                    boolean parseresponce = false;
                    try {
                        parseresponce = Parser.setDriverDataSplash(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (parseresponce) {

                        mAppManager.App_setDriver_ride_id(Parser.getDriver_ride_id());
                        mAppManager.App_setDriver_request_id(Parser.getDriver_request_id());
                        mAppManager.App_setDriver_ride_status(Parser.getDriver_ride_status());
                        mAppManager.App_setDriver_driver_id(Parser.getDriver_driver_id());
                        mAppManager.App_setDriver_driver_name(Parser.getDriver_driver_name());
                        mAppManager.App_setDriver_driver_phone(Parser.getDriver_driver_phone());
                        mAppManager.App_setDriver_driver_vehicle(Parser.getDriver_driver_vehicle());
                        mAppManager.App_setDriver_driver_profile_pic(Parser.getDriver_driver_profile_pic());
                        mAppManager.App_setDriver_driver_latitude(Parser.getDriver_driver_latitude());
                        mAppManager.App_setDriver_driver_longitude(Parser.getDriver_driver_longitude());

                        mAppManager.App_setDriver_push_Status_Flag(Parser.getDriver_push_Status_Flag());
                        mAppManager.App_setDriver_rating(Parser.getDriver_rating());

                        mAppManager.App_setDriver_customer_destination_point_lat(Parser.getDriver_customer_destination_point_lat());
                        mAppManager.App_setDriver_customer_destination_point_long(Parser.getDriver_customer_destination_point_long());
                        mAppManager.App_setDriver_customer_destination_point_name(Parser.getDriver_customer_destination_point_name());
                        mAppManager.App_setDriver_customer_pick_up_point_lat(Parser.getDriver_customer_pick_up_point_lat());
                        mAppManager.App_setDriver_customer_pick_up_point_long(Parser.getDriver_customer_pick_up_point_long());
                        mAppManager.App_setDriver_customer_pick_up_point_name(Parser.getDriver_customer_pick_up_point_name());

                        mAppManager.App_setDriver_vehicle_model(Parser.getDriver_vehicle_models());
                        mAppManager.App_setDriver_vehicle_number(Parser.getDriver_vehicle_numbers());
                        mAppManager.App_setDriver_vehicle_type(Parser.getDriver_vehicle_type());
                        mAppManager.App_setDriver_vehicle_image(Parser.getDriver_vehicle_image());

                        mAppManager.App_setDriver_fare_multiplier(Parser.getDriver_fare_multiplier());
                        mAppManager.App_setDriver_car_type(Parser.getDriver_car_type());
                        mAppManager.App_setDriver_map_car_image(Parser.getDriver_map_car_image());
                        mAppManager.App_setDriver_total(Parser.getDriver_total());
                        mAppManager.App_setDriver_trip_distance(Parser.getDriver_trip_distance());
                        mAppManager.App_setDriver_trip_duration(Parser.getDriver_trip_duration());
                        mAppManager.App_setDriver_tip_the_driver(Parser.getDriver_tip_the_driver());
                        mAppManager.App_setDriver_payment_type(Parser.getDriver_payment_type());
                        mAppManager.App_setDriver_total_cost(Parser.getDriver_total_cost());
                        mAppManager.App_setDriver_discount_applied(Parser.getDriver_discount_applied());
                        mAppManager.App_setDriver_discount_total(Parser.getDriver_discount_total());
                        mAppManager.App_setDriver_promo_code(Parser.getDriver_promo_code());
                        mAppManager.App_setDriver_payment_mode(Parser.getDriver_payment_mode());
                        mAppManager.App_setDriver_mobile_no(Parser.getDriver_mobile_no());
                    }

                    switch (StatusParser.getMessage()) {

                        case "Driver is not arrived":
                            mAppManager.App_setDriver_ride_status("2");
                            startActivity(new Intent(SplashActivity.this, TrackRide.class));
                            finish();
                            break;

                        case "Driver arrived at the the pick up location":
                            mAppManager.App_setDriver_ride_status("5");
                            startActivity(new Intent(SplashActivity.this, TrackRide.class));
                            finish();
                            break;

                        case "Journey started":
                            mAppManager.App_setDriver_ride_status("6");
                            startActivity(new Intent(SplashActivity.this, TrackRide.class));
                            finish();
                            break;

                        case "Tip is not given for this ride":
                            if (Parser.isDriver_Is_Payment_Done()) {
                                Intent toReceipt1 = new Intent(SplashActivity.this, RideReceipt.class);
                                toReceipt1.putExtra("payment_status", 1);
                                startActivity(toReceipt1);
                                finish();
                            } else {
                                Intent toReceipt1 = new Intent(SplashActivity.this, RideReceipt.class);
                                toReceipt1.putExtra("payment_status", 0);
                                startActivity(toReceipt1);
                                finish();
                            }
                            break;

                        case "Tip is successfully added for this ride":

                            Intent toReceipt = new Intent(SplashActivity.this, RideReceipt.class);
                            toReceipt.putExtra("payment_status", 1);
                            startActivity(toReceipt);
                            finish();
                            break;


                        case "Journey completed and payment status updated":

                            startActivity(new Intent(SplashActivity.this, CustomerHomeActivity.class));
                            finish();
                            break;

                        case "Journey completed":

                            startActivity(new Intent(SplashActivity.this, CustomerHomeActivity.class));
                            finish();
                            break;

                        case "Incomplete ride rating":

                            startActivity(new Intent(SplashActivity.this, RideReceipt.class));
                            finish();
                            break;
                        case "Driver rejected the request":

                            startActivity(new Intent(SplashActivity.this, CustomerHomeActivity.class));
                            finish();
                            break;

                        case "No response from the driver":

                            mAppManager.savePrevActivity("TRACK", "SPLASHC_NR");
                            startActivity(new Intent(SplashActivity.this, ConfirmRideActivity.class));
                            finish();

                            break;

                        case "The ride is cancelled by the driver":


                            mAppManager.savePrevActivity("TRACK", "SPLASHC_RC");
                            startActivity(new Intent(SplashActivity.this, CustomerHomeActivity.class));
                            finish();
                            break;
                        case "The ride is cancelled by the customer":


                            startActivity(new Intent(SplashActivity.this, CustomerHomeActivity.class));
                            finish();
                            break;

                        case "Request is expired":

                            startActivity(new Intent(SplashActivity.this, CustomerHomeActivity.class));
                            finish();
                            break;

                        default:
                            break;
                    }


                }
            } else {
                if (StatusParser.getMessage().equals("Invalid token code")) {

                    mAppManager.savePrevActivity("RATE", "");
                    mAppManager.savePrevActivity("REC", "");
                    mAppManager.savePrevActivity("REC1", "");
                    mAppManager.savePrevActivity("REC2", "");
                    mAppManager.savePrevActivity("TRACK", "");

                    setContentView(R.layout.activity_splash);
                    setupView();

                } else if (mAppManager.getuser().equals("customer")) {
                    mAppManager.savePrevActivity("RATE", "");
                    mAppManager.savePrevActivity("REC", "");
                    mAppManager.savePrevActivity("TRACK", "");
                    Intent ride = new Intent(SplashActivity.this,
                            CustomerHomeActivity.class);
                    startActivity(ride);
                    finish();
                }
            }

        } else {
            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
            finish();
        }

    }


    @Override
    public void showProgreass() {
        if (!loader.isShowing()) {
            loader.setContentView(R.layout.dialog_progress);
            loader_wheel = (ProgressWheel) loader.findViewById(R.id.progress_wheel);
            loader_wheel.setCircleRadius(50);
            loader.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            loader.setCancelable(false);
            loader.show();
//            loader.requestWindowFeature(Window.FEATURE_NO_TITLE);
        }
    }

    @Override
    public void hideProgress() {
        if (loader != null) {
            loader.dismiss();
        }

    }

    Dialog loader;
    ProgressWheel loader_wheel;

    @Override
    public void onLoginFail(Throwable t) {
        hideProgress();
    }

    @Override
    public void onUpdateFail(Throwable t) {
        hideProgress();
    }

    @Override
    public void forgotResponseSuccess(String response) {

    }

    @Override
    public void forgotFail(Throwable t) {
        hideProgress();
    }

    @Override
    public void setPresenter(LoginContract.Presenter presenter) {
        this.presenter = presenter;

    }

    public void googleClicked(View view) {
        googleSignIn();
    }

    public void fbClicked(View view) {
        fbLoginButton.performClick();
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    class SplashShow extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public void onPostExecute(Void result) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    setContentView(R.layout.activity_splash);
                    setupView();
                }
            }, 3000);
        }
    }


    public static int getLocationMode(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (SettingNotFoundException e) {
                e.printStackTrace();
            }


        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

            if (TextUtils.isEmpty(locationProviders)) {
                locationMode = Settings.Secure.LOCATION_MODE_OFF;
            } else if (locationProviders.contains(LocationManager.GPS_PROVIDER) && locationProviders.contains(LocationManager.NETWORK_PROVIDER)) {
                locationMode = Settings.Secure.LOCATION_MODE_HIGH_ACCURACY;
            } else if (locationProviders.contains(LocationManager.GPS_PROVIDER)) {
                locationMode = Settings.Secure.LOCATION_MODE_SENSORS_ONLY;
            } else if (locationProviders.contains(LocationManager.NETWORK_PROVIDER)) {
                locationMode = Settings.Secure.LOCATION_MODE_BATTERY_SAVING;
            }

        }

        return locationMode;
    }


    public void callSplashRequest() {
        presenter.updateData(mAppManager.getTokenCode(), id, SplashActivity.this);
    }

    Dialog retry;

    private void retryDialog(final int i) {

        retry = new Dialog(SplashActivity.this, R.style.Theme_Dialog);
        retry.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = retry.getWindow();
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        retry.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        retry.setContentView(R.layout.thaks_dialog_twobutton);
        retry.setCancelable(false);
        retry.setCanceledOnTouchOutside(false);
        retry.show();

        TextView mTitle = (TextView) retry.findViewById(R.id.txt_title);
        mTitle.setText("Error!");
        mTitle.setTypeface(tf, Typeface.BOLD);
        TextView mDialogBody = (TextView) retry.findViewById(R.id.txt_dialog_body);
        mDialogBody.setText("The server is taking too long to respond OR something is wrong with your internet connection.");
        LinearLayout mOK = (LinearLayout) retry.findViewById(R.id.lin_Ok);
        LinearLayout mCancel = (LinearLayout) retry.findViewById(R.id.lin_cancel);
        TextView mRetry = (TextView) retry.findViewById(R.id.txt_dialog_ok);
        TextView mExit = (TextView) retry.findViewById(R.id.txt_dialog_cancel);
        mRetry.setText("RETRY");
        mExit.setText("EXIT");

        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


                retry.dismiss();

                switch (i) {
                    case 1:
                        if (mAppManager.getuser().equals("customer")) {
                            id_type = "customer_id";
                            id = mAppManager.getCustomerID();

                            if (cd.isConnectingToInternet()) {

                                callSplashRequest();

                            } else {
                                DialogAlert("No Internet !",
                                        "Check your connection and try again.");
                            }

                        }
                        break;

                    default:
                        break;

                }

            }
        });

        mCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                retry.dismiss();
//				System.exit(0);
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });

    }


    Dialog dialogA;

    private void DialogAlert(String title, String body) {

        dialogA = new Dialog(SplashActivity.this, R.style.Theme_Dialog);
        dialogA.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialogA.getWindow();
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialogA.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);

        dialogA.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);
        dialogA.setContentView(R.layout.dialog_onebutton);
        dialogA.setCancelable(false);
        dialogA.setCanceledOnTouchOutside(false);
        dialogA.show();

        TextView mTitle = (TextView) dialogA.findViewById(R.id.text_title);
        mTitle.setTypeface(tf, Typeface.BOLD);
        TextView mDialogBody = (TextView) dialogA
                .findViewById(R.id.txt_dialog_body);
        TextView mOK = (TextView) dialogA.findViewById(R.id.txt_dialog_ok);

        mTitle.setText(title);
        mDialogBody.setText(body);
        mOK.setText("OK");

        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogA.cancel();

                finish();
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == RequestPermissionCode) {

            if (grantResults.length > 0) {

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[2] == PackageManager.PERMISSION_GRANTED
                        ) {
                    createView();
                } else {
                    DialogAlert("Alert", "All permission are necessary to continue, please allow them from app setting to continue");

                }
            } else {
                DialogAlert("Alert", "All permission are necessary to continue, please allow them from app setting to continue");
            }
        }
    }


    public Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        showProgreass();
        // Pass the activity result back to the Facebook SDK
        mCallbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount acct = result.getSignInAccount();
                Log.d("social_login_google", acct.getDisplayName() + " , " + acct.getEmail() + " , " + acct.getId());

                userEmail = acct.getEmail();
                userName = acct.getDisplayName();
                googleId = acct.getId();

                Uri photoUrl = acct.getPhotoUrl();
                Target target = new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
                        MyApplication.bitmap = bitmap;
//                        final ImageView profilePicture = findViewById(R.id.img);
//                        profilePicture.setImageBitmap(MyApplication.bitmap);
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable drawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable drawable) {

                    }
                };
                Picasso.get().load(photoUrl).into(target);
                callWebServiceForGoogleLogin(acct.getDisplayName(), acct.getEmail(), acct.getId());
            }
        }
    }

    private void callWebServiceForGoogleLogin(String userName, String email, String uniqueId) {
       // FirebaseAuth.getInstance().signOut();
        presenter.loginGoogle(uniqueId, "", "", userName, email, SplashActivity.this);
        hideProgress();
    }

    private void callWebServiceForFBLogin(String userName, String email, String uniqueId) {
       // FirebaseAuth.getInstance().signOut();
        presenter.loginFb(uniqueId, "", "", userName, email, SplashActivity.this);
        hideProgress();
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d("fb_token", "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            Log.d("social_login_google", user.getDisplayName() + " , " + user.getEmail() + " , " + user.getUid());

                            userName = user.getDisplayName();
                            userEmail = user.getEmail();
                            fbId = user.getUid();
//                            callWebServiceForFBLogin(user.getDisplayName(), user.getEmail(), user.getUid());

                            getFacebookImage();
                        } else {
                            Log.w("fb_error", "signInWithCredential:failure", task.getException());
                            Toast.makeText(SplashActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    void getFacebookImage() {
        String facebookUserId = "";
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null && user.getProviderData() != null && user.getProviderData().size() > 0) {
            for (UserInfo profile : user.getProviderData()) {
                if (FacebookAuthProvider.PROVIDER_ID.equals(profile.getProviderId()))
                    facebookUserId = profile.getUid();
            }

            // construct the URL to the profile picture, with a custom height
            // alternatively, use '?type=small|medium|large' instead of ?height=
            String photoUrl = "https://graph.facebook.com/" + facebookUserId + "/picture?height=500";

            Target target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
                    MyApplication.bitmap = bitmap;
                    callWebServiceForFBLogin(userName, userEmail, fbId);
                }

                @Override
                public void onBitmapFailed(Exception e, Drawable drawable) {
                    callWebServiceForFBLogin(userName, userEmail, fbId);
                }

                @Override
                public void onPrepareLoad(Drawable drawable) {

                }
            };

            Picasso.get().load(photoUrl).into(target);
        }else {
            callWebServiceForFBLogin(userName, userEmail, fbId);
        }

    }


    private void googleSignIn() {
        try {
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, RC_SIGN_IN);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onDestroy() {
        try {
            if (mGoogleApiClient.isConnected() && mGoogleApiClient != null) {
                mGoogleApiClient.stopAutoManage((FragmentActivity) SplashActivity.this);
                mGoogleApiClient.disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onDestroy();

    }

}
