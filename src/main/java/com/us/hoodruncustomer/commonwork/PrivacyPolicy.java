package com.us.hoodruncustomer.commonwork;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.configure.Configure;
import com.us.hoodruncustomer.parser.ConnectionDetector;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PrivacyPolicy extends SimpleActivity {


	private TextView headertext;

	WebView webView;

	private Toolbar toolbar;

	private  String htmlText;
	private ConnectionDetector cd;
	private String mimeType;
	private String encoding;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_privacy);
		MyApplication.activity= PrivacyPolicy.this;
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		if (toolbar != null) {
			int color = Color.parseColor("#ffffff");
			toolbar.setTitleTextColor(color);
			setSupportActionBar(toolbar);
			getSupportActionBar().setDisplayShowTitleEnabled(false);

			getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
			toolbar.setNavigationIcon(R.drawable.back_arrow);
			toolbar.setNavigationOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					onBackPressed();
					finish();
				}
			});
		}
		font_path="fonts/opensans.ttf";
		tf = Typeface.createFromAsset(this.getAssets(), font_path);

		headertext=(TextView) findViewById(android.R.id.text1);
		headertext.setText("Privacy Policy");
		headertext.setTypeface(tf, Typeface.BOLD);
//         	      textview=(TextView)findViewById(R.id.textview1);
//        	      textview.setMovementMethod(new ScrollingMovementMethod());
		webView=(WebView)findViewById(R.id.webview);
		htmlText = "<body><p>" +getResources().getString(R.string.advise)+"</p>";


		mimeType = "text/html";
		encoding = "UTF-8";

		cd=new ConnectionDetector(PrivacyPolicy.this);
		if(cd.isConnectingToInternet()){
			showProgress();
			getTerms();
		}else{
			Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_LONG).show();
			System.exit(0);
		}

		mAppManager.savePrevActivity("HIS", "");
		//textview.setText(Html.fromHtml(htmlText));
		//webView.loadDataWithBaseURL("",htmlText,mimeType,encoding,"");
	}

	private void getTerms(){

		StringRequest stringRequest = new StringRequest(Request.Method.GET, Configure.URL_privacy_policy,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						hideProgress();
						System.out.println("GETTERMS==="+response);


						if(response != null) {

							htmlText = response;
							//	 textview.setText(Html.fromHtml( "<body><p>" +htmlText+"</p></body>"));
							webView.loadDataWithBaseURL("", "<body><p>" + htmlText + "</p></body>", mimeType, encoding, "");



						}else {
							Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
							System.exit(0);
						}


					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						hideProgress();
						retryDialog(1);
					}


				})
		{
			@Override
			protected Map<String,String> getParams(){

				Map<String,String> params = new HashMap<String, String>();

				params.put("user_Type", "1");


				return params;
			}

		};

		RequestQueue requestQueue = Volley.newRequestQueue(this);
		int socketTimeout = 60000;//60 seconds - change to what you want
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		stringRequest.setRetryPolicy(policy);
		requestQueue.add(stringRequest);
	}
	Dialog retry;
	private void retryDialog(final int i){

		retry = new Dialog(PrivacyPolicy.this, R.style.Theme_Dialog);
		retry.requestWindowFeature(Window.FEATURE_NO_TITLE);
		Window window = retry.getWindow();
		window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);
		retry.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		retry.setContentView(R.layout.thaks_dialog_twobutton);
		retry.setCancelable(false);
		retry.setCanceledOnTouchOutside(false);
		retry.show();

		TextView mTitle=(TextView)retry.findViewById(R.id.txt_title);
		mTitle.setText("Error!");
		mTitle.setTypeface(tf, Typeface.BOLD);
		TextView mDialogBody=(TextView)retry.findViewById(R.id.txt_dialog_body);
		mDialogBody.setText("The server is taking too long to respond OR something is wrong with your internet connection.");
		LinearLayout mOK=(LinearLayout)retry.findViewById(R.id.lin_Ok);
		LinearLayout mCancel=(LinearLayout)retry.findViewById(R.id.lin_cancel);
		TextView mRetry=(TextView)retry.findViewById(R.id.txt_dialog_ok);
		TextView mExit=(TextView)retry.findViewById(R.id.txt_dialog_cancel);
		mRetry.setText("RETRY");
		mExit.setText("EXIT");

		mOK.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {


				retry.dismiss();

				switch (i)
				{
					case 1:
						if(cd.isConnectingToInternet())
						{
							showProgress();
							getTerms();
						}

						else{
							Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_SHORT).show();
							System.exit(0);
						}
						break;

					default:
						break;

				}



			}
		});

		mCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				retry.dismiss();
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);

			}
		});

	}
	@Override
	protected void onResume() {
		super.onResume();
		MyApplication.activityResumed();
	}

	@Override
	protected void onPause() {
		super.onPause();
		MyApplication.activityPaused();
	}

}
