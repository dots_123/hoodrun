package com.us.hoodruncustomer.rider.beans;

import com.google.android.gms.maps.model.Marker;

/**
 * Created by admin on 23-May-17.
 */

public class MarkerHolder {

    private int driver_id;
    private Marker marker;

    public int getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(int driver_id) {
        this.driver_id = driver_id;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }
}
