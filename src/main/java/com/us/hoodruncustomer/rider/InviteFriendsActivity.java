package com.us.hoodruncustomer.rider;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.commonwork.BaseActivity;
import com.us.hoodruncustomer.configure.Configure;
import com.us.hoodruncustomer.exceptionhandler.ExceptionHandler;
import com.us.hoodruncustomer.fragment.PushDialog;
import com.us.hoodruncustomer.home.CustomerHomeActivity;
import com.us.hoodruncustomer.parser.ConnectionDetector;
import com.us.hoodruncustomer.parser.Parser;

import java.util.HashMap;
import java.util.Map;

public class InviteFriendsActivity extends BaseActivity {

	private Toolbar toolbar;
	private TextView headertext,promocode,invite_description,tc,tctext;
	private RelativeLayout invite_btn;
	DisplayMetrics metrics;
	int width;
	String user_type,font_path;
	Typeface tf;
	ConnectionDetector cd;
	String promocode_value=null;
	String promocode_description=null;
	String promocode_link=null;
	String promocode_tc=null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_invite_friends);

		/**
		 * Important to catch the exceptions
		 */

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

		/*****************************************************************************************/


		MyApplication.activity= InviteFriendsActivity.this;
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		if (toolbar != null) {
			int color = Color.parseColor("#ffffff");
			toolbar.setTitleTextColor(color);
			setSupportActionBar(toolbar);
			getSupportActionBar().setDisplayShowTitleEnabled(false);

			getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
			headertext=(TextView) findViewById(android.R.id.text1);
			promocode =(TextView) findViewById(R.id.promocode);
			tc =(TextView) findViewById(R.id.tc);
			tctext =(TextView) findViewById(R.id.tctext);
			invite_description=(TextView) findViewById(R.id.invite_description);
			invite_btn =(RelativeLayout) findViewById(R.id.offer_btn);
			headertext.setText("Invite");
			tc.setText(Html.fromHtml("<u>Terms & Conditions</u>"));

		}

		invite_btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent sharingIntent = new Intent(Intent.ACTION_SEND);
				sharingIntent.setType("text/plain");
				String shareBody = promocode_link;
				sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Download HoodRun Using the below link");
				sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
				startActivity(Intent.createChooser(sharingIntent, "Share via"));
			}
		});

		font_path = "fonts/opensans.ttf";
		tf = Typeface.createFromAsset(this.getAssets(), font_path);
		String[] navMenuTitles;
		TypedArray navMenuIcons;

		if(mAppManager.getuser().equals("customer")){

			navMenuTitles = getResources().getStringArray(R.array.nav_drawer_user_items);
			navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons_user);
			user_type="1";
		}else{

			navMenuTitles = getResources().getStringArray(R.array.nav_drawer_driver_items);
			navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);
			user_type="2";
		}


		set(navMenuTitles, navMenuIcons);
		mAppManager.savePrevActivity("HIS", "");

		headertext.setTypeface(tf,Typeface.BOLD);


		metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		width = metrics.widthPixels;
		//this code for adjusting the group indicator into right side of the view

		cd=new ConnectionDetector(InviteFriendsActivity.this);
		if(cd.isConnectingToInternet()) {
			showProgress();
			getHelp();
		}
		else {
			Toast.makeText(getApplicationContext(), "No internet connection available.", Toast.LENGTH_LONG).show();
			System.exit(0);
		}



	}

	public int GetDipsFromPixel(float pixels)
	{
		// Get the screen's density scale
		final float scale = getResources().getDisplayMetrics().density;
		// Convert the dps to pixels, based on density scale
		return (int) (pixels * scale + 0.5f);
	}


	@Override
	public void onBackPressed()
	{
		// code here to show dialog
		super.onBackPressed();  // optional depending on your needs

			startActivity(new Intent(InviteFriendsActivity.this,CustomerHomeActivity.class));

	}


	String parsresp;
	private void getHelp(){

		StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_invite_friends,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {

						hideProgress();
						System.out.println("GETINVITE==="+response);

						try {
							parsresp=Parser.getInvites(response);
						} catch (Exception e) {
							e.printStackTrace();
						}
						if (!TextUtils.isEmpty(parsresp)) {

							switch (parsresp) {
								case "true":

									promocode_value = Parser.getInvite_referral_code();
									promocode_description = Parser.getInvite_description();
									promocode_link = Parser.getInvite_referral_link();
									promocode_tc = Parser.getInvite_tc();
									tctext.setText(promocode_tc);

									if(promocode_value != null){
										promocode.setText(promocode_value);
									}

									if(promocode_description != null){
										invite_description.setText(promocode_description);
									}


									break;
								case "false":


									break;
							}


						}else {

							PushDialog pushDialog=new PushDialog(InviteFriendsActivity.this,"You have not completed any rides.",user_type);
							pushDialog.show();
						}
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						hideProgress();
						retryDialog(1);
					}


				})
		{
			@Override
			protected Map<String,String> getParams(){

				Map<String,String> params = new HashMap<String, String>();

				params.put("customer_id", mAppManager.getCustomerID());

                params.put("token_code", mAppManager.getTokenCode());

				return params;
			}

		};

		RequestQueue requestQueue = Volley.newRequestQueue(this);
		int socketTimeout = 60000;//60 seconds - change to what you want
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		stringRequest.setRetryPolicy(policy);
		requestQueue.add(stringRequest);
	}

	@Override
	protected void onResume() {
		super.onResume();
		MyApplication.activityResumed();
	}

	@Override
	protected void onPause() {
		super.onPause();
		MyApplication.activityPaused();
	}



	Dialog retry;
	private void retryDialog(final int i){

		retry = new Dialog(InviteFriendsActivity.this, R.style.Theme_Dialog);
		retry.requestWindowFeature(Window.FEATURE_NO_TITLE);
		Window window = retry.getWindow();
		window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);
		retry.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		retry.setContentView(R.layout.thaks_dialog_twobutton);
		retry.setCancelable(false);
		retry.setCanceledOnTouchOutside(false);
		retry.show();

		TextView mTitle=(TextView)retry.findViewById(R.id.txt_title);
		mTitle.setText("Error!");
		mTitle.setTypeface(tf, Typeface.BOLD);
		TextView mDialogBody=(TextView)retry.findViewById(R.id.txt_dialog_body);
		mDialogBody.setText("Internet connection is slow or disconnected. Try connecting again.");
		LinearLayout mOK=(LinearLayout)retry.findViewById(R.id.lin_Ok);
		LinearLayout mCancel=(LinearLayout)retry.findViewById(R.id.lin_cancel);
		TextView mRetry=(TextView)retry.findViewById(R.id.txt_dialog_ok);
		TextView mExit=(TextView)retry.findViewById(R.id.txt_dialog_cancel);
		mRetry.setText("RETRY");
		mExit.setText("EXIT");

		mOK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {


				retry.dismiss();

				switch (i)
				{
					case 1:
						if(cd.isConnectingToInternet())
						{
							showProgress();
							getHelp();
						}

						else{
							Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_SHORT).show();
							System.exit(0);
						}
						break;

					default:
						break;

				}



			}
		});

		mCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				retry.dismiss();
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);

			}
		});

	}
}