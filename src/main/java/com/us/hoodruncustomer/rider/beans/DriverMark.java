package com.us.hoodruncustomer.rider.beans;

import com.google.android.gms.maps.model.Marker;

/**
 * Created by dell pc on 28-05-2017.
 */

public class DriverMark {

    String carType;
    Marker marker;

    public DriverMark(String carType, Marker marker) {
        this.carType = carType;
        this.marker = marker;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }
}
