package com.us.hoodruncustomer.authentication;

import android.content.Context;
import android.support.annotation.NonNull;

import com.us.hoodruncustomer.data.DataSource;
import com.us.hoodruncustomer.data.remote.RemoteDataSource;


/**
 * Created by admin
 * Created on 11/9/2016.
 * Modified on 09,November,2016
 */
public class LoginPresenter implements LoginContract.Presenter, DataSource.LoginRsponseI, DataSource.UpdateDataI, DataSource.ForgotPasswordI, DataSource.GoogleLoginRsponseI, DataSource.FbLoginRsponseI {
    private final DataSource loginDataSource;
    private LoginContract.View mLoginView;
    private Context context;


    public LoginPresenter(@NonNull RemoteDataSource listDataSource, LoginContract.View loginFragment) {
        loginDataSource = listDataSource;
        mLoginView = loginFragment;
        mLoginView.setPresenter(this);

    }


    @Override
    public void login(String username, String password, Context context) {
        mLoginView.showProgreass();
        loginDataSource.loginService(username, password, this, context);

    }

    @Override
    public void loginFb(String facebookId, String device_id, String os, String name, String email, Context context) {
        mLoginView.showProgreass();
        loginDataSource.fbLoginService(facebookId, device_id, os, name, email, this, context);
    }


    @Override
    public void loginGoogle(String googleId, String device_id, String os, String name, String email, Context context) {

        mLoginView.showProgreass();
        loginDataSource.googleLoginService(googleId, device_id, os, name, email, this, context);
    }

    @Override
    public void updateData(String tokencode, String id_type, Context context) {

        mLoginView.showProgreass();
        loginDataSource.updateDataService(tokencode, id_type, this, context);
    }

    @Override
    public void forgotPassword(String email, Context context) {
        mLoginView.showProgreass();
        loginDataSource.forgotPassword(email, this, context);
    }


    @Override
    public void loginResponse(String baseResponse) {
        mLoginView.hideProgress();
        mLoginView.nevigateToHeme(baseResponse);


    }


    @Override
    public void googleloginResponse(String baseResponse) {
        mLoginView.hideProgress();
        mLoginView.nevigateToHeme(baseResponse);
    }

    @Override
    public void fbloginResponse(String baseResponse) {
        mLoginView.hideProgress();
        mLoginView.nevigateToHeme(baseResponse);
    }

    @Override
    public void onFail(Throwable t) {
        mLoginView.hideProgress();
        mLoginView.onLoginFail(t);

    }

    @Override
    public void drop() {
        mLoginView.hideProgress();
        mLoginView = null;
    }

    @Override
    public void updateDataResponse(String baseResponse) {
        mLoginView.hideProgress();
        mLoginView.updateDataResponse(baseResponse);

    }

    @Override
    public void onUpdateDataFail(Throwable t) {
        mLoginView.hideProgress();
        mLoginView.onUpdateFail(t);

    }


    @Override
    public void forgotDataResponse(String baseResponse) {
        mLoginView.hideProgress();
        mLoginView.forgotResponseSuccess(baseResponse);
    }

    @Override
    public void onforgotFail(Throwable t) {
        mLoginView.hideProgress();
        mLoginView.forgotFail(t);
    }
}
