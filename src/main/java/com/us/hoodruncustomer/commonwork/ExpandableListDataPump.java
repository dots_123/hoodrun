package com.us.hoodruncustomer.commonwork;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ExpandableListDataPump {
public static String status,status_messsage;
public static HashMap<String, List<String>> getData(String json) throws JSONException {

    JSONObject mJObject=new JSONObject(json);
    status=mJObject.optString("status");
    status_messsage=mJObject.optString("message");
    JSONArray jArray= mJObject.optJSONArray("help");
    HashMap<String, List<String>> expandableListDetail = null;
    expandableListDetail = new HashMap<String, List<String>>();
    for (int i = 0; i < jArray.length(); i++) {
        JSONObject mJObj=jArray.getJSONObject(i);
        String title=mJObj.optString("title");
     List<String> descriptions=new ArrayList<String>();
     String desc=mJObj.optString("description");
        descriptions.add(desc);
        expandableListDetail.put(title, descriptions);
    }

    return expandableListDetail;
}
}
