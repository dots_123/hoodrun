package com.us.hoodruncustomer.location;

import android.location.Address;
import android.util.Log;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
public class MyGeocoder {


    public static List<Address> getFromLocation(double lat, double lng, int maxResult) {

    	String gurl = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + ","
                + lng + "&sensor=false&language=en";

        InputStream is=null;

        List<Address> retList = null;
       

        try {
        	
        
            HttpGet httpGet = new HttpGet(gurl);
                HttpClient client = new DefaultHttpClient();
                HttpResponse response;
           

                
                response = client.execute(httpGet);
                HttpEntity entity = response.getEntity();
               is= entity.getContent();
                        

        } catch (ClientProtocolException e) {
            Log.e(MyGeocoder.class.getName(), "Error calling Google geocode webservice.", e);
        } catch (IOException e) {
            Log.e(MyGeocoder.class.getName(), "Error calling Google geocode webservice.", e);
        }

		try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
              sb.append(line + "\n");
            }
            is.close();
            System.out.println("GEO---"+sb.toString());
            
            JSONObject jsonObject = new JSONObject(sb.toString());

            retList = new ArrayList<Address>();
           	
            StringBuilder sBuilder=new StringBuilder();
            String components=null;
                JSONArray results = jsonObject.getJSONArray("results");
                if (results.length() > 0) { 
//                	for (int i = 0; i < results.length(); i++)
//                
//                {
                    // loop among all addresses within this result
                    JSONObject result = results.getJSONObject(0);
                    if (result.has("address_components"))
                    {
                        JSONArray addressComponents = result.getJSONArray("address_components");
                        // loop among all address component to find a 'locality' or 'sublocality'
                        for (int j = 0; j < 3; j++)
                        {
                            JSONObject addressComponent = addressComponents.getJSONObject(j);
                            components=addressComponent.getString("long_name");
                            System.out.println("COMPONENTS "+j+components);
                            if(j<2)
                            sBuilder.append(components).append(" ,\n");
                            else
                            	sBuilder.append(components);
                           
                        }
                        System.out.println("SSSS "+sBuilder.toString());
                        Address mAddres=new Address(Locale.getDefault());
                        mAddres.setPremises(sBuilder.toString());
                        retList.add(mAddres);
                    }
                //}
                }
		}
            
            catch (Exception ignored)
            {
                ignored.printStackTrace();
            }

            
            

        return retList;
    
		}
    
    }

