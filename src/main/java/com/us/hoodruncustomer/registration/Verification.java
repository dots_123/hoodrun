package com.us.hoodruncustomer.registration;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.commonwork.Injection;
import com.us.hoodruncustomer.commonwork.SimpleActivity;
import com.us.hoodruncustomer.configure.Configure;
import com.us.hoodruncustomer.custom.MyTextView;
import com.us.hoodruncustomer.exceptionhandler.ExceptionHandler;
import com.us.hoodruncustomer.home.CustomerHomeActivity;
import com.us.hoodruncustomer.nodeservice.ConnectorService;
import com.us.hoodruncustomer.parser.ConnectionDetector;
import com.us.hoodruncustomer.parser.Parser;

import org.json.JSONException;
import org.json.JSONObject;

public class Verification extends SimpleActivity implements RegisterContract.View {

    EditText txt_code1, txt_code2, txt_code3, txt_code4;
    TextView title, ok;

    String customer_id, otp;
    ConnectionDetector cd;
    String parsresp, message;
    String name, mobile_number, prev_activity, email;
    TextView mtxtmobno, mTxtResendCode;
    MyTextView mtxtname;
    String font_path;
    Typeface tf;
    TextView headertext;
    String pMessage = "";
    private RegisterContract.Presenter presenter;
    // private String verify_url="http://www.worldatclick.com/hoponn/api/customers/otp_verification";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new RegisterPresenter(Injection.remoteDataRepository(getApplicationContext()), this);
        /**
         * Important to catch the exceptions
         */

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        /*****************************************************************************************/

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            int color = Color.parseColor("#ffffff");
            toolbar.setTitleTextColor(color);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            toolbar.setNavigationIcon(R.drawable.back_arrow);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            headertext = (TextView) findViewById(android.R.id.text1);
            headertext.setText("Verification");

        }


        cd = new ConnectionDetector(Verification.this);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        customer_id = mAppManager.getCustomerID();
        name = mAppManager.getCustomerName();
        mobile_number = mAppManager.getMobileNumber();
        email = mAppManager.getEmail();
        prev_activity = mAppManager.getPrevActivity("VERIFY");

        font_path = "fonts/opensans.ttf";

        tf = Typeface.createFromAsset(this.getAssets(), font_path);
        System.out.println("cccccc" + customer_id);
        mtxtname = (MyTextView) findViewById(R.id.txt_name);
        mtxtmobno = (TextView) findViewById(R.id.txt_mobile);
        mTxtResendCode = (TextView) findViewById(R.id.txt_resend);
        txt_code1 = (EditText) findViewById(R.id.txt_code1);
        txt_code2 = (EditText) findViewById(R.id.txt_code2);
        txt_code3 = (EditText) findViewById(R.id.txt_code3);
        txt_code4 = (EditText) findViewById(R.id.txt_code4);


        headertext.setTypeface(tf, Typeface.BOLD);
        mtxtname.setTypeface(tf, Typeface.BOLD);
        pMessage = "You are now registered and ready to go.";

        if (prev_activity.equals("LOGIN")) {
            mtxtname.setText("HELLO, " + name);
            mtxtmobno.setText("We have sent a text with your verification code to " + mobile_number + ", please enter it to complete your registration");
        } else if (prev_activity.equals("ADD")) {
            mtxtname.setText("HELLO, " + name);
            mtxtmobno.setText("We have sent a text with your verification code to " + mobile_number + ", please enter it to complete your registration");
        } else {

            pMessage = "Profile updated successfully.";

            if (mAppManager.getOtpDestiantion().equals("mobile")) {

                mtxtname.setText("HELLO, " + name);
                mtxtmobno.setText("We have sent a text with your verification code to " + mobile_number + ", please enter it to complete your registration");
            } else {
                mtxtname.setText("HELLO!, " + name);
                mtxtmobno.setText("We have sent a text with your verification code to " + email + ", please enter it to complete your registration");
            }

        }


        txt_code1.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
                txt_code1.setSelection(count);

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (txt_code1.length() == 1) { // check for number of characters

                    txt_code1.clearFocus();

                    txt_code2.requestFocus();

                }
            }
        });
        txt_code2.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
                txt_code2.setSelection(count);

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (txt_code2.length() == 1) { // check for number of characters

                    txt_code2.clearFocus();

                    txt_code3.requestFocus();

                }

            }
        });
        txt_code3.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
                txt_code3.setSelection(count);

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (txt_code3.length() == 1) { // check for number of characters

                    txt_code3.clearFocus();

                    txt_code4.requestFocus();

                }

            }
        });


        SpannableString ss = new SpannableString("I haven't received the code, please re-send it");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {


                if (cd.isConnectingToInternet()) {
                    if (mAppManager.getOtpDestiantion().equals("mobile")) {
                        //showProgress();
                        resendCode(Configure.URL_resend_otp);
                    } else {
                        // showProgress();
                        resendCode(Configure.URL_resend_otp_email);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "No internet connection available, please try again later.", Toast.LENGTH_LONG).show();
                    System.exit(0);
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(Color.parseColor("#4570FD"));
            }
        };
        ss.setSpan(clickableSpan, 36, 43, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


        mTxtResendCode.setText(ss);

        mTxtResendCode.setMovementMethod(LinkMovementMethod.getInstance());
        mTxtResendCode.setHighlightColor(Color.TRANSPARENT);
    }


    Dialog dialog2;

    private void Dialog(final int i) {

        dialog2 = new Dialog(Verification.this, R.style.Theme_Dialog);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog2.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog2.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);
        dialog2.setContentView(R.layout.dialog_onebutton);
        dialog2.setCancelable(false);
        dialog2.setCanceledOnTouchOutside(false);
        dialog2.show();

        TextView mTitle = (TextView) dialog2.findViewById(R.id.text_title);
        TextView mDialogBody = (TextView) dialog2.findViewById(R.id.txt_dialog_body);
        TextView mOK = (TextView) dialog2.findViewById(R.id.txt_dialog_ok);
        if (i == 1) {
            mTitle.setText("SUCCESS");

            mDialogBody.setText(pMessage);
        } else {
            mTitle.setText("RESEND-CODE");
            if (mAppManager.getOtpDestiantion().equals("mobile"))
                mDialogBody.setText("The verification code has been sent to your registered mobile number");
            else
                mDialogBody.setText("The verification code has been sent to your registered email");

        }
        mOK.setText("OK");
        mOK.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                dialog2.dismiss();
                if (i == 1) {
//					if(mAppManager.getTokenCode() != null && !mAppManager.getTokenCode().isEmpty()) {
//						MyApplication app = (MyApplication) getApplication();
//						app.StartNode();
//					}
                    Intent myintent = new Intent(Verification.this, CustomerHomeActivity.class);
                    startActivity(myintent);
                    finish();
                }
            }
        });

    }


    public void dr_home(View v) {

        if (cd.isConnectingToInternet()) {
            if (txt_code1.getText().toString().equals("") || txt_code2.getText().toString().equals("") || txt_code3.getText().toString().equals("") || txt_code4.getText().toString().equals("")) {
                Toast.makeText(getApplicationContext(), "OTP field is required.", Toast.LENGTH_LONG).show();
            } else {
                otp = txt_code1.getText().toString().trim() + "" + txt_code2.getText().toString().trim() + "" + txt_code3.getText().toString().trim() + "" + txt_code4.getText().toString().trim();

                if (mAppManager.getOtpDestiantion().equals("mobile")) {
                    System.out.println("OTP destination " + mAppManager.getOtpDestiantion());
                    //  showProgress();
                    attemptingToVerification(Configure.URL_otp_verification);
                } else {
                    System.out.println("OTP destination2 " + mAppManager.getOtpDestiantion());
                    // showProgress();
                    attemptingToVerification(Configure.URL_email_otp_verification);
                }
            }


        } else {
            Toast.makeText(getApplicationContext(), "No internet connection available.", Toast.LENGTH_LONG).show();
            System.exit(0);
        }

    }


    private void attemptingToVerification(String url) {

        presenter.saveOtp(customer_id, otp, email, mobile_number, url, this);


    }


    private void resendCode(String url) {


        presenter.resendOtp(customer_id, mAppManager.getTokenCode(), mAppManager.getEmail(), mAppManager.getMobileNumber(), url, this);


    }


    Dialog retry;

    private void retryDialog(final int i) {

        retry = new Dialog(Verification.this, R.style.Theme_Dialog);
        retry.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = retry.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        retry.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        retry.setContentView(R.layout.thaks_dialog_twobutton);
        retry.setCancelable(false);
        retry.setCanceledOnTouchOutside(false);
        retry.show();

        TextView mTitle = (TextView) retry.findViewById(R.id.txt_title);
        mTitle.setText("Error!");
        mTitle.setTypeface(tf, Typeface.BOLD);
        TextView mDialogBody = (TextView) retry.findViewById(R.id.txt_dialog_body);
        mDialogBody.setText("Internet connection is slow or disconnected. Try connecting again.");
        LinearLayout mOK = (LinearLayout) retry.findViewById(R.id.lin_Ok);
        LinearLayout mCancel = (LinearLayout) retry.findViewById(R.id.lin_cancel);
        TextView mRetry = (TextView) retry.findViewById(R.id.txt_dialog_ok);
        TextView mExit = (TextView) retry.findViewById(R.id.txt_dialog_cancel);
        mRetry.setText("RETRY");
        mExit.setText("EXIT");

        mOK.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                retry.dismiss();

                switch (i) {
                    case 1:
                        if (cd.isConnectingToInternet()) {
                            if (mAppManager.getOtpDestiantion().equals("mobile")) {
                                System.out.println("OTP destination " + mAppManager.getOtpDestiantion());
                                // showProgress();
                                attemptingToVerification(Configure.URL_otp_verification);
                            } else {
                                System.out.println("OTP destination2 " + mAppManager.getOtpDestiantion());
                                // showProgress();
                                attemptingToVerification(Configure.URL_email_otp_verification);
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_SHORT).show();
                            System.exit(0);
                        }
                        break;
                    case 2:
                        if (cd.isConnectingToInternet()) {
                            if (mAppManager.getOtpDestiantion().equals("mobile")) {
                                //  showProgress();
                                resendCode(Configure.URL_resend_otp);
                            } else {
                                //   showProgress();
                                resendCode(Configure.URL_resend_otp_email);
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "No internet connection available, please try again later.", Toast.LENGTH_LONG).show();
                            System.exit(0);
                        }
                        break;
                    default:
                        break;

                }


            }
        });

        mCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                retry.dismiss();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });

    }


    @Override
    public void registerSuccessResponse(String response) {

    }

    @Override
    public void showProgreass() {

        loader = new Dialog(Verification.this);
        loader.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loader.setContentView(R.layout.dialog_progress);
        loader_wheel = (ProgressWheel) loader.findViewById(R.id.progress_wheel);
        loader_wheel.setCircleRadius(50);
        loader.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        loader.setCancelable(false);
        loader.show();

    }

    @Override
    public void hideProgress() {

        if (loader != null) {
            loader.dismiss();
        }

    }


    @Override
    public void onLoginFail(Throwable t) {

        hideProgress();
        retryDialog(1);

    }


    @Override
    public void onSaveResponseOtp(String response) {

        hideProgress();
        System.out.println("VERIFICATION===" + response);
        parsresp = Parser.verification(response);

        if (!TextUtils.isEmpty(parsresp)) {

            switch (parsresp) {
                case "TRUE":
                    mAppManager.saveTokenCode(Parser.getToken_code());
                    mAppManager.saveCustomerID(Parser.customer_id);
                    mAppManager.saveUser("customer");
                    mAppManager.saveCustomerName(Parser.getName());
                    mAppManager.setLogin_status(true);
                    if (mAppManager.getTokenCode() != null && !mAppManager.getTokenCode().isEmpty()) {
                        if (!mSocket.connected()) {
                            Log.d("NodeConnection", "SuperBaseActivity-onResume");
                            Intent start_Serv = new Intent(getApplicationContext(), ConnectorService.class);
                            startService(start_Serv);
                        }
                    }
                    Dialog(1);

                    break;
                case "FALSE":
                    Toast.makeText(getApplicationContext(), "" + Parser.getVerification_Status(), Toast.LENGTH_SHORT).show();
                    txt_code1.setText(null);
                    txt_code2.setText(null);
                    txt_code3.setText(null);
                    txt_code4.setText(null);

                    break;
            }


        } else {
            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
            System.exit(0);
        }


    }

    @Override
    public void onSuccessResendOtp(String response) {

        hideProgress();
        System.out.println("RESENDOTP===" + response);
        JSONObject dataObject = null;
        try {
            dataObject = new JSONObject(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        parsresp = dataObject.optString("status");
        message = dataObject.optString("message");

        if (!TextUtils.isEmpty(parsresp)) {

            switch (parsresp) {
                case "TRUE":


                    Dialog(2);


                    break;
                case "FALSE":
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    break;
            }


        } else {

            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
            System.exit(0);
        }

    }

    @Override
    public void onFailResend(Throwable t) {
        hideProgress();
        retryDialog(2);

    }

    @Override
    public void setPresenter(RegisterContract.Presenter presenter) {
        this.presenter = presenter;

    }
}
