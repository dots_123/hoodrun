package com.us.hoodruncustomer.model;

public  class Drivers {

	private  String driver_id;
	private  String lat;
	private  String lng;
	private  boolean surge_applied;
	private  String surge_message;
	private  String fare_multiplier;
	private  String car_type;

	public boolean isSurge_applied() {
		return surge_applied;
	}

	public void setSurge_applied(boolean surge_applied) {
		this.surge_applied = surge_applied;
	}

	public String getSurge_message() {
		return surge_message;
	}

	public void setSurge_message(String surge_message) {
		this.surge_message = surge_message;
	}

	public String getFare_multiplier() {
		return fare_multiplier;
	}

	public void setFare_multiplier(String fare_multiplier) {
		this.fare_multiplier = fare_multiplier;
	}

	public String getDriver_id() {
		return driver_id;
	}

	public void setDriver_id(String driver_id) {
		this.driver_id = driver_id;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getCar_type() {
		return car_type;
	}

	public void setCar_type(String car_type) {
		this.car_type = car_type;
	}

}
