package com.us.hoodruncustomer.trackride;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.StrictMode;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.android.volley.RequestQueue;
import com.bumptech.glide.Glide;
import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnCameraIdleListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.CircleTransform;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.commonwork.Injection;
import com.us.hoodruncustomer.commonwork.SimpleActivity;
import com.us.hoodruncustomer.configure.SphericalUtil;
import com.us.hoodruncustomer.configure.Util;
import com.us.hoodruncustomer.exceptionhandler.ExceptionHandler;
import com.us.hoodruncustomer.fragment.InvalidTokenDialog;
import com.us.hoodruncustomer.fragment.PushDialog;
import com.us.hoodruncustomer.home.CustomerHomeActivity;
import com.us.hoodruncustomer.nodeservice.ConnectorService;
import com.us.hoodruncustomer.nodeservice.NodeUtil;
import com.us.hoodruncustomer.parser.ConnectionDetector;
import com.us.hoodruncustomer.parser.Parser;
import com.us.hoodruncustomer.share.ShareTheFare;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class TrackRide extends SimpleActivity implements OnClickListener, OnMapReadyCallback, OnCameraChangeListener, RoutingListener, OnCameraIdleListener, TrackRideContract.View {

    private TrackRideContract.Presenter presenter;
    public static final String RECEIVER_DROP_CHNAGED = "com.uk.hoponn.chnaged_drop";
    public static final String RECEIVER_RIDE_CANCELLED = "com.uk.hoponn.ride_cancelled";
    public static final int NOTIFICATION_ID = 1000;
    private static final int[] COLORS = new int[]{R.color.black, R.color.black, R.color.black, R.color.black, R.color.black};
    private static boolean nodeStatus = false;
    private static int CallRequestPermissionCode = 123;
    final int RQS_GooglePlayServices = 1;
    private final Handler mHandler = new Handler();
    public String cancel_status;
    protected LatLng customerstart;
    protected LatLng driverstart;
    protected LatLng destinationend;
    private boolean isCancelled;
    protected LatLng start, end;
    boolean parseresponce;
    double driver_latitude = 51.5013673, driver_longitude = -0.1440787, user_latitude = 51.501367, user_longitude = -0.1440787, user_destn_lat, user_destn_lon;
    String parsresp = "", message1 = "", message = "";
    Marker marker, marker1, marker_d;
    int r = 0, d = 0;
    float mZoom = 14;
    int a = 0;
    int show = 0;
    boolean kill_ride_status = false;
    boolean kill_driver_location = false;
    boolean kill_end_ride = false;
    MapFragment mMapFragment;
    PowerManager pm;
    PowerManager.WakeLock wl;
    int status;
    ConnectionDetector cd;
    Typeface tf;
    String font_path;
    String s_msg;
    ProgressWheel pWheel;
    TextView headertext;
    int cancel_stat = 0;
    Activity context;
    InvalidTokenDialog iDialog;
    RelativeLayout layout_bg;
    ImageView driver_imageImg, car_imageImg, driver_image1, destination_drop;
    ViewSwitcher switcher;
    /**
     * Node Socket
     *
     * @param savedInstanceState
     */

    Dialog dialog5;
    Dialog dialog3;
    Dialog dialog2;
    Dialog dialog;
    Dialog cdialog;
    boolean doubleBackToExitPressedOnce = false;
    Dialog retry;
    /**
     * Draw Route Between Locations
     */
    Routing routing;
    boolean killMe = false;
    Handler boundHandler;
    Runnable updateboundHandler = new Runnable() {
        @Override
        public void run() {
            if (!killMe) {


            }

        }
    };
    NotificationManager mNotificationManager;
    PendingIntent contentIntent;
    int totalDistane = 0;
    // Google Map
    private GoogleMap googleMap;
    private List<Polyline> polylines;
    private List<Marker> markers = new ArrayList<Marker>();
    private int driver_status = 0;
    private int timeFromDriver;
    private int routeStatus = 0;
    private int currentway = 1;
    private NotificationManager myNManager;
    private HashMap<String, Integer> rejected_drivers_list = new HashMap<String, Integer>();
    private Marker selectedMarker;
    private String driver_image;
    private String driver_name;
    private String driver_cartype;
    private String driver_rating;
    private String driver_number;
    private String car_image;
    private String car_name;
    private String car_number;
    private String car_surge;
    private int ride_status;
    private Bitmap carImage;
    private boolean arriving_status;
    private int CarStatus = 0;
    private TextView driver_nametxt, driver_nametxt_start, driver_cartypetxt_start, car_name_start, car_number_start, surge_start, driver_cartypetxt, driver_ratingtxt, driver_ratingStartTxt, car_nametxt, car_numbertxt, car_surgetxt, cancelride, dropup, dropups, pickedup, call;
    /**
     * ********************************** Node Start ****************************************************** //
     */
    private LinearLayout view_driverinfo, surge_container, surge_start_lay;
    private LinearLayout start_before;
    private RelativeLayout pickview;
    private String UniqueId = "";
    private boolean activityinpause = false;
    private String dest_name, picup_name;
    private RelativeLayout mapContainer;
    private BroadcastReceiver Node_CUSTOMER_Location_Update = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {

            String request = in.getStringExtra("Object");
            try {
                if (request != null) {
                    JSONArray arr = new JSONArray(request);
                    if (arr != null && arr.length() > 0) {


                        JSONObject data = null;

                        data = arr.getJSONObject(0);

                        if (data != null) {

                            double latitute = data.getDouble("lat");
                            double longitude = data.getDouble("lng");

                            LatLng last = new LatLng(driver_latitude, driver_longitude);
                            LatLng newone = new LatLng(latitute, longitude);

                            double distance = SphericalUtil.computeDistanceBetween(last, newone);

                            if (distance > 10.0) {

                                driver_latitude = latitute;
                                driver_longitude = longitude;

                                float bearing = bearingBetweenLatLngs(last, newone);
                                driverstart = newone;
                                marker1.setRotation(bearing);
                                marker1.setAnchor(0.5f, 0.5f);
                                marker1.setFlat(true);

                                animateMarker(marker1, newone, false);

                                if (routing != null) {
                                    routing.cancel(true);
                                }
                                if (ride_status > 2) {

                                    route(driverstart, destinationend, 2);
                                } else {
                                    route(customerstart, driverstart, 1);
                                }

                                double distance_cust_driver = SphericalUtil.computeDistanceBetween(customerstart, newone);

                                if (ride_status == 2 && distance_cust_driver < 500.0) {

                                    if (!arriving_status) {
                                        arriving_status = true;
                                        headertext.setText("Arriving Now".toUpperCase());
                                        MyApplication.startPlayer(R.raw.wolf);
                                    }

                                }

                            }

                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    };
    private BroadcastReceiver dropupChanged = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {

            JSONObject locationchnaged = null;
            try {

                locationchnaged = new JSONObject(in.getStringExtra("dropResponse"));
                mAppManager.App_setDriver_customer_destination_point_lat(locationchnaged.optString("customer_destination_point_lat"));
                mAppManager.App_setDriver_customer_destination_point_long(locationchnaged.optString("customer_destination_point_long"));
                mAppManager.App_setDriver_customer_destination_point_name(locationchnaged.optString("destination_Location_Name"));

                dest_name = locationchnaged.optString("destination_Location_Name");
                dropup.setText(dest_name);
                dropups.setText(dest_name);
                user_destn_lat = Double.parseDouble(locationchnaged.optString("customer_destination_point_lat"));
                user_destn_lon = Double.parseDouble(locationchnaged.optString("customer_destination_point_long"));
                destinationend = new LatLng(user_destn_lat, user_destn_lon);

                for (Polyline line : polylines) {
                    line.remove();
                }

                polylines.clear();
                marker_d.remove();

                marker_d = googleMap.addMarker(new MarkerOptions().position(new LatLng(user_destn_lat, user_destn_lon)).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pointer)));

                driverstart = new LatLng(driver_latitude, driver_longitude);
                if (routing != null) {
                    routing.cancel(true);
                }
                route(driverstart, destinationend, 2);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };

    private BroadcastReceiver RideCancelled = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            Bundle extras = intent.getExtras();
            if (extras != null) {
                if (extras.containsKey("push_flag")) {
                    push_flag = extras.getString("push_flag");
                    push_message = extras.getString("push_message");

                    if (!TrackRide.this.isFinishing()) {
                        try {
                            if (!isCancelled) {
                                isCancelled = true;
                                PushDialog pushDialog = new PushDialog(TrackRide.this, push_message, "009");
                                pushDialog.show();
                            }
                        } catch (Exception e) {

                        }
                    }

                }
            }
        }
    };

    private BroadcastReceiver SharedMessage = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            Bundle extras = intent.getExtras();
            if (extras != null) {
                if (extras.containsKey("push flag")) {
                    push_flag = extras.getString("push flag");
                    push_message = extras.getString("push message");
                }
            }

            if (!TrackRide.this.isFinishing()) {
                try {
                    PushDialog pDialog = new PushDialog(TrackRide.this, push_message, push_flag);
                    pDialog.show();
                    MyApplication.dialogShown();
                } catch (Exception e) {

                }
            }

        }
    };
    private BroadcastReceiver FirebaseWorker = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {

            String request = in.getStringExtra("data");
            try {

                if (request != null) {
                    Parser.customerRideStatus(request, 2);
                    mAppManager.saveRide_ID(Parser.getRide_id());
                    mAppManager.saveRequest_ID(Parser.getRequest_id());

                    if (Parser.getBool_ride_status_code() > CarStatus) {
                        CarStatus = Parser.getBool_ride_status_code();
                        chnageCarStatus(CarStatus, 2);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    };
    private BroadcastReceiver ConnectionStateOnline = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {

            if (!mSocket.connected()) {
                Log.d("NodeConnection", "TrackRide-ConnectionStateOnline");
                Intent start_Serv = new Intent(getApplicationContext(), ConnectorService.class);
                startService(start_Serv);
            }
        }
    };
    private BroadcastReceiver Node_CUSTOMER_TRACK = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {

            String request = in.getStringExtra("Object");

            Log.d("Node_CUSTOMER_TRACK", "" + request);

            hideProgress();

            try {

                parseresponce = Parser.customerRideStatus(request, 1);

                if (parseresponce) {

                    mAppManager.saveRide_ID(Parser.getRide_id());
                    mAppManager.saveRequest_ID(Parser.getRequest_id());

                    if (Parser.getBool_ride_status_code() > CarStatus) {
                        CarStatus = Parser.getBool_ride_status_code();
                        chnageCarStatus(CarStatus, 2);
                    }

                } else {
                    if (!isCancelled) {
                        isCancelled = true;
                        PushDialog pushDialog = new PushDialog(TrackRide.this, Parser.getRide_status_message(), "009");
                        pushDialog.show();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.track_ride);
        ButterKnife.bind(this);
        new TrackridePresenter(Injection.remoteDataRepository(getApplicationContext()), this);
        polylines = new ArrayList<>();
        MyApplication.activity = TrackRide.this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar6);

        dialog5 = new Dialog(TrackRide.this, R.style.Theme_Dialog);
        dialog5.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog5.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog5.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog5.setContentView(R.layout.dialog_onebutton);
        dialog5.setCancelable(false);
        dialog5.setCanceledOnTouchOutside(false);

        if (toolbar != null) {

            int color = Color.parseColor("#ffffff");
            toolbar.setTitleTextColor(color);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            headertext = (TextView) findViewById(android.R.id.text1);
            headertext.setText("En Route".toUpperCase());

        }

        /**
         * Important to catch the exceptions
         */

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        /*****************************************************************************************/


        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        wl.acquire();
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        font_path = "fonts/opensans.ttf";

        tf = Typeface.createFromAsset(this.getAssets(), font_path);

        headertext.setTypeface(tf, Typeface.BOLD);

        initialize_Objects();

        setupView();

    }

    /**
     * Split the fare
     */

    @OnClick(R.id.split)
    public void splitFare() {
        Intent toshare = new Intent(this, ShareTheFare.class);
        startActivity(toshare);
    }

    @OnClick(R.id.call)
    public void callDriver() {

        if (driver_number != null) {
            try {
                if (checkPermission()) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + driver_number));
                    startActivity(callIntent);
                } else {
                    requestPermission();
                }
            } catch (Exception e) {

            }

        }
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(TrackRide.this, new String[]
                {
                        Manifest.permission.CALL_PHONE,
                        Manifest.permission.READ_PHONE_STATE,
                }, CallRequestPermissionCode);

    }

    public boolean checkPermission() {

        int readPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_PHONE_STATE);
        int writePermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE);

        return readPermissionResult == PackageManager.PERMISSION_GRANTED &&
                writePermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == CallRequestPermissionCode) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                // required permissions granted, make call
                if (driver_number != null) {
                    try {
                        Uri number = Uri.parse(driver_number);
                        Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
                        startActivity(callIntent);

                    } catch (Exception e) {
                    }

                }
            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void initialize_Objects() {

        polylines = new ArrayList<>();

        mAppManager.saveCurrentActivity("TrackRide");

        cd = new ConnectionDetector(TrackRide.this);
        UniqueId = mAppManager.getUniqueId();
        context = this;
        MyApplication.context = context;
        MyApplication.activity = TrackRide.this;

        s_msg = "Your journey has started now.";
        pWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        pWheel.setVisibility(View.INVISIBLE);
        layout_bg = (RelativeLayout) findViewById(R.id.lyt_bg);

        driver_nametxt = (TextView) findViewById(R.id.driver_name);
        driver_cartypetxt = (TextView) findViewById(R.id.carname);
        driver_ratingtxt = (TextView) findViewById(R.id.rating);
        driver_ratingStartTxt = (TextView) findViewById(R.id.rating_start);
        car_nametxt = (TextView) findViewById(R.id.car_name);
        car_numbertxt = (TextView) findViewById(R.id.car_number);
        car_surgetxt = (TextView) findViewById(R.id.surge_amount);
        cancelride = (TextView) findViewById(R.id.cancelride);
        dropup = (TextView) findViewById(R.id.dropup);
        dropups = (TextView) findViewById(R.id.dropups);
        mapContainer = (RelativeLayout) findViewById(R.id.mapContainer);
        pickedup = (TextView) findViewById(R.id.pickedup);
        call = (TextView) findViewById(R.id.call);
        switcher = (ViewSwitcher) findViewById(R.id.layswitch);
        pickview = (RelativeLayout) findViewById(R.id.pickview);
        surge_container = (LinearLayout) findViewById(R.id.surge_container);
        destination_drop = (ImageView) findViewById(R.id.imageView14);
        surge_start_lay = (LinearLayout) findViewById(R.id.surge_start_lay);


        driver_nametxt_start = (TextView) findViewById(R.id.driver_name_start);
        driver_cartypetxt_start = (TextView) findViewById(R.id.carname_start);
        car_name_start = (TextView) findViewById(R.id.car_name_start);
        car_number_start = (TextView) findViewById(R.id.car_number_start);
        surge_start = (TextView) findViewById(R.id.surge_start);


        view_driverinfo = (LinearLayout) findViewById(R.id.start_lays);
        start_before = (LinearLayout) findViewById(R.id.start_before);
        driver_imageImg = (ImageView) findViewById(R.id.driver_image);
        car_imageImg = (ImageView) findViewById(R.id.car_image);
        driver_image1 = (ImageView) findViewById(R.id.driver_image1);

        myNManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        iDialog = new InvalidTokenDialog(TrackRide.this);

        mMapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);

        checkGooglePlayServices();

        /**
         * Getting User, Driver & Destination latitude Longitude
         */

        if (mAppManager.App_getDriver_ride_status() != "") {
            ride_status = Integer.parseInt(mAppManager.App_getDriver_ride_status());
        }

        if (mAppManager.App_getDriver_car_type() != null) {
            carImage = MyApplication.carImages.get(mAppManager.App_getDriver_car_type());

            if (carImage == null) {
                boolean status = downloadCarImage(mAppManager.App_getDriver_car_type(), mAppManager.App_getDriver_map_car_image());
                if (status) {
                    carImage = MyApplication.carImages.get(mAppManager.App_getDriver_car_type());
                }
            }

        }

        if (ride_status != 7) {

            if (mAppManager.App_getDriver_customer_pick_up_point_lat() != "") {

                user_latitude = Double.parseDouble(mAppManager.App_getDriver_customer_pick_up_point_lat());
                user_longitude = Double.parseDouble(mAppManager.App_getDriver_customer_pick_up_point_long());
                user_destn_lat = Double.parseDouble(mAppManager.App_getDriver_customer_destination_point_lat());
                user_destn_lon = Double.parseDouble(mAppManager.App_getDriver_customer_destination_point_long());
                driver_latitude = Double.parseDouble(mAppManager.App_getDriver_driver_latitude());
                driver_longitude = Double.parseDouble(mAppManager.App_getDriver_driver_longitude());

                driver_image = (mAppManager.App_getDriver_driver_profile_pic() != null) ? mAppManager.App_getDriver_driver_profile_pic() : "http://www.iconsdb.com/icons/download/gray/user-512.png";
                driver_name = (mAppManager.App_getDriver_driver_name() != null) ? mAppManager.App_getDriver_driver_name() : "";
                driver_cartype = (mAppManager.App_getDriver_vehicle_type() != null) ? mAppManager.App_getDriver_vehicle_type() : "";
                driver_rating = (mAppManager.App_getDriver_rating() != null) ? mAppManager.App_getDriver_rating() : "";
                driver_number = (mAppManager.App_getDriver_driver_phone() != null) ? mAppManager.App_getDriver_driver_phone() : "";
                car_image = (mAppManager.App_getDriver_vehicle_image() != null) ? mAppManager.App_getDriver_vehicle_image() : "";
                car_name = (mAppManager.App_getDriver_vehicle_model() != null) ? mAppManager.App_getDriver_vehicle_model() : "";
                car_number = (mAppManager.App_getDriver_vehicle_number() != null) ? mAppManager.App_getDriver_vehicle_number() : "";
                car_surge = (mAppManager.App_getDriver_fare_multiplier() != null) ? mAppManager.App_getDriver_fare_multiplier() : "";
                dest_name = (mAppManager.App_getDriver_customer_destination_point_name() != null) ? mAppManager.App_getDriver_customer_destination_point_name() : "";
                picup_name = (mAppManager.App_getDriver_customer_pick_up_point_name() != null) ? mAppManager.App_getDriver_customer_pick_up_point_name() : "";

                if (driver_image.contains(".png") || driver_image.contains(".jpeg") || driver_image.contains(".jpg") || driver_image.contains(".JPEG")) {

                } else {
                    driver_image = "http://www.iconsdb.com/icons/download/gray/user-512.png";
                }

                if (driver_number.equalsIgnoreCase("")) {
                    if (mAppManager.App_getDriver_mobile_no() != null && !mAppManager.App_getDriver_mobile_no().isEmpty()) {
                        driver_number = mAppManager.App_getDriver_mobile_no();
                    }
                }


                Glide.with(TrackRide.this)
                        .load(driver_image) // add your image url
                        .transform(new CircleTransform(TrackRide.this)) // applying the image transformer
                        .into(driver_imageImg);

                Glide.with(TrackRide.this)
                        .load(car_image) // add your image url
                        .placeholder(R.drawable.defaultcar)
                        .transform(new CircleTransform(TrackRide.this)) // applying the image transformer
                        .into(car_imageImg);

                Glide.with(TrackRide.this)
                        .load(driver_image) // add your image url
                        .transform(new CircleTransform(TrackRide.this)) // applying the image transformer
                        .into(driver_image1);

                driver_nametxt.setText(driver_name);
                driver_cartypetxt.setText(driver_cartype);
                driver_ratingtxt.setText(driver_rating + "");
                driver_ratingStartTxt.setText(driver_rating + "");
                car_nametxt.setText(car_name);
                car_numbertxt.setText(car_number);

                if (car_surge != null && Double.parseDouble(car_surge) > 1.0) {
                    surge_container.setVisibility(View.VISIBLE);
                    car_surgetxt.setText(car_surge);
                    surge_start_lay.setVisibility(View.VISIBLE);
                    surge_start.setText(car_surge + " x");
                } else {
                    surge_start_lay.setVisibility(View.GONE);
                    surge_container.setVisibility(View.GONE);
                }

                driver_nametxt_start.setText(driver_name);
                driver_cartypetxt_start.setText(driver_cartype);
                car_name_start.setText(car_name);
                car_number_start.setText(car_number);


                pickedup.setText(picup_name);
                dropup.setText(dest_name);
                dropups.setText(dest_name);

                /**
                 * Setting up routes between locations
                 */
                customerstart = new LatLng(user_latitude, user_longitude);
                driverstart = new LatLng(driver_latitude, driver_longitude);
                destinationend = new LatLng(user_destn_lat, user_destn_lon);
            } else {

            }

        }
    }

    /**
     * Node Connection Setup With Implemented methods
     */


    private void cancelRide() {

        String request_id = mAppManager.App_getDriver_request_id();
        String user_type = mAppManager.getuser();
        JSONObject obj = new JSONObject();

        try {

            obj.put("request_id", request_id);
            obj.put("user_type", user_type);
            obj.put("auth_token", mAppManager.getTokenCode());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (mSocket.connected()) {
            mSocket.emit("cancel_ride", obj);
        } else {

        }
    }

    /**
     * Dialog For Node Track Ride
     */


    private void DialogServer(final int f, String message) {

        if (dialog5.isShowing()) {
            dialog5.dismiss();
        }

        TextView mTitle = (TextView) dialog5.findViewById(R.id.text_title);
        mTitle.setTypeface(tf, Typeface.BOLD);
        TextView mDialogBody = (TextView) dialog5.findViewById(R.id.txt_dialog_body);
        TextView mOK = (TextView) dialog5.findViewById(R.id.txt_dialog_ok);
        mDialogBody.setText(message);
        if (f == 2) {
            mTitle.setVisibility(View.VISIBLE);
            mTitle.setText("REQUEST ACCEPTED");

        } else if (f == 5) {
            mTitle.setVisibility(View.VISIBLE);
            mTitle.setText("DRIVER ARRIVED");


        } else if (f == 6) {
            mTitle.setVisibility(View.VISIBLE);
            mTitle.setText("JOURNEY STARTED");

            //	mDialogBody.setText("Your journey is started now.");

        } else {
            mTitle.setVisibility(View.GONE);
            mTitle.setText("ALERT");

        }

        //mDialogBody.setText("Our driver is on the way.Please wait");
        mOK.setText("OK");
        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                myNManager.cancelAll();

                dialog5.dismiss();
                if (f == -1) {

                    startActivity(new Intent(TrackRide.this, CustomerHomeActivity.class));
                    finish();
                }
            }


        });
        dialog5.show();
    }

    /**
     * ********************************** Node End ****************************************************** //
     */


    private void setupView() {

        setMap();
        cancelride.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dismissWheel();

                if (cd.isConnectingToInternet()) {

                    customerCancellationCharge();
                } else {
                    Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_SHORT).show();
                    System.exit(0);
                }

            }
        });

    }


    private void DialogAlert(String message) {

        dialog = new Dialog(TrackRide.this, R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_twobutton);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView mTitle = (TextView) dialog.findViewById(R.id.text_title);
        mTitle.setTypeface(tf, Typeface.BOLD);
        TextView mDialogBody = (TextView) dialog
                .findViewById(R.id.txt_dialog_body);
        TextView mOK = (TextView) dialog.findViewById(R.id.txt_dialog_ok);
        TextView mCancel = (TextView) dialog
                .findViewById(R.id.txt_dialog_cancel);

        mTitle.setText("WARNING");

        mDialogBody.setText(Html.fromHtml(message));
        mOK.setText("YES");
        mCancel.setText("NO");

        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                dialog.dismiss();

                if (cd.isConnectingToInternet()) {


                    customerCancelRide();

                } else {
                    Toast.makeText(getApplicationContext(), "No internet connection available, please try again later.", Toast.LENGTH_SHORT).show();
                    System.exit(0);
                }
            }
        });

        mCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });

    }

    private void setMap() {
        int status_gservice = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

        // Showing status
        if (status_gservice != ConnectionResult.SUCCESS) { // Google Play Services are not available

            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();

        } else
            onMapReady(googleMap);

    }

    @Override
    protected void onResume() {
        super.onResume();
        activityinpause = false;
        if (myNManager != null) {
            myNManager.cancelAll();
        }

        MyApplication.activityResumed();

        if (!mSocket.connected()) {
            Log.d("NodeConnection", "TrackRide-onResume");
            Intent start_Serv = new Intent(getApplicationContext(), ConnectorService.class);
            startService(start_Serv);
        }

    }

    /**
     * function to load map If map is not created it will create it for you
     */

    private void initializeMap(final GoogleMap gMap) {
        this.googleMap = gMap;
        try {

            // Showing / hiding your current location
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            if (googleMap != null) {
                try {
                    // Customise the styling of the base map using a JSON object defined
                    // in a raw resource file.
                    boolean success = googleMap.setMapStyle(
                            MapStyleOptions.loadRawResourceStyle(
                                    this, R.raw.mapstyle));

                    if (!success) {
                        Log.e("onMapReady", "Style parsing failed.");
                    }
                } catch (Resources.NotFoundException e) {
                    Log.e("onMapReady", "Can't find style. Error: ", e);
                }
                //googleMap.setPadding(20,dpToPx(240),20,20);
                // Changing map type
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);


                googleMap.setMyLocationEnabled(false);
                googleMap.setOnCameraIdleListener(this);
                // Enable / Disable zooming controls
                googleMap.getUiSettings().setZoomControlsEnabled(false);

                // Enable / Disable my location button
                googleMap.getUiSettings().setMyLocationButtonEnabled(false);

                // Enable / Disable Compass icon
                googleMap.getUiSettings().setCompassEnabled(false);

                // Enable / Disable Rotate gesture
                googleMap.getUiSettings().setRotateGesturesEnabled(true);

                // Enable / Disable zooming functionality
                googleMap.getUiSettings().setZoomGesturesEnabled(true);

                googleMap.clear();

                if (d == 0) {
                    marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(user_latitude, user_longitude)).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pointer_yellow)));
                }
                marker_d = googleMap.addMarker(new MarkerOptions().position(new LatLng(user_destn_lat, user_destn_lon)).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pointer)));

                marker1 = googleMap.addMarker(new MarkerOptions().position(new LatLng(driver_latitude, driver_longitude)).icon(BitmapDescriptorFactory.fromBitmap(createcar(carImage))));

                googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                    @Override
                    public View getInfoWindow(Marker arg0) {
                        return null;
                    }

                    @Override
                    public View getInfoContents(Marker marker) {
                        // Getting view from the layout file info_window_layout

                        if (CarStatus == 2) {
                            if (!marker.equals(TrackRide.this.marker))
                                return null;
                            View v = getLayoutInflater().inflate(R.layout.window_layout, null);

                            TextView snippet = (TextView) v.findViewById(R.id.tv_lat);

                            snippet.setText(marker.getSnippet());

                            return v;
                        } else {

                            if (!marker.equals(marker_d))
                                return null;
                            View v = getLayoutInflater().inflate(R.layout.window_layout, null);

                            TextView snippet = (TextView) v.findViewById(R.id.tv_lat);

                            snippet.setText(marker.getSnippet());

                            return v;
                        }
                    }
                });
                ArrayList<LatLng> allBounds = new ArrayList<>();

                if (d == 0) {

                    allBounds.add(marker.getPosition());
                    allBounds.add(marker1.getPosition());

                } else {

                    allBounds.add(marker.getPosition());
                    allBounds.add(marker_d.getPosition());
                }

                if (googleMap == null || allBounds == null || allBounds.isEmpty()) return;

                LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
                for (LatLng latLngPoint : allBounds)
                    boundsBuilder.include(latLngPoint);

                int routePadding = 100;

                LatLngBounds latLngBounds = boundsBuilder.build();
                try {
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, routePadding));
                } catch (Exception e) {

                }

            }

            /**
             *  Change Ride Status According to Confirm Ride Response OR Splash Response
             */

            if (ride_status > CarStatus) {
                CarStatus = ride_status;
                chnageCarStatus(CarStatus, 1);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        if (googleMap != null) {
            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

                @Override
                public void onMapClick(LatLng arg0) {
                    if (marker_d != null)
                        marker_d.showInfoWindow();
                    // TODO Auto-generated method stub
                    Log.d("arg0", arg0.latitude + "-" + arg0.longitude);
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBtn_FindDriver:

                break;

            default:
                break;
        }

    }

    @Override
    public void onMapReady(GoogleMap arg0) {
        // TODO Auto-generated method stub
        initializeMap(arg0);
    }

    @Override
    protected void onPause() {
        super.onPause();
        activityinpause = true;
        MyApplication.activityPaused();
    }

    @Override
    protected void onStop() {

        mAppManager.savePrevActivity("TRACK", "");

        if (customerCancellationChargerequestQueue != null) {
            customerCancellationChargerequestQueue.cancelAll(this);
        }
        if (customerCancelRiderequestQueue != null) {
            customerCancelRiderequestQueue.cancelAll(this);
        }
        if (userCheckIsRideEndrequestQueue != null) {
            userCheckIsRideEndrequestQueue.cancelAll(this);
        }

        if (routing != null) {
            routing.cancel(true);
        }
        if (cdialog != null) {
            cdialog.dismiss();
        }
        if (dialog5 != null) {
            dialog5.dismiss();
        }
        if (dialog != null) {
            dialog.dismiss();
        }
        unregisterReceiver(Node_CUSTOMER_TRACK);
        unregisterReceiver(Node_CUSTOMER_Location_Update);
        unregisterReceiver(ConnectionStateOnline);
        unregisterReceiver(dropupChanged);
        unregisterReceiver(FirebaseWorker);
        unregisterReceiver(SharedMessage);
        unregisterReceiver(RideCancelled);

        super.onStop();
    }

    private void checkGooglePlayServices() {

        status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        // Check Google Play Service Available
        try {
            if (status != ConnectionResult.SUCCESS) {
                GooglePlayServicesUtil.getErrorDialog(status, this, RQS_GooglePlayServices).show();
            }
        } catch (Exception e) {
            Log.e("Error:GooglePlay:", e.toString());

        }
    }

    private void cDialog() {

        cdialog = new Dialog(TrackRide.this, R.style.Theme_Dialog);
        cdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = cdialog.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        cdialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        cdialog.setContentView(R.layout.dialog_onebutton);
        cdialog.setCancelable(false);
        cdialog.setCanceledOnTouchOutside(false);
        cdialog.show();

        TextView mTitle = (TextView) cdialog.findViewById(R.id.text_title);
        mTitle.setTypeface(tf, Typeface.BOLD);
        TextView mDialogBody = (TextView) cdialog
                .findViewById(R.id.txt_dialog_body);
        TextView mOK = (TextView) cdialog.findViewById(R.id.txt_dialog_ok);
        mTitle.setText("CANCELLED");
        mDialogBody.setText("You have cancelled the ride.");

        mOK.setText("OK");
        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (myNManager != null)
                    myNManager.cancelAll();
                cdialog.dismiss();

                startActivity(new Intent(TrackRide.this, CustomerHomeActivity.class));
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finish();
            ActivityCompat.finishAffinity(this);
            System.exit(0);
            return;
        }

        this.doubleBackToExitPressedOnce = true;

        Toast.makeText(this, "Please click BACK again to exit HoodRun", Toast.LENGTH_SHORT).show();

    }

    private void showWheel() {
        // TODO Auto-generated method stub
        pWheel.setVisibility(View.VISIBLE);


    }

    private void dismissWheel() {
        // TODO Auto-generated method stub
        pWheel.setVisibility(View.INVISIBLE);
    }

    RequestQueue customerCancellationChargerequestQueue;

    private void customerCancellationCharge() {
        if (myNManager != null)


            presenter.cusCancelCharge(mAppManager.getTokenCode(), mAppManager.App_getDriver_ride_id(), this);


    }

    RequestQueue customerCancelRiderequestQueue;

    private void customerCancelRide() {
        if (myNManager != null)
            myNManager.cancelAll();

        presenter.customerCancelRide(cancel_stat, this);

    }

    RequestQueue userCheckIsRideEndrequestQueue;

    private void userCheckIsRideEnd() {


        presenter.checkUserEndRide(mAppManager.getTokenCode(), mAppManager.getRide_ID(), this);


    }

    private void retryDialog(final int i) {

        retry = new Dialog(TrackRide.this, R.style.Theme_Dialog);
        retry.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = retry.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        retry.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        retry.setContentView(R.layout.thaks_dialog_twobutton);
        retry.setCancelable(false);
        retry.setCanceledOnTouchOutside(false);
        retry.show();

        TextView mTitle = (TextView) retry.findViewById(R.id.txt_title);
        mTitle.setText("Error!");
        mTitle.setTypeface(tf, Typeface.BOLD);
        TextView mDialogBody = (TextView) retry.findViewById(R.id.txt_dialog_body);
        mDialogBody.setText("Internet connection is slow or disconnected. Try connecting again.");
        LinearLayout mOK = (LinearLayout) retry.findViewById(R.id.lin_Ok);
        LinearLayout mCancel = (LinearLayout) retry.findViewById(R.id.lin_cancel);
        TextView mRetry = (TextView) retry.findViewById(R.id.txt_dialog_ok);
        TextView mExit = (TextView) retry.findViewById(R.id.txt_dialog_cancel);
        mRetry.setText("RETRY");
        mExit.setText("EXIT");

        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


                retry.dismiss();

                switch (i) {
                    case 1:

                        break;
                    case 2:

                        break;
                    case 3:
                        if (cd.isConnectingToInternet()) {

                            customerCancelRide();

                        } else {
                            Toast.makeText(getApplicationContext(), "No internet connection available, please try again later.", Toast.LENGTH_SHORT).show();
                            System.exit(0);
                        }
                        break;
                    case 4:

                        break;
                    default:
                        break;

                }

            }
        });

        mCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                retry.dismiss();

                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });

    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(this.Node_CUSTOMER_TRACK, new IntentFilter(NodeUtil.NODE_CUSTOMER_TRACKRIDE));
        registerReceiver(this.Node_CUSTOMER_Location_Update, new IntentFilter(NodeUtil.NODE_CUSTOMER_LOCATION_UPDATE));
        registerReceiver(this.ConnectionStateOnline, new IntentFilter(NodeUtil.CONNECTIVITY_STATE_ONLINE));
        registerReceiver(this.dropupChanged, new IntentFilter(RECEIVER_DROP_CHNAGED));
        registerReceiver(this.FirebaseWorker, new IntentFilter(NodeUtil.NODE_CUSTOMER_FIREBASE_UPDATE));
        registerReceiver(this.SharedMessage, new IntentFilter(NodeUtil.NODE_CUSTOMER_SHARED));
        registerReceiver(this.RideCancelled, new IntentFilter(RECEIVER_RIDE_CANCELLED));

        if (!mSocket.connected()) {
            Log.d("NodeConnection", "TrackRide-ConnectionStateOnline");
            Intent start_Serv = new Intent(getApplicationContext(), ConnectorService.class);
            startService(start_Serv);
        }

    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public void animateMarker(final Marker m, final LatLng toPosition, final boolean hideMarke) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = googleMap.getProjection();
        Point startPoint = proj.toScreenLocation(m.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 1500;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                m.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarke) {
                        m.setVisible(false);
                    } else {
                        m.setVisible(true);
                    }
                }
            }
        });
    }

    private Location convertLatLngToLocation(LatLng latLng) {
        Location loc = new Location("someLoc");
        loc.setLatitude(latLng.latitude);
        loc.setLongitude(latLng.longitude);
        return loc;
    }

    private float bearingBetweenLatLngs(LatLng begin, LatLng end) {
        Location beginL = convertLatLngToLocation(begin);
        Location endL = convertLatLngToLocation(end);

        return beginL.bearingTo(endL);
    }

    public void route(LatLng start, LatLng end, int i) {

        currentway = i;

        if (start != null && end != null) {
            this.start = start;
            this.end = end;

            routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(this)
                    .alternativeRoutes(false)
                    .key(getString(R.string.google_direction_api_key))
                    .waypoints(start, end)
                    .build();

            routing.execute();

        }

    }

    @Override
    public void onRoutingFailure(RouteException e) {
        Log.d("Routing", "" + e.toString());
    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {

        if (polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }

        if (route.get(0).getPoints().size() > 800) {
            //In case of more than 5 alternative routes
            int colorIndex = 0 % COLORS.length;
            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(getResources().getColor(COLORS[colorIndex]));
            polyOptions.width(10 + 0 * 3);
            polyOptions.addAll(route.get(0).getPoints());
            Polyline polyline = googleMap.addPolyline(polyOptions);
            polylines.add(polyline);

        } else {

            if (route.size() > 0) {
                final float red = 205.0f;
                final float green = 1.0f;
                final float yellow = 2.0f;
                float redSteps = (red / route.get(0).getPoints().size());
                float greenSteps = (green / route.get(0).getPoints().size());
                float yellowSteps = (yellow / route.get(0).getPoints().size());
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(route.get(0).getPoints().get(0));
                for (int i = 1; i < route.get(0).getPoints().size(); i++) {
                    builder.include(route.get(0).getPoints().get(i));
                    int redColor = (int) (red - (redSteps * i));
                    int greenColor = (int) (green - (greenSteps * i));
                    int yellowColor = (int) (yellow - (yellowSteps * i));
                    Log.e("Color", "" + redColor);
                    int color = Color.rgb(redColor, greenColor, yellowColor);

                    PolylineOptions options = new PolylineOptions().width(8).color(color).geodesic(true);
                    options.add(route.get(0).getPoints().get(i - 1));
                    options.add(route.get(0).getPoints().get(i));
                    Polyline line = googleMap.addPolyline(options);
                    polylines.add(line);
                    line.setEndCap(new RoundCap());
                }
            /*LatLngBounds bounds = builder.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 0);
			mMap.animateCamera(cu);*/


            }
        }

        zoomRoute(googleMap, route.get(0).getPoints(), route.get(0).getDurationValue() / 60);
    }

    public void zoomRoute(GoogleMap googleMap, List<LatLng> lstLatLngRoute, int time) {
        ArrayList<LatLng> allBounds = new ArrayList<>();
        if (ride_status > 2) {

            allBounds.add(driverstart);
            allBounds.addAll(lstLatLngRoute);
            allBounds.add(marker_d.getPosition());

        } else {

            allBounds.add(marker.getPosition());
            allBounds.addAll(lstLatLngRoute);
            allBounds.add(driverstart);
        }


        if (googleMap == null || allBounds == null || allBounds.isEmpty()) return;

        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
        for (LatLng latLngPoint : allBounds)
            boundsBuilder.include(latLngPoint);
        LatLngBounds latLngBounds = boundsBuilder.build();
        int px = Util.pxToDp(getApplicationContext(), 20);
        int top = (int) (switcher.getHeight() + switcher.getY()) + px * 2;
        googleMap.setPadding(px, top, px, px * 2);
        try {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 0));
        } catch (Exception e) {

        }

        timeFromDriver = time;
        try {

            if (currentway == 1) {
                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.map_pointer_yellow));
                marker.setSnippet("" + timeFromDriver + " min");


            } else {
                //	marker_d.setIcon(BitmapDescriptorFactory.fromBitmap(createbitmap("" + timeFromDriver + " min",2)));
                marker_d.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.map_pointer));
                marker_d.setSnippet("" + timeFromDriver + " min");
                marker.setVisible(false);
            }
        } catch (Exception e) {

        }

    }

    @Override
    public void onRoutingCancelled() {

    }

    @Override
    public void onCameraIdle() {
        try {

            if (currentway == 1) {
                if (marker != null && marker.getSnippet() != null && marker.getSnippet().length() > 0) {

                    marker.showInfoWindow();
                }

            } else {
                //	marker_d.setIcon(BitmapDescriptorFactory.fromBitmap(createbitmap("" + timeFromDriver + " min",2)));
                if (marker_d.getSnippet() != null && marker_d.getSnippet().length() > 0) {
                    marker_d.showInfoWindow();
                }
                if (marker != null) {
                    marker.setVisible(false);
                }
            }
        } catch (Exception e) {

        }
    }

    private Bitmap createcar(Bitmap car) {

        View view = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.car, null);
        ImageView carImage = (ImageView) view.findViewById(R.id.carimg);

        if (car != null) {
            carImage.setImageBitmap(car);
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }


    private boolean downloadCarImage(final String car_id, final String car_url) {

        if (!MyApplication.carImages.containsKey(car_id)) {


            try {

                Thread thread = new Thread() {
                    @Override
                    public void run() {

                        Bitmap carImg = getBitmapFromURL(car_url);
                        MyApplication.carImages.put(car_id, carImg);
                    }
                };

                thread.start();
                thread.join(5000);

            } catch (Exception e) {

            }

        }
        return true;
    }

    /**
     * Notification Receiver
     **/

    private void chnageCarStatus(int status, int way) {

        if (status == 2) {
            ride_status = 2;

            if (activityinpause) {
                //	sendNotifications(Parser.getRide_status_message());
            }
            if (routing != null) {
                routing.cancel(true);
            }
            route(customerstart, driverstart, 1);

        } else if (status == 5) {
            ride_status = 5;
            headertext.setText("Driver Arrived".toUpperCase());
            cancel_stat = 1;

            if (activityinpause) {
                //	sendNotifications(Parser.getRide_status_message());
            } else {
                MyApplication.startPlayer(R.raw.wolf);
            }
            if (routing != null) {
                routing.cancel(true);
            }

            ArrayList<LatLng> allBounds = new ArrayList<>();
            allBounds.add(driverstart);
            allBounds.add(destinationend);
            if (googleMap != null && allBounds == null) {

                LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
                for (LatLng latLngPoint : allBounds)
                    boundsBuilder.include(latLngPoint);
                LatLngBounds latLngBounds = boundsBuilder.build();
                int px = Util.pxToDp(getApplicationContext(), 20);
                int top = (int) (switcher.getHeight() + switcher.getY()) + px * 2;
                googleMap.setPadding(px, top, px, px * 2);
                try {
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 0));
                } catch (Exception e) {

                }
            }

            route(driverstart, destinationend, 2);

        } else if (status == 6) {
            ride_status = 6;
            pickview.setVisibility(View.GONE);
            switcher.setDisplayedChild(1);
            headertext.setText("En Route".toUpperCase());
            view_driverinfo.setVisibility(View.VISIBLE);
            start_before.setVisibility(View.GONE);
            if (activityinpause) {
                //	sendNotifications(Parser.getRide_status_message());
            } else {
                MyApplication.startPlayer(R.raw.wolf);
            }
            if (way == 1) {
                if (routing != null) {
                    routing.cancel(true);
                }
                ArrayList<LatLng> allBounds = new ArrayList<>();
                allBounds.add(driverstart);
                allBounds.add(destinationend);
                if (googleMap != null && allBounds == null) {

                    LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
                    for (LatLng latLngPoint : allBounds)
                        boundsBuilder.include(latLngPoint);
                    LatLngBounds latLngBounds = boundsBuilder.build();
                    int px = Util.pxToDp(getApplicationContext(), 20);
                    int top = (int) (switcher.getHeight() + switcher.getY()) + px * 2;
                    googleMap.setPadding(px, top, px, px * 2);
                    try {
                        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 0));
                    } catch (Exception e) {

                    }
                }
                route(driverstart, destinationend, 2);
            }

        } else if (status == 7) {
            MyApplication.startPlayer(R.raw.wolf);

            userCheckIsRideEnd();
        } else {
            if (dialog5 != null) {
                dialog5.dismiss();
                dialog5 = null;
            }
            DialogServer(-1, "Error!!! Please Try Again.");
        }

    }


    @Override
    public void customerCancellationChargeResponse(String response) {

        hideProgress();

        System.out.println("CANCELCHARGE===" + response);
        try {
            JSONObject dataObject = new JSONObject(response);

            parsresp = dataObject.optString("status");
            message = dataObject.optString("message");

        } catch (Exception e) {

            Log.e("Buffer Error", "Error converting result " + e.toString());

        }

        if (!TextUtils.isEmpty(parsresp)) {
            switch (parsresp) {
                case "TRUE":

                    if (dialog != null) {
                        dialog.dismiss();
                        dialog = null;
                    }
                    DialogAlert(message);

                    break;
                case "FALSE":
                    PushDialog pushDialog;

                    switch (message) {
                        case "Invalid token code":
                            iDialog.show();
                            break;
                        case "You can't cancel the ride":
                            pushDialog = new PushDialog(TrackRide.this, message, "cancel");
                            pushDialog.show();
                            break;
                        default:
                            pushDialog = new PushDialog(TrackRide.this, message, "009");
                            pushDialog.show();
                            break;
                    }
                    break;
            }


        } else {

            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
            System.exit(0);
        }


    }

    @Override
    public void customerCancelRideResponse(String response) {
        hideProgress();
        System.out.println("CANCELRIDE===" + response);
        try {
            JSONObject dataObject = new JSONObject(response);

            parsresp = dataObject.optString("status");
            message = dataObject.optString("message");

        } catch (Exception e) {

            Log.e("Buffer Error", "Error converting result " + e.toString());

        }

        if (!TextUtils.isEmpty(parsresp)) {
            switch (parsresp) {
                case "TRUE":

                    cancelRide();
                    if (cdialog != null) {
                        cdialog.dismiss();
                        cdialog = null;
                    }
                    cDialog();

                    break;
                case "FALSE":
                    PushDialog pushDialog;

                    switch (message) {
                        case "Invalid token code":
                            iDialog.show();
                            break;
                        case "You can't cancel the ride":
                            pushDialog = new PushDialog(TrackRide.this, message, "cancel");
                            pushDialog.show();
                            break;
                        default:
                            pushDialog = new PushDialog(TrackRide.this, message, "009");
                            pushDialog.show();
                            break;
                    }
                    break;
            }


        } else {

            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
            System.exit(0);
        }

    }

    @Override
    public void userCheckIsRideEndResponse(String response) {
        hideProgress();
        System.out.println("ISRIDEEND===" + response);

        boolean parseresponce = false;
        try {

            parseresponce = Parser.setDriverDataSplash(response);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (parseresponce) {

            mAppManager.App_setDriver_driver_name(Parser.getDriver_driver_name());
            mAppManager.App_setDriver_driver_vehicle(Parser.getDriver_driver_vehicle());
            mAppManager.App_setDriver_driver_profile_pic(Parser.getDriver_driver_profile_pic());

            mAppManager.App_setDriver_vehicle_model(Parser.getDriver_vehicle_models());
            mAppManager.App_setDriver_vehicle_number(Parser.getDriver_vehicle_numbers());
            mAppManager.App_setDriver_vehicle_type(Parser.getDriver_vehicle_type());
            mAppManager.App_setDriver_vehicle_image(Parser.getDriver_vehicle_image());

            mAppManager.App_setDriver_fare_multiplier(Parser.getDriver_fare_multiplier());
            mAppManager.App_setDriver_car_type(Parser.getDriver_car_type());
            mAppManager.App_setDriver_map_car_image(Parser.getDriver_map_car_image());
            mAppManager.App_setDriver_total(Parser.getDriver_total());
            mAppManager.App_setDriver_trip_distance(Parser.getDriver_trip_distance());
            mAppManager.App_setDriver_trip_duration(Parser.getDriver_trip_duration());

            mAppManager.App_setDriver_customer_destination_point_name(Parser.getDriver_customer_destination_point_name());
            mAppManager.App_setDriver_customer_pick_up_point_name(Parser.getDriver_customer_pick_up_point_name());


            mAppManager.App_setDriver_payment_type(Parser.getDriver_payment_type());
            mAppManager.App_setDriver_discount_applied(Parser.getDriver_discount_applied());
            mAppManager.App_setDriver_discount_total(Parser.getDriver_discount_total());
            mAppManager.App_setDriver_promo_code(Parser.getDriver_promo_code());
            mAppManager.App_setDriver_payment_mode(Parser.getDriver_payment_mode());
            mAppManager.App_setDriver_mobile_no(Parser.getDriver_mobile_no());
            mAppManager.App_setDriver_no_of_seats(Parser.getDriver_no_of_seats());
            mAppManager.App_setDriver_list_car_image(Parser.getDriver_list_car_image());


            Intent toReceipt = new Intent(getApplicationContext(), RideReceipt.class);
            startActivity(toReceipt);
            finish();

        }


    }


    @Override
    public void onFail(Throwable t) {
        hideProgress();
        if (retry != null) {
            retry.dismiss();
            retry = null;
        }
        retryDialog(3);

    }

    @Override
    public void onFailRideEnd(Throwable t) {
        hideProgress();
        Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_LONG).show();
        finish();


    }

    @Override
    public void rideRatinResponse(String response) {

    }

    @Override
    public void rideRatingFail(Throwable t) {

    }

    @Override
    public void setPresenter(TrackRideContract.Presenter presenter) {
        this.presenter = presenter;

    }

    @Override
    public void showProgreass() {
        loader = new Dialog(TrackRide.this);
        loader.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loader.setContentView(R.layout.dialog_progress);
        loader_wheel = (ProgressWheel) loader.findViewById(R.id.progress_wheel);
        loader_wheel.setCircleRadius(50);
        loader.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        loader.setCancelable(false);
        loader.show();
    }

    @Override
    public void hideProgress() {
        if (loader != null) {
            loader.dismiss();
        }

    }
}
                
             
    
    
        
       
    


