package com.us.hoodruncustomer.model;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.custom.CircularImageView;

import java.util.ArrayList;

public class HistoryListAdapter extends BaseAdapter {
	Activity mContext;
	ArrayList<History> historyList;
	String user;

	public HistoryListAdapter(Activity ctx, ArrayList<History> historyList, String user) {

		this.mContext = ctx;
		this.historyList = historyList;
		this.user = user;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return historyList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return historyList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		convertView = inflater.inflate(R.layout.history_item, parent, false);
		holder = new ViewHolder();
		holder.date_time = (TextView) convertView.findViewById(R.id.txt_datetime);
		holder.driver_image = (CircularImageView) convertView.findViewById(R.id.img_driver);
		holder.total = (TextView) convertView.findViewById(R.id.txt_total);
		holder.vehicle_model = (TextView) convertView.findViewById(R.id.txt_veh_model);
		holder.status = (TextView) convertView.findViewById(R.id.txt_status);
		holder.surgedisplay = (ImageView) convertView.findViewById(R.id.surgedisplay);
		holder.ride_map = (ImageView) convertView.findViewById(R.id.img_map);

		Glide.with(mContext).load(historyList.get(position).getRide_map())
//		.placeholder(R.drawable.driver_details_background)
				.into(holder.ride_map);

		Glide.with(mContext).load(historyList.get(position).getImage())
//		.placeholder(R.drawable.placeholder)
				.into(holder.driver_image);

		holder.total.setText(mContext.getResources().getString(R.string.pound) + historyList.get(position).getTotal());

		holder.date_time.setText(getDateTime(historyList.get(position).getRide_time()));

		if (historyList.get(position).getSurge_applied().equalsIgnoreCase("false")) {
			holder.surgedisplay.setVisibility(View.GONE);
		} else {
			holder.surgedisplay.setVisibility(View.VISIBLE);
		}


		switch (user) {
			case "customer":

				if (historyList.get(position).getTrip_distance() != null && !historyList.get(position).getTrip_distance().isEmpty())
					holder.vehicle_model.setText(historyList.get(position).getTrip_distance() + " mi");

				if (historyList.get(position).getShare_status().equals("1")) {

					if (historyList.get(position).getDriver_cancel_status().equals("Cancelled") || historyList.get(position).getCustomer_cancel_status().equals("Cancelled")) {

						holder.status.setText("CANCELLED");
						holder.status.setTextColor(Color.parseColor("#f43b26"));
						holder.total.setText(mContext.getResources().getString(R.string.pound)  + historyList.get(position).getTotal());
					} else {

						holder.status.setText("SUCCESS (SHARED)");
						holder.status.setTextColor(Color.parseColor("#318b32"));
						holder.total.setText(mContext.getResources().getString(R.string.pound)  + historyList.get(position).getShare_amt());
					}
				} else {


					if (historyList.get(position).getDriver_cancel_status().equals("Cancelled") || historyList.get(position).getCustomer_cancel_status().equals("Cancelled")) {
						holder.status.setText("CANCELLED");
						holder.status.setTextColor(Color.parseColor("#f43b26"));

					} else {
						holder.status.setText("SUCCESS");
						holder.status.setTextColor(Color.parseColor("#318b32"));
					}

				}

				break;

			case "driver":
				holder.vehicle_model.setText(historyList.get(position).getTrip_distance() + " mi");

				if (historyList.get(position).getDriver_cancel_status().equals("Cancelled") || historyList.get(position).getCustomer_cancel_status().equals("Cancelled")) {
					holder.status.setText("CANCELLED");
					holder.status.setTextColor(Color.parseColor("#f43b26"));

				} else {
					holder.status.setText("SUCCESS");
					holder.status.setTextColor(Color.parseColor("#318b32"));
				}

				break;
			default:
				break;
		}


		return convertView;
	}

	private String getDateTime(String ride_time) {
		// TODO Auto-generated method stub
		ride_time = ride_time.replace(" ", " at ");
		return ride_time;
	}

	public static class ViewHolder {

		TextView date_time, total, vehicle_model, status;
		ImageView ride_map, surgedisplay;
		CircularImageView driver_image;

	}

}
