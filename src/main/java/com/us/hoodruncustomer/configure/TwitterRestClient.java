package com.us.hoodruncustomer.configure;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class TwitterRestClient {
	    private static final String BASE_URL = "https://www.hoodrunnow.com/";

		private static AsyncHttpClient client = new AsyncHttpClient();

		public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
			client.get(getAbsoluteUrl(url), params, responseHandler);
		}

		public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
			client.addHeader("Csrf-Token","0123456789");
			client.addHeader("Accept","application/json");
			client.addHeader("Authorization","Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIyOCwiZXhwIjoxNTE4ODYwNTE4fQ.F4NBS0_kkr24NSIFvubiha_8Qra-DVqzEmY-ZnVClhE");
			client.post(getAbsoluteUrl(url), params, responseHandler);
		}

		private static String getAbsoluteUrl(String relativeUrl) {
			return BASE_URL + relativeUrl;
		}
	}