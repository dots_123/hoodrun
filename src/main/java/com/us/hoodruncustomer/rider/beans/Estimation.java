package com.us.hoodruncustomer.rider.beans;

/**
 * Created by admin on 19-May-17.
 */

public class Estimation {

    private String estimated_fare;
    private String surge_applied;
    private String fare_multiplier;
    private String fare_expiration_time;
    private String total_passengers;

    public String getSurge_applied() {
        return surge_applied;
    }

    public void setSurge_applied(String surge_applied) {
        this.surge_applied = surge_applied;
    }

    public String getFare_multiplier() {
        return fare_multiplier;
    }

    public void setFare_multiplier(String fare_multiplier) {
        this.fare_multiplier = fare_multiplier;
    }

    public String getFare_expiration_time() {
        return fare_expiration_time;
    }

    public void setFare_expiration_time(String fare_expiration_time) {
        this.fare_expiration_time = fare_expiration_time;
    }

    public String getTotal_passengers() {
        return total_passengers;
    }

    public void setTotal_passengers(String total_passengers) {
        this.total_passengers = total_passengers;
    }

    public String getEstimated_fare() {
        return estimated_fare;
    }

    public void setEstimated_fare(String estimated_fare) {
        this.estimated_fare = estimated_fare;
    }
}
