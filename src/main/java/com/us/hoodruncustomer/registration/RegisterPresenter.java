package com.us.hoodruncustomer.registration;

import android.content.Context;

import com.us.hoodruncustomer.data.DataSource;
import com.us.hoodruncustomer.data.remote.RemoteDataSource;

/**
 * Created by admin on 11/14/2017.
 */

public class RegisterPresenter implements RegisterContract.Presenter, DataSource.RegisterResponseI {

    private final DataSource loginDataSource;
    private RegisterContract.View mLoginView;
    private Context context;
    private boolean isSkip;

    public RegisterPresenter(RemoteDataSource registerRemoteDataSource, RegisterContract.View loginFragment) {
        loginDataSource = registerRemoteDataSource;
        mLoginView = loginFragment;
        mLoginView.setPresenter(this);

    }

    @Override
    public void registerResponse(String baseResponse) {
        mLoginView.hideProgress();
        mLoginView.registerSuccessResponse(baseResponse);

    }

    @Override
    public void onFail(Throwable t) {
        mLoginView.hideProgress();
        mLoginView.onLoginFail(t);

    }


    @Override
    public void onResendResponse(String response) {

        mLoginView.hideProgress();
        mLoginView.onSuccessResendOtp(response);

    }

    @Override
    public void onSaveOtpResponse(String response) {
        mLoginView.hideProgress();
        mLoginView.onSaveResponseOtp(response);

    }

    @Override
    public void onFailResend(Throwable t) {
        mLoginView.hideProgress();
        mLoginView.onFailResend(t);

    }

    @Override
    public void register(String fb_id, String google_id, String name, String email, String password, String mobile_number, String profile_picture, String device_id, String os, Context context) {
        mLoginView.showProgreass();
        loginDataSource.registerService(fb_id, google_id, name, email, password, mobile_number, profile_picture, device_id, os, this, context);
    }


    @Override
    public void saveOtp(String customerId, String otp, String email, String mobilenumber, String url, Context context) {
        mLoginView.showProgreass();
        loginDataSource.saveOtp(customerId, otp, email, mobilenumber, url, this, context);

    }

    @Override
    public void resendOtp(String customerId, String tokencode, String email, String mobilenumber, String url, Context context) {
        mLoginView.showProgreass();
        loginDataSource.resendOtp(customerId, tokencode, email, mobilenumber, url, this, context);
    }

    @Override
    public void drop() {
        mLoginView.hideProgress();
        mLoginView = null;
    }
}
