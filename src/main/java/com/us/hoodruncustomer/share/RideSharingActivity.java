package com.us.hoodruncustomer.share;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.commonwork.Injection;
import com.us.hoodruncustomer.commonwork.SimpleActivity;
import com.us.hoodruncustomer.custom.CircularImageView;
import com.us.hoodruncustomer.exceptionhandler.ExceptionHandler;
import com.us.hoodruncustomer.fcm.MyFirebaseMessagingService;
import com.us.hoodruncustomer.fragment.InvalidTokenDialog;
import com.us.hoodruncustomer.home.CustomerHomeActivity;
import com.us.hoodruncustomer.parser.ConnectionDetector;
import com.us.hoodruncustomer.parser.StatusParser;

import org.json.JSONException;
import org.json.JSONObject;

public class RideSharingActivity extends SimpleActivity implements ShareContract.View {
    private ShareContract.Presenter presenter;
    TextView mVehicletype, mPayment, mVehiclemodel, mPick, mDrop, mFare, mCustomerName, mShare;

    CircularImageView circularImageView;
    LinearLayout mReject, mAccept;
    ConnectionDetector cd;
    String parsresp = "", share_status, message;
    String font_path;
    Typeface tf;
    TextView headertext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_sharing);
        new SharePresenter(Injection.remoteDataRepository(getApplicationContext()), this);

        /**
         * Important to catch the exceptions
         */

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        /*****************************************************************************************/

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            int color = Color.parseColor("#ffffff");
            toolbar.setTitleTextColor(color);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            toolbar.setNavigationIcon(R.drawable.back_arrow);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            headertext = (TextView) findViewById(android.R.id.text1);
            headertext.setText("Ride Sharing");

        }

        cd = new ConnectionDetector(RideSharingActivity.this);


        font_path = "fonts/opensans.ttf";

        tf = Typeface.createFromAsset(this.getAssets(), font_path);
        headertext.setTypeface(tf, Typeface.BOLD);
        setupView();
        mAccept.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                aDialogAlert();
            }
        });

        mReject.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                share_status = "2";
                if (cd.isConnectingToInternet()) {

                    shareResponse();
                } else
                    DialogAlert("No Internet !",
                            "Check your connection and try again.");

            }
        });
    }

    private void setupView() {
        // TODO Auto-generated method stub

        //mVehicletype = (TextView) findViewById(R.id.txt_vehicle_type);
        mVehiclemodel = (TextView) findViewById(R.id.txt_labelseat);

        mPayment = (TextView) findViewById(R.id.txt_labelpayment);
        mPick = (TextView) findViewById(R.id.txt_pick);
        mDrop = (TextView) findViewById(R.id.txt_drop);
        mFare = (TextView) findViewById(R.id.txt_total_estimated_fare);
        mCustomerName = (TextView) findViewById(R.id.txt_customer_name);
        circularImageView = (CircularImageView) findViewById(R.id.customer_img);
        mReject = (LinearLayout) findViewById(R.id.lay_reject);
        mAccept = (LinearLayout) findViewById(R.id.lay_accept);
        mShare = (TextView) findViewById(R.id.txt_estimated_fare);

        if (StatusParser.getShared_Shared_ustomerImage() != null
                && !StatusParser.getShared_Shared_ustomerImage().isEmpty()) {

            Glide.with(RideSharingActivity.this)
                    .load(StatusParser.getShared_Shared_ustomerImage())
                    .placeholder(R.drawable.placeholder)
                    .into(circularImageView);
        } else
            circularImageView.setImageResource(R.drawable.placeholder);

        //	mVehicletype.setText(StatusParser.getShared_vehicleType());
        if (StatusParser.getNo_of_seats() != null && !StatusParser.getNo_of_seats().isEmpty())
            mVehiclemodel.setText("1-" + StatusParser.getNo_of_seats());
        else
            mVehiclemodel.setText("1-4");
        mPayment.setText("CARD");
        mPick.setText(StatusParser.getShared_destinationLocationName());
        mDrop.setText(StatusParser.getShared_pickingLocationName());
        mFare.setText(getResources().getString(R.string.pound) + StatusParser.getShared_estimatedFare());
        mShare.setText(getResources().getString(R.string.pound) + StatusParser.getShare_amount());
        mCustomerName.setText(StatusParser.getShared_customerName());
    }

    Dialog dialog;

    private void aDialogAlert() {

        dialog = new Dialog(RideSharingActivity.this, R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_twobutton);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView mTitle = (TextView) dialog.findViewById(R.id.text_title);
        TextView mDialogBody = (TextView) dialog
                .findViewById(R.id.txt_dialog_body);
        TextView mOK = (TextView) dialog.findViewById(R.id.txt_dialog_ok);
        TextView mCancel = (TextView) dialog
                .findViewById(R.id.txt_dialog_cancel);

        mTitle.setText("WARNING");
        mTitle.setTypeface(tf, Typeface.BOLD);
        mDialogBody
                .setText("Trip fare will be equally divided and will be charged to your account.");
        mOK.setText("PROCEED");
        mCancel.setText("CANCEL");

        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                dialog.dismiss();
                share_status = "1";
                if (cd.isConnectingToInternet()) {


                    shareResponse();
                } else
                    DialogAlert("No Internet !",
                            "Check your connection and try again.");

            }
        });

        mCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });

    }


    Dialog cdialog;

    private void cDialog(final int f) {

        cdialog = new Dialog(RideSharingActivity.this, R.style.Theme_Dialog);
        cdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = cdialog.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        cdialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        cdialog.setContentView(R.layout.dialog_onebutton);
        cdialog.setCancelable(false);
        cdialog.setCanceledOnTouchOutside(false);
        cdialog.show();

        TextView mTitle = (TextView) cdialog.findViewById(R.id.text_title);
        mTitle.setTypeface(tf, Typeface.BOLD);
        TextView mDialogBody = (TextView) cdialog
                .findViewById(R.id.txt_dialog_body);
        TextView mOK = (TextView) cdialog.findViewById(R.id.txt_dialog_ok);
        if (f == 0) {

            mTitle.setText("Alert");
            mDialogBody.setText("Acceptance time has expired.");
        } else if (f == 1) {
            mTitle.setText("REJECTED");
            mDialogBody.setText("The request you are trying to respond has already been rejected by the driver");
        } else {
            mTitle.setText("CANCELLED");
            mDialogBody.setText("The ride is cancelled by the customer");
        }
        mOK.setText("OK");
        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                cdialog.dismiss();
                startActivity(new Intent(RideSharingActivity.this,
                        CustomerHomeActivity.class));
                finish();
            }
        });

    }


    Dialog dialogA;

    private void DialogAlert(String title, String body) {

        dialogA = new Dialog(RideSharingActivity.this, R.style.Theme_Dialog);
        dialogA.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialogA.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialogA.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);

        dialogA.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);
        dialogA.setContentView(R.layout.dialog_onebutton);
        dialogA.setCancelable(false);
        dialogA.setCanceledOnTouchOutside(false);
        dialogA.show();

        TextView mTitle = (TextView) dialogA.findViewById(R.id.text_title);
        mTitle.setTypeface(tf, Typeface.BOLD);
        TextView mDialogBody = (TextView) dialogA
                .findViewById(R.id.txt_dialog_body);
        TextView mOK = (TextView) dialogA.findViewById(R.id.txt_dialog_ok);

        mTitle.setText(title);
        mDialogBody.setText(body);
        mOK.setText("OK");

        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogA.cancel();

                finish();
            }
        });

    }


    Dialog dialog2;

    private void mDialog(String message) {

        dialog2 = new Dialog(RideSharingActivity.this, R.style.Theme_Dialog);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog2.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog2.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);
        dialog2.setContentView(R.layout.dialog_onebutton);
        dialog2.setCancelable(false);
        dialog2.setCanceledOnTouchOutside(false);
        dialog2.show();

        TextView mTitle = (TextView) dialog2.findViewById(R.id.text_title);
        TextView mDialogBody = (TextView) dialog2.findViewById(R.id.txt_dialog_body);
        TextView mOK = (TextView) dialog2.findViewById(R.id.txt_dialog_ok);

        mTitle.setText("Alert");

        mDialogBody.setText(message);

        mOK.setText("OK");
        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                dialog2.dismiss();

                startActivity(new Intent(RideSharingActivity.this, CustomerHomeActivity.class));

                finish();

            }
        });

    }


    private void shareResponse() {




        presenter.shareInfo( mAppManager.getCustomerID(),mAppManager.getTokenCode(),share_status,StatusParser.getShared_requestId(),this);


    }


    @Override
    public void onPause() {
        super.onPause();
        MyApplication.activityPaused();
    }

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.activityResumed();
    }


    Dialog retry;

    private void retryDialog(final int i) {

        retry = new Dialog(RideSharingActivity.this, R.style.Theme_Dialog);
        retry.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = retry.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        retry.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        retry.setContentView(R.layout.thaks_dialog_twobutton);
        retry.setCancelable(false);
        retry.setCanceledOnTouchOutside(false);
        retry.show();

        TextView mTitle = (TextView) retry.findViewById(R.id.txt_title);
        mTitle.setText("Error!");
        mTitle.setTypeface(tf, Typeface.BOLD);
        TextView mDialogBody = (TextView) retry.findViewById(R.id.txt_dialog_body);
        mDialogBody.setText("Internet connection is slow or disconnected. Try connecting again.");
        LinearLayout mOK = (LinearLayout) retry.findViewById(R.id.lin_Ok);
        LinearLayout mCancel = (LinearLayout) retry.findViewById(R.id.lin_cancel);
        TextView mRetry = (TextView) retry.findViewById(R.id.txt_dialog_ok);
        TextView mExit = (TextView) retry.findViewById(R.id.txt_dialog_cancel);
        mRetry.setText("RETRY");
        mExit.setText("EXIT");

        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


                retry.dismiss();

                switch (i) {
                    case 1:
                        if (cd.isConnectingToInternet()) {


                            shareResponse();
                        } else
                            DialogAlert("No Internet !",
                                    "Check your connection and try again.");
                        break;

                    default:
                        break;

                }


            }
        });

        mCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                retry.dismiss();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });

    }

    @Override
    public void getShareInfoResponse(String response) {
        hideProgress();

        System.out.println("SHARERESP===" + response);
        JSONObject dataObject = null;
        try {
            dataObject = new JSONObject(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        parsresp = dataObject.optString("status").toString();
        message = dataObject.optString("message").toString();

        if (!TextUtils.isEmpty(parsresp)) {
            if (parsresp.equals("TRUE")) {

                MyFirebaseMessagingService.push_Status_Flag = "";
                mAppManager.setShared(true);
                mDialog(message);

            } else {
                MyFirebaseMessagingService.push_Status_Flag = "";
                if (message.equals("Invalid token code")) {

                    InvalidTokenDialog iDialog = new InvalidTokenDialog(RideSharingActivity.this);
                    iDialog.show();
                } else if (message.equals("Acceptance time has expired")) {
                    cDialog(0);
                } else if (message.equals("The request you are trying to respond has already been rejected by the driver")) {
                    cDialog(1);
                } else if (message.equals("The ride is cancelled by the customer")) {
                    cDialog(2);
                } else {
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(RideSharingActivity.this, CustomerHomeActivity.class));
                    finish();
                }
            }


        } else {

            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
            System.exit(0);
        }
    }

    @Override
    public void showProgreass() {
        loader = new Dialog(RideSharingActivity.this);
        loader.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loader.setContentView(R.layout.dialog_progress);
        loader_wheel = (ProgressWheel) loader.findViewById(R.id.progress_wheel);
        loader_wheel.setCircleRadius(50);
        loader.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        loader.setCancelable(false);
        loader.show();
    }

    @Override
    public void hideProgress() {
        if (loader != null) {
            loader.dismiss();
        }

    }

    @Override
    public void onFail(Throwable t) {
        hideProgress();
        retryDialog(1);
    }

    @Override
    public void mobileVerifyResponse(String response) {

    }

    @Override
    public void shareFareResponse(String response) {

    }

    @Override
    public void shareFareFail(Throwable t) {

    }

    @Override
    public void setPresenter(ShareContract.Presenter presenter) {
        this.presenter = presenter;

    }
}
