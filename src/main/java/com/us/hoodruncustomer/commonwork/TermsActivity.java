package com.us.hoodruncustomer.commonwork;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.configure.Configure;
import com.us.hoodruncustomer.parser.ConnectionDetector;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class TermsActivity extends SimpleActivity {
	
	private TextView headertext;

	private Toolbar toolbar;

	private  String htmlText;
	String cpw_status;
    String cpw_message;
    ConnectionDetector cd;
	WebView webView;
	final String mimeType = "text/html";
	final String encoding = "UTF-8";
    @SuppressLint("NewApi") @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
     		 if (toolbar != null) {
  	            int color = Color.parseColor("#ffffff");
  	            toolbar.setTitleTextColor(color);
  	            setSupportActionBar(toolbar);
  	           getSupportActionBar().setDisplayShowTitleEnabled(false);
			       
		            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
		            toolbar.setNavigationIcon(R.drawable.back_arrow);
		            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
		                @Override
		                public void onClick(View v) {
		                	 onBackPressed();
		                	 finish();
		                }
		            });
  		 }
             
             
             
     	

				
	     headertext=(TextView) findViewById(android.R.id.text1); 
	       headertext.setText("Terms & Privacy");
//	      textview=(TextView)findViewById(R.id.textview1);
//	      textview.setMovementMethod(new ScrollingMovementMethod());
		  webView=(WebView)findViewById(R.id.webview);
	      htmlText = "<body><p>" +getResources().getString(R.string.advise)+"</p>";

	      cd=new ConnectionDetector(TermsActivity.this);
   		if(cd.isConnectingToInternet()){
   		    showProgress();
			getTerms();
   		}else{
			Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_LONG).show();
			System.exit(0);
		}


	}
	

	 private void getTerms(){

		 StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_terms_and_conditions,
				 new Response.Listener<String>() {
					 @Override
					 public void onResponse(String response) {
						 hideProgress();
						 System.out.println("GETTERMS==="+response);
						 JSONObject dataObject=null;
						 try {
							 dataObject	= new JSONObject(response);
						 } catch (JSONException e) {
							 e.printStackTrace();
						 }
						 cpw_status=dataObject.optString("status");
						 cpw_message=dataObject.optString("terms_and_conditions");

						 if (!TextUtils.isEmpty(cpw_status)) {
							 if(cpw_status.equals("TRUE")){
								 htmlText = cpw_message;
							//	 textview.setText(Html.fromHtml( "<body><p>" +htmlText+"</p></body>"));
								 webView.loadDataWithBaseURL("","<body><p>" +htmlText+"</p></body>",mimeType,encoding,"");
							 }
						 else
							 Toast.makeText(getApplicationContext(),
									 "Something went wrong, please try again later.",
									 Toast.LENGTH_SHORT).show();


						 }else {
							 Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
							 System.exit(0);
						 }



					 }
				 },
				 new Response.ErrorListener() {
					 @Override
					 public void onErrorResponse(VolleyError error) {
						 hideProgress();
						retryDialog(1);
					 }


				 })
		 {
			 @Override
			 protected Map<String,String> getParams(){

				 Map<String,String> params = new HashMap<String, String>();

				 params.put("user_Type", "1");

				 return params;
			 }

		 };

		 RequestQueue requestQueue = Volley.newRequestQueue(this);
		 int socketTimeout = 60000;//60 seconds - change to what you want
		 RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		 stringRequest.setRetryPolicy(policy);
		 requestQueue.add(stringRequest);
	 }

	Dialog retry;
	private void retryDialog(final int i){

		retry = new Dialog(TermsActivity.this, R.style.Theme_Dialog);
		retry.requestWindowFeature(Window.FEATURE_NO_TITLE);
		Window window = retry.getWindow();
		window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);
		retry.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		retry.setContentView(R.layout.thaks_dialog_twobutton);
		retry.setCancelable(false);
		retry.setCanceledOnTouchOutside(false);
		retry.show();

		TextView mTitle=(TextView)retry.findViewById(R.id.txt_title);
		mTitle.setText("Error!");
		mTitle.setTypeface(tf, Typeface.BOLD);
		TextView mDialogBody=(TextView)retry.findViewById(R.id.txt_dialog_body);
		mDialogBody.setText("The server is taking too long to respond OR something is wrong with your internet connection.");
		LinearLayout mOK=(LinearLayout)retry.findViewById(R.id.lin_Ok);
		LinearLayout mCancel=(LinearLayout)retry.findViewById(R.id.lin_cancel);
		TextView mRetry=(TextView)retry.findViewById(R.id.txt_dialog_ok);
		TextView mExit=(TextView)retry.findViewById(R.id.txt_dialog_cancel);
		mRetry.setText("RETRY");
		mExit.setText("EXIT");

		mOK.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {


				retry.dismiss();

				switch (i)
				{
					case 1:
						if(cd.isConnectingToInternet())
						{
							showProgress();
						getTerms();
						}

						else{
							Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_SHORT).show();
							System.exit(0);
						}
						break;

					default:
						break;

				}



			}
		});

		mCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				retry.dismiss();
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);

			}
		});

	}


    @Override
	protected void onResume() {
	  super.onResume();
	  MyApplication.activityResumed();
	}

	@Override
	protected void onPause() {
	  super.onPause();
	  MyApplication.activityPaused();
	}
}
