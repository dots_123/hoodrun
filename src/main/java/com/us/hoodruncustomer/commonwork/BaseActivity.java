package com.us.hoodruncustomer.commonwork;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.configure.Configure;
import com.us.hoodruncustomer.db.AppManager;

import com.us.hoodruncustomer.ridehistory.RideHistoryActivity;
import com.us.hoodruncustomer.exceptionhandler.ExceptionHandler;
import com.us.hoodruncustomer.fragment.MyDialogLogout;
import com.us.hoodruncustomer.home.CustomerHomeActivity;
import com.us.hoodruncustomer.rider.InviteFriendsActivity;
import com.us.hoodruncustomer.payment.PaymentActivity;
import com.us.hoodruncustomer.splash.SplashActivity;

import java.util.ArrayList;


public class BaseActivity extends SuperBaseActivity {


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	protected RelativeLayout _completeLayout, _activityLayout;
	// nav drawer title
	private CharSequence mDrawerTitle;

	// used to store app title
	private CharSequence mTitle;
	private Toolbar toolbar;
	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
	private String mFrom;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.drawer);


		if (toolbar != null) {
			toolbar.setTitle("HoodRun");
			int color = Color.parseColor("#0582f4");
			toolbar.setTitleTextColor(color);
			setSupportActionBar(toolbar);
		}

		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));


	}

	Dialog dialogA;
	public void DialogAlert(String title, String body, final int direction) {

		dialogA = new Dialog(BaseActivity.this, R.style.Theme_Dialog);
		dialogA.requestWindowFeature(Window.FEATURE_NO_TITLE);
		Window window = dialogA.getWindow();
		window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);
		dialogA.getWindow().setBackgroundDrawableResource(
				android.R.color.transparent);

		dialogA.getWindow().setBackgroundDrawableResource(
				android.R.color.transparent);
		dialogA.setContentView(R.layout.dialog_onebutton);
		dialogA.setCancelable(false);
		dialogA.setCanceledOnTouchOutside(false);
		dialogA.show();

		TextView mTitle = (TextView) dialogA.findViewById(R.id.text_title);
		mTitle.setTypeface(tf, Typeface.BOLD);
		TextView mDialogBody = (TextView) dialogA
				.findViewById(R.id.txt_dialog_body);
		TextView mOK = (TextView) dialogA.findViewById(R.id.txt_dialog_ok);

		mTitle.setText(title);
		mDialogBody.setText(body);
		mOK.setText("OK");

		mOK.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dialogA.cancel();

				if (direction == 0) {
					finish();
				}

			}
		});

	}

	public void set(String[] navMenuTitles, TypedArray navMenuIcons) {

		mTitle = mDrawerTitle = getTitle();
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);

		navDrawerItems = new ArrayList<NavDrawerItem>();

		// adding nav drawer items
		if (navMenuIcons == null) {

			for (int i = 0; i < navMenuTitles.length; i++) {

				navDrawerItems.add(new NavDrawerItem(navMenuTitles[i]));

			}

		} else {

			for (int i = 0; i < navMenuTitles.length; i++) {

				navDrawerItems.add(new NavDrawerItem(navMenuTitles[i],
						navMenuIcons.getResourceId(i, -1)));
			}

		}

		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

		// setting the nav drawer list adapter
		adapter = new NavDrawerListAdapter(getApplicationContext(),
				navDrawerItems);
		mDrawerList.setAdapter(adapter);

		// enabling action bar app icon and behaving it as toggle button
		// getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		// getSupportActionBar().setHomeButtonEnabled(true);
		// // getActionBar().setIcon(R.drawable.ic_drawer);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar,
				R.string.drawer_open, R.string.drawer_close) {
			@Override
			public void onDrawerClosed(View view) {
				// getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				supportInvalidateOptionsMenu();
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				// getActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				supportInvalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

	}

	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
								long id) {
			// display view for selected nav drawer item
			// ConnectionDetector cd = new
			// ConnectionDetector(BaseActivity.this);
			// if (cd.isConnectingToInternet()) {
			//
			displayView(position);
			//
			// }else{
			//
			// Toast.makeText(getApplicationContext(),
			// "Network connection needed...", Toast.LENGTH_LONG).show();
			// }
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// getSupportMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (item.getItemId() == android.R.id.home) {

			if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
				mDrawerLayout.closeDrawer(mDrawerList);
			} else {
				mDrawerLayout.openDrawer(mDrawerList);

			}


		}




		return super.onOptionsItemSelected(item);
	}

	/***
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		// boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		// menu.findItem(R.id.action_dictionary).setVisible(!drawerOpen);
		// menu.findItem(R.id.action_home).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	private void displayView(int position) {
		AppManager mAppManager=AppManager.getInstance(this);
		switch (position) {
			case 0:
				if(mAppManager.getuser().equals("customer")){
					if(mAppManager.getLogin_status())
					{
						Intent intent0 = new Intent(this, CustomerHomeActivity.class);
						startActivity(intent0);
						finish();
					}// finishes the current activity
					else {
						Toast.makeText(getApplicationContext(), "Login to go for a ride", Toast.LENGTH_LONG).show();
						startActivity(new Intent(this,SplashActivity.class));
						finish();
					}

				}
				break;

			case 1:
				if(mAppManager.getuser().equals("customer")) {
					Intent intent1 = new Intent(this, ProfileActivity.class);
					startActivity(intent1);
					finish();// finishes the current activity
				}
				break;
			case 2:

				if(mAppManager.getuser().equals("customer")){
					Intent intent2 = new Intent(this, PaymentActivity.class);
					startActivity(intent2);
					finish();// finishes the current activity
				}else{

					Intent intent1 = new Intent(this, ProfileActivity.class);
					startActivity(intent1);
					finish();// finishes the current activity
				}

				break;
			case 3:

				if(mAppManager.getuser().equals("customer")) {

					Intent intent3 = new Intent(this, InviteFriendsActivity.class);
					startActivity(intent3);
					finish();// finishes the current activity

				}

				break;
			case 4:

				if(mAppManager.getuser().equals("customer")) {

					Intent intent3 = new Intent(this, RideHistoryActivity.class);
					startActivity(intent3);
					finish();// finishes the current activity
				}else{


					Intent intent3 = new Intent(this, RideHistoryActivity.class);
					startActivity(intent3);
					finish();// finishes the current activity


				}


				break;

			case 5:

				if(mAppManager.getuser().equals("customer")) {
					Intent intent4 = new Intent(this, HelpActivity.class);
					startActivity(intent4);
					finish();// finishes the current activity
				}else{
					mDrawerLayout.closeDrawer(Gravity.LEFT);
					// passing boolean to show false case also
					driverOffers(true);
				}

				break;

			case 6:

				if(mAppManager.getuser().equals("customer")) {
					Intent intent5 = new Intent(this, LoginedTermsActivity.class);
					startActivity(intent5);
					finish();// finishes the current activity
				}
				break;

			case 7:

				if(mAppManager.getuser().equals("customer")) {
					MyDialogLogout logout = new MyDialogLogout(this, Configure.URL_customer_logout, "customer");
					logout.show();
				}
				else {


					Intent intent4 = new Intent(this, HelpActivity.class);
					startActivity(intent4);
					finish();// finishes the current activity


				}

				break;

			case 8:
				// FOR DRIVER CASE ONLY
				if(!mAppManager.getuser().equals("customer")) {
					Intent intent5 = new Intent(this, LoginedTermsActivity.class);
					startActivity(intent5);
					finish();// finishes the current activity
				}
				break;

			case 9:

				if(!mAppManager.getuser().equals("customer")) {
					MyDialogLogout logout = new MyDialogLogout(this, Configure.URL_driver_logout, "driver");
					logout.show();
				}

				break;


//   Intent intent6 = new Intent(this, LoginActivity.class);
//   startActivity(intent6);
//   finish();// finishes the current activity

			default:
				break;
		}

		mDrawerList.setItemChecked(position, true);
		mDrawerList.setSelection(position);
		// mDrawerLayout.closeDrawer(mDrawerList);
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		// getActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

}
