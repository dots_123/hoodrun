package com.us.hoodruncustomer.fragment;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.us.hoodruncustomer.R;

import com.us.hoodruncustomer.configure.Configure;
import com.us.hoodruncustomer.db.AppManager;

import com.us.hoodruncustomer.model.TransactionsHistoryDao;
import com.us.hoodruncustomer.model.TransactionsListAdapter;
import com.us.hoodruncustomer.parser.ConnectionDetector;
import com.us.hoodruncustomer.parser.Parser;
import com.us.hoodruncustomer.home.CustomerHomeActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class TransactionHistory extends Fragment{

    ListView mHistoryListView;
    Typeface tf;
    AppManager mAppManager;
    String mURL;
    String parsresp;
    private ViewSwitcher viewSwitcher;
    ConnectionDetector cd;
    TransactionsListAdapter transactionListAdapter;
    int offset = 0;
    int AnalyticsSize;
    boolean scroll_flag = true;

    public static ArrayList<TransactionsHistoryDao> getmAnalyticsList() {
        return mTransactionList;
    }

    public static ArrayList<TransactionsHistoryDao> mTransactionList = new ArrayList<TransactionsHistoryDao>();


    public TransactionHistory() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transaction_history, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);
    }

    private void initView(View view) {

        mAppManager = AppManager.getInstance(getActivity());
        cd = new ConnectionDetector(getActivity());
        viewSwitcher = (ViewSwitcher) view.findViewById(R.id.switcher);


        mURL = Configure.URL_wallet_transaction_history;

        mHistoryListView = (ListView) view.findViewById(R.id.list_transaction_history);


        transactionListAdapter = new TransactionsListAdapter(getActivity(), mTransactionList, mAppManager.getuser());
        mHistoryListView.setAdapter(transactionListAdapter);



        mHistoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        });



        callAsync();
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    private void callAsync() {
        // TODO Auto-generated method stub

        if (cd.isConnectingToInternet()) {
            showProgress();
            historyViewer(mURL, offset);
        } else {
            Toast.makeText(getActivity(), "No internet connection avilable.", Toast.LENGTH_LONG).show();
            System.exit(0);
        }
    }

    Dialog loader;
    ProgressWheel loader_wheel;

    private void showProgress() {

        loader = new Dialog(getActivity());
        loader.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loader.setContentView(R.layout.dialog_progress);
        loader_wheel = (ProgressWheel) loader.findViewById(R.id.progress_wheel);
        loader_wheel.setCircleRadius(50);
        loader.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        loader.setCancelable(false);
        loader.show();
    }

    Dialog dialog2;

    private void Dialog() {
        TextView mDialogBody, mOK;


        dialog2 = new Dialog(getActivity(), R.style.Theme_Dialog);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog2.getWindow();
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog2.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);
        dialog2.setContentView(R.layout.dialog_no_title);
        dialog2.setCancelable(false);
        dialog2.setCanceledOnTouchOutside(false);
        dialog2.show();
        mDialogBody = (TextView) dialog2.findViewById(R.id.txt_dialog_body);
        mOK = (TextView) dialog2.findViewById(R.id.txt_dialog_ok);

        mDialogBody.setText("Ride history not available.");
        mOK.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (mAppManager.getuser().equals("customer")) {
                    startActivity(new Intent(getActivity(), CustomerHomeActivity.class));
                    getActivity().finish();

                }

            }
        });
    }

    private void historyViewer(String url, final int offset) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        viewSwitcher.setDisplayedChild(0);

                        System.out.println("TransactionHistoryVIEWER===" + response);

                        try {
                            parsresp = Parser.getTransactionsHistory(response);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (!TextUtils.isEmpty(parsresp)) {

                            switch (parsresp) {
                                case "TRUE":

                                    AnalyticsSize = mTransactionList.size();
                                    Log.i("historySize", "" + AnalyticsSize);
                                    mTransactionList.clear();

                                    for (int i = 0; i < Parser.getTransactionList().size(); i++) {

                                        mTransactionList.add(Parser.getTransactionList().get(i));
                                        Log.i("historySizeforloop", "" + AnalyticsSize);
                                        AnalyticsSize++;

                                    }

                                    transactionListAdapter.notifyDataSetChanged();
                                    break;

                                case "FALSE":
                                    switch (Parser.getTransaction_message()) {
                                        case "Invalid token code":
                                            new InvalidTokenDialog(getActivity()).show();
                                            break;

                                        case "No record found":
                                            scroll_flag = false;
                                            if (loader != null) {
                                                loader.dismiss();
                                            }
                                            if (offset == 0) {
                                                viewSwitcher.setDisplayedChild(1);
                                            }
                                            break;
                                        case "Ride history not available":
                                            if (offset == 0) {
                                                Dialog();
                                            } else {
                                                scroll_flag = false;
                                                //Toast.makeText(getApplicationContext(), Parser.getHistory_status_message(), Toast.LENGTH_SHORT).show();
                                                if (loader != null)
                                                    loader.dismiss();
                                            }
                                            break;
                                        default:

                                            break;
                                    }

                                    break;
                            }
                            loader.dismiss();

                        } else {
                            loader.dismiss();

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        retryDialog(1);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                params.put("driver_id", mAppManager.getDriverID_Number());
                params.put("token_code", mAppManager.getTokenCode());
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }


    Dialog retry;

    private void retryDialog(final int i) {

        retry = new Dialog(getActivity(), R.style.Theme_Dialog);
        retry.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = retry.getWindow();
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        retry.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        retry.setContentView(R.layout.thaks_dialog_twobutton);
        retry.setCancelable(false);
        retry.setCanceledOnTouchOutside(false);
        retry.show();

        TextView mTitle = (TextView) retry.findViewById(R.id.txt_title);
        mTitle.setText("Error!");
        mTitle.setTypeface(tf, Typeface.BOLD);
        TextView mDialogBody = (TextView) retry.findViewById(R.id.txt_dialog_body);
        mDialogBody.setText("The server is taking too long to respond OR something is wrong with your internet connection.");
        LinearLayout mOK = (LinearLayout) retry.findViewById(R.id.lin_Ok);
        LinearLayout mCancel = (LinearLayout) retry.findViewById(R.id.lin_cancel);
        TextView mRetry = (TextView) retry.findViewById(R.id.txt_dialog_ok);
        TextView mExit = (TextView) retry.findViewById(R.id.txt_dialog_cancel);
        mRetry.setText("RETRY");
        mExit.setText("EXIT");

        mOK.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                retry.dismiss();

                switch (i) {
                    case 1:
                        callAsync();
                        break;

                    default:
                        break;

                }

            }
        });

        mCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                retry.dismiss();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });

    }

}
