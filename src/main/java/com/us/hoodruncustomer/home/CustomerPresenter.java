package com.us.hoodruncustomer.home;

import android.content.Context;

import com.us.hoodruncustomer.data.DataSource;
import com.us.hoodruncustomer.data.remote.RemoteDataSource;

import java.util.Map;

/**
 * Created by admin on 11/14/2017.
 */

public class CustomerPresenter implements CustomerContract.Presenter, DataSource.CarsResponseI, DataSource.ConfirmRideI {
    private DataSource loginDataSource;
    private CustomerContract.View mLoginView;
    private Context context;


    public CustomerPresenter(RemoteDataSource customerRemoteDataSource, CustomerContract.View mLoginView) {

        loginDataSource = customerRemoteDataSource;
        this.mLoginView = mLoginView;
        this.mLoginView.setPresenter(this);
    }

    @Override
    public void getcarsResponse(String baseResponse) {
        mLoginView.getCarsResponse(baseResponse);
    }

    @Override
    public void onFail(Throwable t) {
        //  mLoginView.hideProgress();
        mLoginView.onFail(t);
    }

    @Override
    public void getEstimatess(String response) {
        mLoginView.getEstimateResponse(response);

    }

    @Override
    public void getCarsByLocation(String lat, String lng, String token_code, String city, String checkCity, String customerId, Context context) {
        //mLoginView.showProgreass();
        loginDataSource.getCarsService(lat, lng, token_code, city, checkCity, customerId, this, context);

    }

    @Override
    public void getEstimate(Map<String, String> params, Context context) {

        loginDataSource.getEstimates(params, this, context);

    }

    @Override
    public void driverSelect(Map<String, String> params, Context context) {
//mLoginView.showProgreass();
        loginDataSource.driverSelect(params, this, context);
    }

    @Override
    public void checkPromoCode(Map<String, String> params, Context context) {

        mLoginView.showProgreass();
        loginDataSource.checkPromoCode(params, this, context);
    }

    @Override
    public void drop() {
        //  mLoginView.hideProgress();
        mLoginView = null;
    }

    @Override
    public void onDriverSelectResponse(String response) {
        mLoginView.hideProgress();
        mLoginView.driverSelectREsponse(response);


    }


    @Override
    public void onCheckPromocodeResponse(String response) {
        mLoginView.hideProgress();
        mLoginView.getPromoCodeResponse(response);

    }

    @Override
    public void onDriverSelecterror(Throwable t) {
        mLoginView.driverSelectError(t);

    }

    @Override
    public void onCheckPromoerror(Throwable t) {
        mLoginView.hideProgress();
        mLoginView.getPromocodeerror(t);

    }
}
