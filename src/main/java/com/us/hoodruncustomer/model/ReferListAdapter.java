package com.us.hoodruncustomer.model;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.us.hoodruncustomer.R;

import java.util.ArrayList;

public class ReferListAdapter extends BaseAdapter{
	Activity mContext;
	ArrayList<ReferredDrivers> historyList;

	public ReferListAdapter(Activity ctx, ArrayList<ReferredDrivers> historyList){
		
		this.mContext=ctx;
		this.historyList=historyList;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return historyList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return historyList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	public static class ViewHolder{
		
		TextView name,email,phone,status,textView11;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;
		LayoutInflater inflater=(LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


		convertView=inflater.inflate(R.layout.refer_item, parent,false);

		holder=new ViewHolder();
		holder.name=(TextView)convertView.findViewById(R.id.txt_name);
		holder.email=(TextView)convertView.findViewById(R.id.txt_email);
		holder.phone=(TextView)convertView.findViewById(R.id.txt_phone);
		holder.status=(TextView)convertView.findViewById(R.id.txt_status);
		holder.textView11=(TextView)convertView.findViewById(R.id.textView11);

		holder.name.setText(historyList.get(position).getFirst_name()+" "+historyList.get(position).getSecond_name());
		holder.email.setText(historyList.get(position).getEmail());
		holder.phone.setText(historyList.get(position).getPhone());
		holder.status.setText(historyList.get(position).getRefer_status());

		if(historyList.get(position).getReferral_amount_alloted().trim().length() > 0){
			holder.textView11.setText(historyList.get(position).getReferral_amount_alloted());
		}


		return convertView;
	}



}
