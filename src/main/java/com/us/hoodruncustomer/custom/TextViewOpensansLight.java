package com.us.hoodruncustomer.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;


public class TextViewOpensansLight extends TextView {

	public TextViewOpensansLight(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);

	        init();
		// TODO Auto-generated constructor stub
	}
	   public TextViewOpensansLight(Context context, AttributeSet attrs) {
	        super(context, attrs);
	        init();
	    }

	    public TextViewOpensansLight(Context context) {
	        super(context);
	        init();
	    }

	    private void init() {
	        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
	                                               "OpenSans-Light.ttf");
	        setTypeface(tf);
	    
}}
