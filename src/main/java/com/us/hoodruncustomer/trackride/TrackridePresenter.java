package com.us.hoodruncustomer.trackride;

import android.content.Context;

import com.us.hoodruncustomer.data.DataSource;
import com.us.hoodruncustomer.data.remote.RemoteDataSource;

/**
 * Created by admin on 11/15/2017.
 */

public class TrackridePresenter implements TrackRideContract.Presenter, DataSource.TrackRideI ,DataSource.RideRatingI{


    private DataSource dataSource;
    private TrackRideContract.View mpView;
    private Context context;

    public TrackridePresenter(RemoteDataSource remoteDataSource, TrackRideContract.View mLoginView) {

        dataSource = remoteDataSource;
        this.mpView = mLoginView;
        this.mpView.setPresenter(this);
    }

    @Override
    public void drop() {
        mpView = null;
    }

    @Override
    public void cusCancelCharge(String tokencode, String customerId, Context context) {
        mpView.showProgreass();
        dataSource.cusCancelChargeservice(tokencode, customerId, this, context);


    }

    @Override
    public void customerCancelRide(int cancelStart, Context context) {
        mpView.showProgreass();
        dataSource.customerCancelRideservice(cancelStart, this, context);
    }

    @Override
    public void checkUserEndRide(String tokencode, String rideId, Context context) {

        mpView.showProgreass();
        dataSource.checkUserEndRideService(tokencode, rideId, this, context);

    }

    @Override
    public void rideRating(String tokencode, String rateValue, String riderId, Context context) {
        mpView.showProgreass();
        dataSource.rideRatingservice(tokencode,rateValue,riderId,this,context);

    }

    @Override
    public void cusCancellationChargeResponse(String response) {
        mpView.hideProgress();
        mpView.customerCancellationChargeResponse(response);

    }

    @Override
    public void custCancelRideResponse(String response) {
        mpView.hideProgress();
        mpView.customerCancelRideResponse(response);

    }

    @Override
    public void userCheckIsRideEndData(String response) {
        mpView.hideProgress();
        mpView.userCheckIsRideEndResponse(response);

    }

    @Override
    public void rideRatingResopnse(String baseResponse) {
        mpView.hideProgress();
        mpView.rideRatinResponse(baseResponse);

    }

    @Override
    public void onRatingFail(Throwable t) {
        mpView.hideProgress();
        mpView.rideRatingFail(t);

    }

    @Override
    public void onFail(Throwable t) {
        mpView.hideProgress();
        mpView.onFail(t);

    }

    @Override
    public void onFailRideEndd(Throwable t) {
        mpView.hideProgress();
        mpView.onFailRideEnd(t);

    }
}
