package com.us.hoodruncustomer.model;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.us.hoodruncustomer.R;

import java.util.ArrayList;

public class CarListAdapter extends BaseAdapter{
	Activity mContext;
	ArrayList<Cars> carList = new ArrayList<>();
	String user;
	public CarListAdapter(Activity ctx, String user){

		this.mContext=ctx;
		this.user=user;
	}


	public void setData(ArrayList<Cars> carList){
		this.carList = carList;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return carList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return carList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	public static class ViewHolder{

		TextView car_name,base_fare,min_fare,per_min_fare,per_mile_fare;
		ImageView car_image;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;
		LayoutInflater inflater=(LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		convertView=inflater.inflate(R.layout.car_item, parent,false);
		holder=new ViewHolder();

		holder.car_name=(TextView)convertView.findViewById(R.id.car_name);
		holder.base_fare=(TextView)convertView.findViewById(R.id.base_fare);
		holder.min_fare=(TextView)convertView.findViewById(R.id.min_fare);
		holder.per_min_fare=(TextView)convertView.findViewById(R.id.per_min_fare);
		holder.per_mile_fare=(TextView)convertView.findViewById(R.id.per_mile_fare);
		holder.car_image=(ImageView) convertView.findViewById(R.id.car_image);

		Glide.with(mContext).load(carList.get(position).getList_car_image())
//		.placeholder(R.drawable.driver_details_background)
				.into(holder.car_image);

		holder.car_name.setText("" + carList.get(position).getName());
		holder.base_fare.setText(mContext.getResources().getString(R.string.pound) + carList.get(position).getBase_fare());
		holder.min_fare.setText(mContext.getResources().getString(R.string.pound) + carList.get(position).getMinimum_fare());
		holder.per_min_fare.setText(mContext.getResources().getString(R.string.pound) + carList.get(position).getPer_minute_fare());
		holder.per_mile_fare.setText(mContext.getResources().getString(R.string.pound) + carList.get(position).getPer_mile_fare());
		return convertView;
	}


}
