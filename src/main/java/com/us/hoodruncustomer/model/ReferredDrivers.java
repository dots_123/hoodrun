package com.us.hoodruncustomer.model;

public class ReferredDrivers {
	
	private  String id;
	private String first_name;
	private String second_name;
	private String address;
	private String refer_status;
	private String email;
	private String phone;
	private String referral_amount_alloted;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getSecond_name() {
		return second_name;
	}

	public void setSecond_name(String second_name) {
		this.second_name = second_name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getRefer_status() {
		return refer_status;
	}

	public void setRefer_status(String refer_status) {
		this.refer_status = refer_status;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getReferral_amount_alloted() {
		return referral_amount_alloted;
	}

	public void setReferral_amount_alloted(String referral_amount_alloted) {
		this.referral_amount_alloted = referral_amount_alloted;
	}
}
