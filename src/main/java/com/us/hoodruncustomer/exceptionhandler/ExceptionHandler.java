package com.us.hoodruncustomer.exceptionhandler;

import java.io.PrintWriter;
import java.io.StringWriter;

import com.us.hoodruncustomer.R;

import com.us.hoodruncustomer.authentication.LoginActivity;
import com.us.hoodruncustomer.db.AppManager;
import com.us.hoodruncustomer.home.CustomerHomeActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

public class ExceptionHandler implements
		java.lang.Thread.UncaughtExceptionHandler {
	private final Activity myContext;
	private final String LINE_SEPARATOR = "\n";
	AppManager mAppManager;
	public ExceptionHandler(Activity context) {
		myContext = context;
	}

	public void uncaughtException(Thread thread, Throwable exception) {
		StringWriter stackTrace = new StringWriter();
		exception.printStackTrace(new PrintWriter(stackTrace));
		StringBuilder errorReport = new StringBuilder();
		errorReport.append("************ CAUSE OF ERROR ************\n\n");
		errorReport.append(stackTrace.toString());

		errorReport.append("\n************ DEVICE INFORMATION ***********\n");
		errorReport.append("Brand: ");
		errorReport.append(Build.BRAND);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Device: ");
		errorReport.append(Build.DEVICE);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Model: ");
		errorReport.append(Build.MODEL);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Id: ");
		errorReport.append(Build.ID);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Product: ");
		errorReport.append(Build.PRODUCT);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("\n************ FIRMWARE ************\n");
		errorReport.append("SDK: ");
		errorReport.append(Build.VERSION.SDK);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Release: ");
		errorReport.append(Build.VERSION.RELEASE);
		errorReport.append(LINE_SEPARATOR);
		errorReport.append("Incremental: ");
		errorReport.append(Build.VERSION.INCREMENTAL);
		errorReport.append(LINE_SEPARATOR);
		Log.d("error", errorReport.toString());
//
//		Intent intent = new Intent(myContext, SplashActivity.class);
//		intent.putExtra("error", errorReport.toString());
//		myContext.startActivity(intent);
//		mAppManager=AppManager.getInstance(myContext);
//		if(dialog2 != null){
//			dialog2.dismiss();
//			dialog2 = null;
//		}
//		android.os.Process.killProcess(android.os.Process.myPid());
//		System.exit(10);

	}

	android.app.Dialog dialog2;
	private void dialogError() {

		dialog2 = new Dialog(myContext, R.style.Theme_Dialog);
		dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
		Window window = dialog2.getWindow();
		window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);
		dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog2.setContentView(R.layout.dialog_onebutton);
		dialog2.setCancelable(false);
		dialog2.setCanceledOnTouchOutside(false);
		dialog2.show();
		
		TextView mTitle=(TextView)dialog2.findViewById(R.id.text_title);
		TextView mDialogBody=(TextView)dialog2.findViewById(R.id.txt_dialog_body);
		TextView mOK=(TextView)dialog2.findViewById(R.id.txt_dialog_ok);
		
		mTitle.setText("Error!");
		mDialogBody.setText("Unexpected error occured"+"\n"+"Please try again");
		mOK.setText("OK");
		mOK.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent myintent = null;
				dialog2.dismiss();
				 if(mAppManager.getuser().equals("customer") && mAppManager.getLogin_status())
				{
					myintent = new Intent(myContext,CustomerHomeActivity.class);
				}
				else
				myintent = new Intent(myContext,LoginActivity.class);
				myContext.startActivity(myintent);
				myContext.finish();
				
			}
		});
	
	}
}