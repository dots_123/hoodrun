package com.us.hoodruncustomer.parser;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.us.hoodruncustomer.model.Cars;
import com.us.hoodruncustomer.model.Driver;
import com.us.hoodruncustomer.model.Offers;

public class DriverParser {


	public static void setTrip_distance_miles(String trip_distance_miles) {
		DriverParser.trip_distance_miles = trip_distance_miles;
	}

	public static void setTrip_duration(String trip_duration) {
		DriverParser.trip_duration = trip_duration;
	}

	public static void setPayment_mode(String payment_mode) {
		DriverParser.payment_mode = payment_mode;
	}

	public static void setTotal(String total) {
		DriverParser.total = total;
	}

	private static String status;
	private static String status_message;
	private static String suspension_reason;
	public static  String unique_request_id;

	public static String getFare_multiplier() {
		return fare_multiplier;
	}

	public static void setFare_multiplier(String fare_multiplier) {
		DriverParser.fare_multiplier = fare_multiplier;
	}

	public static  String fare_multiplier;

	public static String getFare_expiration_time() {
		return fare_expiration_time;
	}

	public static void setFare_expiration_time(String fare_expiration_time) {
		DriverParser.fare_expiration_time = fare_expiration_time;
	}

	public static  String fare_expiration_time;

	private static ArrayList<Driver> driverList;

	static Driver mDriver;

	public static String driverDetails(String data) throws Exception {

		try {

			JSONObject cobj = new JSONObject(data);
			status = cobj.getString("status");

			if (status.equals("TRUE")) {
				request_id = cobj.getString("request_id");
				unique_request_id = cobj.getString("unique_request_id");
				fare_multiplier = cobj.getString("fare_multiplier");
				fare_expiration_time = cobj.getString("fare_expiration_time");

				JSONArray jArray = cobj.optJSONArray("result");

				System.out.println("LLLLLLL " + jArray.length());
				driverList = new ArrayList<Driver>();
				for (int i = 0; i < jArray.length(); i++) {
					mDriver = new Driver();
					System.out.println("inside for loop ");
					JSONObject obj = jArray.getJSONObject(i);
					mDriver.setDriver_id(obj.getString("driver_id"));

					mDriver.setDriver_name(obj.getString("driver_name"));
					System.out.println("e54345743 "
							+ obj.getString("driver_name"));
					mDriver.setLatitude(obj.getString("latitude"));
					mDriver.setLongitude(obj.getString("longitude"));
					mDriver.setDistance_away(obj.getString("distance_away"));
					mDriver.setCity(obj.getString("city"));
					mDriver.setFav_status(obj.getString("fav_Status"));
					mDriver.setRating(obj.getString("driver_Rating"));
					mDriver.setVehicle_type(obj.getString("vehicle_type"));
					mDriver.setVehicle_model(obj.getString("vehicle_Model"));
					mDriver.setEstimated_fare(obj.getString("estimated_fare"));
					mDriver.setDriver_image(obj.getString("profile_pic"));
					mDriver.setMinutes_away(obj.getString("distance_away_in_minutes"));
					// mDriver.setRequest_id(obj.getString("request_id"));
					mDriver.setMax_seat(obj.getString("max_seat"));
					driverList.add(mDriver);
				}

				System.out.println("driverList:" + driverList.size());
			}

			else if (status.equals("FALSE")) {
				JSONObject error = new JSONObject(data);
				status_message = error.getString("message");
				suspension_reason = error.getString("reason");
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return status;
	}


	public static String getCar_status() {
		return car_status;
	}

	public static void setCar_status(String car_status) {
		DriverParser.car_status = car_status;
	}

	private static String car_status;

	public static String getCar_status_message() {
		return car_status_message;
	}

	public static void setCar_status_message(String car_status_message) {
		DriverParser.car_status_message = car_status_message;
	}

	private static String car_status_message;

	private static String car_is_suspended;

	private static String car_is_suspended_reason;

	private static int car_is_suspended_reason_type;


	public static ArrayList<Cars> getCarList() {
		return carList;
	}

	public static void setCarList(ArrayList<Cars> carList) {
		DriverParser.carList = carList;
	}

	private static ArrayList<Cars> carList;


	public static String getCars(String data) throws Exception {

		try {

			JSONObject cobj = new JSONObject(data);
			car_status = cobj.getString("status");
			car_status_message = cobj.getString("message");

			if (car_status.equals("true")) {

				JSONObject obj_main = cobj.optJSONObject("data");

				car_is_suspended = obj_main.getString("suspended");

				car_is_suspended_reason = obj_main.getString("reason");

				if(car_is_suspended.equalsIgnoreCase("TRUE")){
					car_is_suspended_reason_type = obj_main.getInt("reason_type");
				}

				JSONArray jArray = obj_main.optJSONArray("cars");

				carList = new ArrayList<Cars>();

					for (int i = 0; i < jArray.length(); i++) {

						Cars car  = new Cars();

					    JSONObject obj = jArray.getJSONObject(i);

						car.setId(obj.getString("id"));

						car.setName(obj.getString("name"));

						car.setNo_of_seats(obj.getString("no_of_seats"));

						carList.add(car);
				}

			}

			else if (car_status.equals("false")) {
				JSONObject error = new JSONObject(data);
				car_status_message = error.getString("message");
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return car_status;
	}


	private static String Future_customer_id;
	private static String Future_request_id="";
	private static String Future_customer_pick_up_point_lat;
	private static String Future_customer_pick_up_point_long;
	private static String Future_customer_destination_point_lat;
	private static String Future_customer_destination_point_long;
	private static String Future_customer_name;
	private static String Future_customer_image;
	private static String Future_no_seats;
	private static String Future_reason;
	private static String Future_payment_type;
	private static String Future_mobile_no;
	private static String Future_estimated_fare;
	private static String Future_customer_pick_up_name, Future_customer_destination_name;
	private static boolean Future_driver_status;
	private static String Future_status_message;

	public static String getFuture_customer_id() {
		return Future_customer_id;
	}

	public static void setFuture_customer_id(String future_customer_id) {
		Future_customer_id = future_customer_id;
	}

	public static String getFuture_request_id() {
		return Future_request_id;
	}

	public static void setFuture_request_id(String future_request_id) {
		Future_request_id = future_request_id;
	}

	public static String getFuture_customer_pick_up_point_lat() {
		return Future_customer_pick_up_point_lat;
	}

	public static void setFuture_customer_pick_up_point_lat(String future_customer_pick_up_point_lat) {
		Future_customer_pick_up_point_lat = future_customer_pick_up_point_lat;
	}

	public static String getFuture_customer_pick_up_point_long() {
		return Future_customer_pick_up_point_long;
	}

	public static void setFuture_customer_pick_up_point_long(String future_customer_pick_up_point_long) {
		Future_customer_pick_up_point_long = future_customer_pick_up_point_long;
	}

	public static String getFuture_customer_destination_point_lat() {
		return Future_customer_destination_point_lat;
	}

	public static void setFuture_customer_destination_point_lat(String future_customer_destination_point_lat) {
		Future_customer_destination_point_lat = future_customer_destination_point_lat;
	}

	public static String getFuture_customer_destination_point_long() {
		return Future_customer_destination_point_long;
	}

	public static void setFuture_customer_destination_point_long(String future_customer_destination_point_long) {
		Future_customer_destination_point_long = future_customer_destination_point_long;
	}

	public static String getFuture_customer_name() {
		return Future_customer_name;
	}

	public static void setFuture_customer_name(String future_customer_name) {
		Future_customer_name = future_customer_name;
	}

	public static String getFuture_customer_image() {
		return Future_customer_image;
	}

	public static void setFuture_customer_image(String future_customer_image) {
		Future_customer_image = future_customer_image;
	}

	public static String getFuture_no_seats() {
		return Future_no_seats;
	}

	public static void setFuture_no_seats(String future_no_seats) {
		Future_no_seats = future_no_seats;
	}

	public static String getFuture_reason() {
		return Future_reason;
	}

	public static void setFuture_reason(String future_reason) {
		Future_reason = future_reason;
	}

	public static String getFuture_payment_type() {
		return Future_payment_type;
	}

	public static void setFuture_payment_type(String future_payment_type) {
		Future_payment_type = future_payment_type;
	}

	public static String getFuture_mobile_no() {
		return Future_mobile_no;
	}

	public static void setFuture_mobile_no(String future_mobile_no) {
		Future_mobile_no = future_mobile_no;
	}

	public static String getFuture_estimated_fare() {
		return Future_estimated_fare;
	}

	public static void setFuture_estimated_fare(String future_estimated_fare) {
		Future_estimated_fare = future_estimated_fare;
	}

	public static String getFuture_customer_pick_up_name() {
		return Future_customer_pick_up_name;
	}

	public static void setFuture_customer_pick_up_name(String future_customer_pick_up_name) {
		Future_customer_pick_up_name = future_customer_pick_up_name;
	}

	public static String getFuture_customer_destination_name() {
		return Future_customer_destination_name;
	}

	public static void setFuture_customer_destination_name(String future_customer_destination_name) {
		Future_customer_destination_name = future_customer_destination_name;
	}

	public static boolean isFuture_driver_status() {
		return Future_driver_status;
	}

	public static void setFuture_driver_status(boolean future_driver_status) {
		Future_driver_status = future_driver_status;
	}

	public static String getFuture_status_message() {
		return Future_status_message;
	}

	public static void setFuture_status_message(String future_status_message) {
		Future_status_message = future_status_message;
	}

	public static boolean Future_driverAccept(String data) {
		try {

			JSONObject dataObject = new JSONObject(data);

			Future_driver_status = dataObject.optBoolean("status");

			if (Future_driver_status) {

				Future_status_message = dataObject.optString("message").toString();


				Future_customer_id = dataObject.optString("customer_id").toString();
				Future_request_id = dataObject.optString("request_id").toString();
				Future_customer_pick_up_point_lat = dataObject.optString(
						"customer_pick_up_point_lat").toString();
				Future_customer_pick_up_point_long = dataObject.optString(
						"customer_pick_up_point_long").toString();
				Future_customer_destination_point_lat = dataObject.optString(
						"customer_destination_point_lat").toString();
				Future_customer_destination_point_long = dataObject.optString(
						"customer_destination_point_long").toString();

				Future_customer_pick_up_name = dataObject.optString(
						"picking_Location_Name").toString();
				Future_customer_destination_name = dataObject.optString(
						"destination_Location_Name").toString();

				Future_customer_name = dataObject.optString("customer_name")
						.toString();
				Future_customer_image = dataObject.optString("customer_image")
						.toString();
				Future_no_seats = dataObject.optString("no_Seats").toString();
				Future_payment_type = dataObject.optString("payment_type").toString();
				Future_mobile_no = dataObject.optString("mobile_no").toString();
				Future_estimated_fare = dataObject.optString("estimated_fare")
						.toString();

				Log.d("Inside true", "" + status_message);
			}

			else {
				Future_status_message = dataObject.optString("message").toString();
				if(Future_status_message.equalsIgnoreCase("DRIVER IS SUSPENDED")){
					Future_reason = dataObject.optString("reason").toString();
				}

				Future_status_message = dataObject.optString("message").toString();
			}
		}

		catch (Exception e) {

		}
		return Future_driver_status;
	}














	private static String customer_id;
	private static String request_id="";
	private static String customer_pick_up_point_lat;
	private static String customer_pick_up_point_long;
	private static String customer_destination_point_lat;
	private static String customer_destination_point_long;
	private static String customer_name;
	private static String customer_image;
	private static String no_seats;
	private static String reason;
	private static String payment_type;
	private static String mobile_no;
	private static String estimated_fare;
	private static String surge_applied;
	private static String customer_pick_up_name, customer_destination_name;
	private static boolean driver_status;

	public static boolean driverAccept(String data) {
		try {

			JSONObject dataObject = new JSONObject(data);

			driver_status = dataObject.optBoolean("status");

			if (driver_status) {

				status_message = dataObject.optString("message").toString();
				surge_applied = dataObject.optString("fare_multiplier").toString();
				customer_id = dataObject.optString("customer_id").toString();
				request_id = dataObject.optString("request_id").toString();
				customer_pick_up_point_lat = dataObject.optString(
						"customer_pick_up_point_lat").toString();
				customer_pick_up_point_long = dataObject.optString(
						"customer_pick_up_point_long").toString();
				customer_destination_point_lat = dataObject.optString(
						"customer_destination_point_lat").toString();
				customer_destination_point_long = dataObject.optString(
						"customer_destination_point_long").toString();

				customer_pick_up_name = dataObject.optString(
						"picking_Location_Name").toString();
				customer_destination_name = dataObject.optString(
						"destination_Location_Name").toString();

				customer_name = dataObject.optString("customer_name")
						.toString();
				customer_image = dataObject.optString("customer_image")
						.toString();
				no_seats = dataObject.optString("no_Seats").toString();
				payment_type = dataObject.optString("payment_type").toString();
				mobile_no = dataObject.optString("mobile_no").toString();
				estimated_fare = dataObject.optString("estimated_fare")
						.toString();

				Log.d("Inside true", "" + status_message);
			}

			else {
				status_message = dataObject.optString("message").toString();
				if(status_message.equalsIgnoreCase("DRIVER IS SUSPENDED")){
					reason = dataObject.optString("reason").toString();
				}

				status_message = dataObject.optString("message").toString();
			}
		}

		catch (Exception e) {

		}
		return driver_status;
	}


	private static String offer_status;
	private static String offer_code;
	private static String offer_message;
	private static ArrayList<Offers> offer_list;


	public static String driverOffers(String data) {
		try {

			JSONObject dataObject = new JSONObject(data);

			offer_status = dataObject.optString("status");

			if (offer_status.equals("true")) {

				JSONArray jArray = dataObject.optJSONArray("data");

				offer_list = new ArrayList<Offers>();

				for (int i = 0; i < jArray.length(); i++) {

					Offers offfer  = new Offers();

					JSONObject obj = jArray.getJSONObject(i);

					offfer.setTitle(obj.getString("title"));

					offfer.setAmount(obj.getString("amount"));

					offfer.setCompleted_rides(obj.getString("completed_rides"));

					offfer.setCancelled_rides(obj.getString("rejected_rides"));

					offfer.setIgnored_rides(obj.getString("cancelled_rides"));

					offfer.setIgnored_rides(obj.getString("ignored_rides"));

					offer_list.add(offfer);
				}


			}

			else {

			}
		}

		catch (Exception e) {

		}
		return offer_status;
	}


	private static String customer_lat;
	private static String customer_long;
	private static String cancel_charge;
	private static String current_request_id;
	private static String current_status_message;
	private static String customer_mobile_number;
	private static String ride_id;
	private static boolean ride_status;

	public static boolean getRideResponse(String data) {
		try {
			JSONObject dataObject = new JSONObject(data);

			ride_status = dataObject.optBoolean("status");
			status_message = dataObject.optString("message").toString();

			if (ride_status) {

				JSONObject mainObject = dataObject.getJSONObject("data");

				current_request_id = mainObject.optString("request_id").toString();
				cancel_charge = mainObject.optString("cancel_charge").toString();
				customer_lat = mainObject.optString("customer_lat").toString();
				customer_long = mainObject.optString("customer_lng").toString();
				ride_id = mainObject.optString("ride_id").toString();
				customer_mobile_number = mainObject.optString("mobile_no").toString();
				Log.d("Inside true", "" + status_message);
			}

			else {
				status_message = dataObject.optString("message").toString();

			}
		}

		catch (Exception e) {

		}
		return ride_status;
	}

	public static String parseDriverRequest(String message) {
		// TODO Auto-generated method stub

		try {
			JSONObject dataObject = new JSONObject(message);

			customer_id = dataObject.optString("customer_id").toString();
			request_id = dataObject.optString("request_id").toString();
			customer_pick_up_point_lat = dataObject.optString(
					"customer_pick_up_point_lat").toString();
			customer_pick_up_point_long = dataObject.optString(
					"customer_pick_up_point_long").toString();
			customer_destination_point_lat = dataObject.optString(
					"customer_destination_point_lat").toString();
			customer_destination_point_long = dataObject.optString(
					"customer_destination_point_long").toString();

			customer_pick_up_name = dataObject.optString(
					"picking_Location_Name").toString();
			customer_destination_name = dataObject.optString(
					"destination_Location_Name").toString();

			customer_name = dataObject.optString("customer_name").toString();
			customer_image = dataObject.optString("customer_image").toString();
			no_seats = dataObject.optString("no_Seats").toString();
			payment_type = dataObject.optString("payment_type").toString();
			mobile_no = dataObject.optString("mobile_no").toString();
			estimated_fare = dataObject.optString("estimated_fare").toString();

			Log.d("Inside true", "" + status_message);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	public static String getStatusMessage() {
		return status_message;
	}

	public static ArrayList<Driver> getDriverdetails() {

		return driverList;
	}

	public static String getCustomer_id() {
		return customer_id;
	}

	public static String getRequest_id() {
		return request_id;
	}

	public static String getCustomer_pick_up_point_lat() {
		return customer_pick_up_point_lat;
	}

	public static String getCustomer_pick_up_point_long() {
		return customer_pick_up_point_long;
	}

	public static String getRide_id() {
		return ride_id;
	}

	public static String getCustomer_destination_point_lat() {
		return customer_destination_point_lat;
	}

	public static String getCustomer_destination_point_long() {
		return customer_destination_point_long;
	}

	public static String getCustomer_name() {
		return customer_name;
	}

	public static String getCustomer_image() {
		return customer_image;
	}

	public static String getNo_seats() {
		return no_seats;
	}

	public static String getPayment_type() {
		return payment_type;
	}

	public static String getMobile_no() {
		return mobile_no;
	}

	public static String getEstimated_fare() {
		return estimated_fare;
	}

	public static String getCustomer_lat() {
		return customer_lat;
	}

	public static String getCustomer_lon() {
		return customer_long;
	}

	public static String getCancel_charge() {
		return cancel_charge;
	}

	public static String getCustomer_pick_up_name() {
		return customer_pick_up_name;
	}

	public static String getCustomer_destination_name() {
		return customer_destination_name;
	}

	private static String trip_distance_KM;
	private static String trip_distance_miles;
	private static String trip_duration;

	// added by sudeep
	private static String start_location_name;
	private static String end_location_name;
	private static String list_car_image;
	private static String no_of_seats;
	private static String vehicle_type;
	private static String driver_profile_pic;

	public static String getEnd_location_name() {
		return end_location_name;
	}

	public static void setEnd_location_name(String end_location_name) {
		DriverParser.end_location_name = end_location_name;
	}

	public static String getStart_location_name() {
		return start_location_name;
	}

	public static void setStart_location_name(String start_location_name) {
		DriverParser.start_location_name = start_location_name;
	}

	public static String getList_car_image() {
		return list_car_image;
	}

	public static void setList_car_image(String list_car_image) {
		DriverParser.list_car_image = list_car_image;
	}

	public static String getNo_of_seats() {
		return no_of_seats;
	}

	public static void setNo_of_seats(String no_of_seats) {
		DriverParser.no_of_seats = no_of_seats;
	}

	public static String getVehicle_type() {
		return vehicle_type;
	}

	public static void setVehicle_type(String vehicle_type) {
		DriverParser.vehicle_type = vehicle_type;
	}

	public static String getDriver_profile_pic() {
		return driver_profile_pic;
	}

	public static void setDriver_profile_pic(String driver_profile_pic) {
		DriverParser.driver_profile_pic = driver_profile_pic;
	}

	private static String payment_mode;
	private static String total;

	private static String discount_total;
	private static String discount_applied;


	public static String driverEndRide(String data) {
		try {

			JSONObject dataObject = new JSONObject(data);

			status = dataObject.optString("status");

			if (status.equals("TRUE")) {

				status_message = dataObject.optString("message").toString();
				trip_distance_KM = dataObject.optString("trip_distance_KM")
						.toString();
				trip_distance_miles = dataObject.optString(
						"trip_distance_miles").toString();
				trip_duration = dataObject.optString("trip_duration")
						.toString();
				payment_mode = dataObject.optString("payment_mode").toString()
						.trim();
				total = dataObject.optString("total").toString();

				discount_total = dataObject.optString("discount_total").toString();

				discount_applied = dataObject.optString("discount_applied").toString();

				start_location_name = dataObject.optString("start_location_name").toString();
				end_location_name = dataObject.optString("end_location_name").toString();
				no_of_seats = dataObject.optString("no_of_seats").toString();
				vehicle_type = dataObject.optString("vehicle_type").toString();
				list_car_image = dataObject.optString("list_car_image").toString();
				driver_profile_pic = dataObject.optString("driver_profile_pic").toString();




				Log.d("Inside true", "" + status_message);
			}

			else {
				status_message = dataObject.optString("message").toString();

			}
		}

		catch (Exception e) {

		}
		return status;
	}

	public static String getTrip_distance_KM() {
		return trip_distance_KM;
	}

	public static String getTrip_distance_miles() {
		return trip_distance_miles;
	}

	public static String getTrip_duration() {
		return trip_duration;
	}

	public static String getPayment_mode() {
		return payment_mode;
	}

	public static String getTotal() {
		return total;
	}

	public static String getReason() {
		return reason;
	}

	public static void setReason(String reason) {
		DriverParser.reason = reason;
	}

	public static String getSuspension_reason() {
		return suspension_reason;
	}

	public static void setSuspension_reason(String suspension_reason) {
		DriverParser.suspension_reason = suspension_reason;
	}

	public static String getCar_is_suspended() {
		return car_is_suspended;
	}

	public static void setCar_is_suspended(String car_is_suspended) {
		DriverParser.car_is_suspended = car_is_suspended;
	}

	public static String getCar_is_suspended_reason() {
		return car_is_suspended_reason;
	}

	public static void setCar_is_suspended_reason(String car_is_suspended_reason) {
		DriverParser.car_is_suspended_reason = car_is_suspended_reason;
	}

	public static String getOffer_status() {
		return offer_status;
	}

	public static void setOffer_status(String offer_status) {
		DriverParser.offer_status = offer_status;
	}

	public static String getOffer_code() {
		return offer_code;
	}

	public static void setOffer_code(String offer_code) {
		DriverParser.offer_code = offer_code;
	}

	public static String getOffer_message() {
		return offer_message;
	}

	public static void setOffer_message(String offer_message) {
		DriverParser.offer_message = offer_message;
	}

	public static ArrayList<Offers> getOffer_list() {
		return offer_list;
	}

	public static void setOffer_list(ArrayList<Offers> offer_list) {
		DriverParser.offer_list = offer_list;
	}

	public static String getDiscount_total() {
		return discount_total;
	}

	public static void setDiscount_total(String discount_total) {
		DriverParser.discount_total = discount_total;
	}

	public static String getDiscount_applied() {
		return discount_applied;
	}

	public static void setDiscount_applied(String discount_applied) {
		DriverParser.discount_applied = discount_applied;
	}

	public static String getCurrent_request_id() {
		return current_request_id;
	}

	public static void setCurrent_request_id(String current_request_id) {
		DriverParser.current_request_id = current_request_id;
	}

	public static String getCurrent_status_message() {
		return current_status_message;
	}

	public static void setCurrent_status_message(String current_status_message) {
		DriverParser.current_status_message = current_status_message;
	}

	public static boolean isDriver_status() {
		return driver_status;
	}

	public static void setDriver_status(boolean driver_status) {
		DriverParser.driver_status = driver_status;
	}

	public static int getCar_is_suspended_reason_type() {
		return car_is_suspended_reason_type;
	}

	public static void setCar_is_suspended_reason_type(int car_is_suspended_reason_type) {
		DriverParser.car_is_suspended_reason_type = car_is_suspended_reason_type;
	}

	public static String getSurge_applied() {
		return surge_applied;
	}

	public static void setSurge_applied(String surge_applied) {
		DriverParser.surge_applied = surge_applied;
	}



	public static String getCustomer_mobile_number() {
		return customer_mobile_number;
	}

	public static void setCustomer_mobile_number(String customer_mobile_number) {
		DriverParser.customer_mobile_number = customer_mobile_number;
	}
}
