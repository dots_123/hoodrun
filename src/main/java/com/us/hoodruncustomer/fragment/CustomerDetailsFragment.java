package com.us.hoodruncustomer.fragment;

import android.Manifest;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.custom.CircularImageView;
import com.us.hoodruncustomer.db.AppManager;

public class CustomerDetailsFragment extends DialogFragment {
	CircularImageView customer_image;
	TextView customer_name,number_seats,payment_type,mobile_number,pickup_location,destination_location,estimated_fare;
	LinearLayout mDismiss;
	ImageView img_pay_type;
	AppManager mAppManager;
	String font_path;
	Typeface tf;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		mAppManager=AppManager.getInstance(getActivity());
		
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		View rootView = inflater.inflate(R.layout.fragment_customer_details, container, false);

	    customer_image=(CircularImageView)rootView.findViewById(R.id.customer_img);
		img_pay_type=(ImageView) rootView.findViewById(R.id.img_pay_type);
		customer_name=(TextView)rootView.findViewById(R.id.txt_customer_name);
		//number_seats=(TextView)rootView.findViewById(R.id.txt_labelseat);
		payment_type=(TextView)rootView.findViewById(R.id.txt_labelpayment);
		mobile_number=(TextView)rootView.findViewById(R.id.txt_labelseat);
		pickup_location=(TextView)rootView.findViewById(R.id.txt_pick);
		destination_location=(TextView)rootView.findViewById(R.id.txt_drop);
		estimated_fare=(TextView)rootView.findViewById(R.id.txt_estimated_fare);
		mDismiss=(LinearLayout)rootView.findViewById(R.id.layout_dismiss);
		
		font_path = "fonts/opensans.ttf";
        
	     tf = Typeface.createFromAsset(getActivity().getAssets(), font_path);
		
		setupView();
		//getDialog().setTitle("Customer Details");
		mDismiss.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			dismiss();
			}
		});
		
		
		mobile_number.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					if (checkPermission()) {
						DialogAlert();
					} else {
						requestPermission();
					}
				} catch (Exception e) {
				}

			}
		});
		
		
		return rootView;
	}

	private void setupView() {
		// TODO Auto-generated method stub
		Glide.with(this)
		.load(Uri.parse(mAppManager.getCustomerImage()))
		.placeholder(R.drawable.placeholder)
				.into(customer_image);
		customer_name.setText(mAppManager.getCustomerName());
		//number_seats.setText("No. of Seats:"+mAppManager.getCustomerSeatCount());
		if(mAppManager.getPaymentType().equals("1"))
		{
			payment_type.setText("CARD");
			img_pay_type.setImageResource(R.drawable.creditcard);
			
		}else{
			payment_type.setText("CASH");
			img_pay_type.setImageResource(R.drawable.cash);

		}
		if(mAppManager.App_getCustomer_mobile_no() != null) {
			mobile_number.setText(mAppManager.App_getCustomer_mobile_no());
		}
		pickup_location.setText(mAppManager.getCustomerPickupLocation());
		destination_location.setText(mAppManager.getCustomerDestinationLocation());
		estimated_fare.setText(getActivity().getString(R.string.pound)+""+mAppManager.getEstimatedFare());
		
	}

	Dialog dialog;
	private void DialogAlert() {

		dialog = new Dialog(getActivity(), R.style.Theme_Dialog);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.call_phone);
		dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
		dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		dialog.getWindow().setGravity(Gravity.CENTER);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();

		TextView mDialogBody = (TextView) dialog.findViewById(R.id.txt_dialog_body);
		mDialogBody.setPadding(20, 10, 20, 10);
		TextView mOK = (TextView) dialog.findViewById(R.id.txt_dialog_ok);
		TextView mCancel = (TextView) dialog.findViewById(R.id.txt_dialog_cancel);
	
		mDialogBody.setText(mAppManager.getMobileNumber());
		mDialogBody.setTypeface(tf,Typeface.BOLD);

		mOK.setText("CALL");
		mCancel.setText("CANCEL");

		mOK.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				
				call();
				
				dialog.dismiss();
			}
		});

		mCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				dialog.dismiss();

			}
		});
		
}

public boolean checkPermission() {

		int readPermissionResult = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.READ_PHONE_STATE);
		int writePermissionResult = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.CALL_PHONE);

		return readPermissionResult == PackageManager.PERMISSION_GRANTED &&
				writePermissionResult == PackageManager.PERMISSION_GRANTED;
	}

	private static int CallRequestPermissionCode = 123;
	private void requestPermission() {

		ActivityCompat.requestPermissions(getActivity(), new String[]
				{
						Manifest.permission.CALL_PHONE,
						Manifest.permission.READ_PHONE_STATE,
				}, CallRequestPermissionCode);

	}
	
	public void call()
	{

		if (mAppManager.getMobileNumber() != null) {
			try {
				if (checkPermission()) {
					Intent callIntent = new Intent(Intent.ACTION_CALL);
					callIntent.setData(Uri.parse("tel:"+mAppManager.getMobileNumber()));
					startActivity(callIntent);
				} else {
					requestPermission();
				}
			} catch (Exception e) {
			}

		}

	}
}
