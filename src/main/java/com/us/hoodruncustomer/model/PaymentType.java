package com.us.hoodruncustomer.model;

public class PaymentType {
	
	private String id;
	private String cardType;
	private String cardNumber;
	private String IsSelectedCard;
	
	public PaymentType(String id, String cardType, String cardNumber,
			String isSelectedCard) {
		super();
		this.id = id;
		this.cardType = cardType;
		this.cardNumber = cardNumber;
		IsSelectedCard = isSelectedCard;
	}
	
	


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getIsSelectedCard() {
		return IsSelectedCard;
	}
	public void setIsSelectedCard(String isSelectedCard) {
		IsSelectedCard = isSelectedCard;
	}
	

}
