package com.us.hoodruncustomer.fragment;


import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.authentication.LoginActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class InvalidTokenDialog extends Dialog{

	Activity a;
	TextView mTitle,mBody,mOk;
	String font_path;
	Typeface tf;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawable(new ColorDrawable(0));
		setContentView(R.layout.dialog_onebutton);
		
	
	
	mTitle=(TextView)findViewById(R.id.text_title);
		mBody=(TextView)findViewById(R.id.txt_dialog_body);
		mOk=(TextView)findViewById(R.id.txt_dialog_ok);
		
		font_path = "fonts/opensans.ttf";
        
	     tf = Typeface.createFromAsset(a.getAssets(), font_path);
		
	 	setCancelable(false);
		setCanceledOnTouchOutside(false);
	     
		setupView();
		mOk.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				dismiss();
				Intent myintent = new Intent(a,
						LoginActivity.class);
				a.startActivity(myintent);
				a.finish();

			}
		});
	}

	private void setupView() {
		// TODO Auto-generated method stub
		
		
		mTitle.setText("ALERT");
		mTitle.setTypeface(tf, Typeface.BOLD);

		mBody.setText("You have already logged in from other device"
				+ "\n" + "Please login to continue..");

		mOk.setText("OK");
	}

	public InvalidTokenDialog(Activity a) {
		super(a);
		this.a=a;
		
	}


	


}
