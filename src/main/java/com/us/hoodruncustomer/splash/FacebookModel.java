package com.us.hoodruncustomer.splash;

/**
 * Created by admin on 5/11/2018.
 */

class FacebookModel {
    private String userId;
    private String username;
    private String firstName;
    private String middleName;
    private String lastName;
    private String email;
    private String birthday;
    private int gender;
    private String profileLink;

    public FacebookModel(String userId, String username, String firstName, String middleName, String lastName, String email, String birthday, int gender, String profileLink) {
        this.userId = userId;
        this.username = username;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.email = email;
        this.birthday = birthday;
        this.gender = gender;
        this.profileLink = profileLink;
    }

    public FacebookModel() {
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMiddleName() {
        return this.middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getBirthday() {
        return this.birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getGender() {
        return this.gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getProfileLink() {
        return this.profileLink;
    }

    public void setProfileLink(String profileLink) {
        this.profileLink = profileLink;
    }

    public String toString() {
        return "SmartUser{userId='" + this.userId + '\'' + ", username='" + this.username + '\'' + ", firstName='" + this.firstName + '\'' + ", middleName='" + this.middleName + '\'' + ", lastName='" + this.lastName + '\'' + ", email='" + this.email + '\'' + ", birthday='" + this.birthday + '\'' + ", gender=" + this.gender + ", profileLink='" + this.profileLink + '\'' + '}';
    }
}
