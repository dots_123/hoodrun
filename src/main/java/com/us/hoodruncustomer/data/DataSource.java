package com.us.hoodruncustomer.data;


import android.content.Context;

import java.util.Map;

/**
 * Created by admin
 * Created on 11/10/2016.
 * Modified on 10,November,2016
 */
public interface DataSource {
    void loginService(String userName, String password, LoginRsponseI loginPresenter, Context context);

    void googleLoginService(String googleId, String device_id, String os, String name, String email, GoogleLoginRsponseI loginPresenter, Context context);

    void fbLoginService(String googleId, String device_id, String os, String name, String email, FbLoginRsponseI loginPresenter, Context context);

    void updateDataService(String deviceCode, String id_type, UpdateDataI loginPresenter, Context context);

    void forgotPassword(String email, ForgotPasswordI loginPresenter, Context context);

    void getTokenService(String customerId, String userType, AddPaymentI loginPresenter, Context context);


    void getCarsService(String lat, String lng, String token_code, String city, String checkCity, String customerId, CarsResponseI carsPresnetrs, Context context);

    void registerService(String fb_id, String google_id, String name, String email, String password, String mobile_number, String profile_picture, String device_id, String os, RegisterResponseI loginPresenter, Context context);

    void savePaymentData(Map<String, String> params, String url, AddPaymentI loginPresenter, Context context);


    void saveOtp(String customerId, String otp, String email, String mobilenumber, String url, RegisterResponseI loginPresenter, Context context);

    void resendOtp(String customerId, String tokencode, String email, String mobilenumber, String url, RegisterResponseI loginPresenter, Context context);

    void getEstimates(Map<String, String> params, ConfirmRideI loginPresenter, Context context);

    void driverSelect(Map<String, String> params, ConfirmRideI loginPresenter, Context context);

    void checkPromoCode(Map<String, String> params, ConfirmRideI loginPresenter, Context context);

    void getHistory(Map<String, String> params, RideHistoryI loginPresenter, Context context);

    void getCards(String tokenCode, String customerId, PaymentI loginPresenter, Context context);

    void cusCancelChargeservice(String tokenCode, String customerId, TrackRideI loginPresenter, Context context);

    void customerCancelRideservice(int canstart, TrackRideI loginPresenter, Context context);

    void checkUserEndRideService(String tokenCode, String rideId, TrackRideI loginPresenter, Context context);


    void rideRatingservice(String tokencode, String rateValue, String rideId, RideRatingI loginPresenter, Context context);

    void shareInfoservice(String customerId, String tokenCode, String shareStatus, String requestId, ShareI loginPresenter, Context context);


    void mobileVerificationService(Map<String, String> params, ShareI loginPresenter, Context context);

    void shareFareService(String tokencode, String requestId, ShareI loginPresenter, Context context);


    interface RegisterResponseI {
        public void registerResponse(String baseResponse);

        public void onFail(Throwable t);


        public void onResendResponse(String response);

        public void onSaveOtpResponse(String response);

        public void onFailResend(Throwable t);
    }

    interface ShareI {
        public void shareInfoResponse(String baseResponse);

        public void onFail(Throwable t);

        public void mobileVerificationResponse(String response);

        public void shareFareResponse(String response);

        public void mobileVerifyFail(Throwable t);

        public void shareFareFail(Throwable t);
    }

    interface AddPaymentI {
        public void getTokenResponse(String baseResponse);

        public void onTokenFail(Throwable t);

        public void onPaymentResponse(String respnse);

        public void onFail(Throwable t);
    }

    interface RideRatingI {
        public void rideRatingResopnse(String baseResponse);

        public void onRatingFail(Throwable t);
    }

    interface RideHistoryI {
        public void rideHistoryResponse(String baseResponse);

        public void onFail(Throwable t);

    }

    interface PaymentI {
        public void getCardsResponse(String baseResponse);

        public void onFail(Throwable t);

    }


    interface CarsResponseI {
        public void getcarsResponse(String baseResponse);

        public void onFail(Throwable t);


    }

    interface TrackRideI {

        void cusCancellationChargeResponse(String response);

        void custCancelRideResponse(String response);

        void userCheckIsRideEndData(String response);

        public void onFail(Throwable t);

        void onFailRideEndd(Throwable t);

    }

    interface ConfirmRideI {
        public void getEstimatess(String response);

        public void onFail(Throwable t);

        public void onDriverSelectResponse(String response);

        public void onCheckPromocodeResponse(String response);

        public void onDriverSelecterror(Throwable t);

        public void onCheckPromoerror(Throwable t);
    }

    interface LoginRsponseI {
        public void loginResponse(String baseResponse);

        public void onFail(Throwable t);


    }

    interface GoogleLoginRsponseI {
        public void googleloginResponse(String baseResponse);

        public void onFail(Throwable t);


    }

    interface FbLoginRsponseI {
        public void fbloginResponse(String baseResponse);

        public void onFail(Throwable t);


    }

    interface UpdateDataI {
        public void updateDataResponse(String baseResponse);

        public void onUpdateDataFail(Throwable t);

    }

    interface ForgotPasswordI {
        public void forgotDataResponse(String baseResponse);

        public void onforgotFail(Throwable t);

    }


}
