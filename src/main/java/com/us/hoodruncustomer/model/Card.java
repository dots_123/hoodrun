package com.us.hoodruncustomer.model;

public class Card {
	
	public  String customer_id;
	public  String card_type;
	public  String payment_token;
	public  String card_no;
	

	public  String card_id;
	public  String getCard_id() {
		return card_id;
	}
	public  void setCard_id(String card_id) {
		this.card_id = card_id;
	}
	public  String getCustomer_id() {
		return customer_id;
	}
	public  void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	public  String getCard_type() {
		return card_type;
	}
	public  void setCard_type(String card_type) {
		this.card_type = card_type;
	}
	public  String getPayment_token() {
		return payment_token;
	}
	public  void setPayment_token(String payment_token) {
		this.payment_token = payment_token;
	}
	public  String getCard_no() {
		return card_no;
	}
	public  void setCard_no(String card_no) {
		this.card_no = card_no;
	}

}
