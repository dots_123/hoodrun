package com.us.hoodruncustomer.data.remote;


import android.content.Context;
import android.net.Uri;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.configure.Configure;
import com.us.hoodruncustomer.data.DataSource;
import com.us.hoodruncustomer.db.AppManager;


import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by admin
 * Created on 11/10/2016.
 * Modified on 10,November,2016
 */
public class RemoteDataSource implements DataSource {
    private static RemoteDataSource INSTANCE;


    public static RemoteDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RemoteDataSource();
        }
        return INSTANCE;
    }

    private RemoteDataSource() {

    }


    @Override
    public void loginService(final String email, final String password, final DataSource.LoginRsponseI loginPresenter, Context context) {

        final AppManager mAppManager = AppManager.getInstance(context);
       /* JSONObject object1=new JSONObject();

        Map<String, String> params = new HashMap<String, String>();
        params.put("email", email);
        params.put("password", password);
        params.put("device_id", "1234");
        params.put("os", "1");

        try {

           // Call<BaseResponse<LoginData>> responceCall = networkCall.getRetrofit(true).callLoginService(body);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_login,  networkCall.getResponse(),networkCall.getErrorListener());
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            int socketTimeout = 60000;//60 seconds - change to what you want
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRequest.setRetryPolicy(policy);
            requestQueue.add(stringRequest);
        }catch (Exception e){
            e.printStackTrace();
        }*/


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_login,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.loginResponse(response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginPresenter.onFail(error);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", password);
                params.put("app", "customer");
                params.put("device_id", mAppManager.getDeviceId());
                params.put("os", "1");

                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);

    }

    @Override
    public void googleLoginService(final String googleId, String device_id, String os, final String name, final String email, final GoogleLoginRsponseI loginPresenter, Context context) {

        final AppManager mAppManager = AppManager.getInstance(context);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_google_login,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.googleloginResponse(response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginPresenter.onFail(error);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {

//                {"facebook_id" =>""1234567", "device_id" => "123", "os" => "1", "name" => "test", "email" => "test@yopmail.com"}
//                {"google_id" =>""1234567", "device_id" => "123", "os" => "1", "name" => "test", "email" => "test@yopmail.com"}

                Map<String, String> params = new HashMap<String, String>();
                params.put("google_id", googleId);
                params.put("device_id", mAppManager.getDeviceId());
                params.put("os", "1");
                params.put("name", name);
                params.put("email", email);

                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);

    }

    @Override
    public void fbLoginService(final String googleId, String device_id, String os, final String name, final String email, final FbLoginRsponseI loginPresenter, Context context) {
        final AppManager mAppManager = AppManager.getInstance(context);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_fb_login,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.fbloginResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginPresenter.onFail(error);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

//              {"facebook_id" =>""1234567", "device_id" => "123", "os" => "1", "name" => "test", "email" => "test@yopmail.com"}
//              {"google_id" =>""1234567", "device_id" => "123", "os" => "1", "name" => "test", "email" => "test@yopmail.com"}

                Map<String, String> params = new HashMap<String, String>();
                params.put("facebook_id", googleId);
                params.put("device_id", mAppManager.getDeviceId());
                params.put("os", "1");
                params.put("name", name);
                params.put("email", email);

                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    @Override
    public void updateDataService(final String deviceCode, final String id_type, final UpdateDataI loginPresenter, Context context) {

        final AppManager mAppManager = AppManager.getInstance(context);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_ride_completeness_customer,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.updateDataResponse(response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginPresenter.onUpdateDataFail(error);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("token_code", deviceCode);

                params.put("customer_id", id_type);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);

    }

    @Override
    public void forgotPassword(final String email, final ForgotPasswordI loginPresenter, Context context) {


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_forgot_password,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.forgotDataResponse(response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginPresenter.onforgotFail(error);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email_id", email);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);

    }

    @Override
    public void getTokenService(final String customerId, final String userType, final AddPaymentI loginPresenter, Context context) {


        final AppManager mAppManager = AppManager.getInstance(context);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_braintree_token,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.getTokenResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginPresenter.onTokenFail(error);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", customerId);
                params.put("user_type", userType);

                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    @Override
    public void getCarsService(final String lat, final String lng, final String token_code, final String city, String checkCity, final String customerId, final CarsResponseI carsPresnetrs, Context context) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_cities,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        carsPresnetrs.getcarsResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        carsPresnetrs.onFail(error);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("lat", "" + lat);
                params.put("lng", "" + lng);
                params.put("token_code", token_code);
                params.put("city_name", city);
                params.put("check_city", "true");
                params.put("customer_id", customerId);
                return params;
            }

        };

        MyApplication application = (MyApplication) context.getApplicationContext();

        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        application.requestQueue.add(stringRequest);
    }

    @Override
    public void registerService(final String fb_id, final String google_id, final String name, final String email, final String password, final String mobile_number, final String profile_picture, final String device_id, String os, final RegisterResponseI loginPresenter, Context context) {

        AndroidNetworking.post(Configure.URL_registration)
                .addBodyParameter("name", name)
                .addBodyParameter("email", email)
                .addBodyParameter("password", (password==null ? "" : password))
                .addBodyParameter("facebook_id", (fb_id==null ? "" : fb_id))
                .addBodyParameter("google_id", (google_id==null ? "" : google_id))
                .addBodyParameter("mobile_number", mobile_number)
                .addBodyParameter("profile_picture", profile_picture)
                .addBodyParameter("device_id", device_id)
                .addBodyParameter("os", "1")
                .setTag("test")
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.registerResponse(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        loginPresenter.registerResponse(anError.toString());
                    }
                });

       /* StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_registration,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.registerResponse(response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginPresenter.onFail(error);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("name", name);
                params.put("email", email);
                params.put("password", (password==null ? "" : password));

                params.put("facebook_id", (fb_id==null ? "" : fb_id));
                params.put("google_id", (google_id==null ? "" : google_id));

                params.put("mobile_number", mobile_number);
                params.put("profile_picture", profile_picture);
                params.put("device_id", device_id);
                params.put("os", "1");

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);*/
    }

    @Override
    public void savePaymentData(final Map<String, String> params, String url, final AddPaymentI loginPresenter, Context context) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.onPaymentResponse(response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginPresenter.onFail(error);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {


                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    @Override
    public void saveOtp(final String customerId, final String otp, final String email, final String mobilenumber, String url, final RegisterResponseI loginPresenter, Context context) {

        final AppManager appManager = AppManager.getInstance(context);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.onSaveOtpResponse(response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginPresenter.onFail(error);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("customer_id", customerId);
                params.put("otp", otp);
                if (appManager.getOtpDestiantion().equals("email"))
                    params.put("email", email);
                else
                    params.put("mobile_number", mobilenumber);
                return params;
            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    @Override
    public void resendOtp(final String customerId, String tokencode, String email, String mobilenumber, String url, final RegisterResponseI loginPresenter, Context context) {


        final AppManager appManager = AppManager.getInstance(context);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.onResendResponse(response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginPresenter.onFailResend(error);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("customer_id", customerId);
                if (appManager.getOtpDestiantion().equals("mobile"))
                    params.put("mobile_number", appManager.getMobileNumber());
                else {
                    params.put("token_code", appManager.getTokenCode());
                    params.put("email", appManager.getEmail());
                }

                return params;
            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);

    }

    @Override
    public void getEstimates(final Map<String, String> params, final ConfirmRideI loginPresenter, Context context) {


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_request_info,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.getEstimatess(response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginPresenter.onFail(error);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {


                return params;
            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);


    }

    @Override
    public void driverSelect(final Map<String, String> params, final ConfirmRideI loginPresenter, Context context) {


        final AppManager appManager = AppManager.getInstance(context);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_driver,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.onDriverSelectResponse(response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginPresenter.onDriverSelecterror(error);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {


                return params;
            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    @Override
    public void checkPromoCode(final Map<String, String> params, final ConfirmRideI loginPresenter, Context context) {


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_promocode,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.onCheckPromocodeResponse(response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginPresenter.onCheckPromoerror(error);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {


                return params;
            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);


    }

    @Override
    public void getHistory(final Map<String, String> params, final RideHistoryI loginPresenter, Context context) {


        final AppManager appManager = AppManager.getInstance(context);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_ride_history_customer,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.rideHistoryResponse(response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginPresenter.onFail(error);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {


                return params;
            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    @Override
    public void getCards(final String tokenCode, final String customerId, final PaymentI loginPresenter, Context context) {


        final AppManager appManager = AppManager.getInstance(context);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_saved_cards,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.getCardsResponse(response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginPresenter.onFail(error);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token_code", tokenCode);
                params.put("customer_id", customerId);

                return params;
            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);

    }

    @Override
    public void cusCancelChargeservice(final String tokenCode, final String customerId, final TrackRideI loginPresenter, Context context) {


        final AppManager appManager = AppManager.getInstance(context);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_customer_cancellation_charge,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.cusCancellationChargeResponse(response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginPresenter.onFail(error);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token_code", tokenCode);
                params.put("ride_id", customerId);

                return params;
            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    @Override
    public void customerCancelRideservice(final int cancel_stat, final TrackRideI loginPresenter, Context context) {

        final AppManager mAppManager = AppManager.getInstance(context);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_customer_cancel,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.custCancelRideResponse(response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginPresenter.onFail(error);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token_code", mAppManager.getTokenCode());
                params.put("customer_id", mAppManager.getCustomerID());

                if (cancel_stat == 0) {
                    params.put("request_id", mAppManager.App_getDriver_request_id());

                } else

                    params.put("ride_id", mAppManager.App_getDriver_ride_id());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);


    }

    @Override
    public void checkUserEndRideService(final String tokenCode, final String rideId, final TrackRideI loginPresenter, Context context) {


        final AppManager appManager = AppManager.getInstance(context);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_user_check_end_ride,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.userCheckIsRideEndData(response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginPresenter.onFailRideEndd(error);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token_code", tokenCode);
                params.put("ride_id", rideId);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);

    }


    @Override
    public void rideRatingservice(final String tokencode, final String rateValue, final String rideId, final RideRatingI loginPresenter, Context context) {


        final AppManager appManager = AppManager.getInstance(context);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_ride_rating,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.rideRatingResopnse(response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginPresenter.onRatingFail(error);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token_code", tokencode);
                params.put("rate_ride", rateValue);
                params.put("ride_id", rideId);

                return params;
            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);

    }

    @Override
    public void shareInfoservice(final String customerId, final String tokenCode, final String shareStatus, final String requestId, final ShareI loginPresenter, Context context) {


        final AppManager appManager = AppManager.getInstance(context);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_share_response,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.shareInfoResponse(response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginPresenter.onFail(error);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("customer_id",
                        customerId);
                params.put("request_id",
                        requestId);
                params.put("token_code",
                        tokenCode);
                params.put("share_status",
                        shareStatus);

                return params;
            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);

    }

    @Override
    public void mobileVerificationService(final Map<String, String> params, final ShareI loginPresenter, Context context) {
        final AppManager appManager = AppManager.getInstance(context);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_share_number_verification,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.mobileVerificationResponse(response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginPresenter.mobileVerifyFail(error);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {

                return params;
            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);

    }

    @Override
    public void shareFareService(final String tokencode, final String requestId, final ShareI loginPresenter, Context context) {
        final AppManager appManager = AppManager.getInstance(context);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_share_number,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loginPresenter.shareFareResponse(response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loginPresenter.shareFareFail(error);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token_code", tokencode);
                params.put("request_id", requestId);

                return params;
            }


        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);

    }

}
