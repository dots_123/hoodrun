package com.us.hoodruncustomer.commonwork;


import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.CustomVolleyRequestQueue;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.configure.Configure;
import com.us.hoodruncustomer.db.AppManager;

import com.us.hoodruncustomer.fcm.MyFirebaseMessagingService;
import com.us.hoodruncustomer.fragment.JobDialog;
import com.us.hoodruncustomer.fragment.PushDialog;
import com.us.hoodruncustomer.fragment.RateDialog;
import com.us.hoodruncustomer.nodeservice.ConnectorService;
import com.us.hoodruncustomer.nodeservice.NodeUtil;
import com.us.hoodruncustomer.parser.DriverParser;
import com.us.hoodruncustomer.payment.RiderPaymentActivity;
import com.us.hoodruncustomer.splash.SplashActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.socket.client.Socket;

public class SuperBaseActivity extends AppCompatActivity {

    public static String push_flag = "", push_message = "";
    public String push_ride_id;
    public static Context mContext;
    public static Activity mActivity;
    public static FragmentManager fm;
    public static String status_id;
    public NotificationManager myNManager;
    public Typeface tf;
    private boolean isRideShowing;
    public String font_path;
    public AppManager mAppManager;
    public Socket mSocket;
    public MyApplication app;
    public Dialog loader;
    public ProgressWheel loader_wheel;
    public static boolean drivercurrentStatus = false;
    public static final String RECEIVER_FUTURE_REQUEST = "com.uk.hoponn.future_request_id";
    public static final String RECEIVER_REFER_OFFER = "com.uk.hoponn.refer_offer_get";
    public static final String RECEIVER_SUSPEND_DRIVER_SUPER = "com.uk.hoponn.receiver_driver";
    public static final String RECEIVER_SUSPEND_CUSTOMER_SUPER = "com.uk.hoponn.receiver_customer_super";
    public static final String RECEIVER_SUSPEND_CUSTOMER_NEW_RIDE = "com.uk.hoponn.receiver_customer_new_ride_super";
    public static final String RECEIVER_SUSPEND_CUSTOMER_Time_ExPIRY = "com.uk.hoponn.receiver_customer_time_expiry";
    public String driver_id;
    private boolean rideShowed;
    private String lastrequest_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.apprunning = 1;
        app = (MyApplication) getApplication();
        mSocket = app.getSocket();
        mContext = this;
        mActivity = this;
        font_path = "fonts/opensans.ttf";
        mAppManager = AppManager.getInstance(this);
        tf = Typeface.createFromAsset(this.getAssets(), font_path);
        myNManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        fm = getFragmentManager();
        //token = mAppManager.getTokenCode();
        driver_id = mAppManager.getDriverID_Number();

        /**
         * Initialize Loader
         */

        loader = new Dialog(this);
        loader.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loader.setContentView(R.layout.dialog_progress);
        loader_wheel = (ProgressWheel) loader.findViewById(R.id.progress_wheel);
        loader_wheel.setCircleRadius(50);
        loader.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        loader.setCancelable(false);

        switch (MyFirebaseMessagingService.push_Status_Flag) {
            case "16":
                PushDialog pDialog = new PushDialog(mContext, MyFirebaseMessagingService.messages, MyFirebaseMessagingService.push_Status_Flag);
                pDialog.show();
                MyApplication.dialogShown();
                break;
            case "17":
                PushDialog pDialog1 = new PushDialog(mContext, MyFirebaseMessagingService.messages, MyFirebaseMessagingService.push_Status_Flag);
                pDialog1.show();
                MyApplication.dialogShown();
                break;
            case "19":
                PushDialog pDialog2 = new PushDialog(mContext, MyFirebaseMessagingService.messages, MyFirebaseMessagingService.push_Status_Flag);
                pDialog2.show();
                MyApplication.dialogShown();
                break;
            case "20":
                PushDialog pDialog3 = new PushDialog(mContext, MyFirebaseMessagingService.messages, MyFirebaseMessagingService.push_Status_Flag);
                pDialog3.show();
                MyApplication.dialogShown();
                break;
            case "7":
                PushDialog pDialog4 = new PushDialog(mContext, MyFirebaseMessagingService.messages, MyFirebaseMessagingService.push_Status_Flag);
                pDialog4.show();
                MyApplication.dialogShown();
                break;
            default:
                break;
        }

    }

    public static class MyFcmReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub

            Bundle extras = intent.getExtras();
            if (extras != null) {
                if (extras.containsKey("push flag")) {
                    push_flag = extras.getString("push flag");
                    push_message = extras.getString("push message");
                }
            }
            System.out.println("PPP===" + push_flag + push_message);
            switch (push_flag) {
                case "1":
                    if (!mActivity.isFinishing()) {

                        if (!MyApplication.isjobDialogVisible()) {
                            MyApplication.jobdialogShown();
                            JobDialog jDialog = new JobDialog(mContext, push_message, push_flag);
                            jDialog.show();
                        }
                    }
                    break;
                case "6":
                    if (MyApplication.getAppActivity().getClass().getSimpleName().equals("TrackRide")) {
                        PushDialog pDialog = new PushDialog(mContext, push_message, push_flag);
                        pDialog.show();
                        MyApplication.dialogShown();
                    }
                    break;
                case "8":
                    if (!mActivity.isFinishing()) {
                        RateDialog rDialog = new RateDialog(mContext);

                        if (rDialog != null && !rDialog.isShowing()) {
                            try {
                                rDialog.show();
                            } catch (Exception e) {

                            }
                        }

                        MyApplication.dialogShown();
                    }
                    break;

                case "13":

                    if (!mActivity.isFinishing()) {
                        try {
                            PushDialog pDialog = new PushDialog(mContext, push_message, push_flag);
                            pDialog.show();
                            MyApplication.dialogShown();
                        } catch (Exception e) {

                        }
                    }
                    break;

                default:

                    if (!mActivity.isFinishing()) {
                        PushDialog pDialog = new PushDialog(mContext, push_message, push_flag);
                        pDialog.show();
                        MyApplication.dialogShown();

                    }

                    break;

            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (dialogfuture2 != null && !dialogfuture2.isShowing()) {
            dialogfuture2.dismiss();
            dialogfuture2 = null;
        }
        drivercurrentStatus = true;
        try {
            registerReceiver(this.Node_OnConnect, new IntentFilter(NodeUtil.NODE_CONNECTION));
            registerReceiver(this.Node_OnDisConnect, new IntentFilter(NodeUtil.NODE_DISCONNECTION));
            registerReceiver(this.Node_Driver_Requests, new IntentFilter(NodeUtil.NODE_DRIVER_REQUESTS));
            registerReceiver(this.Node_Driver_Requests_Future, new IntentFilter(NodeUtil.NODE_DRIVER_FUTURE_REQUESTS));
            registerReceiver(this.NODE_CUSTOMER_LOGOUT, new IntentFilter(NodeUtil.NODE_CUSTOMER_LOGOUT));
            if (mAppManager.getuser().equalsIgnoreCase("customer")) {
                registerReceiver(this.NODE_CUSTOMER_CONFIRM_PAYMENT, new IntentFilter(NodeUtil.NODE_CUSTOMER_CONFIRM_PAYMENT));
            }

            if (mAppManager.getTokenCode() != null && !mAppManager.getTokenCode().isEmpty()) {
                if (!mSocket.connected()) {
                    Log.d("NodeConnection", "SuperBaseActivity-onResume");
                    Intent start_Serv = new Intent(getApplicationContext(), ConnectorService.class);
                    startService(start_Serv);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!NotificationManagerCompat.from(this).areNotificationsEnabled()) {
            if (ad != null) {
                ad.dismiss();
                ad = null;
            }
            showNotificationAlert();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        drivercurrentStatus = false;
        try {
            unregisterReceiver(Node_OnConnect);
            unregisterReceiver(Node_OnDisConnect);
            unregisterReceiver(Node_Driver_Requests_Future);
            unregisterReceiver(Node_Driver_Requests);
            unregisterReceiver(NODE_CUSTOMER_LOGOUT);

            if (mAppManager.getuser().equalsIgnoreCase("customer")) {
                unregisterReceiver(NODE_CUSTOMER_CONFIRM_PAYMENT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private BroadcastReceiver Node_OnConnect = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {

            String connctionStatus = in.getStringExtra("Object");

            Log.d("Node_OnConnect", "Node_OnConnect=======" + connctionStatus);

        }

    };

    private BroadcastReceiver NODE_CUSTOMER_CONFIRM_PAYMENT = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {

            String connctionStatus = in.getStringExtra("Object");

            try {
                JSONObject obj = new JSONObject(connctionStatus);

                String origin = obj.getString("origin");

                String message = obj.getString("message");

                if (origin.equalsIgnoreCase("driver")) {

                    if (mAppManager.getuser().equalsIgnoreCase("customer")) {

                        if (confirmation != null) {
                            confirmation.dismiss();
                            confirmation = null;
                        }
                        Dialogconfirmation(message);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    };

    private BroadcastReceiver NODE_CUSTOMER_LOGOUT = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {

            if (!mActivity.isFinishing()) {
                if (logoutconfirmation != null) {
                    logoutconfirmation.dismiss();
                    logoutconfirmation = null;
                }
                try {
                    logoutConfirmation("You are already logged in from another device .");
                } catch (Exception e) {

                }
            }

        }

    };


    private BroadcastReceiver Node_OnDisConnect = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {

            String connctionStatus = in.getStringExtra("Object");
            Log.d("Node_OnConnect", "Node_DisOnConnect=======" + connctionStatus);

            if (!mSocket.connected()) {
                Log.d("NodeConnection", "SuperBase-allowDriver");
                if (mAppManager.getTokenCode() != null && !mAppManager.getTokenCode().isEmpty()) {
                    Intent start_Serv = new Intent(getApplicationContext(), ConnectorService.class);
                    startService(start_Serv);
                }
            }

        }

    };

    private BroadcastReceiver Node_Driver_Requests_Future = new BroadcastReceiver() {

        @Override
        public void onReceive(Context contexts, Intent in) {

            if (mAppManager.getuser().equalsIgnoreCase("driver")) {

                String request = in.getStringExtra("Object");

                try {

                    System.out.println("Sresponse" + request.toString());

                    if (MyApplication.last_request_id.equals(DriverParser.getRequest_id())) {

                    } else {
                        MyApplication.last_request_id = DriverParser.getRequest_id();

                        boolean parsresp = DriverParser.Future_driverAccept(request);

                        if (parsresp) {

                            if (DriverParser.getFuture_status_message().equals("Driver is not ready for the ride")) {

                            } else {

                                if (!mActivity.isFinishing() && !MyApplication.isDialogVisible()) {
                                    if (!isRideShowing) {
                                        isRideShowing = true;
                                        MyApplication.startPlayer(R.raw.hoponn_request);
                                        DialogDriverFuture();
                                    }

                                }
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }

    };

    private BroadcastReceiver Node_Driver_Requests = new BroadcastReceiver() {

        @Override
        public void onReceive(Context contexts, Intent in) {

            Log.d("SUPER_BASE", "InNewRide - NODE: ");

            if (mAppManager.getuser().equalsIgnoreCase("driver")) {

                String request = in.getStringExtra("Object");

                try {

                    System.out.println("Sresponse--" + request.toString());

                    boolean parsresp = DriverParser.driverAccept(request);

                    if (MyApplication.last_request_id.equals(DriverParser.getRequest_id())) {

                    } else {
                        MyApplication.last_request_id = DriverParser.getRequest_id();

                        if (parsresp) {

                            if (DriverParser.getStatusMessage().equals("Driver is not ready for the ride")) {

                            } else {
                                if (!isRideShowing) {
                                    isRideShowing = true;
                                    MyApplication.startPlayer(R.raw.hoponn_request);
                                    if (dialog2 != null) {
                                        dialog2.dismiss();
                                        dialog2 = null;
                                    }

                                    if (mAppManager.getRequest_ID() != null && !mAppManager.getRequest_ID().equalsIgnoreCase("")) {

                                    } else {
                                        mAppManager.App_setCustomer_mobile_no(DriverParser.getMobile_no());
                                    }

                                    if (!MyApplication.currentActivity.equalsIgnoreCase("CustomerDetailsActivity")) {
                                        DialogDriver();
                                    }
                                }
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }

    };


    Dialog dialogfuture;

    private void DialogDriverFuture() {

        if (!mActivity.isFinishing()) {

            dialogfuture = new Dialog(SuperBaseActivity.this, R.style.Theme_Dialog);
            dialogfuture.requestWindowFeature(Window.FEATURE_NO_TITLE);
            Window window = dialogfuture.getWindow();
            window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);
            dialogfuture.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialogfuture.setContentView(R.layout.dialog_onebutton);
            dialogfuture.setCancelable(false);
            dialogfuture.setCanceledOnTouchOutside(false);

            if (dialogfuture != null && !dialogfuture.isShowing()) {

                dialogfuture.show();
            }

            TextView mTitle = (TextView) dialogfuture.findViewById(R.id.text_title);
            mTitle.setTypeface(tf, Typeface.BOLD);
            TextView mDialogBody = (TextView) dialogfuture
                    .findViewById(R.id.txt_dialog_body);
            TextView mOK = (TextView) dialogfuture.findViewById(R.id.txt_dialog_ok);

            mTitle.setText("A CUSTOMER IS WAITING");

            mDialogBody.setText("At" + "\n"
                    + DriverParser.getCustomer_pick_up_name());

            mOK.setText("VIEW JOB");

            mOK.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                  /*  isRideShowing = false;
                    dialogfuture.dismiss();
                    if (MyApplication.mPlayer != null)
                        if (MyApplication.mPlayer.isPlaying()) {
                            MyApplication.stopPlayer();
                        }
                    Intent myintent = new Intent(SuperBaseActivity.this,
                            CustomerDetailsActivity.class);
                    startActivity(myintent);
                    finish();*/

                }
            });
        }

    }


    Dialog dialog2;

    private void DialogDriver() {

        if (!mActivity.isFinishing()) {
            dialog2 = new Dialog(SuperBaseActivity.this, R.style.Theme_Dialog);
            dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
            Window window = dialog2.getWindow();
            window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);
            dialog2.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog2.setContentView(R.layout.dialog_onebutton);
            dialog2.setCancelable(false);
            dialog2.setCanceledOnTouchOutside(false);

            if (dialog2 != null && !dialog2.isShowing()) {

                try {
                    dialog2.show();
                } catch (Exception e) {

                }
            }

            TextView mTitle = (TextView) dialog2.findViewById(R.id.text_title);
            mTitle.setTypeface(tf, Typeface.BOLD);
            TextView mDialogBody = (TextView) dialog2
                    .findViewById(R.id.txt_dialog_body);
            TextView mOK = (TextView) dialog2.findViewById(R.id.txt_dialog_ok);

            mTitle.setText("A CUSTOMER IS WAITING");

            mDialogBody.setText("At" + "\n"
                    + DriverParser.getCustomer_pick_up_name());

            mOK.setText("VIEW JOB");

            mOK.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    isRideShowing = false;

                    if (MyApplication.mPlayer != null) {

                        MyApplication.stopPlayer();
                    }
                    if (mAppManager.getRequest_ID() != null && !mAppManager.getRequest_ID().equalsIgnoreCase("")) {

                    } else {

                        if (status_id.equals("1")) {
                            mAppManager.App_setCustomer_mobile_no(DriverParser.getMobile_no());
                            myNManager.cancelAll();
                            MyFirebaseMessagingService.messages = "";
                            mAppManager.savePaymentType(DriverParser.getPayment_type());

                         /*   Intent myintent = new Intent(SuperBaseActivity.this,
                                    CustomerDetailsActivity.class);
                            startActivity(myintent);
                            finish();*/

                        } else {
                            //Toast.makeText(getApplicationContext(),"You are currently unavailable, please switch to get available.",Toast.LENGTH_LONG).show();

                            myNManager.cancelAll();
                            PushDialog pushDialog = new PushDialog(SuperBaseActivity.this, "You are currently unavailable, please switch to get available.", "007");
                            pushDialog.show();
                        }
                    }

                    dialog2.dismiss();


                }
            });
        }

    }

    /**
     * Confirmation Dialog
     */


    Dialog confirmation;

    private void Dialogconfirmation(String message) {

        if (!mActivity.isFinishing()) {

            confirmation = new Dialog(SuperBaseActivity.this, R.style.Theme_Dialog);
            confirmation.requestWindowFeature(Window.FEATURE_NO_TITLE);
            Window window = confirmation.getWindow();
            window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);
            confirmation.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            confirmation.setContentView(R.layout.dialog_twobutton);
            confirmation.setCancelable(false);
            confirmation.setCanceledOnTouchOutside(false);
            confirmation.show();

            TextView mTitle = (TextView) confirmation.findViewById(R.id.text_title);
            mTitle.setTypeface(tf, Typeface.BOLD);
            TextView mDialogBody = (TextView) confirmation
                    .findViewById(R.id.txt_dialog_body);
            TextView mOK = (TextView) confirmation.findViewById(R.id.txt_dialog_ok);

            TextView mNo = (TextView) confirmation.findViewById(R.id.txt_dialog_cancel);

            mTitle.setText("Alert");

            mDialogBody.setText(message);

            mOK.setText("Yes");

            mNo.setText("No");

            mOK.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    confirmation.dismiss();
                    allowDriver("true");
                }
            });

            mNo.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    confirmation.dismiss();
                    allowDriver("false");
                }
            });
        }


    }


    private void allowDriver(String status) {

        String driver_id = mAppManager.App_getDriver_driver_id();
        String customer_id = mAppManager.getCustomerID();

        JSONObject obj = new JSONObject();

        try {
            obj.put("driver_id", driver_id);
            obj.put("customer_id", customer_id);
            obj.put("auth_token", mAppManager.getTokenCode());
            obj.put("response", status);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (mSocket.connected()) {
            mSocket.emit("cofirm_switch_reason", obj);
        } else {
            Toast.makeText(getApplicationContext(), "Error In Connection. Try Again", Toast.LENGTH_LONG).show();
            if (!mSocket.connected()) {
                Log.d("NodeConnection", "SuperBase-allowDriver");
                if (mAppManager.getTokenCode() != null && !mAppManager.getTokenCode().isEmpty()) {
                    Intent start_Serv = new Intent(getApplicationContext(), ConnectorService.class);
                    startService(start_Serv);
                }
            }
        }

    }

    public void showProgress() {
        // TODO Auto-generated method stub

        if (loader != null) {
            if (loader.isShowing()) {
                hideProgress();
            }

            loader_wheel.setVisibility(View.VISIBLE);
            try {
                if (loader != null && !loader.isShowing() && !mActivity.isFinishing()) {
                    loader.show();
                }
            } catch (Exception e) {

            }
        }
    }


    public void hideProgress() {
        // TODO Auto-generated method stub

        if (loader != null) {

            if (loader.isShowing()) {
                loader_wheel.setVisibility(View.GONE);
                loader.dismiss();
            }
        }
    }


    /**
     * Logout
     */


    Dialog logoutconfirmation;

    private void logoutConfirmation(String message) {
        if (!mActivity.isFinishing()) {
            logoutconfirmation = new Dialog(SuperBaseActivity.this, R.style.Theme_Dialog);
            logoutconfirmation.requestWindowFeature(Window.FEATURE_NO_TITLE);
            Window window = logoutconfirmation.getWindow();
            window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);
            logoutconfirmation.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            logoutconfirmation.setContentView(R.layout.dialog_onebutton);
            logoutconfirmation.setCancelable(false);
            logoutconfirmation.setCanceledOnTouchOutside(false);
            logoutconfirmation.show();

            TextView mTitle = (TextView) logoutconfirmation.findViewById(R.id.text_title);
            mTitle.setTypeface(tf, Typeface.BOLD);
            TextView mDialogBody = (TextView) logoutconfirmation
                    .findViewById(R.id.txt_dialog_body);
            TextView mOK = (TextView) logoutconfirmation.findViewById(R.id.txt_dialog_ok);

            mTitle.setText("Logged Out !!!");

            mDialogBody.setText(message);

            mOK.setText("Ok");

            mOK.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    logoutconfirmation.dismiss();
                    mAppManager.saveUser("");
                    mAppManager.setLogin_status(false);
                    mAppManager.savePrevActivity("ADD", "");
                    startActivity(new Intent(getApplicationContext(), SplashActivity.class));
                    finish();
                }
            });
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(this.refreshAfterMarkedNa, new IntentFilter(RECEIVER_FUTURE_REQUEST));
        registerReceiver(this.refreshAfterMarkedOFFER, new IntentFilter(RECEIVER_REFER_OFFER));
        registerReceiver(this.refreshAfterMarkedNaSuspend, new IntentFilter(RECEIVER_SUSPEND_DRIVER_SUPER));
        registerReceiver(this.refreshAfterMarkedNaCustomer, new IntentFilter(RECEIVER_SUSPEND_CUSTOMER_SUPER));
        registerReceiver(this.refreshAfterMarkedNaCustomerNewRide, new IntentFilter(RECEIVER_SUSPEND_CUSTOMER_NEW_RIDE));
        registerReceiver(this.refreshAfterMarkedNaTimeExpiry, new IntentFilter(RECEIVER_SUSPEND_CUSTOMER_Time_ExPIRY));
    }

    @Override
    protected void onStop() {
        unregisterReceiver(refreshAfterMarkedNa);
        unregisterReceiver(refreshAfterMarkedOFFER);
        unregisterReceiver(refreshAfterMarkedNaSuspend);
        unregisterReceiver(refreshAfterMarkedNaCustomer);
        unregisterReceiver(refreshAfterMarkedNaCustomerNewRide);
        unregisterReceiver(refreshAfterMarkedNaTimeExpiry);

        if (requestQueueoffer != null) {
            requestQueueoffer.cancelAll(this);
        }
        if (driverofferrequestQueue != null) {
            driverofferrequestQueue.cancelAll(this);
        }

        if (loader != null) {
            loader.dismiss();
        }
        if (dialogfuture != null) {
            dialogfuture.dismiss();
        }
        if (dialog2 != null) {
            dialog2.dismiss();
        }
        if (confirmation != null) {
            confirmation.dismiss();
        }
        if (loader != null) {
            loader.dismiss();
        }
        if (ad != null) {
            ad.dismiss();
        }
        if (logoutconfirmation != null) {
            logoutconfirmation.dismiss();
        }
        if (dialogsuspendsupercustomer != null) {
            dialogsuspendsupercustomer.dismiss();
        }
        if (dialogsuspendsuper != null) {
            dialogsuspendsuper.dismiss();
        }
        if (dialogfuture2 != null) {
            dialogfuture2.dismiss();
        }

        if (dialogB != null) {
            dialogB.dismiss();
        }

        if (customerOffer != null) {
            customerOffer.dismiss();
        }

        super.onStop();
    }

    private BroadcastReceiver refreshAfterMarkedNaCustomerNewRide = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {

            Log.d("SUPER_BASE", "InNewRide - PUSH: ");

            String message = in.getStringExtra("push_message");
            System.out.println("Sresponsefcm----" + message.toString());

            try {
                boolean parsresp = DriverParser.driverAccept(message);

                if (MyApplication.last_request_id.equals(DriverParser.getRequest_id())) {

                } else {
                    MyApplication.last_request_id = DriverParser.getRequest_id();

                    if (parsresp) {
                        if (DriverParser.getStatusMessage().equals("Driver is not ready for the ride")) {

                        } else {
                            if (!isRideShowing) {
                                isRideShowing = true;

                                MyApplication.startPlayer(R.raw.hoponn_request);
                                if (dialog2 != null) {
                                    dialog2.dismiss();
                                    dialog2 = null;
                                }

                                if (mAppManager.getRequest_ID() != null && !mAppManager.getRequest_ID().equalsIgnoreCase("")) {

                                } else {
                                    mAppManager.App_setCustomer_mobile_no(DriverParser.getMobile_no());
                                }

                                if (!MyApplication.currentActivity.equalsIgnoreCase("CustomerDetailsActivity")) {
                                    DialogDriver();
                                }
                            }
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    };


    private BroadcastReceiver refreshAfterMarkedNaCustomer = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {


            String message = in.getStringExtra("push_message");

            int suspend_type = in.getIntExtra("suspend_type", 0);

            if (dialogsuspendsupercustomer != null) {
                dialogsuspendsupercustomer.dismiss();
                dialogsuspendsupercustomer = null;
            }
            DialogSuspend(message, suspend_type);

        }

    };


    Dialog dialogsuspendsupercustomer;

    private void DialogSuspend(String title, int type) {
        if (!mActivity.isFinishing()) {
            dialogsuspendsupercustomer = new Dialog(SuperBaseActivity.this, R.style.Theme_Dialog);
            dialogsuspendsupercustomer.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogsuspendsupercustomer.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialogsuspendsupercustomer.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            dialogsuspendsupercustomer.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            dialogsuspendsupercustomer.setContentView(R.layout.dialog_suspension);
            dialogsuspendsupercustomer.setCancelable(false);
            dialogsuspendsupercustomer.setCanceledOnTouchOutside(false);

            TextView reason = (TextView) dialogsuspendsupercustomer.findViewById(R.id.reason);
            LinearLayout mDialogBody = (LinearLayout) dialogsuspendsupercustomer.findViewById(R.id.clear_view);
            TextView payment = (TextView) dialogsuspendsupercustomer.findViewById(R.id.txt_dialog_ok);
            reason.setText(title);

            if (type == 2) {
                mDialogBody.setVisibility(View.VISIBLE);
            } else {
                mDialogBody.setVisibility(View.GONE);
            }

            payment.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialogsuspendsupercustomer.cancel();

                    Intent toPayment = new Intent(getApplicationContext(), RiderPaymentActivity.class);
                    startActivity(toPayment);
                    finish();

                }
            });


            if (dialogsuspendsupercustomer.isShowing()) {
                dialogsuspendsupercustomer.dismiss();
            }
            dialogsuspendsupercustomer.show();
        }

    }

    private BroadcastReceiver refreshAfterMarkedNaTimeExpiry = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {


            String message = in.getStringExtra("push_message");

            if (dialogsuspendsuper != null) {
                dialogsuspendsuper.dismiss();
                dialogsuspendsuper = null;
            }
            DialogSuspendSuper(message, 1);


        }

    };


    private BroadcastReceiver refreshAfterMarkedNaSuspend = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {


            String message = in.getStringExtra("push_message");

            int suspend_type = in.getIntExtra("suspend_type", 0);

            if (suspend_type == 0) {
                if (dialogsuspendsuper != null && dialogsuspendsuper.isShowing()) {
                    dialogsuspendsuper.dismiss();
                }
            } else {
                if (dialogsuspendsuper != null) {
                    dialogsuspendsuper.dismiss();
                    dialogsuspendsuper = null;
                }
                DialogSuspendSuper(message, suspend_type);
            }


        }

    };

    Dialog dialogsuspendsuper;

    private void DialogSuspendSuper(String title, final int type) {
        if (!mActivity.isFinishing()) {
            dialogsuspendsuper = new Dialog(SuperBaseActivity.this, R.style.Theme_Dialog);
            dialogsuspendsuper.requestWindowFeature(Window.FEATURE_NO_TITLE);
            Window window = dialogsuspendsuper.getWindow();
            window.setLayout(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);
            dialogsuspendsuper.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

            dialogsuspendsuper.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialogsuspendsuper.setContentView(R.layout.dialog_suspension);
            dialogsuspendsuper.setCancelable(false);
            dialogsuspendsuper.setCanceledOnTouchOutside(false);


            TextView reason = (TextView) dialogsuspendsuper.findViewById(R.id.reason);
            LinearLayout mDialogBody = (LinearLayout) dialogsuspendsuper.findViewById(R.id.clear_view);
            TextView payment = (TextView) dialogsuspendsuper.findViewById(R.id.txt_dialog_ok);

            reason.setText(title);

            if (type == 2) {
                mDialogBody.setVisibility(View.VISIBLE);
                payment.setText("Clear Payment");
            } else if (type == 0) {
                dialogsuspendsuper.cancel();
            } else {
                mDialogBody.setVisibility(View.VISIBLE
                );
                payment.setText("Ok");
            }

            payment.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    dialogsuspendsuper.cancel();


                }
            });

            if (dialogsuspendsuper.isShowing()) {
                dialogsuspendsuper.dismiss();
            }
            dialogsuspendsuper.show();
        }

    }


    private BroadcastReceiver refreshAfterMarkedOFFER = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {

            if (mAppManager.getuser().equalsIgnoreCase("customer")) {

                String response = in.getStringExtra("request");
                if (response.equalsIgnoreCase("offer")) {
                    customerOffers();
                }

            }

        }

    };


    private BroadcastReceiver refreshAfterMarkedNa = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {

            if (mAppManager.getuser().equalsIgnoreCase("driver")) {

                String response = in.getStringExtra("request");
                if (response.equalsIgnoreCase("offer")) {
                    driverOffers(false);
                } else {
                    boolean status = DriverParser.Future_driverAccept(response);

                    if (MyApplication.last_request_id.equals(DriverParser.getRequest_id())) {

                    } else {
                        MyApplication.last_request_id = DriverParser.getRequest_id();

                        if (status) {
                            if (dialogfuture2 != null && !dialogfuture2.isShowing()) {
                                dialogfuture2.dismiss();
                                dialogfuture2 = null;
                            }
                            DialogFutureDriver();


                        }
                    }
                }
            }

        }

    };

    Dialog dialogfuture2;

    private void DialogFutureDriver() {

        if (!mActivity.isFinishing()) {
            dialogfuture2 = new Dialog(SuperBaseActivity.this, R.style.Theme_Dialog);
            dialogfuture2.requestWindowFeature(Window.FEATURE_NO_TITLE);
            Window window = dialogfuture2.getWindow();
            window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);
            dialogfuture2.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialogfuture2.setContentView(R.layout.dialog_onebutton);
            dialogfuture2.setCancelable(false);
            dialogfuture2.setCanceledOnTouchOutside(false);

            if (dialogfuture2 != null && !dialogfuture2.isShowing()) {
                dialogfuture2.show();
            }

            TextView mTitle = (TextView) dialogfuture2.findViewById(R.id.text_title);
            mTitle.setTypeface(tf, Typeface.BOLD);
            TextView mDialogBody = (TextView) dialogfuture2
                    .findViewById(R.id.txt_dialog_body);
            TextView mOK = (TextView) dialogfuture2.findViewById(R.id.txt_dialog_ok);

            mTitle.setText("A CUSTOMER IS WAITING");

            mDialogBody.setText("At" + "\n"
                    + DriverParser.getFuture_customer_pick_up_name());

            mOK.setText("VIEW JOB");

            mOK.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {


                }
            });
        }

    }

    RequestQueue driverofferrequestQueue;

    public void driverOffers(final boolean showFalseCase) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_current_driver_offers,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        System.out.println("DRIVEROFFERS===" + response);

                        try {

                            JSONObject dataObject = new JSONObject(response);

                            String driver_status = dataObject.optString("status");


                            if (driver_status.equalsIgnoreCase("TRUE")) {


                                app.OfferShown();

                                JSONArray arr = dataObject.getJSONArray("data");

                                String totaloffers = "";

                                for (int i = 0; i < arr.length(); i++) {

                                    JSONObject offer = arr.optJSONObject(i);

                                    String offerString = offer.getString("title");

                                    totaloffers = totaloffers + "";

                                    totaloffers = totaloffers + offerString;

                                    totaloffers = totaloffers + "<br><br>";
                                }

                                if (dialogB != null) {
                                    dialogB.dismiss();
                                    dialogB = null;
                                }
                                DialogDriverAlertOffer("Offer", totaloffers);

                            } else {
                                if (showFalseCase) {
                                    app.OfferShown();

                                    String totaloffers = dataObject.getString("message");
                                    DialogDriverAlertOffer("Offer", totaloffers);
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        //retryDialog(1);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("token_code", mAppManager.getTokenCode());
                params.put("driver_id", driver_id);
                return params;
            }

        };

        driverofferrequestQueue = CustomVolleyRequestQueue.getInstance(mContext.getApplicationContext())
                .getRequestQueue();
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        driverofferrequestQueue.add(stringRequest);
    }

    Dialog dialogB;

    public void DialogDriverAlertOffer(String title, String body) {

        if (!mActivity.isFinishing()) {
            dialogB = new Dialog(SuperBaseActivity.this, R.style.Theme_Dialog);
            dialogB.requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialogB.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialogB.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            dialogB.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

            dialogB.setContentView(R.layout.dialog_driveroffer);
            dialogB.setCancelable(false);
            dialogB.setCanceledOnTouchOutside(false);

            TextView mDialogBody = (TextView) dialogB.findViewById(R.id.txt_offers);

            mDialogBody.setText(Html.fromHtml(body));

            LinearLayout offerButton = (LinearLayout) dialogB.findViewById(R.id.clear_view);

            offerButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialogB.cancel();

                }
            });

            if (dialogB.isShowing()) {
                dialogB.dismiss();
            }
            dialogB.show();
        }

    }

    /**
     * Customer Offers
     */

    public android.app.Dialog customerOffer;

    public void DialogAlertcustomerOffer(String title, String body) {
        if (!mActivity.isFinishing()) {
            customerOffer = new Dialog(SuperBaseActivity.this, R.style.Theme_Dialog);
            customerOffer.requestWindowFeature(Window.FEATURE_NO_TITLE);

            customerOffer.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            customerOffer.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            customerOffer.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            customerOffer.setContentView(R.layout.dialog_driveroffer);
            customerOffer.setCancelable(false);
            customerOffer.setCanceledOnTouchOutside(false);

            TextView mDialogBody = (TextView) customerOffer.findViewById(R.id.txt_offers);

            mDialogBody.setText(Html.fromHtml(body));

            TextView offerButton = (TextView) customerOffer.findViewById(R.id.offer_btn);

            offerButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    customerOffer.cancel();

                }
            });
            if (customerOffer.isShowing()) {
                customerOffer.dismiss();
            }
            customerOffer.show();
        }
    }


    /**
     * Customer Offers
     */
    public RequestQueue requestQueueoffer;

    public void customerOffers() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_customer_offers,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        System.out.println("CustomerOFFERS===" + response);

                        try {

                            JSONObject dataObject = new JSONObject(response);

                            String driver_status = dataObject.optString("status");

                            if (driver_status.equalsIgnoreCase("TRUE")) {

                                app.OfferShown();

                                JSONObject offer = dataObject.getJSONObject("data");

                                String offerString = offer.getString("note");

                                if (customerOffer != null) {
                                    customerOffer.dismiss();
                                    customerOffer = null;
                                }
                                DialogAlertcustomerOffer("Offer", offerString);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }

                }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("token_code", mAppManager.getTokenCode());
                params.put("customer_id", mAppManager.getCustomerID());
                return params;
            }

        };

        requestQueueoffer = CustomVolleyRequestQueue.getInstance(mContext.getApplicationContext())
                .getRequestQueue();
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueueoffer.add(stringRequest);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    AlertDialog ad;
    String packageName = "com.uk.hoponn";

    private void showNotificationAlert() {
        // TODO Auto-generated method stub

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(SuperBaseActivity.this);
                alertDialog.setTitle("NOTIFICATIONS");
                alertDialog.setMessage("Please enable push notifications in your settings to recieve ride alerts");

                alertDialog.setPositiveButton("ENABLE",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                try {
                                    //Open the specific App Info page:
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    intent.setData(Uri.parse("package:" + packageName));
                                    startActivity(intent);

                                } catch (ActivityNotFoundException e) {
                                    //e.printStackTrace();

                                    //Open the generic Apps page:
                                    Intent intent = new Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
                                    startActivity(intent);

                                }


                            }
                        });
                if (!SuperBaseActivity.this.isFinishing())
                    ad = alertDialog.show();

                ad.setCancelable(false);
                ad.setCanceledOnTouchOutside(false);

            }
        }, 3000);

    }

}
