package com.us.hoodruncustomer.model;

import java.util.ArrayList;

public  class Cars  {

	private  String id;
	private  String name;
	private  String no_of_seats;
	private  String map_car_image;
	private  String list_car_image;
	private  String surge_message;
	private  boolean default_car;
	private  String avg_time;
	private  boolean surge_applied;
	private ArrayList<Drivers> drivers;
	private  String base_fare;
	private  String cancelation_fee;
	private  String per_mile_fare;
	private  String per_minute_fare;
	private  String minimum_fare;

	public String getBase_fare() {
		return base_fare;
	}

	public void setBase_fare(String base_fare) {
		this.base_fare = base_fare;
	}

	public String getCancelation_fee() {
		return cancelation_fee;
	}

	public void setCancelation_fee(String cancelation_fee) {
		this.cancelation_fee = cancelation_fee;
	}

	public String getPer_mile_fare() {
		return per_mile_fare;
	}

	public void setPer_mile_fare(String per_mile_fare) {
		this.per_mile_fare = per_mile_fare;
	}

	public String getPer_minute_fare() {
		return per_minute_fare;
	}

	public void setPer_minute_fare(String per_minute_fare) {
		this.per_minute_fare = per_minute_fare;
	}

	public String getMinimum_fare() {
		return minimum_fare;
	}

	public void setMinimum_fare(String minimum_fare) {
		this.minimum_fare = minimum_fare;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNo_of_seats() {
		return no_of_seats;
	}

	public void setNo_of_seats(String no_of_seats) {
		this.no_of_seats = no_of_seats;
	}

	public String getMap_car_image() {
		return map_car_image;
	}

	public void setMap_car_image(String map_car_image) {
		this.map_car_image = map_car_image;
	}

	public String getList_car_image() {
		return list_car_image;
	}

	public void setList_car_image(String list_car_image) {
		this.list_car_image = list_car_image;
	}

	public String getSurge_message() {
		return surge_message;
	}

	public void setSurge_message(String surge_message) {
		this.surge_message = surge_message;
	}

	public boolean isSurge_applied() {
		return surge_applied;
	}

	public void setSurge_applied(boolean surge_applied) {
		this.surge_applied = surge_applied;
	}

	public ArrayList<Drivers> getDrivers() {
		return drivers;
	}

	public void setDrivers(ArrayList<Drivers> drivers) {
		this.drivers = drivers;
	}

	public boolean isDefault_car() {
		return default_car;
	}

	public void setDefault_car(boolean default_car) {
		this.default_car = default_car;
	}

	public String getAvg_time() {
		return avg_time;
	}

	public void setAvg_time(String avg_time) {
		this.avg_time = avg_time;
	}
}
