package com.us.hoodruncustomer.commonwork;

import android.widget.TextView;

import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.custom.MyEditText;

import java.util.regex.Pattern;


public class Validation {

 
    // Regular Expression
    // you can change the expression based on your need
    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]{2,}+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String PHONE_REGEX = "^\\s*\\(?(020[7,8]{1}\\)?[ ]?[1-9]{1}[0-9]{2}[ ]?[0-9]{4})|(0[1-8]{1}[0-9]{3}\\)?[ ]?[1-9]{1}[0-9]{2}[ ]?[0-9]{3})\\s*$";
	
    // Error Messages
    private static final String REQUIRED_MSG = "Required";
    private static final String EMAIL_MSG = "Invalid email";
    private static final String PHONE_MSG = "Invalid mobile number";
 
    // call this method when you need to check email validation
    public static boolean isEmailAddress(MyEditText editText, boolean required) {
        return isValid(editText, EMAIL_REGEX, EMAIL_MSG, required);
    }
 
    // call this method when you need to check phone number validation
    public static boolean isPhoneNumber(MyEditText editText, boolean required) {
        return isValid(editText, PHONE_REGEX, PHONE_MSG, required);
    }
 
    // return true if the input field is valid, based on the parameter passed
    public static boolean isValid(MyEditText editText, String regex, String errMsg, boolean required) {
 
        String text = editText.getText().toString().trim();
        // clearing the error, if it was previously set by some other values
      /////////  editText.setError(null);
        
 
        // text required and editText is blank, so return false
        if ( required && !hasText(editText) ){ 
        	  editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        	return false;
        	}
 
        // pattern doesn't match so returning false
        if (required && !Pattern.matches(regex, text)) {
            //editText.setError(null,wrong);
            editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.wrong, 0);
            
            return false;
        }
        if (editText.getText().toString().equals("")) {
            //editText.setError(null,wrong);
            editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.wrong, 0);
            return false;
        }
        if(editText.getText().length()<=9)
        {
        editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        return false;
        }
        editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick, 0);
        return true;
    }
 
    // check the input field has any text or not
    // return true if it contains text otherwise false
    public static boolean hasText(MyEditText editText) {
 
        String text = editText.getText().toString();
        editText.setError(null);
        //editText.setError(REQUIRED_MSG);
   
        // length 0 means there is no text
        if (text.length() == 0) {
         //   editText.setError(REQUIRED_MSG);
        	  editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            return false;
        }
 
        
        else   {
        	
        	editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick, 0);
        }
        editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick, 0);
 
        return true;
    }


    public static boolean hasVText(TextView editText) {

        String text = editText.getText().toString();
        editText.setError(null);
        //editText.setError(REQUIRED_MSG);

        // length 0 means there is no text
        if (text.length() == 0) {
            //   editText.setError(REQUIRED_MSG);
            editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            return false;
        }


        else   {

            editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick, 0);
        }
        editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick, 0);

        return true;
    }

    
    public static boolean hasText1(MyEditText editText) {
    	 
        String text = editText.getText().toString().trim();
        editText.setError(null);
        //editText.setError(REQUIRED_MSG);
   
        // length 0 means there is no text
        if (text.length() == 0) {
         //   editText.setError(REQUIRED_MSG);
        	  editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            return false;
        }else    {  editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick, 0);
        }
 
        return true;
    }

  
}