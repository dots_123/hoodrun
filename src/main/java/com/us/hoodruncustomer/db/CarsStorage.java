package com.us.hoodruncustomer.db;

/**
 * Created by admin on 05-Jun-17.
 */

public class CarsStorage {

    private int carId;
    private String carName;
    private String carLocStorage;
    private String carDynStorage;

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getCarLocStorage() {
        return carLocStorage;
    }

    public void setCarLocStorage(String carLocStorage) {
        this.carLocStorage = carLocStorage;
    }

    public String getCarDynStorage() {
        return carDynStorage;
    }

    public void setCarDynStorage(String carDynStorage) {
        this.carDynStorage = carDynStorage;
    }
}
