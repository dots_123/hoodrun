package com.us.hoodruncustomer.commonwork;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.custom.CircularImageView;
import com.us.hoodruncustomer.ridehistory.RideHistoryActivity;
import com.us.hoodruncustomer.model.History;

import java.util.ArrayList;
import java.util.Locale;

public class HistoryDetailsActivity extends SimpleActivity {

    private Toolbar toolbar;
    private TextView headertext;
    private Button complain;
    LinearLayout discount_visibility, layout_help;
    Typeface tf;
    String font_path;

    TextView TripDate, tot_fare, trip_distance, trip_duration, payment_done, driver_name, status, mPickAddress, mDestAddress, trip_time, total_text, total_surge, totaldis, discount;
    RatingBar mRateBar;
    public static ArrayList<History> historyList;
    int position;
    CircularImageView driver_image;
    ImageView ride_map;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_details);
        MyApplication.activity = HistoryDetailsActivity.this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {

            int color = Color.parseColor("#ffffff");
            toolbar.setTitleTextColor(color);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

            headertext = (TextView) findViewById(android.R.id.text1);
            complain = (Button) findViewById(android.R.id.button1);

            if (mAppManager.getuser().equals("driver")) {
                complain.setVisibility(View.VISIBLE);
            } else {
                complain.setVisibility(View.GONE);
            }

            headertext.setText("Ride Details");
            toolbar.setNavigationIcon(R.drawable.back_arrow);


            toolbar.setNavigationOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    onBackPressed();
                    mAppManager.savePrevActivity("HIS", "HISD");

                }
            });


        }

        font_path = "fonts/opensans.ttf";

        tf = Typeface.createFromAsset(this.getAssets(), font_path);

        headertext.setTypeface(tf, Typeface.BOLD);


        this.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Intent in = getIntent();
        position = in.getIntExtra("position", 0);
        setupView();

        if (mAppManager.getuser().equalsIgnoreCase("driver")) {
            layout_help.setVisibility(View.VISIBLE);
        } else {
            layout_help.setVisibility(View.GONE);
        }

        layout_help.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent toComplain = new Intent(getApplicationContext(), ComplainActivity.class);
                toComplain.putExtra("ride_id", historyList.get(position).getRide_id());
                startActivity(toComplain);
            }
        });

    }

    private void setupView() {

        TripDate = (TextView) findViewById(R.id.txt_tripdate);
        tot_fare = (TextView) findViewById(R.id.txt_total_fare);
        trip_distance = (TextView) findViewById(R.id.txt_trip_distane);
        trip_duration = (TextView) findViewById(R.id.txt_trip_duratiion);
        total_surge = (TextView) findViewById(R.id.total_surge);
        payment_done = (TextView) findViewById(R.id.txt_paymentdone);
        mRateBar = (RatingBar) findViewById(R.id.rate_bar_history);
        driver_name = (TextView) findViewById(R.id.txt_driver_name);
        driver_image = (CircularImageView) findViewById(R.id.img_driver);
        ride_map = (ImageView) findViewById(R.id.img_map);
        status = (TextView) findViewById(R.id.txt_status);
        trip_time = (TextView) findViewById(R.id.txt_triptime);
        mPickAddress = (TextView) findViewById(R.id.pick_up_address1);
        mDestAddress = (TextView) findViewById(R.id.dest_address1);
        total_text = (TextView) findViewById(R.id.total_text);
        historyList = new ArrayList<History>();
        historyList = RideHistoryActivity.getmHistoryList();
        TripDate.setText(getdate(historyList.get(position).getRide_time()));
        discount_visibility = (LinearLayout) findViewById(R.id.discount_visibility);
        layout_help = (LinearLayout) findViewById(R.id.layout_help);
        totaldis = (TextView) findViewById(R.id.totaldis);
        discount = (TextView) findViewById(R.id.discount);
        trip_distance.setText(historyList.get(position).getTrip_distance() + " mi");
        trip_duration.setText(historyList.get(position).getTrip_duration());
        payment_done.setText(historyList.get(position).getPayment_type().toUpperCase(Locale.getDefault()));

        mPickAddress.setText(historyList.get(position).getPickup_location_name());
        mDestAddress.setText(historyList.get(position).getDestination_name());
        trip_time.setText(getTime(historyList.get(position).getRide_time()));


        if (historyList.get(position).getSurge_applied().equalsIgnoreCase("true")) {
            total_surge.setText(" (Surge" + historyList.get(position).getSurge() + " x)");
        }

        if (mAppManager.getuser().equals("customer")) {
            if (historyList.get(position).getShare_status().equals("1") && (!historyList.get(position).getCustomer_cancel_status().equals("Cancelled") && !historyList.get(position).getDriver_cancel_status().equals("Cancelled"))) {
                tot_fare.setText(getResources().getString(R.string.pound) + "" + historyList.get(position).getShare_amt());

                if (historyList.get(position).getPromo_code() != null && historyList.get(position).getPromo_code().trim().length() > 0) {
                    discount_visibility.setVisibility(View.VISIBLE);
                    totaldis.setText(getResources().getString(R.string.pound) + "" + (Double.parseDouble(historyList.get(position).getShare_amt()) + Double.parseDouble(historyList.get(position).getPromotion_discount())));
                    discount.setText(getResources().getString(R.string.pound) + "" + Double.parseDouble(historyList.get(position).getPromotion_discount()));

                }


                total_text.setText("SHARED FARE");
            } else {

                tot_fare.setText(getResources().getString(R.string.pound) + "" + historyList.get(position).getTotal());
                if (historyList.get(position).getPromo_code() != null && historyList.get(position).getPromo_code().trim().length() > 0) {
                    discount_visibility.setVisibility(View.VISIBLE);
                    totaldis.setText(getResources().getString(R.string.pound) + "" + (Double.parseDouble(historyList.get(position).getTotal()) + Double.parseDouble(historyList.get(position).getPromotion_discount())));
                    discount.setText(getResources().getString(R.string.pound) + "" + Double.parseDouble(historyList.get(position).getPromotion_discount()));

                }

            }

        } else {
            tot_fare.setText(getResources().getString(R.string.pound) + "" + historyList.get(position).getTotal());
            if (historyList.get(position).getPromo_code() != null && historyList.get(position).getPromo_code().trim().length() > 0) {
                discount_visibility.setVisibility(View.VISIBLE);
                totaldis.setText(getResources().getString(R.string.pound) + "" + (Double.parseDouble(historyList.get(position).getTotal()) + Double.parseDouble(historyList.get(position).getPromotion_discount())));
                discount.setText(getResources().getString(R.string.pound) + "" + Double.parseDouble(historyList.get(position).getPromotion_discount()));

            }
        }

        Glide.with(HistoryDetailsActivity.this).load(historyList.get(position).getImage())
//				.placeholder(R.drawable.placeholder)
                .into(driver_image);
        Glide.with(HistoryDetailsActivity.this).load(historyList.get(position).getDetailed_ride_map())
//				.placeholder(R.drawable.driver_details_background)
                .into(ride_map);
        driver_name.setText(historyList.get(position).getName().toUpperCase(Locale.getDefault()));
        mRateBar.setRating(Float.parseFloat(historyList.get(position).getCustomer_rating()));
        mRateBar.setEnabled(false);
        if (mAppManager.getuser().equals("customer")) {

            if (historyList.get(position).getShare_status().equals("1")) {


                if (historyList.get(position).getCustomer_cancel_status().equals("Cancelled")) {
                    status.setText("Cancelled");
                    status.setTextColor(Color.parseColor("#f43b26"));
                } else if (historyList.get(position).getDriver_cancel_status().equals("Cancelled")) {
                    status.setText("Cancelled by driver");
                    status.setTextColor(Color.parseColor("#f43b26"));
                } else {

                    status.setText("SUCCESS (SHARED)");
                    status.setTextColor(Color.parseColor("#318b32"));
                }


            } else {


                if (historyList.get(position).getCustomer_cancel_status().equals("Cancelled")) {
                    status.setText("Cancelled");
                    status.setTextColor(Color.parseColor("#f43b26"));
                } else if (historyList.get(position).getDriver_cancel_status().equals("Cancelled")) {
                    status.setText("Cancelled by driver");
                    status.setTextColor(Color.parseColor("#f43b26"));
                } else {
                    status.setText("SUCCESS");
                    status.setTextColor(Color.parseColor("#318b32"));
                }
            }
        }


    }

    @Override
    public void onBackPressed() {

        // code here to show dialog
        super.onBackPressed(); // optional depending on your needs
        startActivity(new Intent(HistoryDetailsActivity.this,
                RideHistoryActivity.class));
        mAppManager.savePrevActivity("HIS", "HISD");

        finish();
    }


    private String getdate(String dateTime) {


        String[] words = dateTime.split("\\s+");
        for (int i = 0; i < words.length; i++) {
            // You may want to check for a non-word character before blindly
            // performing a replacement
            // It may also be necessary to adjust the character class
            words[i] = words[i].replaceAll("[^\\w]", "/");
        }
        return words[0];
    }


    private String getTime(String dateTime) {


        String[] words = dateTime.split("\\s+");

        return words[1];
    }


    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.activityPaused();

    }


}
