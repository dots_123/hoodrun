package com.us.hoodruncustomer.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.us.hoodruncustomer.R;

import java.util.ArrayList;

/**
 * Created by admin on 2/8/2016.
 */
public class SortingAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    ArrayList<SortDao> sorting_items;

    public SortingAdapter(Context context, ArrayList<SortDao> sorting_items) {
        this.context = context;
        this.sorting_items = sorting_items;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return sorting_items.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.filter_item, null);


        LinearLayout mainLayout = (LinearLayout) convertView.findViewById(R.id.main_layout);

        TextView filterText = (TextView) convertView.findViewById(R.id.filter_item);
        ImageView isChekBox = (ImageView) convertView.findViewById(R.id.checkBox);
        filterText.setText(sorting_items.get(position).getItemName());

        if (sorting_items.get(position).isSelected()) {
            isChekBox.setImageResource(R.drawable.check);

        }else
            isChekBox.setImageResource(R.drawable.uncheck);



        return convertView;
    }
    public void setSelectedIndex(int position){
        for (int i = 0; i < sorting_items.size(); i++) {
            SortDao services = (SortDao) sorting_items.get(i);

            if(position == i){
                services.setSelected(true);
            }else
                services.setSelected(false);
        }

        notifyDataSetChanged();
    }
}
