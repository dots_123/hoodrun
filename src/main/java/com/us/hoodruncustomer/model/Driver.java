package com.us.hoodruncustomer.model;

public class Driver {
	private String driver_id;
	private String driver_name;
	private String latitude;
	private String longitude;
	private String distance_away;
	private String city;
	private String fav_status;
	private String rating;
	private String vehicle_type;
	private String vehicle_model;
	private String estimated_fare;
	private String driver_image;
	private String ride_id;
	private String max_seat;
	private String request_id;
	private String minutes_away;
	
	public String getMinutes_away() {
		return minutes_away;
	}
	public void setMinutes_away(String minutes_away) {
		this.minutes_away = minutes_away;
	}
	public String getRequest_id() {
		return request_id;
	}
	public void setRequest_id(String request_id) {
		this.request_id = request_id;
	}
	public String getMax_seat() {
		return max_seat;
	}
	public void setMax_seat(String max_seat) {
		this.max_seat = max_seat;
	}
	public String getRide_id() {
		return ride_id;
	}
	public void setRide_id(String ride_id) {
		this.ride_id = ride_id;
	}
	public String getDriver_id() {
		return driver_id;
	}
	public void setDriver_id(String driver_id) {
		this.driver_id = driver_id;
	}
	public String getDriver_name() {
		return driver_name;
	}
	public void setDriver_name(String driver_name) {
		this.driver_name = driver_name;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getDistance_away() {
		return distance_away;
	}
	public void setDistance_away(String distance_away) {
		this.distance_away = distance_away;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getFav_status() {
		return fav_status;
	}
	public void setFav_status(String fav_status) {
		this.fav_status = fav_status;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getVehicle_type() {
		return vehicle_type;
	}
	public void setVehicle_type(String vehicle_type) {
		this.vehicle_type = vehicle_type;
	}
	public String getVehicle_model() {
		return vehicle_model;
	}
	public void setVehicle_model(String vehicle_model) {
		this.vehicle_model = vehicle_model;
	}
	public String getEstimated_fare() {
		return estimated_fare;
	}
	public void setEstimated_fare(String estimated_fare) {
		this.estimated_fare = estimated_fare;
	}
	public String getDriver_image() {
		return driver_image;
	}
	public void setDriver_image(String driver_image) {
		this.driver_image = driver_image;
	}

}
