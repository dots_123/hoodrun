package com.us.hoodruncustomer.model;

/**
 * Created by admin on 25-Jan-17.
 */

public class TransactionsHistoryDao {



    private  String debit_account;
    private String credit_account;
    private String amount;
    private String transaction_date;
    private String transaction_time;
    private String driver_name;
    private String description;
    private String color;


    public TransactionsHistoryDao(String debit_account, String credit_account, String amount, String transaction_date, String transaction_time, String description, String driver_name,String color) {
        this.debit_account = debit_account;
        this.credit_account = credit_account;
        this.amount = amount;
        this.transaction_date = transaction_date;
        this.transaction_time = transaction_time;
        this.description = description;
        this.driver_name = driver_name;
        this.color = color;
    }

    public String getDebit_account() {
        return debit_account;
    }

    public String getCredit_account() {
        return credit_account;
    }

    public String getAmount() {
        return amount;
    }

    public String getTransaction_date() {
        return transaction_date;
    }

    public String getTransaction_time() {
        return transaction_time;
    }

    public String getDescription() {
        return description;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
