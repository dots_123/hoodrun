package com.us.hoodruncustomer.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;


import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.custom.MyTextView;
import com.us.hoodruncustomer.db.AppManager;
import com.us.hoodruncustomer.parser.DriverParser;

public class JobDialog extends Dialog implements OnClickListener{
	String push_message,push_flag;
Context context;
MyTextView title, body,ok;
AppManager mAppManager;
String font_path;
Typeface tf;

public JobDialog(Context context, String push_message, String push_flag) {
		super(context,  R.style.Theme_Dialog);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.push_message=push_message;
		this.push_flag=push_flag;
		mAppManager=AppManager.getInstance(context);
	    font_path="fonts/opensans.ttf";
		tf = Typeface.createFromAsset(context.getAssets(), font_path);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		getWindow().setGravity(Gravity.CENTER);
		getWindow().setBackgroundDrawable(new ColorDrawable(0));
		setContentView(R.layout.dialog_onebutton);
		setCancelable(false);
		setCanceledOnTouchOutside(false);
	    title=(MyTextView)findViewById(R.id.text_title);
	    title.setTypeface(tf, Typeface.BOLD);
	 	body=(MyTextView)findViewById(R.id.txt_dialog_body);
		ok=(MyTextView)findViewById(R.id.txt_dialog_ok);

		ok.setOnClickListener(this);
		title.setText("A CUSTOMER IS WAITING");
		ok.setText("VIEW JOB");
		body.setText("At" + "\n"
				+ DriverParser.getCustomer_pick_up_name());


	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	if(v.getId()==R.id.txt_dialog_ok){
		if(MyApplication.mPlayer!=null)
if(MyApplication.mPlayer.isPlaying()){
	MyApplication.stopPlayer();
}
		dismiss();

		MyApplication.dialogDismissed();
	}

	}



}
