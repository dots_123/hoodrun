package com.us.hoodruncustomer.commonwork;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.RelativeLayout;

import com.us.hoodruncustomer.R;


public class SimpleActivity extends SuperBaseActivity {

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}


	
	protected RelativeLayout _completeLayout, _activityLayout;
	// nav drawer title

	// used to store app title
	private CharSequence mTitle;
	private Toolbar toolbar;
	String font_path;
	static Typeface tf;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.regdrawer);

		// if (savedInstanceState == null) {
		// // on first time display view for first nav item
		// // displayView(0);
		// }
		

		if (toolbar != null) {
			toolbar.setTitle("Hoponn");
			int color = Color.parseColor("#0582f4");
			toolbar.setTitleTextColor(color);
			setSupportActionBar(toolbar);
			
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
		}
		font_path = "fonts/opensans.ttf";
        
	     tf = Typeface.createFromAsset(this.getAssets(), font_path);
	}

	
	public void set() {

		toolbar = (Toolbar) findViewById(R.id.toolbar);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// getSupportMenuInflater().inflate(R.menu.main, menu);
		return true;
	}


	
	
}
