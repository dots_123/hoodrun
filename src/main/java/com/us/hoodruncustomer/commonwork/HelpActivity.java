package com.us.hoodruncustomer.commonwork;


import android.app.Dialog;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.configure.Configure;
import com.us.hoodruncustomer.parser.ConnectionDetector;
import com.us.hoodruncustomer.home.CustomerHomeActivity;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HelpActivity extends BaseActivity {

	private Toolbar toolbar;
	private TextView headertext;
    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    DisplayMetrics metrics;
    int width;
    HashMap<String, List<String>> expandableListDetail;

    String user_type,font_path;
    Typeface tf;
    ConnectionDetector cd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_activity);
        MyApplication.activity= HelpActivity.this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
     		 if (toolbar != null) {
     	            int color = Color.parseColor("#ffffff");
     	            toolbar.setTitleTextColor(color);
     	            setSupportActionBar(toolbar);
     	           getSupportActionBar().setDisplayShowTitleEnabled(false);
			       
		            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
     	   	    headertext=(TextView) findViewById(android.R.id.text1); 
     	       headertext.setText("Help");
     	        
          }
             
     		font_path = "fonts/opensans.ttf";
	        
    		tf = Typeface.createFromAsset(this.getAssets(), font_path);  
             
     		 String[] navMenuTitles;
			   	TypedArray navMenuIcons;

				 if(mAppManager.getuser().equals("customer")){
				 
					 navMenuTitles = getResources().getStringArray(R.array.nav_drawer_user_items); 
			   		 navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons_user);
			   		 user_type="1";
				 }else{
					 
					 navMenuTitles = getResources().getStringArray(R.array.nav_drawer_driver_items); 
			   		 navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);
			   		 user_type="2";
				 }
		   		
     		
     		set(navMenuTitles, navMenuIcons);
     		  mAppManager.savePrevActivity("HIS", "");
     		
     		headertext.setTypeface(tf,Typeface.BOLD);
     		expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
       
            metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            width = metrics.widthPixels;
            //this code for adjusting the group indicator into right side of the view
            expandableListView.setIndicatorBounds(width - GetDipsFromPixel(150), width - GetDipsFromPixel(100));
            cd=new ConnectionDetector(HelpActivity.this);
            if(cd.isConnectingToInternet()) {
				showProgress();
				getHelp();
			}
            else {
				Toast.makeText(getApplicationContext(), "No internet connection available.", Toast.LENGTH_LONG).show();
				System.exit(0);
			}
            
            
            
            expandableListView.setOnGroupExpandListener(new OnGroupExpandListener() {

                @Override
                public void onGroupExpand(int groupPosition) {
             
                }
            });

            expandableListView.setOnGroupCollapseListener(new OnGroupCollapseListener() {

                @Override
                public void onGroupCollapse(int groupPosition) {
  

                }
            });

            expandableListView.setOnChildClickListener(new OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v,
                                            int groupPosition, int childPosition, long id) {
               
                    return false;
                }
            });
        }
    
    public int GetDipsFromPixel(float pixels)
    {
     // Get the screen's density scale
     final float scale = getResources().getDisplayMetrics().density;
     // Convert the dps to pixels, based on density scale
     return (int) (pixels * scale + 0.5f);
    }
    

    @Override
    public void onBackPressed()
    {
         // code here to show dialog
         super.onBackPressed();  // optional depending on your needs

             startActivity(new Intent(HelpActivity.this,CustomerHomeActivity.class));
        	 
    }
    

	private void getHelp(){
		StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_help,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {

						hideProgress();

						System.out.println("GETHELP==="+response);
						try {
							expandableListDetail = ExpandableListDataPump.getData(response);
						} catch (JSONException e) {
							e.printStackTrace();
						}

						if(expandableListDetail.size()>0){


							expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
							expandableListAdapter = new ExpandableListAdapter(HelpActivity.this, expandableListTitle, expandableListDetail);
							expandableListView.setAdapter(expandableListAdapter);

						}else {

							Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
							System.exit(0);
						}



					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
					hideProgress();
					retryDialog(1);
					}


				})
		{
			@Override
			protected Map<String,String> getParams(){

				Map<String,String> params = new HashMap<String, String>();
				params.put("user_Type", user_type);


				return params;
			}

		};

		RequestQueue requestQueue = Volley.newRequestQueue(this);
		int socketTimeout = 60000;//60 seconds - change to what you want
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		stringRequest.setRetryPolicy(policy);
		requestQueue.add(stringRequest);
	}

    @Override
	protected void onResume() {
	  super.onResume();
	  MyApplication.activityResumed();
	}

	@Override
	protected void onPause() {
	  super.onPause();
	  MyApplication.activityPaused();
	}



	Dialog retry;
	private void retryDialog(final int i){

		retry = new Dialog(HelpActivity.this, R.style.Theme_Dialog);
		retry.requestWindowFeature(Window.FEATURE_NO_TITLE);
		Window window = retry.getWindow();
		window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);
		retry.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		retry.setContentView(R.layout.thaks_dialog_twobutton);
		retry.setCancelable(false);
		retry.setCanceledOnTouchOutside(false);
		retry.show();

		TextView mTitle=(TextView)retry.findViewById(R.id.txt_title);
		mTitle.setText("Error!");
		mTitle.setTypeface(tf, Typeface.BOLD);
		TextView mDialogBody=(TextView)retry.findViewById(R.id.txt_dialog_body);
		mDialogBody.setText("The server is taking too long to respond OR something is wrong with your internet connection.");
		LinearLayout mOK=(LinearLayout)retry.findViewById(R.id.lin_Ok);
		LinearLayout mCancel=(LinearLayout)retry.findViewById(R.id.lin_cancel);
		TextView mRetry=(TextView)retry.findViewById(R.id.txt_dialog_ok);
		TextView mExit=(TextView)retry.findViewById(R.id.txt_dialog_cancel);
		mRetry.setText("RETRY");
		mExit.setText("EXIT");

		mOK.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {


				retry.dismiss();

				switch (i)
				{
					case 1:
						if(cd.isConnectingToInternet())
						{
							showProgress();
						getHelp();
						}

						else{
							Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_SHORT).show();
							System.exit(0);
						}
						break;

					default:
						break;

				}



			}
		});

		mCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				retry.dismiss();
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);

			}
		});

	}

}
    
    
    
    
  

    
