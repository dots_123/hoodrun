package com.us.hoodruncustomer.commonwork;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.us.hoodruncustomer.splash.SplashActivity;

/**
 * Created by Sreenadh on 16-Sep-16.
 */
public class DeepLinkActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().getAction() == Intent.ACTION_VIEW) {
            Uri uri = getIntent().getData();
            // do stuff with uri
            startActivity(new Intent(DeepLinkActivity.this,SplashActivity.class));
            finish();
            System.out.println("IIIII==="+uri.toString());
        }


    }
}
