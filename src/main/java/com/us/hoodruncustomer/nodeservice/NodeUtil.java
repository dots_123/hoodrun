package com.us.hoodruncustomer.nodeservice;

/**
 * Created by admin on 23-Mar-17.
 */

public class NodeUtil {

    public static final String NODE_CONNECTION = "com.ds.hoponn.node_connection";
    public static final String NODE_DISCONNECTION = "com.ds.hoponn.node_disconnection";
    public static final String NODE_TIMEOUT = "com.ds.hoponn.node_timeout";
    public static final String NODE_CONNECTION_ERROR = "com.ds.hoponn.node_connectionerror";


    public static final String NODE_DRIVER_REQUESTS = "com.ds.hoponn.node_driverrequests";
    public static final String NODE_DRIVER_FUTURE_REQUESTS = "com.ds.hoponn.node_driverrequests_future";
    public static final String NODE_DRIVER_ACTIONS = "com.ds.hoponn.node_driveractions";
    public static final String NODE_CUSTOMER_ACTIONS = "com.ds.hoponn.node_customeractions";
    public static final String NODE_CUSTOMER_TRACKRIDE = "com.ds.hoponn.node_customertrackride";
    public static final String NODE_CUSTOMER_CAR_UPDATE = "com.ds.hoponn.node_carlocationupdate";
    public static final String NODE_CUSTOMER_LOCATION_UPDATE = "com.ds.hoponn.node_customerlocationupdate";
    public static final String NODE_CUSTOMER_LOCATION_OUT_OF_RANGE = "com.ds.hoponn.node_outofrangelocationupdate";
    public static final String NODE_CUSTOMER_FIREBASE_UPDATE = "com.ds.hoponn.node_firebaseupdate";
    public static final String NODE_CUSTOMER_CONFIRM_PAYMENT = "com.ds.hoponn.node_customerconfirmpayment";
    public static final String NODE_CUSTOMER_LOGOUT = "com.ds.hoponn.node_logout";
    public static final String NODE_CUSTOMER_GETCARS = "com.ds.hoponn.node_getcars";
    public static final String NODE_CUSTOMER_SHARED = "com.ds.hoponn.node_shared";

    public static final String NODE_REQUEST_FLIP = "com.ds.hoponn.request_flip";

    public static final String CONNECTIVITY_STATE_ONLINE = "com.ds.hoponn.CONNECTIVITY_STATE_ONLINE";
    public static final String CONNECTIVITY_STATE_OFFLINE = "com.ds.hoponn.CONNECTIVITY_STATE_OFFLINE";


    /**
     * Notify Part
     */

    public static final String NODE_CUSTOMER_DRIVER_RESPONCE = "com.ds.hoponn.node_driverresponce";


}
