package com.us.hoodruncustomer.model;

public class History {


	public String surge_applied;
	public String surge;
	public String ride_id;
	public String name;
	public String trip_distance;
	public String trip_duration;
	public String total;

	public String getPromo_code() {
		return promo_code;
	}

	public void setPromo_code(String promo_code) {
		this.promo_code = promo_code;
	}

	public String getPromotion_discount() {
		return promotion_discount;
	}

	public void setPromotion_discount(String promotion_discount) {
		this.promotion_discount = promotion_discount;
	}

	public String promo_code;
	public String promotion_discount;
	public String driver_cancel_status;
	public String customer_cancel_status;
	public String pickup_location_name;
	public String destination_name;
	public String payment_type;
	public String ride_map;
	public String image;
	public String vehicle_type;
	public String vehicle_model;
	public String ride_time;
	public String customer_rating;
	public String detailed_ride_map;
	public String share_status;
	public String getSurge_applied() {
		return surge_applied;
	}

	public void setSurge_applied(String surge_applied) {
		this.surge_applied = surge_applied;
	}

	public String getSurge() {
		return surge;
	}

	public void setSurge(String surge) {
		this.surge = surge;
	}
	public String getShare_status() {
		return share_status;
	}
	public void setShare_status(String share_status) {
		this.share_status = share_status;
	}
	public String getShare_amt() {
		return share_amt;
	}
	public void setShare_amt(String share_amt) {
		this.share_amt = share_amt;
	}
	public String getShare_participant() {
		return share_participant;
	}
	public void setShare_participant(String share_participant) {
		this.share_participant = share_participant;
	}
	public String share_amt;
	public String share_participant;
	
	public String getDetailed_ride_map() {
		return detailed_ride_map;
	}
	public void setDetailed_ride_map(String detailed_ride_map) {
		this.detailed_ride_map = detailed_ride_map;
	}
	public String getCustomer_rating() {
		return customer_rating;
	}
	public void setCustomer_rating(String customer_rating) {
		this.customer_rating = customer_rating;
	}
	public String getVehicle_type() {
		return vehicle_type;
	}
	public void setVehicle_type(String vehicle_type) {
		this.vehicle_type = vehicle_type;
	}
	public String getVehicle_model() {
		return vehicle_model;
	}
	public void setVehicle_model(String vehicle_model) {
		this.vehicle_model = vehicle_model;
	}
	public String getRide_time() {
		return ride_time;
	}
	public void setRide_time(String ride_time) {
		this.ride_time = ride_time;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String driver_image) {
		this.image = driver_image;
	}
	public String getRide_map() {
		return ride_map;
	}
	public void setRide_map(String ride_map) {
		this.ride_map = ride_map;
	}
	public String getRide_id() {
		return ride_id;
	}
	public void setRide_id(String ride_id) {
		this.ride_id = ride_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String driver_name) {
		this.name = driver_name;
	}
	public String getTrip_distance() {
		return trip_distance;
	}
	public void setTrip_distance(String trip_distance) {
		this.trip_distance = trip_distance;
	}
	public String getTrip_duration() {
		return trip_duration;
	}
	public void setTrip_duration(String trip_duration) {
		this.trip_duration = trip_duration;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getDriver_cancel_status() {
		return driver_cancel_status;
	}
	public void setDriver_cancel_status(String driver_cancel_status) {
		this.driver_cancel_status = driver_cancel_status;
	}
	public String getCustomer_cancel_status() {
		return customer_cancel_status;
	}
	public void setCustomer_cancel_status(String customer_cancel_status) {
		this.customer_cancel_status = customer_cancel_status;
	}
	public String getPickup_location_name() {
		return pickup_location_name;
	}
	public void setPickup_location_name(String pickup_location_name) {
		this.pickup_location_name = pickup_location_name;
	}
	public String getDestination_name() {
		return destination_name;
	}
	public void setDestination_name(String destination_name) {
		this.destination_name = destination_name;
	}
	public String getPayment_type() {
		return payment_type;
	}
	public void setPayment_type(String payment_type) {
		this.payment_type = payment_type;
	}
	

}
