package com.us.hoodruncustomer.commonwork;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.parser.ConnectionDetector;

public class TransferActivity extends BaseActivity {


    Dialog dialog3, dialog1;

    String amount;
    TextView walletBal;
    EditText mAmount;
    String font_path, wallet_amount;
    Typeface tf;
    ConnectionDetector cd;
    TextView headertext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_transfer);
        MyApplication.activity = TransferActivity.this;

        font_path = "fonts/opensans.ttf";

        tf = Typeface.createFromAsset(this.getAssets(), font_path);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            int color = Color.parseColor("#ffffff");
            toolbar.setTitleTextColor(color);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            headertext = (TextView) findViewById(android.R.id.text1);
            headertext.setText("Wallet");
            headertext.setTypeface(tf, Typeface.BOLD);

        }
        cd = new ConnectionDetector(TransferActivity.this);
        showBalance();

        walletBal = (TextView) findViewById(R.id.txt_wallwt_bal);

        walletBal.setTypeface(tf, Typeface.BOLD);

    }


    private void showBalance() {
        // TODO Auto-generated method stub
        if (cd.isConnectingToInternet()) {
            showProgress();
         //   walletBalance();
        } else {
            Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_SHORT).show();
            System.exit(0);
        }
    }


    private void DialogAlert() {

        dialog3 = new Dialog(TransferActivity.this, R.style.Theme_Dialog);
        dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog3.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog3.setContentView(R.layout.dialog_wallet_twobutton);
        dialog3.setCancelable(false);
        dialog3.setCanceledOnTouchOutside(false);
        dialog3.show();

        TextView mTitle = (TextView) dialog3.findViewById(R.id.txt_title);
        mTitle.setTypeface(tf, Typeface.BOLD);
        LinearLayout mOK = (LinearLayout) dialog3.findViewById(R.id.lin_Ok);
        LinearLayout mCancel = (LinearLayout) dialog3.findViewById(R.id.lin_cancel);
        mAmount = (EditText) dialog3.findViewById(R.id.edt_amount);


        mOK.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mAmount.getText().toString().trim().isEmpty())
                    Toast.makeText(getApplicationContext(), "Please enter amount", Toast.LENGTH_SHORT).show();
                else if (!isWithdrawable(walletBal.getText().toString().trim(), mAmount.getText().toString().trim()))
                    Toast.makeText(getApplicationContext(), "Insufficient funds", Toast.LENGTH_SHORT).show();
                else {
                    dialog3.dismiss();

                    DialogThanks();
                }
            }
        });

        mCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                dialog3.dismiss();

            }
        });


    }


    private void DialogThanks() {

        dialog1 = new Dialog(TransferActivity.this, R.style.Theme_Dialog);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog1.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog1.setContentView(R.layout.thaks_dialog_twobutton);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);
        dialog1.show();

        TextView mTitle = (TextView) dialog1.findViewById(R.id.txt_title);
        mTitle.setTypeface(tf, Typeface.BOLD);
        TextView mDialogBody = (TextView) dialog1.findViewById(R.id.txt_dialog_body);
        mDialogBody.setText("Your withdrawal request has been sent to Hoponn admin. " + "\n" + "\n" + "Please allow 2-3 working days for the transfer.");
        LinearLayout mOK = (LinearLayout) dialog1.findViewById(R.id.lin_Ok);
        LinearLayout mCancel = (LinearLayout) dialog1.findViewById(R.id.lin_cancel);


        mOK.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                dialog1.dismiss();
                wallet_amount = mAmount.getText().toString().trim();
                walletUpdation();

            }
        });

        mCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                dialog1.dismiss();

            }
        });

    }


    private void walletUpdation() {

        if (cd.isConnectingToInternet()) {
            showProgress();
            //driverRedeemWallet();
        } else {
            Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_SHORT).show();
            System.exit(0);
        }
    }


    Dialog dialog2;

    private void Dialog() {

        dialog2 = new Dialog(TransferActivity.this, R.style.Theme_Dialog);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog2.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog2.setContentView(R.layout.dialog_onebutton);
        dialog2.setCancelable(false);
        dialog2.setCanceledOnTouchOutside(false);
        dialog2.show();

        TextView mTitle = (TextView) dialog2.findViewById(R.id.text_title);
        mTitle.setTypeface(tf, Typeface.BOLD);
        TextView mDialogBody = (TextView) dialog2.findViewById(R.id.txt_dialog_body);
        TextView mOK = (TextView) dialog2.findViewById(R.id.txt_dialog_ok);

        mTitle.setText("SUCCESS");

        mDialogBody.setText("Your request sent successfully.");

        mOK.setText("OK");
        mOK.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                dialog2.dismiss();

            }
        });

    }


    public boolean isWithdrawable(String balance, String withdraw) {

        balance = balance.replaceAll(getResources().getString(R.string.pound), "");
        float bal = Float.parseFloat(balance);
        float wdraw = Float.parseFloat(withdraw);

        if (bal > wdraw)
            return true;
        else

            return false;


    }



    @Override
    public void onBackPressed() {
        // code here to show dialog
        super.onBackPressed();  // optional depending on your needs
    }

//    private void walletBalance() {
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_wallet_balance,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//
//                        System.out.println("WALLETBALANCE===" + response);
//                        JSONObject dataObject = null;
//                        try {
//                            dataObject = new JSONObject(response);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                        parsresp = dataObject.optString("status");
//
//                        if (!TextUtils.isEmpty(parsresp)) {
//
//                            switch (parsresp) {
//                                case "TRUE":
//                                    JSONObject jObject;
//                                    try {
//                                        jObject = new JSONObject(response);
//                                        JSONObject jObj = jObject.getJSONObject("result");
//
//                                        amount = jObj.optString("wallet").toString();
//                                        walletBal.setText(getResources().getString(R.string.pound) + " " + amount);
//                                        loader.dismiss();
//                                    } catch (JSONException e) {
//                                        // TODO Auto-generated catch block
//                                        e.printStackTrace();
//                                    }
//                                    break;
//                            }
//                        } else if (parsresp.equals("FALSE")) {
//                            loader.dismiss();
//                            walletBal.setText(getResources().getString(R.string.pound) + " ---");
//                            Toast.makeText(getApplicationContext(), "Can't connect to wallet now", Toast.LENGTH_SHORT).show();
//
//
//                        } else {
//                            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
//                            System.exit(0);
//                        }
//
//
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//
//                        retryDialog(1);
//                    }
//
//
//                }) {
//            @Override
//            protected Map<String, String> getParams() {
//
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("token_code", mAppManager.getTokenCode());
//                params.put("driver_id", mAppManager.getDriverID_Number());
//
//                return params;
//            }
//
//        };
//
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//        int socketTimeout = 60000;//60 seconds - change to what you want
//        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        stringRequest.setRetryPolicy(policy);
//        requestQueue.add(stringRequest);
//    }




    Dialog retry;

//    private void retryDialog(final int i) {
//
//        retry = new Dialog(TransferActivity.this, R.style.Theme_Dialog);
//        retry.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        Window window = retry.getWindow();
//        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
//        window.setGravity(Gravity.CENTER);
//        retry.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        retry.setContentView(R.layout.thaks_dialog_twobutton);
//        retry.setCancelable(false);
//        retry.setCanceledOnTouchOutside(false);
//        retry.show();
//
//        TextView mTitle = (TextView) retry.findViewById(R.id.txt_title);
//        mTitle.setText("Error!");
//        mTitle.setTypeface(tf, Typeface.BOLD);
//        TextView mDialogBody = (TextView) retry.findViewById(R.id.txt_dialog_body);
//        mDialogBody.setText("The server is taking too long to respond OR something is wrong with your internet connection.");
//        LinearLayout mOK = (LinearLayout) retry.findViewById(R.id.lin_Ok);
//        LinearLayout mCancel = (LinearLayout) retry.findViewById(R.id.lin_cancel);
//        TextView mRetry = (TextView) retry.findViewById(R.id.txt_dialog_ok);
//        TextView mExit = (TextView) retry.findViewById(R.id.txt_dialog_cancel);
//        mRetry.setText("RETRY");
//        mExit.setText("EXIT");
//
//        mOK.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//
//                retry.dismiss();
//
//                switch (i) {
//                    case 1:
//                        if (cd.isConnectingToInternet()) {
//                            showProgress();
//                            walletBalance();
//                        } else {
//                            Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_SHORT).show();
//                            System.exit(0);
//                        }
//                        break;
//                    case 2:
//                        if (cd.isConnectingToInternet()) {
//                            showProgress();
//                            driverRedeemWallet();
//                        } else {
//                            Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_SHORT).show();
//                            System.exit(0);
//                        }
//                        break;
//                    default:
//                        break;
//
//                }
//
//
//            }
//        });
//
//        mCancel.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                retry.dismiss();
//                Intent intent = new Intent(Intent.ACTION_MAIN);
//                intent.addCategory(Intent.CATEGORY_HOME);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);
//
//            }
//        });
//
//    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.activityPaused();
    }
}
