package com.us.hoodruncustomer.fcm;

/**
 * Created by sr034 on 24-Jun-16.
 */

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.splash.SplashActivity;
import com.us.hoodruncustomer.commonwork.SuperBaseActivity;
import com.us.hoodruncustomer.db.AppManager;

import com.us.hoodruncustomer.nodeservice.NodeUtil;
import com.us.hoodruncustomer.parser.DriverParser;
import com.us.hoodruncustomer.parser.Parser;
import com.us.hoodruncustomer.parser.StatusParser;
import com.us.hoodruncustomer.home.CustomerHomeActivity;
import com.us.hoodruncustomer.trackride.RideReceipt;
import com.us.hoodruncustomer.trackride.TrackRide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import static com.us.hoodruncustomer.commonwork.SuperBaseActivity.RECEIVER_FUTURE_REQUEST;
import static com.us.hoodruncustomer.commonwork.SuperBaseActivity.RECEIVER_REFER_OFFER;
import static com.us.hoodruncustomer.home.ConfirmRideActivity.RECEIVER_CONFIRM_RIDE;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final int NOTIFICATION_ID = 1000;
    private static final String TAG = "MyFirebaseMsgService";
    public static int suspend_type = 0;
    public static String message_id, messages, title, reason_type, push_Status_Flag = "";
    String message;
    String id;
    PendingIntent contentIntent;
    NotificationManager mNotificationManager;
    AppManager mAppManager;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override

    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.

        mAppManager = AppManager.getInstance(MyApplication.getAppContext());

        Log.d(TAG, "From: " + remoteMessage.getFrom());

        Map data = remoteMessage.getData();

        message = (String) data.get("message");

        Log.d(TAG, "Message: " + message);

        System.out.println("ParseGCM---" + message);

        try {

            JSONObject dataObject = new JSONObject(message);
            id = dataObject.optString("id").toString();
            message_id = dataObject.optString("message_id").toString();
            messages = dataObject.optString("message").toString();
            title = dataObject.optString("title").toString();
            reason_type = dataObject.optString("reason_type").toString();
            push_Status_Flag = dataObject.optString("push_Status_Flag");
            suspend_type = dataObject.optInt("reason_type");

            Log.d(TAG, "InNewRide - push_Status_Flag: " + push_Status_Flag);

            switch (push_Status_Flag) {

                case "1": /* New ride is available */

                    if (mAppManager.getRequest_ID() != null && !mAppManager.getRequest_ID().equalsIgnoreCase("")) {
                        sendFutureBroadcastData(message);
                    } else {

                        DriverParser.parseDriverRequest(message);


                        if (MyApplication.isActivityVisible()) {
                            Log.d(TAG, "InNewRide - Node: " + message);
                            io.socket.client.Socket socket = ((MyApplication) getApplication()).getSocket();


                            if (MyApplication.currentActivity != null && !MyApplication.currentActivity.equalsIgnoreCase("CustomerDetailsActivity")) {
                                sendBroadcastDataNewRide();
                            }


                        } else {
                            MyApplication.startPlayer(R.raw.hoponn_request);
                            Log.d(TAG, "InNewRide - Push: " + message);
                            sendNotifications(messages);
                        }
                    }

                    break;

                case "2":/* Driver rejected the ride request */
                    contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                            SplashActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setAction(Intent.ACTION_MAIN)
                            .addCategory(Intent.CATEGORY_LAUNCHER), 0);
                    if (MyApplication.isActivityVisible() && mAppManager.getCurrentActivity().equals("ConfirmRideActivity")) {
                        Intent started = new Intent(RECEIVER_CONFIRM_RIDE);
                        started.putExtra("status", "rejected");
                        started.putExtra("data", message);
                        getApplicationContext().sendBroadcast(started);
                    } else {
                        sendNotifications(messages);
                    }
                    break;

                case "3": /* Driver accepted the ride request */

                    StatusParser.getDriverResponse(message);
                    contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                            SplashActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setAction(Intent.ACTION_MAIN)
                            .addCategory(Intent.CATEGORY_LAUNCHER), 0);
                    if (MyApplication.isActivityVisible() && mAppManager.getCurrentActivity().equals("ConfirmRideActivity")) {
                        Intent started = new Intent(RECEIVER_CONFIRM_RIDE);
                        started.putExtra("status", "accepted");
                        started.putExtra("data", message);
                        getApplicationContext().sendBroadcast(started);
                    } else {
                        if (!MyApplication.isActivityVisible()) {
                            sendNotifications(messages);
                        }
                    }
                    break;

                case "4": /* Driver has arrived at the the pick up location */

                    StatusParser.getDriverResponse(message);
                    contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                            SplashActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setAction(Intent.ACTION_MAIN)
                            .addCategory(Intent.CATEGORY_LAUNCHER), 0);
                    if (MyApplication.isActivityVisible() && mAppManager.getCurrentActivity().equals("TrackRide")) {
                        Intent started = new Intent(NodeUtil.NODE_CUSTOMER_FIREBASE_UPDATE);
                        started.putExtra("data", message);
                        getApplicationContext().sendBroadcast(started);
                    } else {
                        if (!MyApplication.isActivityVisible()) {
                            sendNotifications(messages);
                        }
                    }
                    break;

                case "5": /* Your journey is started now. */

                    StatusParser.getDriverResponse(message);
                    contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                            SplashActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setAction(Intent.ACTION_MAIN)
                            .addCategory(Intent.CATEGORY_LAUNCHER), 0);

                    if (MyApplication.isActivityVisible() && mAppManager.getCurrentActivity().equals("TrackRide")) {
                        Intent started = new Intent(NodeUtil.NODE_CUSTOMER_FIREBASE_UPDATE);
                        started.putExtra("data", message);
                        getApplicationContext().sendBroadcast(started);
                    } else {
                        if (!MyApplication.isActivityVisible()) {
                            sendNotifications(messages);
                        }
                    }

                    break;

                case "6": /* Driver end the journey */

                    StatusParser.getDriverJourney(message);

                    JSONObject mObject = new JSONObject(message);

//                    int fav_status=mObject.getInt("favourite_status");

                    mAppManager.saveTripDistance(mObject.getString("trip_distance"));
                    mAppManager.saveTripDuration(mObject.getString("trip_duration"));
                    mAppManager.saveTotal(mObject.getString("total"));
                    mAppManager.saveDiscountStatus(mObject.getString("discount_applied"));
                    mAppManager.saveDiscount(mObject.getString("discount_total"));
//                    mAppManager.saveFavouriteStatus(fav_status);

                    contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                            SplashActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setAction(Intent.ACTION_MAIN)
                            .addCategory(Intent.CATEGORY_LAUNCHER), 0);
                    if (MyApplication.isActivityVisible() && mAppManager.getCurrentActivity().equals("TrackRide")) {
                        Intent started = new Intent(NodeUtil.NODE_CUSTOMER_FIREBASE_UPDATE);
                        started.putExtra("data", message);
                        getApplicationContext().sendBroadcast(started);
                    } else {
                        if (!MyApplication.isActivityVisible()) {
                            sendNotifications(messages);
                        }
                    }
//                    if(MyApplication.isActivityVisible()){
//                        sendBroadcastData();
//                    }else if(mAppManager.getCurrentActivity().equals("TrackRide"))
//                        sendNotifications(messages);
                    break;

                case "7": /* The request is expired */
                    contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                            SplashActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setAction(Intent.ACTION_MAIN)
                            .addCategory(Intent.CATEGORY_LAUNCHER), 0);
                    sendBroadcastData();
                    sendNotifications(messages);
                    break;
                case "8": /* Customer feedback */
                    StatusParser.getCustomerFeedback(message);
                    contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                            SplashActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setAction(Intent.ACTION_MAIN)
                            .addCategory(Intent.CATEGORY_LAUNCHER), 0);
                    if (MyApplication.isActivityVisible()) {
                        sendBroadcastData();
                    } else
                        sendNotifications(messages);
                    break;

                case "9": /* Ride is cancelled by the customer */
                    StatusParser.getDriverResponse(message);
                    contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                            SplashActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setAction(Intent.ACTION_MAIN)
                            .addCategory(Intent.CATEGORY_LAUNCHER), 0);
                    if (MyApplication.isActivityVisible()) {
                        sendBroadcastData();
                    } else
                        sendNotifications(messages);
                    break;

                case "10": /* The ride is cancelled by the driver */
                    contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                            SplashActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setAction(Intent.ACTION_MAIN)
                            .addCategory(Intent.CATEGORY_LAUNCHER), 0);

                    if (MyApplication.isActivityVisible()) {

                        try {
                            JSONObject jsonObject = new JSONObject(message);
                            String push_ride_id = jsonObject.optString("ride_id");
                            if (push_ride_id != null && push_ride_id.length() > 0) {
                                sendBroadcastCancel();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else
                        sendNotifications(messages);

                    break;

                case "11": /* main_customer_name wants to share hoponn ride with you  */
                    contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                            CustomerHomeActivity.class), 0);
                    StatusParser.setShared(message);

                    if (MyApplication.isActivityVisible()) {
                        sendBroadcastData();
                    } else
                        sendNotifications(messages);
                    break;

                case "12": /* Actual fare you want to share for this ride is ".$share_amt.*/

                    StatusParser.setSharedDetails(message);

                    messages = "HOPONN SHARE AMOUNT";
                    contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                            RideReceipt.class), 0);
                    if (MyApplication.isActivityVisible()) {
                        sendBroadcastData();
                    } else
                        sendNotifications(messages);
                    break;

                case "13": /* Secondary customer name  accepted/rejected the share request  */

                    StatusParser.getShareResponseSec(message);
                    contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                            SplashActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setAction(Intent.ACTION_MAIN)
                            .addCategory(Intent.CATEGORY_LAUNCHER), 0);
                    if (MyApplication.isActivityVisible()) {
                        Intent i = new Intent(NodeUtil.NODE_CUSTOMER_SHARED);
                        i.putExtra("push flag", push_Status_Flag);
                        i.putExtra("push message", messages);
                        getApplicationContext().sendBroadcast(i);
                    } else
                        sendNotifications(messages);

//                    if(MyApplication.isActivityVisible()){
//                        sendBroadcastData();
//
//                    }else {
//                        contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
//                                SplashActivity.class), 0);
//                        sendNotifications(messages);
//                    }
                    break;

                case "14": /* Driver is not responding  */
                    contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                            SplashActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP), 0);
                    sendNotifications(messages);
                    break;


                case "15": /* The ride is cancelled by the driver share */
                    StatusParser.getShareCancelledByDriver(message);
                    contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                            SplashActivity.class), 0);
                    if (MyApplication.isActivityVisible()) {
                        sendBroadcastData();
                    } else
                        sendNotifications(messages);
                    break;
                case "16": /* The ride is cancelled by the customer share */
                    StatusParser.getShareCancelByPrimaryCustomer(message);
                    contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                            SplashActivity.class), 0);
                    if (MyApplication.isActivityVisible()) {
                        sendBroadcastData();
                    } else
                        sendNotifications(messages);
                    break;
                case "17": /* The request is expired share*/
                    StatusParser.getShareExpiredToSecondaryCustomer(message);
                    contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                            SplashActivity.class), 0);
                    if (MyApplication.isActivityVisible()) {
                        sendBroadcastData();
                    } else
                        sendNotifications(messages);
                    break;


                case "19": /*customer_name completed the payment successfully */
                    contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                            SplashActivity.class), 0);

                    Intent i = new Intent(RideReceipt.RECEIVER_PAYMENT_DONE);
                    getApplicationContext().sendBroadcast(i);

                    if (MyApplication.isActivityVisible()) {
                        sendBroadcastData();
                    } else
                        sendNotifications(messages);

                    break;

                case "20": /*driver rejected the shared request */
                    contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                            SplashActivity.class), 0);
                    if (MyApplication.isActivityVisible()) {
                        sendBroadcastData();
                    } else
                        sendNotifications(messages);

                    break;

                case "21": /*driver is suspended */
                    contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                            SplashActivity.class), 0);
                    if (MyApplication.isActivityVisible()) {
                        sendBroadcastDataToDriver();
                    } else
                        sendNotifications(messages);

                    break;

                case "22": /*customer is suspended */
                    contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                            SplashActivity.class), 0);
                    if (MyApplication.isActivityVisible()) {
                        sendBroadcastDataToCustomer();
                    } else
                        sendNotifications(messages);

                    break;


                case "25": /*Driver changed drop off location */

                    JSONObject locationchnaged = new JSONObject(message);
                    mAppManager.App_setDriver_customer_destination_point_lat(locationchnaged.optString("customer_destination_point_lat"));
                    mAppManager.App_setDriver_customer_destination_point_long(locationchnaged.optString("customer_destination_point_long"));
                    mAppManager.App_setDriver_customer_destination_point_name(locationchnaged.optString("destination_Location_Name"));


                    if (MyApplication.isActivityVisible()) {
                        sendBroadcastDataToCustomerTrack(message);
                    }

                    break;

                case "23": /*customer request response*/

                    Parser.shareSubmitPUSH(message);

                    JSONObject dataObject_responce = new JSONObject(message);

                    mAppManager.saveCancelCharge(dataObject_responce.optString("customer_cancel_fee").toString());
                    mAppManager.saveRequest_ID(dataObject_responce.optString("request_id").toString());
                    mAppManager.saveCurrentDriverId(dataObject_responce.optString("driver_id").toString());
                    mAppManager.saveCurrentDriverName(dataObject_responce.optString("driver_name").toString());
                    mAppManager.saveCurrentDriverProfilePic(dataObject_responce.optString("driver_profile_pic").toString());
                    mAppManager.saveCurrentDriverVEHICLEModel(dataObject_responce.optString("driver_vehicle_model").toString());
                    mAppManager.saveCurrentDriverVEHICLENUMBER(dataObject_responce.optString("driver_vehicle_number").toString());

                    sendBroadcastDataToCustomerDetails();

                    break;

                case "26": /* Push notification for driver offers */

                    if (MyApplication.isActivityVisible()) {
                        Intent offer = new Intent(RECEIVER_FUTURE_REQUEST);
                        offer.putExtra("request", "offer");
                        getApplicationContext().sendBroadcast(offer);
                    } else {
                        contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                                SplashActivity.class), 0);
                        sendNotifications(messages);
                    }
                    break;

                case "27": /* Push notification for customer offers */

                    if (MyApplication.isActivityVisible()) {
                        Intent offer = new Intent(RECEIVER_REFER_OFFER);
                        offer.putExtra("request", "offer");
                        getApplicationContext().sendBroadcast(offer);
                    } else {
                        contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                                SplashActivity.class), 0);
                        sendNotifications(messages);
                    }
                    break;


                case "51": /* Push notification for License expiry time */
                    if (MyApplication.isActivityVisible()) {
                        sendBroadcastTimeExpiry();
                    } else {
                        contentIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                                SplashActivity.class), 0);
                        sendNotifications(messages);
                    }

                    break;
                case "31": /* Push notification for customer offers */

                    break;

                case "32": /* Push notification for customer offers */


                    break;

                case "33": /* Push notification for customer offers */

                    break;

                case "34": /* Push notification for customer offers */

                    break;

                case "24": /* Push notification for customer offers */

                    break;

                default:
                    if (MyApplication.isActivityVisible()) {
                        sendBroadcastData();
                    } else
                        sendNotifications(messages);
                    break;
            }


        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }
    // [END receive_message]


    private void sendNotifications(String msg) {

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this).setSmallIcon(R.drawable.centered)
                .setContentTitle("Hoponn")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setContentText(msg);
        mBuilder.setAutoCancel(true);

        mBuilder.setContentIntent(contentIntent);
        AssetFileDescriptor afd = null;
        if (push_Status_Flag.equals("1")) {

//            if(!MyApplication.isActivityVisible()) {
//                MyApplication.startPlayer(R.raw.hoponn_request);
//            }
        } else {
            if (!MyApplication.isActivityVisible()) {
                MyApplication.startPlayer(R.raw.wolf);
            }
        }

        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

    }

    private void sendFutureBroadcastData(String response) {

        Intent i = new Intent(RECEIVER_FUTURE_REQUEST);
        i.putExtra("request", response);
        getApplicationContext().sendBroadcast(i);
        MyApplication.startPlayer(R.raw.wolf);

    }

    private void sendBroadcastCancel() {
        Intent intent = new Intent(TrackRide.RECEIVER_RIDE_CANCELLED);
        intent.putExtra("push_flag", push_Status_Flag);
        intent.putExtra("push_message", messages);
        sendBroadcast(intent);
    }

    private void sendBroadcastTimeExpiry() {
        Intent intent = new Intent(SuperBaseActivity.RECEIVER_SUSPEND_CUSTOMER_Time_ExPIRY);
        intent.putExtra("push_message", messages);
        sendBroadcast(intent);
    }

    private void sendBroadcastData() {
        Intent intent = new Intent();
        intent.setAction("MyDialog");
        intent.putExtra("push flag", push_Status_Flag);
        intent.putExtra("push message", messages);
        sendBroadcast(intent);
        AssetFileDescriptor afd = null;
        if (MyApplication.apprunning == 1) {
            MyApplication.startPlayer(R.raw.wolf);
        }
    }

    private void sendBroadcastDataNewRide() {
        Intent intent = new Intent(SuperBaseActivity.RECEIVER_SUSPEND_CUSTOMER_NEW_RIDE);
        intent.putExtra("push_flag", push_Status_Flag);
        intent.putExtra("push_message", message);
        sendBroadcast(intent);
    }

    private void sendBroadcastDataToDriver() {

        Intent intent = new Intent(SuperBaseActivity.RECEIVER_SUSPEND_DRIVER_SUPER);
        intent.putExtra("push_flag", push_Status_Flag);
        intent.putExtra("push_message", messages);
        intent.putExtra("suspend_type", suspend_type);
        sendBroadcast(intent);

    }

    private void sendBroadcastDataToCustomerTrack(String response) {

        Intent intent = new Intent(TrackRide.RECEIVER_DROP_CHNAGED);
        intent.putExtra("dropResponse", response);
        sendBroadcast(intent);

    }

    private void sendBroadcastDataToCustomer() {

        Intent intent = new Intent(CustomerHomeActivity.RECEIVER_SUSPEND_CUSTOMER);
        intent.putExtra("push_flag", push_Status_Flag);
        intent.putExtra("push_message", messages);
        intent.putExtra("suspend_type", suspend_type);
        sendBroadcast(intent);

    }

    private void sendBroadcastDataToCustomerDetails() {

        Intent i = new Intent(NodeUtil.NODE_CUSTOMER_DRIVER_RESPONCE);
        i.putExtra("Object", message);
        sendBroadcast(i);

    }


}