package com.us.hoodruncustomer.fragment;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.emilsjolander.components.StickyScrollViewItems.StickyScrollView;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.us.hoodruncustomer.R;

import com.us.hoodruncustomer.configure.Configure;
import com.us.hoodruncustomer.custom.ExpandableHeightListView;
import com.us.hoodruncustomer.db.AppManager;

import com.us.hoodruncustomer.model.AnalticsListAdapter;
import com.us.hoodruncustomer.model.Analytics;
import com.us.hoodruncustomer.parser.ConnectionDetector;
import com.us.hoodruncustomer.parser.Parser;
import com.us.hoodruncustomer.home.CustomerHomeActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class AnalyticHistory extends Fragment implements View.OnClickListener {

    ExpandableHeightListView mHistoryListView;
    Toolbar toolbar;
    TextView headertext, clear, totalPayout, graphPeriod;
    ImageView filter, ivPrevious, ivNext;
    String font_path;
    Typeface tf;
    AppManager mAppManager;
    String mURL;
    String parsresp;
    String date = "";

    private boolean scrollstatus = true;
    private ViewSwitcher viewSwitcher;
    public static ArrayList<Analytics> getmAnalyticsList() {
        return mAnalyticsList;
    }

    public static ArrayList<Analytics> mAnalyticsList = new ArrayList<Analytics>();
    ConnectionDetector cd;
    String user_type = "";
    AnalticsListAdapter analticsListAdapter;
    int offset = 0;
    int AnalyticsSize;
    boolean scroll_flag = true;
    private DatePicker datePicker;
    private int year, month, day;
    private Calendar calendar;
    String type = "";
    String selectedMonth = "", selectedDate = "", selectedYear = "", action = "";
    BarChart barchart;
    String heading = "";

    ArrayList<BarEntry> BARENTRY = new ArrayList<BarEntry>();
    ArrayList<String> BarEntryLabels = new ArrayList<String>();
    BarDataSet Bardataset;
    BarData BARDATA;

    StickyScrollView scrollView;

    public AnalyticHistory() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_analytic_history, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        initView(view);
    }

    private void initView(View view) {

        selectedMonth = getArguments().getString("SMONTH");
        selectedDate = getArguments().getString("DATE");
        selectedYear = getArguments().getString("SYEAR");
        action = getArguments().getString("ACTION");
        type = getArguments().getString("TYPE");


        mAnalyticsList.clear();

        mAppManager = AppManager.getInstance(getActivity());
        cd = new ConnectionDetector(getActivity());
        viewSwitcher = (ViewSwitcher) view.findViewById(R.id.switcher);
        // barChartView = (BarChartView) view.findViewById(R.id.barChart);
        totalPayout = (TextView) view.findViewById(R.id.totalearnings);
        graphPeriod = (TextView) view.findViewById(R.id.graphPeriod);
        barchart = (BarChart) view.findViewById(R.id.chart);

        ivPrevious = (ImageView) view.findViewById(R.id.prevIcon);
        ivNext = (ImageView) view.findViewById(R.id.nextIcon);
        scrollView = (StickyScrollView) view.findViewById(R.id.scrollView);


        mURL = Configure.URL_ride_analytics_customer;

        mHistoryListView = (ExpandableHeightListView) view.findViewById(R.id.list_analytics);
        mHistoryListView.setExpanded(true);
        scrollView.setEnabled(false);
        mHistoryListView.setFocusable(false);


        analticsListAdapter = new AnalticsListAdapter(getActivity(), mAnalyticsList, mAppManager.getuser(),type);
        mHistoryListView.setAdapter(analticsListAdapter);

        scrollView.smoothScrollTo(0, 0);


        mHistoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(type.equalsIgnoreCase("week"))
                {
                    String dtStart = mAnalyticsList.get(position).getTime_map()+" "+year;
                    String reportDate= "";
                    SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
                    try {
                        Date date = format.parse(dtStart);
                        format = new SimpleDateFormat("yyyy-MM-dd");
                        reportDate = format.format(date);

                        System.out.println(reportDate);
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }


                }


            }
        });

        ivPrevious.setOnClickListener(this);
        ivNext.setOnClickListener(this);


        callAsync();


    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public void ReplaceFragment(String type, String selectedYear, String selectedMonth, String selectedDate, String action) {

        Bundle bundle = new Bundle();
        bundle.putString("TYPE", type);
        bundle.putString("SYEAR", selectedYear);
        bundle.putString("SMONTH", selectedMonth);
        bundle.putString("DATE", selectedDate);
        bundle.putString("ACTION", action);

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        AnalyticHistory fragment = new AnalyticHistory();
        fragment.setArguments(bundle);
        transaction.replace(R.id.fragment_container, fragment, "Initial");
        transaction.commit();
    }

    private void callAsync() {
        // TODO Auto-generated method stub

        if (cd.isConnectingToInternet()) {
            showProgress();
            historyViewer(mURL, offset);
        } else {
            Toast.makeText(getActivity(), "No internet connection avilable.", Toast.LENGTH_LONG).show();
            System.exit(0);
        }
    }

    Dialog loader;
    ProgressWheel loader_wheel;

    private void showProgress() {

        loader = new Dialog(getActivity());
        loader.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loader.setContentView(R.layout.dialog_progress);
        loader_wheel = (ProgressWheel) loader.findViewById(R.id.progress_wheel);
        loader_wheel.setCircleRadius(50);
        loader.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        loader.setCancelable(false);
        loader.show();
    }

    android.app.Dialog dialog2;

    private void Dialog() {


        TextView mDialogBody, mOK;
        dialog2 = new Dialog(getActivity(), R.style.Theme_Dialog);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog2.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog2.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);
        dialog2.setContentView(R.layout.dialog_no_title);
        dialog2.setCancelable(false);
        dialog2.setCanceledOnTouchOutside(false);
        dialog2.show();
        mDialogBody = (TextView) dialog2.findViewById(R.id.txt_dialog_body);
        mOK = (TextView) dialog2.findViewById(R.id.txt_dialog_ok);

        mDialogBody.setText("Ride history not available.");
        mOK.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (mAppManager.getuser().equals("customer")) {
                    startActivity(new Intent(getActivity(), CustomerHomeActivity.class));
                    getActivity().finish();

                }

            }
        });
    }

    private void historyViewer(String url, final int offset) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        viewSwitcher.setDisplayedChild(0);

                        System.out.println("AnalyticsVIEWER===" + response);

                        try {
                            parsresp = Parser.getAnalytics(response);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (!TextUtils.isEmpty(parsresp)) {

                            switch (parsresp) {
                                case "TRUE":
                                    AnalyticsSize = mAnalyticsList.size();
                                    Log.i("historySize", "" + AnalyticsSize);

                                    mAnalyticsList.clear();
                                    graphPeriod.setText(Parser.getHeading());
                                    heading = Parser.getHeading();

                                    selectedDate = Parser.getPrev_next_date();

                                    for (int i = 0; i < Parser.getAnalyticsList().size(); i++) {

                                        mAnalyticsList.add(AnalyticsSize, Parser.getAnalyticsList().get(i));
                                        Log.i("historySizeforloop", "" + AnalyticsSize);
                                        AnalyticsSize++;
                                    }

                                    createChart(mAnalyticsList);

                                    if(Parser.getAnalyticsList().size() > 0) {
                                        totalPayout.setVisibility(View.VISIBLE);
                                        totalPayout.setText("Total Earnings: $" + Parser.getTotal_earning());
                                    }else{
                                        totalPayout.setVisibility(View.GONE);
                                    }

                                    analticsListAdapter.notifyDataSetChanged();
                                    scrollView.setEnabled(true);

                                    break;
                                case "FALSE":


                                    switch (Parser.getAnalytics_status_message()) {
                                        case "Invalid token code":
                                            new InvalidTokenDialog(getActivity()).show();
                                            break;

                                        case "No record found":
                                            scroll_flag = false;
                                            graphPeriod.setText(Parser.getHeading());
                                            heading = Parser.getHeading();
                                            selectedDate = Parser.getPrev_next_date();

                                            if (loader != null) {
                                                loader.dismiss();
                                            }
                                            if (offset == 0) {
                                                viewSwitcher.setDisplayedChild(1);
                                            }
                                            break;
                                        case "Ride history not available":
                                            if (offset == 0) {
                                                Dialog();
                                            } else {
                                                scroll_flag = false;
                                                //Toast.makeText(getApplicationContext(), Parser.getHistory_status_message(), Toast.LENGTH_SHORT).show();
                                                if (loader != null)
                                                    loader.dismiss();
                                            }
                                            break;
                                        default:

                                            break;
                                    }

                                    break;
                            }
                            loader.dismiss();

                        } else {
                            loader.dismiss();

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        retryDialog(1);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                params.put("driver_id", mAppManager.getDriverID_Number());
                params.put("token_code", mAppManager.getTokenCode());
                params.put("type", type);
                params.put("year", selectedYear);
                params.put("month", selectedMonth);
                params.put("date", selectedDate);
                params.put("action", action);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void createChart(ArrayList<Analytics> analyticsList) {

        // for bar values
        AddValuesToBARENTRY(analyticsList);
        // for x axis labels
        AddValuesToBarEntryLabels(analyticsList);

        Bardataset = new BarDataSet(BARENTRY, "");
        BARDATA = new BarData(BarEntryLabels, Bardataset);

        List<Integer> colors = new ArrayList<>();
        colors.add(Color.GREEN);
        Bardataset.setColors(colors);

        barchart.setData(BARDATA);
        barchart.getBarData().setValueTextColor(Color.WHITE);
        barchart.getAxisLeft().setTextColor(Color.WHITE);
        barchart.getXAxis().setTextColor(Color.WHITE);
        barchart.getLegend().setTextColor(Color.WHITE);
        barchart.getXAxis().setLabelsToSkip(0);
        barchart.getXAxis().setSpaceBetweenLabels(10);
        barchart.setDescription("");
        barchart.setScaleEnabled(false);
        barchart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        barchart.getLegend().setEnabled(false);
        barchart.animateY(1500);
    }

    public void AddValuesToBARENTRY(ArrayList<Analytics> analyticsList) {


        ArrayList<Analytics> data = (ArrayList<Analytics>)analyticsList.clone();

        Collections.reverse(data);
        BARENTRY.clear();
        for (int i = 0; i < data.size(); i++) {
            BARENTRY.add(new BarEntry(Float.parseFloat(data.get(i).getEarning()), i));
        }

    }

    public void AddValuesToBarEntryLabels(ArrayList<Analytics> analyticsList) {

        ArrayList<Analytics> datac = (ArrayList<Analytics>)analyticsList.clone();

        Collections.reverse(datac);
        BarEntryLabels.clear();

        for (int i = 0; i < datac.size(); i++) {
            String data = datac.get(i).getTime_map();
            BarEntryLabels.add(data);
        }

    }


    Dialog retry;

    private void retryDialog(final int i) {

        retry = new Dialog(getActivity(), R.style.Theme_Dialog);
        retry.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = retry.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        retry.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        retry.setContentView(R.layout.thaks_dialog_twobutton);
        retry.setCancelable(false);
        retry.setCanceledOnTouchOutside(false);
        retry.show();

        TextView mTitle = (TextView) retry.findViewById(R.id.txt_title);
        mTitle.setText("Error!");
        mTitle.setTypeface(tf, Typeface.BOLD);
        TextView mDialogBody = (TextView) retry.findViewById(R.id.txt_dialog_body);
        mDialogBody.setText("The server is taking too long to respond OR something is wrong with your internet connection.");
        LinearLayout mOK = (LinearLayout) retry.findViewById(R.id.lin_Ok);
        LinearLayout mCancel = (LinearLayout) retry.findViewById(R.id.lin_cancel);
        TextView mRetry = (TextView) retry.findViewById(R.id.txt_dialog_ok);
        TextView mExit = (TextView) retry.findViewById(R.id.txt_dialog_cancel);
        mRetry.setText("RETRY");
        mExit.setText("EXIT");

        mOK.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                retry.dismiss();

                switch (i) {
                    case 1:
                        callAsync();
                        break;

                    default:
                        break;

                }

            }
        });

        mCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                retry.dismiss();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v == ivPrevious) {


            if (type.equalsIgnoreCase("year")) {
                int newYear = Integer.parseInt(selectedYear) - 1;
                selectedYear = String.valueOf(newYear);
                action = "prev";

                ReplaceFragment(type, selectedYear, selectedMonth, selectedDate, action);
            } else if (type.equalsIgnoreCase("month")) {
                int newMonth = Integer.parseInt(selectedMonth) - 1;
                if (newMonth == 0) {
                    selectedMonth = String.valueOf(12);
                    int newYear = Integer.parseInt(selectedYear) - 1;
                    selectedYear = String.valueOf(newYear);
                } else
                    selectedMonth = String.valueOf(newMonth);

                action = "prev";

                ReplaceFragment(type, selectedYear, selectedMonth, selectedDate, action);
            } else {
                action = "prev";

                ReplaceFragment(type, selectedYear, selectedMonth, selectedDate, action);
            }

        }
        if (v == ivNext) {
            if (type.equalsIgnoreCase("year")) {
                if (Integer.parseInt(selectedYear) != year) {
                    int newYear = Integer.parseInt(selectedYear) + 1;
                    selectedYear = String.valueOf(newYear);
                    action = "next";
                    ReplaceFragment(type, selectedYear, selectedMonth, selectedDate, action);
                }
            } else if (type.equalsIgnoreCase("month")) {
                if (Integer.parseInt(selectedMonth) != month + 1) {
                    int newMonth = Integer.parseInt(selectedMonth) + 1;
                    if (newMonth > 12) {
                        selectedMonth = String.valueOf(01);
                        int newYear = Integer.parseInt(selectedYear) + 1;
                        selectedYear = String.valueOf(newYear);
                    } else
                        selectedMonth = String.valueOf(newMonth);

                    action = "next";
                    ReplaceFragment(type, selectedYear, selectedMonth, selectedDate, action);
                }
            } else {
                Calendar cal = Calendar.getInstance();
                int week_num = Calendar.WEEK_OF_MONTH;

                String input = selectedDate;
                String format = "yyyy-MM-dd";

                SimpleDateFormat df = new SimpleDateFormat(format);
                Date date = null;
                try {
                    date = df.parse(input);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Calendar calen = Calendar.getInstance();
                calen.setTime(date);
                int week = calen.get(Calendar.WEEK_OF_YEAR);

                if (week_num != week) {
                    action = "next";
                    ReplaceFragment(type, selectedYear, selectedMonth, selectedDate, action);
                }
            }

        }
    }


}
