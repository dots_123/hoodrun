package com.us.hoodruncustomer.share;

import android.content.Context;

import com.us.hoodruncustomer.data.DataSource;
import com.us.hoodruncustomer.data.remote.RemoteDataSource;

import java.util.Map;

/**
 * Created by admin on 11/16/2017.
 */

public class SharePresenter implements ShareContract.Presenter, DataSource.ShareI {

    private DataSource loginDataSource;
    private ShareContract.View mShareView;
    private Context context;

    public SharePresenter(RemoteDataSource remoteDataSource, ShareContract.View mLoginView) {

        loginDataSource = remoteDataSource;
        this.mShareView = mLoginView;
        this.mShareView.setPresenter(this);
    }

    @Override
    public void shareInfo(String customerId, String tokenCode, String shatreStatus, String requestId, Context context) {
        mShareView.showProgreass();
        loginDataSource.shareInfoservice(customerId, tokenCode, shatreStatus, requestId, this, context);


    }

    @Override
    public void mobileVerification(Map<String, String> params, Context context) {
        mShareView.showProgreass();
        loginDataSource.mobileVerificationService(params, this, context);


    }

    @Override
    public void shareFare(String tokencode, String requestId, Context context) {
        mShareView.showProgreass();
        loginDataSource.shareFareService(tokencode, requestId, this, context);

    }

    @Override
    public void drop() {
        mShareView = null;
    }


    @Override
    public void shareInfoResponse(String baseResponse) {
        mShareView.hideProgress();
        mShareView.getShareInfoResponse(baseResponse);

    }

    @Override
    public void onFail(Throwable t) {
        mShareView.hideProgress();
        mShareView.onFail(t);

    }

    @Override
    public void mobileVerificationResponse(String response) {
        mShareView.hideProgress();
        mShareView.mobileVerifyResponse(response);

    }

    @Override
    public void shareFareResponse(String response) {
        mShareView.hideProgress();
        mShareView.shareFareResponse(response);

    }

    @Override
    public void mobileVerifyFail(Throwable t) {
        mShareView.hideProgress();
        mShareView.onFail(t);

    }

    @Override
    public void shareFareFail(Throwable t) {
        mShareView.hideProgress();
        mShareView.shareFareFail(t);


    }
}
