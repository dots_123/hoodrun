package com.us.hoodruncustomer.rider.events;

import android.location.Location;

import com.us.hoodruncustomer.app.locationUtils;
import com.us.hoodruncustomer.home.CustomerHomeActivity;
import com.us.hoodruncustomer.model.Cars;
import com.us.hoodruncustomer.model.Drivers;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by admin on 12-May-17.
 */

public class AvailCars {

    private String car_status_message;
    private String car_is_suspended;

    public HashMap<String, HashMap<String, Drivers>> getStringHashMapHashMap() {
        return stringHashMapHashMap;
    }

    public void setStringHashMapHashMap(HashMap<String, HashMap<String, Drivers>> stringHashMapHashMap) {
        this.stringHashMapHashMap = stringHashMapHashMap;
    }

    private String car_status;
    private String car_is_suspended_reason;
    private ArrayList<Cars> carList;
    private HashMap<String, HashMap<String, Drivers>> stringHashMapHashMap= new HashMap<>();
    private int car_is_suspended_reason_type;
    private String path;

    public String getCars(String data) throws Exception {

        try {

            JSONObject cobj = new JSONObject(data);
            car_status = cobj.getString("status");
            car_status_message = cobj.getString("message");

            if (car_status.equals("true")) {

                JSONObject obj_main = cobj.optJSONObject("data");

                car_is_suspended = obj_main.optString("suspended");

                car_is_suspended_reason = obj_main.optString("reason");


                if (car_is_suspended.equalsIgnoreCase("TRUE")) {
                    car_is_suspended_reason_type = obj_main.optInt("reason_type");
                }

                JSONArray jArray = obj_main.optJSONArray("cars");

                carList = new ArrayList<Cars>();

                for (int i = 0; i < jArray.length(); i++) {

                    Cars car = new Cars();

                    JSONObject obj = jArray.getJSONObject(i);

                    car.setId(obj.optString("id"));

                    car.setName(obj.optString("name"));

                    car.setNo_of_seats(obj.optString("no_of_seats"));

                    car.setMap_car_image(obj.optString("map_car_image"));

                    car.setList_car_image(obj.optString("list_car_image"));



                    car.setAvg_time(obj.optString("avg_time"));

                    car.setDefault_car(obj.optBoolean("default_car"));

                    car.setBase_fare(obj.optString("base_fare"));

                    car.setCancelation_fee(obj.optString("cancelation_fee"));

                    car.setPer_mile_fare(obj.optString("per_mile_fare"));

                    car.setPer_minute_fare(obj.optString("per_minute_fare"));

                    car.setMinimum_fare(obj.optString("minimum_fare"));

                    CustomerHomeActivity.timings.put(car.getId(),-1);

                    JSONArray drivers = obj.optJSONArray("drivers");

                    HashMap<String, Drivers> map = new HashMap<>();

                    stringHashMapHashMap.put(car.getId(), map);

                    if (drivers != null && drivers.length() > 0) {

                        ArrayList<Drivers> driverses = new ArrayList<>();

                        for (int j = 0; j < drivers.length(); j++) {

                            JSONObject driverobj = drivers.getJSONObject(j);

                            Drivers driversObj = new Drivers();

                            driversObj.setDriver_id(driverobj.optString("driver_id"));
                            driversObj.setLat(driverobj.optString("lat"));
                            driversObj.setLng(driverobj.optString("lng"));
                            driversObj.setCar_type(driverobj.optString("car_type"));
                            driversObj.setSurge_applied(driverobj.optBoolean("surge_applied"));
                            driversObj.setSurge_message(driverobj.optString("surge_message"));
                            driversObj.setFare_multiplier(driverobj.optString("fare_multiplier"));


                            map.put(driversObj.getDriver_id(), driversObj);

                            Location locationA = new Location("pickup");
                            locationA.setLatitude(CustomerHomeActivity.userLatitude);
                            locationA.setLongitude(CustomerHomeActivity.userLongitude);

                            Location locationb = new Location("drop");
                            locationb.setLatitude(driverobj.optDouble("lat"));
                            locationb.setLongitude(driverobj.optDouble("lng"));

                            int time = locationUtils.getTimebetweenLocation(locationA,locationb);

                            if(CustomerHomeActivity.timings.containsKey(car.getId())){
                                int currentTime = CustomerHomeActivity.timings.get(car.getId());
                                if(currentTime!= -1 && currentTime > time){
                                    CustomerHomeActivity.timings.put(car.getId(),time);
                                }else{
                                    CustomerHomeActivity.timings.put(car.getId(),time);
                                }
                            }else{
                                CustomerHomeActivity.timings.put(car.getId(),time);
                            }

                            stringHashMapHashMap.put(car.getId(), map);

                            if (driversObj != null) {
                                driverses.add(driversObj);
                            }

                        }
                        car.setDrivers(driverses);

                    }

                    carList.add(car);
                }

            } else if (car_status.equals("false")) {
                JSONObject error = new JSONObject(data);
                car_status_message = error.getString("message");
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return car_status;
    }

    public String getCar_status_message() {
        return car_status_message;
    }

    public void setCar_status_message(String car_status_message) {
        this.car_status_message = car_status_message;
    }

    public String getCar_is_suspended() {
        return car_is_suspended;
    }

    public void setCar_is_suspended(String car_is_suspended) {
        this.car_is_suspended = car_is_suspended;
    }

    public String getCar_status() {
        return car_status;
    }

    public void setCar_status(String car_status) {
        this.car_status = car_status;
    }

    public String getCar_is_suspended_reason() {
        return car_is_suspended_reason;
    }

    public void setCar_is_suspended_reason(String car_is_suspended_reason) {
        this.car_is_suspended_reason = car_is_suspended_reason;
    }

    public ArrayList<Cars> getCarList() {
        return carList;
    }

    public void setCarList(ArrayList<Cars> carList) {
        this.carList = carList;
    }

    public int getCar_is_suspended_reason_type() {
        return car_is_suspended_reason_type;
    }

    public void setCar_is_suspended_reason_type(int car_is_suspended_reason_type) {
        this.car_is_suspended_reason_type = car_is_suspended_reason_type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }


}
