package com.us.hoodruncustomer.model;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.us.hoodruncustomer.R;

import java.util.ArrayList;

public class AnalticsListAdapter extends BaseAdapter{

	Activity mContext;
	ArrayList<Analytics> analyticsList;
	String user;
	private LayoutInflater layoutInflater;
	String type;

	public AnalticsListAdapter(Activity ctx, ArrayList<Analytics> historyList, String user, String type){
        this.type = type;
		this.mContext=ctx;
		this.analyticsList=historyList;
		this.user=user;
		layoutInflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return analyticsList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return analyticsList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}


	public static class ViewHolder{

		TextView time, inTime ,amount ,cancelled,accepted,rejected,expired, tvpound;
		ImageView ivDetail;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		ViewHolder holder;

		if (convertView == null) {

			holder = new ViewHolder();

				convertView = layoutInflater.inflate(R.layout.analytic_row, null);
				holder.inTime = (TextView) convertView.findViewById(R.id.in_time);
				holder.time = (TextView) convertView.findViewById(R.id.time);
				holder.amount = (TextView) convertView.findViewById(R.id.amount);
			    holder.cancelled = (TextView) convertView.findViewById(R.id.cancelled);
				holder.accepted = (TextView) convertView.findViewById(R.id.accepted);
				holder.rejected = (TextView) convertView.findViewById(R.id.rejected);
				holder.expired = (TextView) convertView.findViewById(R.id.expired);
				holder.ivDetail = (ImageView) convertView.findViewById(R.id.detailButton);
				holder.tvpound = (TextView) convertView.findViewById(R.id.tvPound);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

			holder.time.setText(analyticsList.get(position).getTime_of_data());
			holder.inTime.setText(analyticsList.get(position).getWorking_hours());
			holder.amount.setText(analyticsList.get(position).getEarning());
			holder.cancelled.setText(analyticsList.get(position).getCanceled_ride());
			holder.accepted.setText(analyticsList.get(position).getAccepted_ride());
			holder.rejected.setText(analyticsList.get(position).getRejected_ride());
			holder.expired.setText(analyticsList.get(position).getIgnored_ride());

		 if(type.equalsIgnoreCase("week"))
			 holder.ivDetail.setVisibility(View.VISIBLE);
		else
			 holder.ivDetail.setVisibility(View.GONE);

		return convertView;
	}

}
