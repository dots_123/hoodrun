package com.us.hoodruncustomer.db;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public final class AppManager {

	private final static String PREF_TAG = "sfsettings";
	private static final String PREF_TAG_PHONE_NUMBER = "phonenumber";
	private static final String PREF_TAG_CUSTOMER_ID = "customer_id";
	private static final String PREF_TAG_CUSTOMER_NAME = "customer_name";
	private static final String PREF_TAG_TOKEN_CODE = "token_code";
	private static final String PREF_TAG_MOBILE_NUMBER = "mobilenumber";
	private static final String PREF_TAG_EMAIL = "email";
	private static final String PREF_TAG_PROFILE_STATUS= "profile_status";
	private static final String PREF_TAG_NAME = "name";
	private static final String PREF_TAG_PAYMENT_TYPE = "payment";
	private static final String PREF_TAG_DRIVER_ID_NUMBER = "driver_id_no";
	private static final String PREF_TAG_RIDE_ID = "ride_id";
	private static final String PREF_TAG_DEVICE_ID = "device_id";
	private static final String PREF_TAG_LOGIN="isLogined";
	private static final String PREF_TAG_CUSTOMER_LATITUDE="current_latitude_customer";
	private static final String PREF_TAG_CUSTOMER_LONGITUDE="current_longitude_customer";
	private static final String PREF_TAG_CUSTOMER_DESTINATION_LATITUDE="destination_latitude_customer";
	private static final String PREF_TAG_CUSTOMER_DESTINATION_LONGITUDE="destination_longitude_customer";
	private static final String PREF_TAG_REQUEST_ID="request_id";
	private static final String PREF_TAG_USER_TIP="user_tip";
	private static final String PREF_TAG_SAGE_PAY="sage_pay";
	private static final String PREF_TAG_LIST_ID="list_id";
	private static final String PREF_TAG_UNIQUE_ID="uniqueid";
	private static final String PREF_TAG_OTP_DESTINATION="otp_destination";
	private static final String PREF_TAG_CUSTOMER_PICKUP_LOCATION="customer_pickup_location";
	private static final String PREF_TAG_CUSTOMER_DESTINATION_LOCATION="customer_destination_location";
	private static final String PREF_TAG_CUSTOMER_SEAT_COUNT="customer_seat_count";
	private static final String PREF_TAG_DRIVER_LATITUDE="current_latitude_driver";
	private static final String PREF_TAG_DRIVER_LONGITUDE="current_longitude_driver";
	private static final String PREF_TAG_FAVOURITE_STATUS="favourite_status";
	private static final String PREF_TAG_CANCEL_CHARGE="cancel_charge";
	private static final String PREF_TAG_CAR_TYPE="car_type";
	private static final String PREF_TAG_ESTIMATED_FARE="estimated_fare";
	private static final String PREF_TAG_CUSTOMER_IMAGE="customer_image";
	private static final String PREF_TAG_TOTAL="total";
	private static final String PREF_TAG_DISCOUNT_AVAILABILITY="discount_applied";
	private static final String PREF_TAG_DISCOUNT="discount_total";
	private static final String PREF_TAG_CARD_TYPE="card_type";
	private static final String PREF_TAG_FARE="fare";
	private static final String PREF_TAG_TRIP_DURATION="trip_duration";
	private static final String PREF_TAG_TRIP_DISTANCE="trip_distance";

	private static final String PREF_TAG_IS_SHARED="isShared";
	private static final String PREF_TAG_ARRIVED_STATUS="arrived_status";
	private static final String PREF_TAG_STARTED_STATUS="started_status";
	private static final String PREF_TAG_CURRENT_ACTIVITY="current_activity";
	private static final String PREF_TAG_DRIVER_STATUS="driver_status";
	private static final String PREF_TAG_CURRENT_DRIVER_ID="driver_id";
	private static final String PREF_TAG_CURRENT_DRIVER_NAME="driver_name";
	private static final String PREF_TAG_CURRENT_DRIVER_VEHICLE_NUMBER="driver_vehicle_number";
	private static final String PREF_TAG_CURRENT_DRIVER__RATING="driver_rating_number";
	private static final String PREF_TAG_CURRENT_DRIVER_VEHICLE_MODEL="driver_vehicle_model";
	private static final String PREF_TAG_CURRENT_DRIVER_PROFILE_PIC="driver_profile_pic";

    private static final String PREF_TAG_CURRENT_DRIVER_CAR_PIC="driver_car_pic";


	/**
	 * Driver Request Data
	 */

	private static final String Driver_ride_id = "Driver_ride_id";
	private static final String Driver_request_id  = "Driver_request_id";
	private static final String Driver_ride_status = "Driver_ride_status";
	private static final String Driver_driver_id = "Driver_driver_id";
	private static final String Driver_driver_name = "Driver_driver_name";
	private static final String Driver_driver_phone = "Driver_driver_phone";
	private static final String Driver_driver_vehicle = "Driver_driver_vehicle";
	private static final String Driver_driver_profile_pic= "Driver_driver_profile_pic";
	private static final String Driver_push_Status_Flag= "Driver_push_Status_Flag";
	private static final String Driver_rating = "Driver_rating";
	private static final String Driver_customer_pick_up_point_lat = "Driver_customer_pick_up_point_lat";
	private static final String Driver_customer_pick_up_point_long = "Driver_customer_pick_up_point_long";
	private static final String Driver_customer_pick_up_point_name = "Driver_customer_pick_up_point_name";
	private static final String Driver_customer_destination_point_lat = "Driver_customer_destination_point_lat";
	private static final String Driver_customer_destination_point_long= "Driver_customer_destination_point_long";
	private static final String Driver_customer_destination_point_name= "Driver_customer_destination_point_name";
	private static final String Driver_vehicle_type = "Driver_vehicle_type";
	private static final String Driver_vehicle_model = "Driver_vehicle_model";
	private static final String Driver_vehicle_number = "Driver_vehicle_number";
	private static final String Driver_vehicle_image = "Driver_vehicle_image";
	private static final String Driver_fare_multiplier= "Driver_fare_multiplier";
	private static final String Driver_car_type= "Driver_car_type";
	private static final String Driver_map_car_image= "Driver_map_car_image";
	private static final String Driver_driver_latitude= "Driver_driver_latitude";
	private static final String Driver_driver_longitude= "Driver_driver_longitude";

	private static final String Driver_total_cost = "Driver_total_cost";
	private static final String Driver_trip_distance= "Driver_trip_distance";
	private static final String Driver_trip_duration= "Driver_trip_duration";
	private static final String Driver_tip_the_driver= "Driver_tip_the_driver";
	private static final String Driver_payment_type= "Driver_payment_type";

	private static final String Driver_total= "Driver_total";
	private static final String Driver_discount_total= "Driver_discount_total";
	private static final String Driver_discount_applied= "Driver_discount_applied";
	private static final String Driver_promo_code= "Driver_promo_code";
	private static final String Driver_payment_mode= "Driver_payment_mode";
	private static final String Driver_mobile_no= "Driver_mobile_no";
	private static final String Customer_mobile_no= "customer_mobile_no";
	private static final String Driver_no_of_seats = "Driver_no_of_seats";
	private static final String Driver_list_car_image = "Driver_list_car_image";


	public  String App_getDriver_total_cost() {
		return mSharedPreference.getString(Driver_total_cost,"driver");
	}

	public  void App_setDriver_total_cost(String Driver_total_cost_etxt) {

		Editor e = mSharedPreference.edit();
		e.putString(Driver_total_cost, Driver_total_cost_etxt);
		e.commit();
	}

	public  String App_getDriver_trip_duration() {
		return mSharedPreference.getString(Driver_trip_duration,"driver");
	}

	public  void App_setDriver_trip_duration(String Driver_trip_durationtxt) {

		Editor e = mSharedPreference.edit();
		e.putString(Driver_trip_duration, Driver_trip_durationtxt);
		e.commit();
	}

	public  String App_getDriver_trip_distance() {
		return mSharedPreference.getString(Driver_trip_distance,"driver");
	}

	public  void App_setDriver_trip_distance(String Driver_trip_distancetxt) {

		Editor e = mSharedPreference.edit();
		e.putString(Driver_trip_distance, Driver_trip_distancetxt);
		e.commit();
	}


	public  String App_getDriver_tip_the_driver() {
		return mSharedPreference.getString(Driver_tip_the_driver,"driver");
	}

	public  void App_setDriver_tip_the_driver(String Driver_tip_the_drivertxt) {

		Editor e = mSharedPreference.edit();
		e.putString(Driver_tip_the_driver, Driver_tip_the_drivertxt);
		e.commit();
	}


	public  String App_getDriver_payment_type() {
		return mSharedPreference.getString(Driver_payment_type,"driver");
	}

	public  void App_setDriver_payment_type(String Driver_payment_typetxt) {

		Editor e = mSharedPreference.edit();
		e.putString(Driver_payment_type, Driver_payment_typetxt);
		e.commit();
	}

	public  String App_getDriver_total() {
		return mSharedPreference.getString(Driver_total,"driver");
	}

	public  void App_setDriver_total(String Driver_totalxtx) {

		Editor e = mSharedPreference.edit();
		e.putString(Driver_total, Driver_totalxtx);
		e.commit();
	}


	public  String App_getDriver_discount_total() {
		return mSharedPreference.getString(Driver_discount_total,"driver");
	}

	public  void App_setDriver_discount_total(String Driver_discount_totaltxt) {

		Editor e = mSharedPreference.edit();
		e.putString(Driver_discount_total, Driver_discount_totaltxt);
		e.commit();
	}


	public  String App_getDriver_discount_applied() {
		return mSharedPreference.getString(Driver_discount_applied,"driver");
	}

	public  void App_setDriver_discount_applied(String Driver_discount_appliedtxt) {

		Editor e = mSharedPreference.edit();
		e.putString(Driver_discount_applied, Driver_discount_appliedtxt);
		e.commit();
	}

	public  String App_getDriver_promo_code() {
		return mSharedPreference.getString(Driver_promo_code,"driver");
	}

	public  void App_setDriver_promo_code(String Driver_promo_codetxt) {

		Editor e = mSharedPreference.edit();
		e.putString(Driver_promo_code, Driver_promo_codetxt);
		e.commit();
	}


	public  String App_getDriver_payment_mode() {
		return mSharedPreference.getString(Driver_payment_mode,"driver");
	}

	public  void App_setDriver_payment_mode(String Driver_payment_modetxt) {

		Editor e = mSharedPreference.edit();
		e.putString(Driver_payment_mode, Driver_payment_modetxt);
		e.commit();
	}

	public  String App_getCustomer_mobile_no() {
		return mSharedPreference.getString(Customer_mobile_no,"");
	}

	public  void App_setCustomer_mobile_no(String Customer_mobile_notxt) {

		Editor e = mSharedPreference.edit();
		e.putString(Customer_mobile_no, Customer_mobile_notxt);
		e.commit();
	}

	public  String App_getDriver_mobile_no() {
		return mSharedPreference.getString(Driver_mobile_no,"");
	}

	public  void App_setDriver_mobile_no(String Driver_mobile_notxt) {

		Editor e = mSharedPreference.edit();
		e.putString(Driver_mobile_no, Driver_mobile_notxt);
		e.commit();
	}


	public  String App_getDriver_list_car_image() {
		return mSharedPreference.getString(Driver_list_car_image,"driver");
	}

	public  void App_setDriver_list_car_image(String Driver_mobile_notxt) {

		Editor e = mSharedPreference.edit();
		e.putString(Driver_list_car_image, Driver_mobile_notxt);
		e.commit();
	}



	public  String App_getDriver_ride_id() {
		return mSharedPreference.getString(Driver_ride_id,"driver");
	}

	public  void App_setDriver_ride_id(String driver_ride_id) {

		Editor e = mSharedPreference.edit();
		e.putString(Driver_ride_id, driver_ride_id);
		e.commit();
	}

	public  String App_getDriver_request_id() {
		return mSharedPreference.getString(Driver_request_id,"driver");
	}

	public  void App_setDriver_request_id(String driver_request_id) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_request_id, driver_request_id);
		e.commit();
	}

	public 	String App_getDriver_ride_status() {
		return mSharedPreference.getString(Driver_ride_status,"driver");
	}

	public  void App_setDriver_ride_status(String driver_ride_status) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_ride_status, driver_ride_status);
		e.commit();
	}

	public  String App_getDriver_driver_id() {
		return mSharedPreference.getString(Driver_driver_id,"driver");
	}

	public  void App_setDriver_driver_id(String driver_driver_id) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_driver_id, driver_driver_id);
		e.commit();
	}

	public  String App_getDriver_driver_name() {
		return mSharedPreference.getString(Driver_driver_name,"driver");
	}

	public  void App_setDriver_driver_name(String driver_driver_name) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_driver_name, driver_driver_name);
		e.commit();
	}

	public  String App_getDriver_driver_phone() {
		return mSharedPreference.getString(Driver_driver_phone,"driver");
	}

	public  void App_setDriver_driver_phone(String driver_driver_phone) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_driver_phone, driver_driver_phone);
		e.commit();
	}

	public  String App_getDriver_driver_vehicle() {
		return mSharedPreference.getString(Driver_driver_vehicle,"driver");
	}

	public  void App_setDriver_driver_vehicle(String driver_driver_vehicle) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_driver_vehicle, driver_driver_vehicle);
		e.commit();
	}

	public  String App_getDriver_driver_profile_pic() {
		return mSharedPreference.getString(Driver_driver_profile_pic,"driver");
	}

	public  void App_setDriver_driver_profile_pic(String driver_driver_profile_pic) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_driver_profile_pic, driver_driver_profile_pic);
		e.commit();
	}

	public  String App_getDriver_push_Status_Flag() {
		return mSharedPreference.getString(Driver_push_Status_Flag,"driver");
	}

	public  void App_setDriver_push_Status_Flag(String driver_push_Status_Flag) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_push_Status_Flag, driver_push_Status_Flag);
		e.commit();
	}

	public  String App_getDriver_rating() {
		return mSharedPreference.getString(Driver_rating,"driver");
	}

	public  void App_setDriver_rating(String driver_rating) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_rating, driver_rating);
		e.commit();
	}

	public  String App_getDriver_customer_pick_up_point_lat() {
		return mSharedPreference.getString(Driver_customer_pick_up_point_lat,"driver");
	}

	public  void App_setDriver_customer_pick_up_point_lat(String driver_customer_pick_up_point_lat) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_customer_pick_up_point_lat, driver_customer_pick_up_point_lat);
		e.commit();
	}

	public  String App_getDriver_customer_pick_up_point_long() {
		return mSharedPreference.getString(Driver_customer_pick_up_point_long,"driver");
	}

	public  void App_setDriver_customer_pick_up_point_long(String driver_customer_pick_up_point_long) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_customer_pick_up_point_long, driver_customer_pick_up_point_long);
		e.commit();
	}

	public  String App_getDriver_customer_pick_up_point_name() {
		return mSharedPreference.getString(Driver_customer_pick_up_point_name,"driver");
	}

	public  void App_setDriver_customer_pick_up_point_name(String driver_customer_pick_up_point_name) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_customer_pick_up_point_name, driver_customer_pick_up_point_name);
		e.commit();
	}

	public  String App_getDriver_customer_destination_point_lat() {
		return mSharedPreference.getString(Driver_customer_destination_point_lat,"driver");
	}

	public  void App_setDriver_customer_destination_point_lat(String driver_customer_destination_point_lat) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_customer_destination_point_lat, driver_customer_destination_point_lat);
		e.commit();
	}

	public  String App_getDriver_customer_destination_point_long() {
		return mSharedPreference.getString(Driver_customer_destination_point_long,"driver");
	}

	public  void App_setDriver_customer_destination_point_long(String driver_customer_destination_point_long) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_customer_destination_point_long, driver_customer_destination_point_long);
		e.commit();
	}

	public  String App_getDriver_customer_destination_point_name() {
		return mSharedPreference.getString(Driver_customer_destination_point_name,"driver");
	}

	public  void App_setDriver_customer_destination_point_name(String driver_customer_destination_point_name) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_customer_destination_point_name, driver_customer_destination_point_name);
		e.commit();
	}

	public  String App_getDriver_vehicle_image() {
		return mSharedPreference.getString(Driver_vehicle_image,"driver");
	}

	public  void App_setDriver_vehicle_image(String Driver_vehicle_image_txtx) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_vehicle_image, Driver_vehicle_image_txtx);
		e.commit();
	}

	public  String App_getDriver_vehicle_type() {
		return mSharedPreference.getString(Driver_vehicle_type,"driver");
	}

	public  void App_setDriver_vehicle_type(String driver_vehicle_type) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_vehicle_type, driver_vehicle_type);
		e.commit();
	}

	public  String App_getDriver_fare_multiplier() {
		return mSharedPreference.getString(Driver_fare_multiplier,"driver");
	}

	public  void App_setDriver_fare_multiplier(String driver_fare_multiplier) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_fare_multiplier, driver_fare_multiplier);
		e.commit();
	}

	public  String App_getDriver_car_type() {
		return mSharedPreference.getString(Driver_car_type,"driver");
	}

	public  void App_setDriver_car_type(String driver_car_type) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_car_type, driver_car_type);
		e.commit();
	}

	public  String App_getDriver_map_car_image() {
		return mSharedPreference.getString(Driver_map_car_image,"driver");
	}

	public  void App_setDriver_map_car_image(String driver_map_car_image) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_map_car_image, driver_map_car_image);
		e.commit();
	}

	public  String App_getDriver_driver_latitude() {
		return mSharedPreference.getString(Driver_driver_latitude,"driver");
	}

	public  void App_setDriver_driver_latitude(String driver_driver_latitude) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_driver_latitude, driver_driver_latitude);
		e.commit();
	}

	public  String App_getDriver_vehicle_model() {
		return mSharedPreference.getString(Driver_vehicle_model,"driver");
	}

	public  void App_setDriver_vehicle_model(String Driver_vehicle_model_txt) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_vehicle_model, Driver_vehicle_model_txt);
		e.commit();
	}


	public  String App_getDriver_driver_longitude() {
		return mSharedPreference.getString(Driver_driver_longitude,"driver");
	}

	public  void App_setDriver_driver_longitude(String driver_driver_longitude) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_driver_longitude, driver_driver_longitude);
		e.commit();
	}

	public  String App_getDriver_vehicle_number() {
		return mSharedPreference.getString(Driver_vehicle_number,"driver");
	}

	public  void App_setDriver_vehicle_number(String Driver_vehicle_numbertxt) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_vehicle_number, Driver_vehicle_numbertxt);
		e.commit();
	}

	public  String App_getDriver_no_of_seats() {
		return mSharedPreference.getString(Driver_no_of_seats,"driver");
	}

	public  void App_setDriver_no_of_seats(String Driver_no_of_seatstxt) {
		Editor e = mSharedPreference.edit();
		e.putString(Driver_no_of_seats, Driver_no_of_seatstxt);
		e.commit();
	}




	private static AppManager mAppManager;
	private Context mContext;
	private boolean login_status=false;

	private SharedPreferences mSharedPreference;









	/**
	 * Singleton model to avoid multiple instance of this class. use getInstance
	 * instead. DO NOT ALTER THE access modifier. Do the necessary
	 * initialization.
	 */
	private AppManager(Context context) {
		this.mContext = context;

		mSharedPreference = mContext.getSharedPreferences(PREF_TAG, Context.MODE_PRIVATE);

	}

	/**
	 * Singleton method to return the object of this class. DO NOT ALTER THE
	 * CODE.
	 *
	 * @return: returns the same object always.
	 */
	public static synchronized AppManager getInstance(Context context) {
		if (mAppManager == null)
			mAppManager = new AppManager(context);
		return mAppManager;
	}


	/**
	 * Save Current Driver Info
	 * @return
	 * @throws CloneNotSupportedException
     */

	public void saveCurrentDriverName(String driver_name) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_CURRENT_DRIVER_NAME, driver_name);
		e.commit();

	}

	public String getCurrentDriverName() {

		return mSharedPreference.getString(PREF_TAG_CURRENT_DRIVER_NAME,"driver");
	}


	public void saveCurrentDriverId(String driver_id) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_CURRENT_DRIVER_ID, driver_id);
		e.commit();

	}

	public String getCurrentDriverId() {

		return mSharedPreference.getString(PREF_TAG_CURRENT_DRIVER_ID,"");
	}



	public void saveCurrentDriverProfilePic(String profile_pic) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_CURRENT_DRIVER_PROFILE_PIC, profile_pic);
		e.commit();

	}

	public String getCurrentDriverProfilePic() {

		return mSharedPreference.getString(PREF_TAG_CURRENT_DRIVER_PROFILE_PIC,"");
	}


	public void saveCurrentDriverRATING(String number) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_CURRENT_DRIVER__RATING, number);
		e.commit();

	}

	public String getCurrentDriverRATING() {

		return mSharedPreference.getString(PREF_TAG_CURRENT_DRIVER__RATING,"");
	}




	public void saveCurrentDriverVEHICLENUMBER(String number) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_CURRENT_DRIVER_VEHICLE_NUMBER, number);
		e.commit();

	}

	public String getCurrentDriverVEHICLENUMBER() {

		return mSharedPreference.getString(PREF_TAG_CURRENT_DRIVER_VEHICLE_NUMBER,"");
	}

	public void saveCurrentDriverVEHICLEModel(String model) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_CURRENT_DRIVER_VEHICLE_MODEL, model);
		e.commit();

	}

	public String getCurrentDriverVEHICLEModel() {

		return mSharedPreference.getString(PREF_TAG_CURRENT_DRIVER_VEHICLE_MODEL,"");
	}




	/*
	 * This is a singleton class. so we must avoid cloning the objects. DO NOT
	 * ALTER THE CODE.
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}


	public void saveArrivedStatus(int arrived_status) {

		Editor e = mSharedPreference.edit();
		e.putInt(PREF_TAG_ARRIVED_STATUS, arrived_status);
		e.commit();

	}

	public int getArrivedStatus() {

		return mSharedPreference.getInt(PREF_TAG_ARRIVED_STATUS, 1);
	}


	public void saveDriverStatus(int driver_status) {

		Editor e = mSharedPreference.edit();
		e.putInt(PREF_TAG_DRIVER_STATUS, driver_status);
		e.commit();
	}

	public int getDriverStatus() {

		return mSharedPreference.getInt(PREF_TAG_DRIVER_STATUS, 1);
	}


	public void saveStartedStatus(int started_status) {

		Editor e = mSharedPreference.edit();
		e.putInt(PREF_TAG_STARTED_STATUS, started_status);
		e.commit();
	}

	public int getStartedStatus() {

		return mSharedPreference.getInt(PREF_TAG_STARTED_STATUS, 1);
	}


	public void savePrevActivity(String PREF_TAG_ACTIVITY,String activityname) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_ACTIVITY, activityname);
		e.commit();
	}

	public String getPrevActivity(String PREF_TAG_ACTIVITY) {

		return mSharedPreference.getString(PREF_TAG_ACTIVITY, "activity");
	}


	public void saveCurrentActivity(String activity_name) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_CURRENT_ACTIVITY, activity_name);
		e.commit();
	}

	public String getCurrentActivity() {

		return mSharedPreference.getString(PREF_TAG_CURRENT_ACTIVITY, "");
	}


    public void saveCar(String carpath) {

        Editor e = mSharedPreference.edit();
        e.putString(PREF_TAG_CURRENT_DRIVER_CAR_PIC,carpath);
        e.commit();
    }

    public String getCar() {

        return mSharedPreference.getString(PREF_TAG_CURRENT_DRIVER_CAR_PIC, "");
    }

	public void saveUser(String etUsername) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_PHONE_NUMBER, etUsername);
		e.commit();
	}

	public String getuser() {

		return mSharedPreference.getString(PREF_TAG_PHONE_NUMBER, "");
	}

	public void setDeviceId(String deviceid) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_DEVICE_ID, deviceid);
		e.commit();

	}

	public String getDeviceId(){
		return mSharedPreference.getString(PREF_TAG_DEVICE_ID,"");
	}

	public void setLogin_status(boolean login) {

		Editor e = mSharedPreference.edit();
		login_status=login;

		e.putBoolean(PREF_TAG_LOGIN, login_status);
		e.commit();
		// return mSharedPreference.getBoolean(PREF_TAG_LOGIN, login);

	}
	public boolean getLogin_status(){
		login_status = mSharedPreference.getBoolean(PREF_TAG_LOGIN, false);

		return mSharedPreference.getBoolean(PREF_TAG_LOGIN, false);
	}


	public void setShared(boolean isShared) {

		Editor e = mSharedPreference.edit();


		e.putBoolean(PREF_TAG_IS_SHARED, isShared);
		e.commit();
		// return mSharedPreference.getBoolean(PREF_TAG_LOGIN, login);

	}
	public boolean getShared(){



		return mSharedPreference.getBoolean(PREF_TAG_IS_SHARED, false);
	}




	public void saveCustomerPickupLocation(String pick_location) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_CUSTOMER_PICKUP_LOCATION, pick_location);
		e.commit();
	}

	public String getCustomerPickupLocation() {

		return mSharedPreference.getString(PREF_TAG_CUSTOMER_PICKUP_LOCATION, "");
	}

	public void saveCustomerSeatCount(String seat_count) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_CUSTOMER_SEAT_COUNT, seat_count);
		e.commit();
	}

	public String getCustomerSeatCount() {

		return mSharedPreference.getString(PREF_TAG_CUSTOMER_SEAT_COUNT, "");
	}

	public void saveFavouriteStatus(int fav_status) {

		Editor e = mSharedPreference.edit();
		e.putInt(PREF_TAG_FAVOURITE_STATUS, fav_status);
		e.commit();
	}

	public int getFavouriteStatus() {

		return mSharedPreference.getInt(PREF_TAG_FAVOURITE_STATUS, 0);
	}

	public void saveEstimatedFare(String estimated_fare) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_ESTIMATED_FARE,estimated_fare);
		e.commit();
	}

	public String getEstimatedFare() {

		return mSharedPreference.getString(PREF_TAG_ESTIMATED_FARE, "");
	}


	public void saveFare(String fare) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_FARE,fare);
		e.commit();
	}

	public String getFare() {

		return mSharedPreference.getString(PREF_TAG_FARE, "");
	}


	public void saveCustomerImage(String cust_image) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_CUSTOMER_IMAGE,cust_image);
		e.commit();
	}

	public String getCustomerImage() {

		return mSharedPreference.getString(PREF_TAG_CUSTOMER_IMAGE, "");
	}


	public void saveCustomerDestinationLocation(String destn_location) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_CUSTOMER_DESTINATION_LOCATION,destn_location);
		e.commit();
	}

	public String getCustomerDestinationLocation() {

		return mSharedPreference.getString(PREF_TAG_CUSTOMER_DESTINATION_LOCATION, "");
	}

	public void saveMobileNumber(String mob_no) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_MOBILE_NUMBER, mob_no);
		e.commit();
	}

	public String getMobileNumber() {

		return mSharedPreference.getString(PREF_TAG_MOBILE_NUMBER, "");
	}

	public void saveEmail(String email) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_EMAIL, email);
		e.commit();
	}

	public String getEmail() {

		return mSharedPreference.getString(PREF_TAG_EMAIL, "");
	}


	public void saveDiscountStatus(String status) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_DISCOUNT_AVAILABILITY, status);
		e.commit();
	}

	public String getDiscountStatus() {

		return mSharedPreference.getString(PREF_TAG_DISCOUNT_AVAILABILITY, "");
	}

	public void saveCardType(int type) {

		Editor e = mSharedPreference.edit();
		e.putInt(PREF_TAG_CARD_TYPE, type);
		e.commit();
	}

	public int getCardType() {

		return mSharedPreference.getInt(PREF_TAG_CARD_TYPE,0);
	}


	public void saveDiscount(String discount) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_DISCOUNT, discount);
		e.commit();
	}

	public String getDiscount() {

		return mSharedPreference.getString(PREF_TAG_DISCOUNT, "");
	}





	public void saveTotal(String total) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_TOTAL, total);
		e.commit();
	}

	public String getTotal() {

		return mSharedPreference.getString(PREF_TAG_TOTAL, "");
	}
	public void saveTripDuration(String duration) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_TRIP_DURATION, duration);
		e.commit();
	}

	public String getTripDuration() {

		return mSharedPreference.getString(PREF_TAG_TRIP_DURATION, "");
	}
	public void saveTripDistance(String distance) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_TRIP_DISTANCE, distance);
		e.commit();
	}

	public String getTripDistance() {

		return mSharedPreference.getString(PREF_TAG_TRIP_DISTANCE, "");
	}




	public void saveCarType(String car_type) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_CAR_TYPE, car_type);
		e.commit();
	}

	public String getCarType() {

		return mSharedPreference.getString(PREF_TAG_CAR_TYPE, "");
	}


	public void saveCancelCharge(String cc) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_CANCEL_CHARGE, cc);
		e.commit();
	}

	public String getCancelCharge() {

		return mSharedPreference.getString(PREF_TAG_CANCEL_CHARGE, "");
	}

	public void saveDriverName(String name) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_NAME, name);
		e.commit();
	}

	public String getDriverName() {

		return mSharedPreference.getString(PREF_TAG_NAME, "");
	}


	public void saveOtpDestiantion(String otp_destn) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_OTP_DESTINATION, otp_destn);
		e.commit();
	}


	public String getOtpDestiantion() {

		return mSharedPreference.getString(PREF_TAG_OTP_DESTINATION, "");
	}


	public void savePaymentType(String name) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_PAYMENT_TYPE, name);
		e.commit();
	}


	public String getPaymentType() {

		return mSharedPreference.getString(PREF_TAG_PAYMENT_TYPE, "");
	}



	public void saveCustomerID(String cust_id) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_CUSTOMER_ID, cust_id);
		e.commit();
	}

	public String getCustomerID() {

		return mSharedPreference.getString(PREF_TAG_CUSTOMER_ID, "");
	}


	public void saveCustomerName(String cust_name) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_CUSTOMER_NAME, cust_name);
		e.commit();
	}

	public String getCustomerName() {

		return mSharedPreference.getString(PREF_TAG_CUSTOMER_NAME, "");
	}

	public void saveTokenCode(String TokenCode) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_TOKEN_CODE, TokenCode);
		e.commit();
	}

	public String getTokenCode() {

		return mSharedPreference.getString(PREF_TAG_TOKEN_CODE, "");
	}


	public void saveProfileStatus(String prof_status) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_PROFILE_STATUS, prof_status);
		e.commit();
	}

	public String getProfileStatus() {

		return mSharedPreference.getString(PREF_TAG_PROFILE_STATUS, "");
	}

	public void saveDriverID_Number(String driverID_no) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_DRIVER_ID_NUMBER, driverID_no);
		e.commit();
	}

	public String getDriverID_Number() {

		return mSharedPreference.getString(PREF_TAG_DRIVER_ID_NUMBER, "");
	}
	public void saveRide_ID(String ride_id) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_RIDE_ID, ride_id);
		e.commit();
	}

	public String getRide_ID() {

		return mSharedPreference.getString(PREF_TAG_RIDE_ID, "");
	}

	public void saveCustomerLatitude(double lat) {

		Editor e = mSharedPreference.edit();
		e.putLong(PREF_TAG_CUSTOMER_LATITUDE, Double.doubleToLongBits(lat));
		e.commit();
	}

	public double getCustomerLatitude() {

		return Double.longBitsToDouble(mSharedPreference.getLong(PREF_TAG_CUSTOMER_LATITUDE, 0));
	}
	public void saveCustomerLongitude(double lon) {

		Editor e = mSharedPreference.edit();
		e.putLong(PREF_TAG_CUSTOMER_LONGITUDE, Double.doubleToLongBits(lon));
		e.commit();
	}

	public double getCustomerLongitude() {

		return Double.longBitsToDouble(mSharedPreference.getLong(PREF_TAG_CUSTOMER_LONGITUDE, 0));
	}
	//111111111111111111111111111111

	public void saveDriverLatitude(double lat) {

		Editor e = mSharedPreference.edit();
		e.putLong(PREF_TAG_DRIVER_LATITUDE, Double.doubleToLongBits(lat));
		e.commit();
	}

	public double getDriverLatitude() {

		return Double.longBitsToDouble(mSharedPreference.getLong(PREF_TAG_DRIVER_LATITUDE, 0));
	}
	public void saveDriverLongitude(double lon) {

		Editor e = mSharedPreference.edit();
		e.putLong(PREF_TAG_DRIVER_LONGITUDE, Double.doubleToLongBits(lon));
		e.commit();
	}

	public double getDriverLongitude() {

		return Double.longBitsToDouble(mSharedPreference.getLong(PREF_TAG_DRIVER_LONGITUDE, 0));
	}

	public void saveCustomerDestinationLatitude(double dlat) {

		Editor e = mSharedPreference.edit();
		e.putLong(PREF_TAG_CUSTOMER_DESTINATION_LATITUDE,Double.doubleToLongBits(dlat));
		e.commit();
	}

	public double getCustomerDestianationLatitude() {

		return Double.longBitsToDouble(mSharedPreference.getLong(PREF_TAG_CUSTOMER_DESTINATION_LATITUDE, 0));
	}

	public void saveCustomerDestinationLongitude(double dlon) {

		Editor e = mSharedPreference.edit();
		e.putLong(PREF_TAG_CUSTOMER_DESTINATION_LONGITUDE,Double.doubleToLongBits(dlon));
		e.commit();
	}

	public double getCustomerDestianationLongitude() {

		return Double.longBitsToDouble(mSharedPreference.getLong(PREF_TAG_CUSTOMER_DESTINATION_LONGITUDE, 0));
	}

	public void saveRequest_ID(String req_id) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_REQUEST_ID, req_id);
		e.commit();
	}

	public String getRequest_ID() {

		return mSharedPreference.getString(PREF_TAG_REQUEST_ID, "");
	}


	public void saveUserTip(String tip) {
		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_USER_TIP, tip);
		e.commit();

	}
	public String getUserTip() {

		return mSharedPreference.getString(PREF_TAG_USER_TIP, "");
	}


	public void saveSagePayInfo(String sagepay) {
		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_SAGE_PAY, sagepay);
		e.commit();

	}
	public String getSagePayInfo() {

		return mSharedPreference.getString(PREF_TAG_SAGE_PAY, "");
	}



	public void saveListId(int id) {

		Editor e = mSharedPreference.edit();
		e.putInt(PREF_TAG_LIST_ID,id);
		e.commit();
	}

	public void saveUniqueId(String id) {

		Editor e = mSharedPreference.edit();
		e.putString(PREF_TAG_UNIQUE_ID,id);
		e.commit();
	}

	public int getListId() {

		return mSharedPreference.getInt(PREF_TAG_LIST_ID, 0);
	}

	public String getUniqueId() {

		return mSharedPreference.getString(PREF_TAG_UNIQUE_ID,"0");
	}


}
