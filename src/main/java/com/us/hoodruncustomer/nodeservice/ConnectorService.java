package com.us.hoodruncustomer.nodeservice;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.db.AppManager;

import com.us.hoodruncustomer.parser.DriverParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static com.us.hoodruncustomer.commonwork.SuperBaseActivity.RECEIVER_FUTURE_REQUEST;

public class ConnectorService extends Service {


    private Socket mSocket;
    MyApplication app;
    private AppManager mAppManager;
    PendingIntent contentIntent;
    public static final int NOTIFICATION_ID = 1100;
    public NotificationManager mNotificationManager;

    public ConnectorService() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        System.out.println("ConnectorService" + "Service Created");
        app = (MyApplication) getApplication();
        mAppManager = AppManager.getInstance(this);
        mSocket = app.getSocket();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (mAppManager.getTokenCode() != null && !mAppManager.getTokenCode().isEmpty()) {
            if (mSocket != null) {
                System.out.println("ConnectorServiceOnstart" + "onStartCommand");
                mSocket.on(Socket.EVENT_CONNECT, onConnect);
                mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
                mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
                mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
                mSocket.on("is_online", AddUserResponse);
                mSocket.on("respond", Response);
                mSocket.on("share_the_fare", RideRequests);
                mSocket.on("share_the_fare_future", RideRequestsFuture);
                mSocket.on("cofirm_switch_reason", ConfirmRequests);
                mSocket.on("toast", ToastMessage);
                mSocket.on("track_ride", TrackRide);
                mSocket.on("driver_location", UpdateLocation);
                mSocket.on("driver_out_of_range", OutOfRangeLocation);
                mSocket.on("force_logout", forceLogout);
                mSocket.on("get_cars", getCars);
                mSocket.connect();
            }

        }
        return START_NOT_STICKY;
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.v("Connection msocket",""+mSocket.connect().id());
            System.out.println("ConnectorService" + "onConnect");
            if (mAppManager.getLogin_status()) {
                attemptSend();
            }
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            System.out.println("ConnectorService" + "onDisconnect");
            Intent i = new Intent(NodeUtil.NODE_DISCONNECTION);
            i.putExtra("Object", "DisConnected");
            getApplicationContext().sendBroadcast(i);
            Log.d("ConnectorService", "Node_DisOnConnect=======onDisconnect");
        }
    };


    private Emitter.Listener Response = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            JSONObject object = (JSONObject) args[0];
            Log.d("ConnectorService", "Response: " + object.toString());

            if (mAppManager.getuser().equalsIgnoreCase("driver")) {
                Intent i = new Intent(NodeUtil.NODE_DRIVER_ACTIONS);
                i.putExtra("Object", object.toString());
                getApplicationContext().sendBroadcast(i);
            } else {
                Intent i = new Intent(NodeUtil.NODE_CUSTOMER_ACTIONS);
                i.putExtra("Object", object.toString());
                getApplicationContext().sendBroadcast(i);
            }

        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d("ConnectorService", "Node_DisOnConnect=======onConnectError");
        }
    };


    private Emitter.Listener AddUserResponse = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            System.out.println("ConnectorService" + "AddUserResponse");
            JSONObject data = null;
            try {
                data = (JSONObject) args[0];
                System.out.println("Noderesponse" + data.toString());
                String userId = data.getString("user_id");
                boolean status = data.getBoolean("online");
                String userType = data.getString("user_type");

                if (status) {
                    if (userId.equalsIgnoreCase(mAppManager.getDriverID_Number()) && userType.equalsIgnoreCase(mAppManager.getuser())) {
                        Intent i = new Intent(NodeUtil.NODE_CONNECTION);
                        i.putExtra("Object", "Connected");
                        getApplicationContext().sendBroadcast(i);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private Emitter.Listener ConfirmRequests = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            JSONObject object = (JSONObject) args[0];
            Log.d("ConnectorService", "ConfirmRequests: " + object.toString());
            Intent i = new Intent(NodeUtil.NODE_CUSTOMER_CONFIRM_PAYMENT);
            i.putExtra("Object", object.toString());
            getApplicationContext().sendBroadcast(i);
        }
    };


    private Emitter.Listener RideRequestsFuture = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            JSONObject object = (JSONObject) args[0];
            Log.d("ConnectorService", "RideRequests: " + object.toString());

            String request = object.toString();

            try {

                System.out.println("Sresponse" + request.toString());

                boolean parsresp = DriverParser.Future_driverAccept(request);

                if (parsresp) {

                    if (DriverParser.getStatusMessage().equals("Driver is not ready for the ride")) {

                    } else {
                        Intent i = new Intent(NodeUtil.NODE_DRIVER_FUTURE_REQUESTS);
                        i.putExtra("Object", object.toString());
                        getApplicationContext().sendBroadcast(i);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };


    private Emitter.Listener RideRequests = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject object = (JSONObject) args[0];
            Log.d("ConnectorService", "RideRequests: " + object.toString());

            String request = object.toString();

            try {

                System.out.println("Sresponse" + request.toString());

                boolean parsresp = DriverParser.driverAccept(request);

                if (parsresp) {

                    if (DriverParser.getStatusMessage().equals("Driver is not ready for the ride")) {

                    } else {
                        if (mAppManager.getRequest_ID() != null && !mAppManager.getRequest_ID().equalsIgnoreCase("")) {
                            sendFutureBroadcastData(object.toString());
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    };


    private void sendFutureBroadcastData(String response) {

        Intent i = new Intent(RECEIVER_FUTURE_REQUEST);
        i.putExtra("request", response);
        getApplicationContext().sendBroadcast(i);
        MyApplication.startPlayer(R.raw.wolf);

    }


    private Emitter.Listener ToastMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

        }
    };

    private Emitter.Listener TrackRide = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject object = (JSONObject) args[0];
            Log.d("ConnectorService", "TrackRide: " + object.toString());
            Intent i = new Intent(NodeUtil.NODE_CUSTOMER_TRACKRIDE);
            i.putExtra("Object", object.toString());
            getApplicationContext().sendBroadcast(i);
        }
    };

    private Emitter.Listener UpdateLocation = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONArray object = (JSONArray) args[0];
            Log.d("ConnectorService", "UpdateLocation: " + object.toString());
            Intent i = new Intent(NodeUtil.NODE_CUSTOMER_LOCATION_UPDATE);
            i.putExtra("Object", object.toString());
            getApplicationContext().sendBroadcast(i);
        }
    };

    private Emitter.Listener OutOfRangeLocation = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            JSONArray object = (JSONArray) args[0];
            Log.d("ConnectorService", "OutOfUpdateLocation: " + object.toString());
            Intent i = new Intent(NodeUtil.NODE_CUSTOMER_LOCATION_OUT_OF_RANGE);
            i.putExtra("Object", object.toString());
            getApplicationContext().sendBroadcast(i);
        }
    };


    private Emitter.Listener forceLogout = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d("ConnectorService", "forceLogout: ");
            CloseNode();
            clearANDTerminate();
        }
    };

    private Emitter.Listener getCars = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            Log.d("ConnectorService", "getCars: " + args[0].toString());
            Intent i = new Intent(NodeUtil.NODE_CUSTOMER_GETCARS);
            i.putExtra("Object", args[0].toString());
            getApplicationContext().sendBroadcast(i);
        }
    };

    public void clearANDTerminate() {
        mAppManager.saveUser("");
        mAppManager.setLogin_status(false);
        Intent i = new Intent(NodeUtil.NODE_CUSTOMER_LOGOUT);
        getApplicationContext().sendBroadcast(i);
    }


    /**
     * attemptSend() to register User
     */

    private void attemptSend() {

        String user_id = null;

        if (mAppManager.getuser().equals("driver")) {
            user_id = mAppManager.getDriverID_Number();
        } else {
            user_id = mAppManager.getCustomerID();
        }

        if (user_id != null) {

            String user_type = mAppManager.getuser();
            if (user_type.equalsIgnoreCase("customer") || user_type.equalsIgnoreCase("driver")) {

                String device_id = mAppManager.getTokenCode();
                String device_type = "android";
                JSONObject obj = new JSONObject();
                try {
                    obj.put("user_id", user_id);
                    obj.put("auth_token", mAppManager.getTokenCode());
                    obj.put("user_type", user_type);
                    obj.put("device_id", device_id);
                    obj.put("device_type", device_type);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (mSocket.connected()) {
                    mSocket.emit("add_user", obj);
                    System.out.println("ConnectorService" + "attemptSend--" + user_type);
                } else {
                    Intent i = new Intent(NodeUtil.NODE_DISCONNECTION);
                    i.putExtra("Object", "DisConnected");
                    getApplicationContext().sendBroadcast(i);
                    Log.d("ConnectorService", "Node_DisOnConnect=======attemptSend");
                }
            } else {
                Intent i = new Intent(NodeUtil.NODE_DISCONNECTION);
                i.putExtra("Object", "DisConnected");
                getApplicationContext().sendBroadcast(i);
                Log.d("ConnectorService", "Node_DisOnConnect=======attemptSend");
            }
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Log.d("onTaskRemoved", "onTaskRemoved=========== Removed");
        MyApplication.apprunning = 2;
        try {
            CloseNode();
            mNotificationManager.cancelAll();
        } catch (Exception e) {

        } finally {
            stopSelf();
        }
    }

    private void CloseNode() {

        if (mSocket != null) {
            mSocket.off(Socket.EVENT_CONNECT, onConnect);
            mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
            mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
            mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
            mSocket.off("is_online", AddUserResponse);
            mSocket.off("respond", Response);
            mSocket.off("share_the_fare", RideRequests);
            mSocket.off("share_the_fare_future", RideRequestsFuture);
            mSocket.off("toast", ToastMessage);
            mSocket.off("track_ride", TrackRide);
            mSocket.off("driver_location", UpdateLocation);
            mSocket.off("driver_out_of", OutOfRangeLocation);
            mSocket.off("cofirm_switch_reason", ConfirmRequests);
            mSocket.off("force_logout", forceLogout);
            mSocket.off("get_cars", getCars);
            mSocket.disconnect();
        }
    }

    @Override
    public void onDestroy() {
        CloseNode();
        super.onDestroy();
    }


}
