package com.us.hoodruncustomer.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.custom.CircularImageView;
import com.us.hoodruncustomer.db.AppManager;

public class DriverDetailsFragment extends DialogFragment {
	CircularImageView driver_image;
	TextView driver_name,driver_car_model,mobile_number;
	LinearLayout mDismiss;
	AppManager mAppManager;
	RatingBar rating;
	String font_path;
	Typeface tf;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mAppManager=AppManager.getInstance(getActivity());

		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		View rootView = inflater.inflate(R.layout.fragment_driver_details, container, false);

		driver_image=(CircularImageView)rootView.findViewById(R.id.customer_img);
		driver_car_model=(TextView)rootView.findViewById(R.id.txt_veh_model);
		driver_name=(TextView)rootView.findViewById(R.id.txt_driver_name);
		mobile_number=(TextView)rootView.findViewById(R.id.txt_telephone);
		mDismiss=(LinearLayout)rootView.findViewById(R.id.layout_dismiss);
		rating =(RatingBar)rootView.findViewById(R.id.rating) ;
		font_path = "fonts/opensans.ttf";

	    tf = Typeface.createFromAsset(getActivity().getAssets(), font_path);

		setupView();
		//getDialog().setTitle("Customer Details");
		mDismiss.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			dismiss();
			}
		});


		mobile_number.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				DialogAlert();
			}
		});


		return rootView;
	}

	private void setupView() {
		// TODO Auto-generated method stub
		Glide.with(this)
		.load(Uri.parse(mAppManager.getCurrentDriverProfilePic()))
//		.placeholder(R.drawable.placeholder)
				.into(driver_image);
		driver_name.setText(mAppManager.getCurrentDriverName());
		driver_car_model.setText(mAppManager.getCurrentDriverVEHICLEModel());
		mobile_number.setText(mAppManager.getCurrentDriverVEHICLENUMBER());

		rating.setStepSize((int) 0.5f);
		rating.setRating(Float.parseFloat(mAppManager.getCurrentDriverRATING()));
		rating.setEnabled(false);


	}

	Dialog dialog;
	private void DialogAlert() {

		dialog = new Dialog(getActivity(), R.style.Theme_Dialog);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		Window window = dialog.getWindow();
		window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.call_phone);
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();


		TextView mDialogBody = (TextView) dialog
				.findViewById(R.id.txt_dialog_body);
		mDialogBody.setPadding(20, 10, 20, 10);
		TextView mOK = (TextView) dialog.findViewById(R.id.txt_dialog_ok);
		TextView mCancel = (TextView) dialog
				.findViewById(R.id.txt_dialog_cancel);


		mDialogBody.setText(mAppManager.getMobileNumber());
		mDialogBody.setTypeface(tf,Typeface.BOLD);

		mOK.setText("CALL");
		mCancel.setText("CANCEL");

		mOK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {






				call();

				dialog.dismiss();
			}
		});

		mCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				dialog.dismiss();

			}
		});

		

		
}
	
	
	public void call()
	{

	
		Intent callIntent = new Intent(Intent.ACTION_CALL);
		callIntent.setData(Uri.parse("tel:"+mAppManager.getMobileNumber()));
		startActivity(callIntent);
	}
}
