package com.us.hoodruncustomer.commonwork;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataBufferUtils;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.us.hoodruncustomer.R;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class CustomPlaces extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,View.OnClickListener {


    private EditText placeText;
    private RecyclerView placesList;
    protected GoogleApiClient mGoogleApiClient;
    public static final String TAG = "MainActivity";
    private placesAdapter placesAdapter;
    private ArrayList<AutocompletePrediction> allPlaces = new ArrayList<>();
    private String hintText;
    private int requestCode;
    private LinearLayout back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_places);
        hintText = getIntent().getStringExtra("hint");

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .build();

        LinearLayoutManager ll = new LinearLayoutManager(this);
        ll.setOrientation(LinearLayoutManager.VERTICAL);
        placeText = (EditText)findViewById(R.id.place);
        placesList= (RecyclerView)findViewById(R.id.my_recycler_view);
        back = (LinearLayout) findViewById(R.id.back);
        back.setOnClickListener(this);
        placesList.setLayoutManager(ll);
        placesAdapter = new placesAdapter(this,placeClicked);
        placesList.setAdapter(placesAdapter);
        placeText.setHint(hintText);

        placeText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (placeText.getText().length() > 3) {
                    new getFilterData().execute(charSequence);
                }else{
                    allPlaces.clear();
                    placesAdapter.setData(allPlaces);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    @Override
    public void onClick(View view) {
        if(view == back){
            onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        View views = CustomPlaces.this.getCurrentFocus();
        if (views != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(views.getWindowToken(), 0);
        }
        super.onBackPressed();
    }

    /**
     * Submits an autocomplete query to the Places Geo Data Autocomplete API.
     * Results are returned as frozen AutocompletePrediction objects, ready to be cached.
     * objects to store the Place ID and description that the API returns.
     * Returns an empty list if no results were found.
     * Returns null if the API client is not available or the query did not complete
     * successfully.
     * This method MUST be called off the main UI thread, as it will block until data is returned
     * from the API, which may include a network request.
     **/

    class getFilterData extends AsyncTask<CharSequence,Integer,ArrayList<AutocompletePrediction>> {

        @Override
        protected ArrayList<AutocompletePrediction> doInBackground(CharSequence... charSequence) {

            if(results != null){
                results.cancel();
            }
            return  getAutocomplete(charSequence[0]);
        }
        @Override
        protected void onPostExecute(ArrayList<AutocompletePrediction> autocompletePredictions) {
            super.onPostExecute(autocompletePredictions);
            if(autocompletePredictions != null) {
                allPlaces = autocompletePredictions;
                placesAdapter.setData(allPlaces);
            }
        }
    }

    PendingResult<AutocompletePredictionBuffer> results;
    private ArrayList<AutocompletePrediction> getAutocomplete(CharSequence constraint) {
        if (mGoogleApiClient.isConnected()) {
            Log.i(TAG, "Starting autocomplete query for: " + constraint);

            // Submit the query to the autocomplete API and retrieve a PendingResult that will
            // contain the results when the query completes.

            AutocompleteFilter autocompleteFilter =  new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_GEOCODE).build();

            results = Places.GeoDataApi
                    .getAutocompletePredictions(mGoogleApiClient, constraint.toString(),
                            null,null);

            // This method should have been called off the main UI thread. Block and wait for at most 60s
            // for a result from the API.
            AutocompletePredictionBuffer autocompletePredictions= results.await(60, TimeUnit.SECONDS);

            // Confirm that the query completed successfully, otherwise return null
            final Status status = autocompletePredictions.getStatus();
            if (!status.isSuccess()) {
//                Toast.makeText(getApplicationContext(), "Error contacting API: " + status.toString(),
//                        Toast.LENGTH_SHORT).show();
                Log.e(TAG, "Error getting autocomplete prediction API call: " + status.toString());
                autocompletePredictions.release();
                return null;
            }

            Log.i(TAG, "Query completed. Received " + autocompletePredictions.getCount()
                    + " predictions.");

            // Freeze the results immutable representation that can be stored safely.
            return DataBufferUtils.freezeAndClose(autocompletePredictions);
        }
        Log.e(TAG, "Google API client is not connected for autocomplete query.");
        return null;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private String primaryText;
    private String secondaryText;
    private static final CharacterStyle STYLE_Normal = new StyleSpan(Typeface.NORMAL);
    private View.OnClickListener placeClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            // Get the Place object from the buffer.
            View views = CustomPlaces.this.getCurrentFocus();
            if (views != null) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(views.getWindowToken(), 0);
            }

            primaryText = null;
            secondaryText = null;

            int position = (int)view.getTag();
            AutocompletePrediction item = allPlaces.get(position);
            String placeId = item.getPlaceId();

            primaryText= item.getPrimaryText(STYLE_Normal).toString();
            secondaryText = item.getSecondaryText(STYLE_Normal).toString();

            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

        }
    };


    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                Log.e(TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }


            Place place = places.get(0);
            Intent placeData = new Intent();
            placeData.putExtra("latitude",place.getLatLng().latitude);
            placeData.putExtra("longitude",place.getLatLng().longitude);
            placeData.putExtra("address",place.getAddress());
            placeData.putExtra("primary",primaryText);
            placeData.putExtra("secondary",secondaryText);
            places.release();
            setResult(Activity.RESULT_OK, placeData);
            finish();
        }
    };


}
