package com.us.hoodruncustomer.rider.beans;

/**
 * Driver Model Class
 */

public class DriverModel {


    private String taxi_id;
    private String taxi_name;
    private String taxi_away_time;


    /**
     * Default Constructor needed for Firebase retrieval
     */
    public DriverModel() {
    }

    public DriverModel(String taxi_id,String taxi_name,String taxi_away_time) {

        this.taxi_id = taxi_id;
        this.taxi_name = taxi_name;
        this.taxi_away_time = taxi_away_time;
    }


    public String getTaxi_id() {
        return taxi_id;
    }

    public void setTaxi_id(String taxi_id) {
        this.taxi_id = taxi_id;
    }

    public String getTaxi_name() {
        return taxi_name;
    }

    public void setTaxi_name(String taxi_name) {
        this.taxi_name = taxi_name;
    }

    

    public String getTaxi_away_time() {
        return taxi_away_time;
    }

    public void setTaxi_away_time(String taxi_away_time) {
        this.taxi_away_time = taxi_away_time;
    }
}
