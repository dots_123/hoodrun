package com.us.hoodruncustomer.commonwork;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.location.places.AutocompletePrediction;
import com.us.hoodruncustomer.R;

import java.util.ArrayList;

/**
 * Created by admin on 05-Jul-17.
 */

public class placesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context context;
    private LayoutInflater inflater=null;
    private ArrayList<AutocompletePrediction> autocompletePredictions = new ArrayList<AutocompletePrediction>();
    private static final CharacterStyle STYLE_BOLD = new StyleSpan(Typeface.BOLD);
    private static final CharacterStyle STYLE_Normal = new StyleSpan(Typeface.NORMAL);
    private View.OnClickListener onClickListener;

    public placesAdapter(Context context,View.OnClickListener placeClicked) {
        this.context = context;
        onClickListener = placeClicked;
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootview = inflater.inflate(R.layout.place_row, parent, false);
        return new MyViewHolder(rootview);
    }

    public void setData(ArrayList<AutocompletePrediction> autocompletePredictionsd){
        this.autocompletePredictions = autocompletePredictionsd;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof MyViewHolder) {
            final MyViewHolder myHolder = ((MyViewHolder) holder);
            myHolder.secondaryName.setText(autocompletePredictions.get(position).getSecondaryText(STYLE_Normal));
            myHolder.primaryName.setText(autocompletePredictions.get(position).getPrimaryText(STYLE_BOLD));
            myHolder.place.setOnClickListener(onClickListener);
            myHolder.place.setTag(position);
        }
    }

    @Override
    public int getItemCount() {
        return autocompletePredictions.size();
    }

    /**
     * ViewHolder For Taxi Listing
     */

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView primaryName, secondaryName;
        LinearLayout place;

        public MyViewHolder(View itemView) {
            super(itemView);
            primaryName = (TextView) itemView.findViewById(R.id.primaryname);
            secondaryName = (TextView) itemView.findViewById(R.id.secondaryname);
            place = (LinearLayout) itemView.findViewById(R.id.place);

        }
    }

}