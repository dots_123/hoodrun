package com.us.hoodruncustomer.model;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.us.hoodruncustomer.R;

import java.util.ArrayList;

/**
 * Created by admin on 26-Jan-17.
 */

public class DayAnalticAdapter extends BaseAdapter {

    Activity mContext;
    ArrayList<AnalyticsData> analyticsList;
    private LayoutInflater layoutInflater;

    public DayAnalticAdapter(Activity ctx, ArrayList<AnalyticsData> datalist){

        this.mContext=ctx;
        this.analyticsList=datalist;
        layoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return analyticsList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return analyticsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }


    public static class ViewHolder{

        TextView outTime, inTime ,cancelled,accepted,rejected,expired;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        DayAnalticAdapter.ViewHolder holder;

        if (convertView == null) {

            holder = new DayAnalticAdapter.ViewHolder();

            convertView = layoutInflater.inflate(R.layout.driver_analytic_row, null);
            holder.inTime = (TextView) convertView.findViewById(R.id.in_time);
            holder.outTime = (TextView) convertView.findViewById(R.id.out_time);
            holder.accepted = (TextView) convertView.findViewById(R.id.accepted);
            holder.cancelled = (TextView) convertView.findViewById(R.id.cancelled);
            holder.rejected = (TextView) convertView.findViewById(R.id.rejected);
            holder.expired = (TextView) convertView.findViewById(R.id.expired);

            convertView.setTag(holder);
        } else {
            holder = (DayAnalticAdapter.ViewHolder) convertView.getTag();
        }

        holder.outTime.setText(analyticsList.get(position).getShift_out());
        holder.inTime.setText(analyticsList.get(position).getShift_in());
        holder.cancelled.setText(analyticsList.get(position).getCanceled_ride());
        holder.accepted.setText(analyticsList.get(position).getAccepted_ride());
        holder.rejected.setText(analyticsList.get(position).getRejected_ride());
        holder.expired.setText(analyticsList.get(position).getIgnored_ride());


        return convertView;
    }
}
