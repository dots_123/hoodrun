package com.us.hoodruncustomer.registration;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.firebase.iid.FirebaseInstanceId;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.commonwork.Injection;
import com.us.hoodruncustomer.commonwork.PrivacyPolicy;
import com.us.hoodruncustomer.commonwork.TermsActivity;
import com.us.hoodruncustomer.commonwork.Validation;
import com.us.hoodruncustomer.custom.CircularImageView;
import com.us.hoodruncustomer.custom.MyEditText;
import com.us.hoodruncustomer.custom.MyTextView;
import com.us.hoodruncustomer.db.AppManager;
import com.us.hoodruncustomer.exceptionhandler.ExceptionHandler;
import com.us.hoodruncustomer.parser.ConnectionDetector;
import com.us.hoodruncustomer.parser.Parser;
import com.us.hoodruncustomer.payment.AddPaymentActivity;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity implements RegisterContract.View {

    boolean isFromSocial = false;
    String userEmail, userName, fbId, googleId;

    MyEditText mEdtName, mEdtEmail, mEdtPassword, mEdtConfirm_Password,
            mEdtMobile_Number;
    ImageView mImgNext, mImgBack, mImgTick;
    CircularImageView mImageAdd;
    ImageView mImageAdd1;
    ConnectionDetector cd;
    String name, email, password, mob_number, image_code = "";
    String parserResp;

    TextView mAddimg, mOptional, mTerms, mPrivacy;
    Typeface tf;
    String font_path;
    boolean exit = false;

    AppManager mAppManager;
    LinearLayout parentLayout;
    private RegisterContract.Presenter presenter;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;


    Handler mHandler;
    Thread myThread;
    private final static String TAG = "Tsasasasas";

    private String regid = null;
    Button nxt;
    MyTextView headertext;
    Drawable wrong;
    private Uri mCropImageUri, mUri = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        new RegisterPresenter(Injection.remoteDataRepository(getApplicationContext()), this);
        cd = new ConnectionDetector(RegisterActivity.this);

        /**
         * Important to catch the exceptions
         */

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        /*****************************************************************************************/
        font_path = "fonts/opensans.ttf";

        tf = Typeface.createFromAsset(this.getAssets(), font_path);
        mAppManager = AppManager.getInstance(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        if (toolbar != null) {
            int color = Color.parseColor("#ffffff");
            toolbar.setTitleTextColor(color);

            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationIcon(R.drawable.back_arrow);
            toolbar.setNavigationOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                    finish();
                }
            });


            headertext = (MyTextView) findViewById(android.R.id.text1);
            nxt = (Button) findViewById(android.R.id.button1);
            headertext.setText("Register");
            headertext.setTypeface(tf, Typeface.BOLD);
        }

        wrong = getResources().getDrawable(R.drawable.wrong);
        wrong.setBounds(new Rect(0, 0, wrong.getIntrinsicWidth(), wrong.getIntrinsicHeight()));


        mHandler = new Handler();
        if (checkPlayServices()) {

            regid = FirebaseInstanceId.getInstance().getToken();

            if (regid == null) {
                regid = mAppManager.getDeviceId();
            }
            if (regid.isEmpty()) {

                myThread = new Thread(mRunId);
                myThread.start();
            }

        } else {
            Log.d(TAG, "No valid Google Play Services APK found.");
        }
        mEdtEmail = (MyEditText) findViewById(R.id.edt_email);
        mEdtName = (MyEditText) findViewById(R.id.edt_name);
        mEdtPassword = (MyEditText) findViewById(R.id.edt_password);
        mEdtConfirm_Password = (MyEditText) findViewById(R.id.edt_confirm_password);
        mEdtMobile_Number = (MyEditText) findViewById(R.id.edt_mobile_number);
        mImgNext = (ImageView) findViewById(R.id.img_next_btn);
        mImgBack = (ImageView) findViewById(R.id.img_back_btn);
        mImageAdd = (CircularImageView) findViewById(R.id.img_add);
        mImageAdd1 = (ImageView) findViewById(R.id.img_add1);
        parentLayout = (LinearLayout) findViewById(R.id.linear_parent);
        mImgTick = (ImageView) findViewById(R.id.img_tick);
        //	mImageAdd.setBorderColor(Color.parseColor("#aaaaaa"));
        mAddimg = (TextView) findViewById(R.id.txt_add_img);
        mOptional = (TextView) findViewById(R.id.txt_optional);
        mTerms = (TextView) findViewById(R.id.txt_terms);
        mPrivacy = (TextView) findViewById(R.id.txt_privacy);
        mTerms.setText("Terms & Conditions");

        nxt.setText("NEXT");
        nxt.setAllCaps(true);
        nxt.setTextColor(Color.parseColor("#b3b3b3"));

        mEdtEmail.setTypeface(tf, Typeface.BOLD);
        mEdtName.setTypeface(tf, Typeface.BOLD);
        mEdtMobile_Number.setTypeface(tf, Typeface.BOLD);
        mEdtConfirm_Password.setTypeface(tf, Typeface.BOLD);
        mEdtPassword.setTypeface(tf, Typeface.BOLD);
        mAddimg.setTypeface(tf, Typeface.BOLD);
        mOptional.setTypeface(tf, Typeface.BOLD);

        registerViews();
        parentLayout.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                hideKeyBoard(v);
                return false;
            }
        });

        mImageAdd.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                selectImage();
            }
        });
        mImgBack.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub


                onBackPressed();
                finish();
            }
        });

        mImageAdd1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                selectImage();


            }
        });

        checkIfFromSocial();
    }

    private void checkIfFromSocial() {
        if (getIntent().getExtras().containsKey("from")) {

            isFromSocial = true;
            userName = getIntent().getStringExtra("name");
            userEmail = getIntent().getStringExtra("email");

            fbId = getIntent().getStringExtra("fb_id");
            googleId = getIntent().getStringExtra("google_id");

            mEdtPassword.setVisibility(View.GONE);
            mEdtConfirm_Password.setVisibility(View.GONE);

            if (!TextUtils.isEmpty(userName))
                mEdtName.setText(userName);
            if (!TextUtils.isEmpty(userEmail))
                mEdtEmail.setText(userEmail);

            if (MyApplication.bitmap != null) {
                mImageAdd.setImageBitmap(MyApplication.bitmap);

                mImageAdd1.setVisibility(View.GONE);
                mImageAdd1.setClickable(false);
                mImageAdd.setVisibility(View.VISIBLE);
                mImageAdd.setClickable(true);
                mImgTick.setVisibility(View.VISIBLE);
            }

        }
    }


    private void registerViews() {
        // TODO Auto-generated method stub
        mImageAdd.setVisibility(View.GONE);
        mImageAdd.setClickable(false);
        mImgTick.setVisibility(View.INVISIBLE);
        // TextWatcher would let us check validation error on the fly
        mEdtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                Validation.hasText(mEdtName);
                if (!isValidWord(s.toString())) {

                    mEdtName.setError("Invalid name", wrong);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });

        mEdtEmail.addTextChangedListener(new TextWatcher() {
            // after every change has been made to this editText, we would like
            // to check validity
            @Override
            public void afterTextChanged(Editable s) {
                Validation.isEmailAddress(mEdtEmail, true);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }
        });

        mEdtMobile_Number.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                Validation.isPhoneNumber(mEdtMobile_Number, false);

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (checkValidation()) {
                    mImgNext.setImageResource(R.drawable.next_btn);
                    nxt.setTextColor(Color.parseColor("#000000"));
                }
            }
        });
        mEdtPassword.addTextChangedListener(new TextWatcher() {
            // after every change has been made to this editText, we would like
            // to check validity
            @Override
            public void afterTextChanged(Editable s) {
                Validation.hasText1(mEdtPassword);
                if (!s.toString().matches("^[a-zA-Z0-9!@#$%^&*()]*$"))
                    mEdtPassword.setError("Only !@#$%^&*() are allowed", wrong);
                else if (s.length() < 6 && s.length() > 0)
                    mEdtPassword.setError("Password should contain minimum 6 characters", wrong);


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {


            }
        });
        mEdtConfirm_Password.addTextChangedListener(new TextWatcher() {
            // after every change has been made to this editText, we would like
            // to check validity

            @Override
            public void afterTextChanged(Editable s) {
                System.out.println("mEdtPassword.getText().toString():" + mEdtPassword.getText().toString());
                System.out.println("s" + s.toString());

                if (s.length() == 0) {
                    mEdtConfirm_Password
                            .setCompoundDrawablesWithIntrinsicBounds(0, 0,
                                    0, 0);
                } else if (s.length() < 6) {
                    mEdtConfirm_Password
                            .setCompoundDrawablesWithIntrinsicBounds(0, 0,
                                    0, 0);
                    mEdtConfirm_Password.setError(null, wrong);

                } else {
                    if (!mEdtPassword.getText().toString()
                            .equals(s.toString())) {
                        mEdtConfirm_Password.setError("Password mismatch", wrong);
                    } else if (mEdtPassword.getText().toString()
                            .equals(s.toString())) {
                        mEdtConfirm_Password.setError(null);
                        mEdtConfirm_Password
                                .setCompoundDrawablesWithIntrinsicBounds(0, 0,

                                        R.drawable.tick, 0);
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }
        });

        mImgNext.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                /*
                 * Validation class will check the error and display the error
				 * on respective fields but it won't resist the form submission,
				 * so we need to check again before submit
				 */
                if (checkValidation()) {
                    //submitForm();

                    if (cd.isConnectingToInternet()) {
                        name = mEdtName.getText().toString().trim();
                        email = mEdtEmail.getText().toString().trim();
                        password = mEdtPassword.getText().toString().trim();
                        mob_number = mEdtMobile_Number.getText().toString().trim();
                        mAppManager.saveCustomerName(name);
                        mAppManager.saveMobileNumber(mob_number);

                        if (isFromSocial && MyApplication.bitmap != null) {

                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            MyApplication.bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                            byte[] byteArray = byteArrayOutputStream.toByteArray();
//                            to encode base64 from byte array use following method

                            image_code = Base64.encodeToString(byteArray, Base64.DEFAULT);

                        } else {
                            if (mUri != null)
                                imageProcess();
                        }
                        attemptingToRegistration();

                    } else {
                        Toast.makeText(getApplicationContext(), "No internet connection available.", Toast.LENGTH_LONG).show();
                        System.exit(0);
                    }
                }
            }
        });

        nxt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

				/*
                 * Validation class will check the error and display the error
				 * on respective fields but it won't resist the form submission,
				 * so we need to check again before submit
				 */
                if (checkValidation()) {
                    //submitForm();
                    ConnectionDetector cd = new ConnectionDetector(RegisterActivity.this);
                    if (cd.isConnectingToInternet()) {
                        name = mEdtName.getText().toString().trim();
                        email = mEdtEmail.getText().toString().trim();
                        password = mEdtPassword.getText().toString().trim();
                        mob_number = mEdtMobile_Number.getText().toString().trim();
                        mAppManager.saveCustomerName(name);
                        mAppManager.saveMobileNumber(mob_number);
                        mAppManager.saveEmail(email);

                        if (mUri != null) {

                            imageProcess();
                        }

                        attemptingToRegistration();

                    } else {
                        Toast.makeText(getApplicationContext(), "No internet connection available.", Toast.LENGTH_LONG).show();
                        System.exit(0);
                    }

                } else
                    Toast.makeText(RegisterActivity.this,
                            "Please enter all fields.", Toast.LENGTH_LONG).show();

            }
        });
        mTerms.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent t = new Intent(RegisterActivity.this, TermsActivity.class);
                startActivity(t);

            }
        });
        mPrivacy.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent terms = new Intent(RegisterActivity.this, PrivacyPolicy.class);
                startActivity(terms);

            }
        });
    }


    private boolean checkValidation() {
        boolean ret = true;

        if (!Validation.hasText(mEdtName)) {

            mEdtName.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                    R.drawable.wrong, 0);
            ret = false;
        }
        if (!Validation.isEmailAddress(mEdtEmail, true)) {
            mEdtEmail.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                    R.drawable.wrong, 0);
            ret = false;
        }
        if (!Validation.isPhoneNumber(mEdtMobile_Number, false)) {
            mEdtMobile_Number.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                    R.drawable.wrong, 0);
            ret = false;
        }

        if(!isFromSocial){

            if (mEdtPassword.getText().toString().trim().equals(""))

            {

                mEdtPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                        R.drawable.wrong, 0);
                ret = false;
            }

            if (mEdtConfirm_Password.getText().toString().trim().equals(""))

            {

                mEdtConfirm_Password.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                        R.drawable.wrong, 0);
                ret = false;
            }

        }


        return ret;
    }

    private void selectImage() {


        CropImage.startPickImageActivity(this);


    }


    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            boolean requirePermissions = false;
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                requirePermissions = true;
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);

            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                // ((ImageView) findViewById(R.id.quick_start_cropped_image)).setImageURI(result.getUri());


                mImageAdd.setImageURI(result.getUri());
                mUri = result.getUri();

                mImageAdd1.setVisibility(View.GONE);
                mImageAdd1.setClickable(false);
                mImageAdd.setVisibility(View.VISIBLE);

                mImageAdd.setClickable(true);

                mImgTick.setVisibility(View.VISIBLE);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // required permissions granted, start crop image activity
            startCropImageActivity(mCropImageUri);
        } else {
            Toast.makeText(this, "Cancelling, required permissions are not granted.", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }


    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }


    protected void hideKeyBoard(View view) {
        InputMethodManager in = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }


    private Runnable mRunId = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            if (cd.isConnectingToInternet()) {
                if (!exit) {
                    Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

                    new AsyncTask<Void, Void, String>() {
                        @Override
                        protected String doInBackground(Void... params) {
                            String msg = "";
                            try {

                                regid = FirebaseInstanceId.getInstance().getToken();

                                System.out.println("Current Device's Registration ID is: " + regid);


                            } catch (Exception ex) {
                                msg = "Error :" + ex.getMessage();

                            }
                            return msg;
                        }

                        @Override
                        protected void onPostExecute(String msg) {
                            //etRegId.setText(msg + "\n");
                            try {

                                if (TextUtils.isEmpty(regid))


                                {
                                    exit = true;
                                    myThread.stop();
                                    myThread.start();
                                } else exit = true;

                            } catch (Exception e) {
                                // TODO: handle exception

                                Toast.makeText(getApplicationContext(), "Something went wrong, Check your connection and try again", Toast.LENGTH_LONG).show();
                                System.exit(0);
                            }


                        }
                    }.execute(null, null, null);


                }
            } else {
                Toast.makeText(getApplicationContext(), "No internet connection available.", Toast.LENGTH_LONG).show();
            }
        }
    };

    public static boolean isValidWord(String w) {
        return w.matches("^[A-Za-z\\s]+$");
    }

    Dialog loader;
    ProgressWheel loader_wheel;

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.d(TAG, "This device is not supported - Google Play Services.");
                finish();
            }
            return false;
        }
        return true;
    }


    private void imageProcess() {
        // TODO Auto-generated method stub


        InputStream iStream = null;
        try {
            iStream = getContentResolver().openInputStream(mUri);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            byte[] inputData = getBytes(iStream);
            image_code = Base64.encodeToString(inputData, Base64.NO_WRAP);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void attemptingToRegistration() {

        presenter.register(fbId, googleId, name, email, password, mob_number, image_code, regid, "1", this);



       /* StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_registration,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        System.out.println("REGISTER===" + response);
                        parserResp = Parser.Registration(response);

                        if (!TextUtils.isEmpty(parserResp)) {

                            switch (parserResp) {
                                case "TRUE":

                                    mAppManager.saveCustomerID(Parser.customer_id);
                                    mAppManager.savePrevActivity("ADD", "REG");
                                    mAppManager.saveProfileStatus(Parser.getProfileStatus());

                                    Intent i = new Intent(RegisterActivity.this, AddPaymentActivity.class);
                                    startActivity(i);
                                    finish();

                                    break;
                                case "FALSE":
                                    mImgNext.setImageResource(R.drawable.next_btngrey);
                                    nxt.setTextColor(Color.parseColor("#b3b3b3"));
                                    Toast.makeText(getApplicationContext(), "" + Parser.getRegStatus(), Toast.LENGTH_SHORT).show();
                                    if (Parser.getRegStatus().equals("Email id you have entered is already registered."))
                                        mEdtEmail.setError(Parser.getRegStatus());
                                    if (Parser.getRegStatus().equals("Mobile number you have entered is already registered."))
                                        mEdtMobile_Number.setError(Parser.getRegStatus());
                                    break;
                            }


                            loader.dismiss();
                        } else {
                            loader.dismiss();
                            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
                            System.exit(0);
                        }

                        if (!TextUtils.isEmpty(parserResp)) {

                            switch (parserResp) {
                                case "TRUE":

                                    mAppManager.saveCustomerID(Parser.customer_id);
                                    mAppManager.savePrevActivity("ADD", "REG");
                                    mAppManager.saveProfileStatus(Parser.getProfileStatus());

                                    Intent i = new Intent(RegisterActivity.this, AddPaymentActivity.class);
                                    startActivity(i);
                                    finish();

                                    break;
                                case "FALSE":
                                    mImgNext.setImageResource(R.drawable.next_btngrey);
                                    nxt.setTextColor(Color.parseColor("#b3b3b3"));
                                    Toast.makeText(getApplicationContext(), "" + Parser.getRegStatus(), Toast.LENGTH_SHORT).show();
                                    if (Parser.getRegStatus().equals("Email id you have entered is already registered."))
                                        mEdtEmail.setError(Parser.getRegStatus());
                                    if (Parser.getRegStatus().equals("Mobile number you have entered is already registered."))
                                        mEdtMobile_Number.setError(Parser.getRegStatus());
                                    break;
                            }


                            loader.dismiss();
                        } else {
                            loader.dismiss();
                            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
                            System.exit(0);
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        retryDialog(1);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("name", name);
                params.put("email", email);
                params.put("password", password);
                params.put("mobile_number", mob_number);
                params.put("profile_picture", image_code);
                params.put("device_id", regid);
                params.put("os", "1");

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);*/
    }

    Dialog retry;

    private void retryDialog(final int i) {

        retry = new Dialog(RegisterActivity.this, R.style.Theme_Dialog);
        retry.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = retry.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        retry.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        retry.setContentView(R.layout.thaks_dialog_twobutton);
        retry.setCancelable(false);
        retry.setCanceledOnTouchOutside(false);
        retry.show();

        TextView mTitle = (TextView) retry.findViewById(R.id.txt_title);
        mTitle.setText("Error!");
        mTitle.setTypeface(tf, Typeface.BOLD);
        TextView mDialogBody = (TextView) retry.findViewById(R.id.txt_dialog_body);
        mDialogBody.setText("Internet connection is slow or disconnected. Try connecting again.");
        LinearLayout mOK = (LinearLayout) retry.findViewById(R.id.lin_Ok);
        LinearLayout mCancel = (LinearLayout) retry.findViewById(R.id.lin_cancel);
        TextView mRetry = (TextView) retry.findViewById(R.id.txt_dialog_ok);
        TextView mExit = (TextView) retry.findViewById(R.id.txt_dialog_cancel);
        mRetry.setText("RETRY");
        mExit.setText("EXIT");

        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


                retry.dismiss();

                switch (i) {
                    case 1:
                        if (cd.isConnectingToInternet()) {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("name", name);
                            params.put("email", email);
                            params.put("password", password);
                            params.put("mobile_number", mob_number);
                            params.put("profile_picture", image_code);
                            params.put("device_id", regid);
                            params.put("os", "1");
                            attemptingToRegistration();
                        } else {
                            Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_SHORT).show();
                            System.exit(0);
                        }
                        break;

                    default:
                        break;

                }


            }
        });

        mCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                retry.dismiss();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });

    }

    @Override
    public void registerSuccessResponse(String response) {


        hideProgress();
        parserResp = Parser.Registration(response);

        if (!TextUtils.isEmpty(parserResp)) {

            switch (parserResp) {
                case "TRUE":

                    mAppManager.saveCustomerID(Parser.customer_id);
                    mAppManager.savePrevActivity("ADD", "REG");
                    mAppManager.saveProfileStatus(Parser.getProfileStatus());

                    Intent i = new Intent(RegisterActivity.this, AddPaymentActivity.class);
                    startActivity(i);
                    finish();

                    break;
                case "FALSE":
                    mImgNext.setImageResource(R.drawable.next_btngrey);
                    nxt.setTextColor(Color.parseColor("#b3b3b3"));
                    Toast.makeText(getApplicationContext(), "" + Parser.getRegStatus(), Toast.LENGTH_SHORT).show();
                    if (Parser.getRegStatus().equals("Email id you have entered is already registered."))
                        mEdtEmail.setError(Parser.getRegStatus());
                    if (Parser.getRegStatus().equals("Mobile number you have entered is already registered."))
                        mEdtMobile_Number.setError(Parser.getRegStatus());
                    break;
            }

        } else {
            hideProgress();
            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
            System.exit(0);
        }

        if (!TextUtils.isEmpty(parserResp)) {

            switch (parserResp) {
                case "TRUE":

                    mAppManager.saveCustomerID(Parser.customer_id);
                    mAppManager.savePrevActivity("ADD", "REG");
                    mAppManager.saveProfileStatus(Parser.getProfileStatus());

                    Intent i = new Intent(RegisterActivity.this, AddPaymentActivity.class);
                    startActivity(i);
                    finish();

                    break;
                case "FALSE":
                    mImgNext.setImageResource(R.drawable.next_btngrey);
                    nxt.setTextColor(Color.parseColor("#b3b3b3"));
                    Toast.makeText(getApplicationContext(), "" + Parser.getRegStatus(), Toast.LENGTH_SHORT).show();
                    if (Parser.getRegStatus().equals("Email id you have entered is already registered."))
                        mEdtEmail.setError(Parser.getRegStatus());
                    if (Parser.getRegStatus().equals("Mobile number you have entered is already registered."))
                        mEdtMobile_Number.setError(Parser.getRegStatus());
                    break;
            }


            loader.dismiss();
        } else {
            loader.dismiss();
            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
            System.exit(0);
        }
    }


    @Override
    public void showProgreass() {
        loader = new Dialog(RegisterActivity.this);
        loader.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loader.setContentView(R.layout.dialog_progress);
        loader_wheel = (ProgressWheel) loader.findViewById(R.id.progress_wheel);
        loader_wheel.setCircleRadius(50);
        loader.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        loader.setCancelable(false);
        loader.show();
    }

    @Override
    public void hideProgress() {

        if (loader != null) {
            loader.dismiss();
        }

    }

    @Override
    public void onLoginFail(Throwable t) {
        hideProgress();
        retryDialog(1);

    }


    @Override
    public void onSaveResponseOtp(String response) {

    }

    @Override
    public void onSuccessResendOtp(String response) {

    }

    @Override
    public void onFailResend(Throwable t) {

    }

    @Override
    public void setPresenter(RegisterContract.Presenter presenter) {
        this.presenter = presenter;

    }
}
