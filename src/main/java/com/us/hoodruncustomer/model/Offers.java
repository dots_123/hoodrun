package com.us.hoodruncustomer.model;

public class Offers {


	public  String title;
	public  String amount;
	public  String completed_rides;
	public  String rejected_rides;
	public  String cancelled_rides;
	public  String ignored_rides;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCompleted_rides() {
		return completed_rides;
	}

	public void setCompleted_rides(String completed_rides) {
		this.completed_rides = completed_rides;
	}

	public String getRejected_rides() {
		return rejected_rides;
	}

	public void setRejected_rides(String rejected_rides) {
		this.rejected_rides = rejected_rides;
	}

	public String getCancelled_rides() {
		return cancelled_rides;
	}

	public void setCancelled_rides(String cancelled_rides) {
		this.cancelled_rides = cancelled_rides;
	}

	public String getIgnored_rides() {
		return ignored_rides;
	}

	public void setIgnored_rides(String ignored_rides) {
		this.ignored_rides = ignored_rides;
	}
}
