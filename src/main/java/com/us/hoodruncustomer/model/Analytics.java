package com.us.hoodruncustomer.model;

public class Analytics {


	public String time_input;
	public String time_of_data;
	public String time_map;

	public String getCanceled_ride() {
		return canceled_ride;
	}

	public void setCanceled_ride(String canceled_ride) {
		this.canceled_ride = canceled_ride;
	}

	public String canceled_ride;
	public String accepted_ride;
	public String rejected_ride;
	public String ignored_ride;
	public String working_hours;
	public String earning;

	public Analytics(String time_input,String time_map, String time_of_data,String canceled_ride, String accepted_ride, String rejected_ride, String ignored_ride, String working_hours, String earning) {
		this.time_input = time_input;
		this.time_map = time_map;
		this.time_of_data = time_of_data;
		this.accepted_ride = accepted_ride;
		this.rejected_ride = rejected_ride;
		this.ignored_ride = ignored_ride;
		this.working_hours = working_hours;
		this.canceled_ride = canceled_ride;
		this.earning = earning;
	}

	public String getTime_map() {
		return time_map;
	}

	public void setTime_map(String time_map) {
		this.time_map = time_map;
	}

	public String getTime_input() {
		return time_input;
	}

	public void setTime_input(String time_input) {
		this.time_input = time_input;
	}

	public String getTime_of_data() {
		return time_of_data;
	}

	public void setTime_of_data(String time_of_data) {
		this.time_of_data = time_of_data;
	}

	public String getAccepted_ride() {
		return accepted_ride;
	}

	public void setAccepted_ride(String accepted_ride) {
		this.accepted_ride = accepted_ride;
	}

	public String getRejected_ride() {
		return rejected_ride;
	}

	public void setRejected_ride(String rejected_ride) {
		this.rejected_ride = rejected_ride;
	}

	public String getIgnored_ride() {
		return ignored_ride;
	}

	public void setIgnored_ride(String ignored_ride) {
		this.ignored_ride = ignored_ride;
	}

	public String getWorking_hours() {
		return working_hours;
	}

	public void setWorking_hours(String working_hours) {
		this.working_hours = working_hours;
	}

	public String getEarning() {
		return earning;
	}

	public void setEarning(String earning) {
		this.earning = earning;
	}
}
