package com.us.hoodruncustomer.rider.events;

/**
 * Created by admin on 12-May-17.
 */

public class ErrorCause {

    private String error_message;

    public String getError_message() {
        return error_message;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }
}
