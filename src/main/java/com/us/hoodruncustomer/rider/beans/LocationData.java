package com.us.hoodruncustomer.rider.beans;

import android.location.Location;

import java.io.Serializable;

/**
 * Created by admin on 14-Nov-16.
 */

public class LocationData implements Serializable {

    private int pickStatus,dropStatus;
    private Location pickLocation,dropLocation;
    private String pickAddress,dropAddress;

    public int getPickStatus() {
        return pickStatus;
    }

    public void setPickStatus(int pickStatus) {
        this.pickStatus = pickStatus;
    }

    public int getDropStatus() {
        return dropStatus;
    }

    public void setDropStatus(int dropStatus) {
        this.dropStatus = dropStatus;
    }

    public Location getPickLocation() {
        return pickLocation;
    }

    public void setPickLocation(Location pickLocation) {
        this.pickLocation = pickLocation;
    }

    public Location getDropLocation() {
        return dropLocation;
    }

    public void setDropLocation(Location dropLocation) {
        this.dropLocation = dropLocation;
    }

    public String getPickAddress() {
        return pickAddress;
    }

    public void setPickAddress(String pickAddress) {
        this.pickAddress = pickAddress;
    }

    public String getDropAddress() {
        return dropAddress;
    }

    public void setDropAddress(String dropAddress) {
        this.dropAddress = dropAddress;
    }
}
