package com.us.hoodruncustomer.ridehistory;

import android.content.Context;

import com.us.hoodruncustomer.data.DataSource;
import com.us.hoodruncustomer.data.remote.RemoteDataSource;

import java.util.Map;

/**
 * Created by admin on 11/15/2017.
 */

public class HistoryPresenter implements Historycontract.Presenter, DataSource.RideHistoryI {

    private DataSource dataSource;
    private Historycontract.View mhistoryview;
    private Context context;

    public HistoryPresenter(RemoteDataSource remoteDataSource, Historycontract.View mLoginView) {

        dataSource = remoteDataSource;
        this.mhistoryview = mLoginView;
        this.mhistoryview.setPresenter(this);

    }

    @Override
    public void getHistoty(Map<String, String> params, Context context) {
        mhistoryview.showProgreass();
        dataSource.getHistory(params, this, context);
    }

    @Override
    public void drop() {
        mhistoryview = null;

    }

    @Override
    public void rideHistoryResponse(String baseResponse) {
        mhistoryview.hideProgress();
        mhistoryview.getHistoryData(baseResponse);

    }

    @Override
    public void onFail(Throwable t) {
        mhistoryview.hideProgress();
        mhistoryview.onFail(t);

    }
}
