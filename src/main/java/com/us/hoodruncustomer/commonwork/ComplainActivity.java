package com.us.hoodruncustomer.commonwork;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.configure.TwitterRestClient;
import com.us.hoodruncustomer.custom.MyEditText;
import com.us.hoodruncustomer.db.AppManager;
import com.us.hoodruncustomer.exceptionhandler.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;


public class ComplainActivity extends SimpleActivity {

	AlertDialog levelDialog;
	TextView headertitle,images_count;
	AppManager mAppManager;
	ImageView back, filter;
	Typeface tf;
	ImageView mImageAdd1;
	Drawable wrong;
	LinearLayout lay_accept;
	MyEditText edt_description,edt_amount;
	String amount,description;
	String ride_id;
	int count=0;
	private Uri mCropImageUri;
	private ArrayList<String> mPath=new ArrayList<>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_damage);

		/**
		 * Important to catch the exceptions
		 */

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

		/*****************************************************************************************/


		ride_id = getIntent().getStringExtra("ride_id");

		tf = Typeface.createFromAsset(this.getAssets(), "fonts/opensans.ttf");

		headertitle = (TextView) findViewById(R.id.text1);
		back = (ImageView) findViewById(R.id.ivBack);
		filter = (ImageView) findViewById(R.id.filter);
		lay_accept = (LinearLayout) findViewById(R.id.lay_accept);
		mImageAdd1 = (ImageView) findViewById(R.id.img_add1);
		images_count = (TextView)findViewById(R.id.images_count);
		edt_description = (MyEditText) findViewById(R.id.edt_description);
		edt_amount = (MyEditText) findViewById(R.id.edt_amount);

		headertitle = (TextView) findViewById(R.id.text1);

		mAppManager = AppManager.getInstance(ComplainActivity.this);

		wrong=getResources().getDrawable(R.drawable.wrong);

		wrong.setBounds(new Rect(0, 0, wrong.getIntrinsicWidth(), wrong.getIntrinsicHeight()));

		hideFilter();

		edt_description.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				Validation.hasText(edt_description);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
									  int count) {
			}
		});

		edt_amount.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				Validation.hasText(edt_amount);

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
									  int count) {
			}
		});

		headertitle.setText("Damage Info");

		back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		lay_accept.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(checkValidation()){

					amount = edt_amount.getText().toString();

					description = edt_description.getText().toString();

					complainRequest(mPath);
				}
			}
		});
		mImageAdd1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				selectImage();


			}
		});

	}
	@Override
	@SuppressLint("NewApi")
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
			Uri imageUri = CropImage.getPickImageResultUri(this, data);

			mPath.add(imageUri.getPath());///(a < b) ? a : b
			images_count.setText(mPath.size()+" "+((mPath.size()>1) ? "Images":"Image")+" Selected");
		}
		// handle result of pick image chooser
		if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
			Uri imageUri = CropImage.getPickImageResultUri(this, data);

			// For API >= 23 we need to check specifically that we have permissions to read external storage.
			boolean requirePermissions = false;
			if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
				// request permissions and handle the result in onRequestPermissionsResult()
				requirePermissions = true;
				mCropImageUri = imageUri;
				requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);

			} else {
				// no permissions required or already grunted, can start crop image activity
				startCropImageActivity(imageUri);
			}
		}

		// handle result of CropImageActivity
		if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
			CropImage.ActivityResult result = CropImage.getActivityResult(data);
			if (resultCode == RESULT_OK) {
				// ((ImageView) findViewById(R.id.quick_start_cropped_image)).setImageURI(result.getUri());




				mPath.add(result.getUri().getPath());


			} else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
				Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
			}
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
			// required permissions granted, start crop image activity
			startCropImageActivity(mCropImageUri);
		} else {
			Toast.makeText(this, "Cancelling, required permissions are not granted.", Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * Start crop image activity for the given image.
	 */
	private void startCropImageActivity(Uri imageUri) {
		CropImage.activity(imageUri)
				.setGuidelines(CropImageView.Guidelines.ON)
				.start(this);
	}

	private void selectImage() {


		CropImage.startPickImageActivity(this);


	}

	private boolean checkValidation() {
		boolean ret = true;

		if (!Validation.hasText(edt_description)){

			edt_description.setCompoundDrawablesWithIntrinsicBounds(0, 0,
					R.drawable.wrong, 0);
			ret = false;
		}

		if (!Validation.hasText(edt_amount)){

			edt_amount.setCompoundDrawablesWithIntrinsicBounds(0, 0,
					R.drawable.wrong, 0);
			ret = false;
		}

		return ret;
	}


	public static boolean isValidWord(String w) {
		return w.matches("^[A-Za-z\\s]+$");
	}


	public void showFilter() {
		filter.setVisibility(View.VISIBLE);
	}

	public void hideFilter() {
		filter.setVisibility(View.GONE);
	}




	private void complainRequest(ArrayList<String> files){

		showProgress();
		RequestParams params = new RequestParams();


		if(files.size() > 0) {
			for (int i = 0; i < files.size(); i++) {
				try {
					params.put("image_" + (i + 1), new File(files.get(i)));
				} catch (FileNotFoundException e) {
				}

			}
		}

		params.put("driver_id",mAppManager.getDriverID_Number());
		params.put("description", description);
		params.put("token_code",mAppManager.getTokenCode());
		params.put("damage_charge",amount);
		params.put("ride_id",ride_id);


			TwitterRestClient.post("api/complaints/submit_complaint", params, new BaseJsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String rawJsonResponse, Object response) {

				System.out.println("Complain==="+rawJsonResponse);
				//{"status":true,"code":200,"message":"Your complain submitted successfully","data":[]}

				JSONObject cobj = null;
				try {
					cobj = new JSONObject(rawJsonResponse);
					String status = cobj.getString("status");
					String message = cobj.getString("message");

					if(status.equalsIgnoreCase("true")){
						noDriverDialog(message,true);
					}else{
						noDriverDialog(message,false);
					}


				} catch (JSONException e) {
					e.printStackTrace();
				}

				hideProgress();


			}

			@Override
			public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, String rawJsonData, Object errorResponse) {
				hideProgress();
				noDriverDialog("Error!!!Please Try Again",false);
			}

			@Override
			protected Object parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
				return null;
			}
		});

	}

	Dialog dialog1;
	private void noDriverDialog(String message, final boolean status) {

		dialog1 = new Dialog(ComplainActivity.this, R.style.Theme_Dialog);
		dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
		Window window = dialog1.getWindow();
		window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);
		dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog1.setContentView(R.layout.dialog_onebutton);
		dialog1.setCancelable(false);
		dialog1.setCanceledOnTouchOutside(false);
		dialog1.show();

		TextView mTitle=(TextView)dialog1.findViewById(R.id.text_title);
		mTitle.setTypeface(tf, Typeface.BOLD);
		TextView mDialogBody=(TextView)dialog1.findViewById(R.id.txt_dialog_body);
		TextView mOK=(TextView)dialog1.findViewById(R.id.txt_dialog_ok);

		mTitle.setText("ALERT");

		mDialogBody.setText(message);

		mOK.setText("OK");
		mOK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if(status){
					dialog1.dismiss();
					finish();
				}else {
					dialog1.dismiss();
				}
			}
		});

	}








}
