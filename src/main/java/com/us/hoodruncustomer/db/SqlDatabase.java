package com.us.hoodruncustomer.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;

/**
 * Created by admin on 16-May-17.
 */

public class SqlDatabase extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "hoponnManager";

    // Contacts table name
    private static final String TABLE_CARS = "cars";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_Loc_Storage = "local_storage";
    private static final String KEY_Dym_Storage = "dynamic_storage";

    String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CARS + "("
            + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT," + KEY_Loc_Storage + " TEXT,"
            + KEY_Dym_Storage + " TEXT" + ")";

    public SqlDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CARS);
        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    private int checkExistance(int carId,String dynPath){

        String selectQuery = "SELECT  * FROM " + TABLE_CARS + "WHERE "+KEY_ID+" = "+carId;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor != null) {
            if (cursor.moveToFirst()) {

                if(cursor.getString(3) != null){

                    if(cursor.getString(3).equalsIgnoreCase(dynPath)){

                        if(cursor.getString(2) != null) {

                            File file = new File(cursor.getString(2));
                            if (!file.isDirectory()) {
                                file = file.getParentFile();
                                if (file.exists()) {
                                    return 1;
                                }
                            }
                        }else{
                            return 2; // Update the Local Path
                        }
                    }else{
                        return 3; // Update The Path
                    }
                }
            }
        }

        return 0;

    }

    public void addCar(int carId,String carName,String locPath,String dynpath){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, carId);
        values.put(KEY_NAME, carName);
        values.put(KEY_Loc_Storage, locPath);
        values.put(KEY_Dym_Storage, dynpath);

        db.insert(TABLE_CARS, null, values);
        db.close();
    }

    public int updateContact(int carId,String carName,String locPath,String dynpath) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, carName);
        values.put(KEY_Loc_Storage, locPath);
        values.put(KEY_Dym_Storage, dynpath);

        // updating row
        return db.update(TABLE_CARS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(carId) });
    }

}
