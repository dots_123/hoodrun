package com.us.hoodruncustomer.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.widget.SlidingPaneLayout.LayoutParams;
import android.support.v7.widget.PopupMenu;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.configure.Configure;
import com.us.hoodruncustomer.db.AppManager;
import com.us.hoodruncustomer.parser.ConnectionDetector;

public class PaymentListAdapter extends BaseAdapter{

	private Context context;
	private Activity activity;
	private ArrayList<Card> mCardList=new ArrayList<Card>();
	LayoutInflater inflater;
	Button insidePopupButton;
	TextView popupText;
	LinearLayout layoutOfPopup;
	Integer selected_position = 0;
	View vi;
	AppManager mAppManager;
	String card_id;
	int pos;
	public PaymentListAdapter(Activity a,ArrayList<Card> cardList) {

		activity=a;
		mCardList=cardList;

		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mAppManager=AppManager.getInstance(activity);
	}




	@Override
	public int getCount() {

		return mCardList.size();
	}

	@Override
	public Object getItem(int position) {
		this.pos=position;

		return position;
	}

	@Override
	public long getItemId(int position) {

		this.pos=position;
		return position;
	}


	public static class ViewHolder{

		TextView CardType;
		TextView CardName;
		ImageButton Overflow;
		CheckBox check;


	}

	@SuppressLint("ViewHolder") @Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final ViewHolder holder;
		vi=convertView;


		vi=inflater.inflate(R.layout.paymenttype,parent,false);

		holder=new ViewHolder();

		holder.CardType=(TextView)vi.findViewById(R.id.txt_cardType);
		holder.CardName=(TextView)vi.findViewById(R.id.txt_cardNumber);
		holder.Overflow=(ImageButton)vi.findViewById(R.id.btn_Overflow);
		holder.check=(CheckBox)vi.findViewById(R.id.checkBox1);
		holder.Overflow.setTag(vi);


		if(position==selected_position)
		{
			holder.check.setChecked(true);
		}
		else
		{
			holder.check.setChecked(false);
		}

		holder.CardType.setText(mCardList.get(position).getCard_type());
		holder.CardName.setText(mCardList.get(position).getCard_no());


		popupInit();

		holder.check.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked)
				{
					selected_position =  position;
					card_id=mCardList.get(position).getCard_id();
					pos=position;
					if(new ConnectionDetector(activity).isConnectingToInternet()){
						new DefaultCard().execute(Configure.URL_default_card);
					}else Toast.makeText(activity, "no internet connection avilable.", Toast.LENGTH_SHORT).show();
				}
				else{
					selected_position = 0;
				}
				notifyDataSetChanged();
			}
		});

		holder.Overflow.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {


				switch (v.getId()) {
					case R.id.btn_Overflow:
						v.measure(View.MeasureSpec.UNSPECIFIED,
								View.MeasureSpec.UNSPECIFIED);
						PopupMenu popup = new PopupMenu(activity, v);
						popup.getMenuInflater().inflate(R.menu.menu,popup.getMenu());

						popup.show();
						popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
							@Override
							public boolean onMenuItemClick(MenuItem item) {

								switch (item.getItemId()) {
									case R.id.delete:

										//  Toast.makeText(activity, " Install Clicked at position " + " : " + position, Toast.LENGTH_LONG).show();
										card_id=mCardList.get(position).getCard_id();
										pos=position;

										if(new ConnectionDetector(activity).isConnectingToInternet()){
											new DeleteCards().execute(Configure.URL_delete_cards);
										}else Toast.makeText(activity, "no internet connection avilable.", Toast.LENGTH_SHORT).show();
										break;


									default:
										break;
								}

								return true;
							}
						});

						break;

					default:
						break;
				}

			}
		});

		vi.setTag(holder);

		return vi;
	}
	PopupWindow popupMessage;

	public void popupInit() {

		popupMessage = new PopupWindow(layoutOfPopup, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		popupMessage.setContentView(layoutOfPopup);

	}




	Dialog loader;
	ProgressWheel loader_wheel;
	private void showProgress() {
		// TODO Auto-generated method stub
		loader = new Dialog (activity);
		loader.requestWindowFeature (Window.FEATURE_NO_TITLE);
		loader.setContentView (R.layout.dialog_progress);
		loader_wheel=(ProgressWheel)loader.findViewById(R.id.progress_wheel);
		loader_wheel.setCircleRadius(50);
		loader.getWindow ().setBackgroundDrawableResource (android.R.color.transparent);
		loader.setCancelable(false);
		loader.show();
	}


	private class DeleteCards extends AsyncTask<String, Void, String> {
		String parsresp,message;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showProgress();

		}



		@SuppressWarnings("deprecation")
		@Override
		protected String doInBackground(String... params) {


			InputStream is = null;
			String json = "";
			try {
				// defaultHttpClient
				DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost(params[0]);
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);


				nameValuePair.add(new BasicNameValuePair("customer_id",mAppManager.getCustomerID()));
				nameValuePair.add(new BasicNameValuePair("token_code",mAppManager.getTokenCode()));

				nameValuePair.add(new BasicNameValuePair("card_id",card_id));


				//Encoding POST data
				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));

				} catch (UnsupportedEncodingException e)
				{
					e.printStackTrace();
				}

				HttpResponse httpResponse = httpClient.execute(httpPost);
				HttpEntity httpEntity = httpResponse.getEntity();
				is = httpEntity.getContent();

			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
				is.close();
				System.out.println("DELETECARD---"+sb.toString());

				JSONObject mJObject=new JSONObject(sb.toString());

				parsresp=mJObject.optString("status");
				message=mJObject.optString("message");

			} catch (Exception e) {
				Log.e("Buffer Error", "Error converting result " + e.toString());
			}



			return parsresp;

		}

		@Override
		protected void onPostExecute(String response){
			if(!TextUtils.isEmpty(parsresp)){


				if(parsresp.equals("TRUE")){

					mCardList.remove(pos);
					notifyDataSetChanged();

				}
				else{

					Toast.makeText(activity, ""+message, Toast.LENGTH_LONG).show();
				}

			}
			else Toast.makeText(activity, "Something went wrong, Please try again later.", Toast.LENGTH_SHORT).show();
			if(loader!=null)
				loader.dismiss();
		}



	}


	private class DefaultCard extends AsyncTask<String, Void, String> {
		String parsresp,message;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			showProgress();
		}



		@SuppressWarnings("deprecation")
		@Override
		protected String doInBackground(String... params) {


			InputStream is = null;
			String json = "";
			try {
				// defaultHttpClient
				DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost(params[0]);
				List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);

				nameValuePair.add(new BasicNameValuePair("customer_id",mAppManager.getCustomerID()));
				nameValuePair.add(new BasicNameValuePair("token_code",mAppManager.getTokenCode()));

				nameValuePair.add(new BasicNameValuePair("card_id",card_id));


				//Encoding POST data
				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));

				} catch (UnsupportedEncodingException e)
				{
					e.printStackTrace();
				}

				HttpResponse httpResponse = httpClient.execute(httpPost);
				HttpEntity httpEntity = httpResponse.getEntity();
				is = httpEntity.getContent();

			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
				is.close();
				System.out.println("DEFAULTCard---"+sb.toString());

				JSONObject mJObject=new JSONObject(sb.toString());

				parsresp=mJObject.optString("status");
				message=mJObject.optString("message");

			} catch (Exception e) {
				Log.e("Buffer Error", "Error converting result " + e.toString());
			}



			return parsresp;

		}

		@Override
		protected void onPostExecute(String response){
			if(!TextUtils.isEmpty(parsresp)){


				if(parsresp.equals("TRUE")){
					System.out.println("post execute default card");

				}
				else{

					Toast.makeText(activity, ""+message, Toast.LENGTH_LONG).show();
				}

			}
			else Toast.makeText(activity, "Something went wrong, Please try again later.", Toast.LENGTH_SHORT).show();


			loader.dismiss();
		}


	}


}
