package com.us.hoodruncustomer.model;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.us.hoodruncustomer.R;

import java.util.ArrayList;

public class TransactionsListAdapter extends BaseAdapter{

	Activity mContext;
	ArrayList<TransactionsHistoryDao> analyticsList;
	String user;
	private LayoutInflater layoutInflater;
	public TransactionsListAdapter(Activity ctx, ArrayList<TransactionsHistoryDao> historyList, String user){

		this.mContext=ctx;
		this.analyticsList=historyList;
		this.user=user;
		layoutInflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return analyticsList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return analyticsList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}


	public static class ViewHolder{

		TextView time, inTime ,amount,description,poundSign;
		LinearLayout amountback;

	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		ViewHolder holder;

		if (convertView == null) {

			holder = new ViewHolder();

				convertView = layoutInflater.inflate(R.layout.transaction_row, null);
				holder.inTime = (TextView) convertView.findViewById(R.id.in_time);
				holder.time = (TextView) convertView.findViewById(R.id.time);
			    holder.amountback = (LinearLayout)convertView.findViewById(R.id.amountback);
				holder.amount = (TextView) convertView.findViewById(R.id.amount);
		    	holder.description = (TextView) convertView.findViewById(R.id.description);
		    	holder.poundSign = (TextView) convertView.findViewById(R.id.tvPoundSign);

			    convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

			holder.time.setText(analyticsList.get(position).getTransaction_date());
			holder.inTime.setText(analyticsList.get(position).getTransaction_time());
			holder.description.setText(analyticsList.get(position).getDescription());

			if(analyticsList.get(position).getCredit_account().equalsIgnoreCase("Driver Wallet")) {
				if(analyticsList.get(position).getColor() != null){
					holder.amount.setTextColor(Color.parseColor(analyticsList.get(position).getColor()));
					holder.poundSign.setTextColor(Color.parseColor(analyticsList.get(position).getColor()));
				}else {
					holder.amount.setTextColor(Color.GREEN);
					holder.poundSign.setTextColor(Color.GREEN);
				}
				holder.amount.setText(analyticsList.get(position).getAmount());
			}else{
				if(analyticsList.get(position).getColor() != null){
					holder.poundSign.setTextColor(Color.parseColor(analyticsList.get(position).getColor()));
					holder.amount.setTextColor(Color.parseColor(analyticsList.get(position).getColor()));
				}else {
					holder.amount.setTextColor(Color.parseColor("#DC143C"));
					holder.poundSign.setTextColor(Color.parseColor("#DC143C"));
				}

				holder.amount.setText(analyticsList.get(position).getAmount());
			}

		return convertView;
	}

}
