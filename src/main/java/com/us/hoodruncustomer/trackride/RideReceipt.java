package com.us.hoodruncustomer.trackride;


import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.android.volley.RequestQueue;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.CircleTransform;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.commonwork.Injection;
import com.us.hoodruncustomer.commonwork.SimpleActivity;
import com.us.hoodruncustomer.home.CustomerHomeActivity;
import com.us.hoodruncustomer.splash.SplashActivity;
import com.us.hoodruncustomer.commonwork.SuperBaseActivity;
import com.us.hoodruncustomer.exceptionhandler.ExceptionHandler;
import com.us.hoodruncustomer.fcm.MyFirebaseMessagingService;
import com.us.hoodruncustomer.fragment.InvalidTokenDialog;
import com.us.hoodruncustomer.nodeservice.ConnectorService;
import com.us.hoodruncustomer.parser.ConnectionDetector;
import com.us.hoodruncustomer.parser.Parser;
import com.us.hoodruncustomer.parser.StatusParser;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class RideReceipt extends SimpleActivity implements TrackRideContract.View {

    private TrackRideContract.Presenter presenter;
    private TextView mDistance, mDuration, mTotal, mPayment, surge;
    private ImageView car, driver;
    private ImageButton mRideComplete;
    private RelativeLayout discount_visibility;
    private TextView amount, discount, cartype, pickup, dropup;
    private RatingBar rate_bar;
    private Button finish_now_btn;
    public static final String RECEIVER_PAYMENT_DONE = "com.uk.hoponn.payment_done";
    private String font_path;
    private Typeface tf;
    private ConnectionDetector cd;
    private TextView headertext;
    private InvalidTokenDialog iDialog;
    private NotificationManager myNManager;
    private Activity activity;
    private String ride_id;
    private String rate_value = "0.0";
    @BindView(R.id.payment__type_show)
    TextView paymentTypeShow;

    @BindView(R.id.duration_show)
    TextView durationShow;

    @BindView(R.id.distance_show)
    TextView distanceShow;

    @BindView(R.id.payment_message)
    TextView paymentMessage;

    @BindView(R.id.switcher)
    ViewSwitcher viewSwitcher;

    @BindView(R.id.sharedview)
    RelativeLayout sharedView;


    private int payemnt_status = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ride_receipt);
        ButterKnife.bind(this);
        new TrackridePresenter(Injection.remoteDataRepository(getApplicationContext()), this);
        ride_id = mAppManager.App_getDriver_ride_id();

        payemnt_status = getIntent().getIntExtra("payment_status", 2);

        if (ride_id == null) {
            finish();
        }


        /**
         * Important to catch the exceptions
         */

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        /*****************************************************************************************/
        MyApplication.activity = RideReceipt.this;
        MyApplication.context = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            int color = Color.parseColor("#ffffff");
            toolbar.setTitleTextColor(color);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            headertext = (TextView) findViewById(android.R.id.text1);
            headertext.setText("Receipt");

        }


        if (payemnt_status == 1) {
            headertext.setText("Review");
            viewSwitcher.setDisplayedChild(1);
        } else {
            viewSwitcher.setDisplayedChild(0);
        }

        cd = new ConnectionDetector(RideReceipt.this);
        myNManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mAppManager.saveCurrentActivity("");
        font_path = "fonts/opensans.ttf";

        tf = Typeface.createFromAsset(this.getAssets(), font_path);

        activity = this;

        amount = (TextView) findViewById(R.id.amount);
        discount = (TextView) findViewById(R.id.discount);
        cartype = (TextView) findViewById(R.id.cartype);
        pickup = (TextView) findViewById(R.id.pickup);
        dropup = (TextView) findViewById(R.id.dropup);
        car = (ImageView) findViewById(R.id.imageView11);
        driver = (ImageView) findViewById(R.id.imageView19);
        surge = (TextView) findViewById(R.id.surge);
        discount_visibility = (RelativeLayout) findViewById(R.id.discount_visibility);
        rate_bar = (RatingBar) findViewById(R.id.rate_bar);
        finish_now_btn = (Button) findViewById(R.id.finish_now_btn);
        amount.setTypeface(tf, Typeface.BOLD);

        rate_bar.setRating(0.0f);

        iDialog = new InvalidTokenDialog(RideReceipt.this);

        rate_bar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

				/*if(rating < 1.0) {
                    rate_bar.setRating(1.0f);
					rate_value = "1.0";
				}else{
					rate_value = String.valueOf(ratingBar.getRating()).trim();
				}
*/
                rate_value = String.valueOf(ratingBar.getRating()).trim();
            }
        });

        finish_now_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rideRating();
            }
        });

        setData();

    }


    private void setData() {

        if (MyFirebaseMessagingService.push_Status_Flag.equals("12") || mAppManager.getPrevActivity("REC").equals("REQ")) {
            viewSwitcher.setDisplayedChild(0);
            paymentMessage.setText("");
            sharedView.setVisibility(View.VISIBLE);


            if (StatusParser.getSharedOut_trip_duration() != null) {
                durationShow.setText(StatusParser.getSharedOut_trip_duration());
            }

            if (StatusParser.getSharedOut_trip_distance() != null) {
                distanceShow.setText(StatusParser.getSharedOut_trip_distance() + " miles");
            }

            if (StatusParser.getSharedOut_payment_mode() != null) {

                if (StatusParser.getSharedOut_payment_mode().equalsIgnoreCase("1")) {
                    paymentTypeShow.setText("Card");
                    paymentMessage.setText(mContext.getResources().getString(R.string.pound) + StatusParser.getSharedOut_total() + " will be deducted from your registered card.");
                } else {
                    paymentTypeShow.setText("Card");
                    paymentMessage.setText(mContext.getResources().getString(R.string.pound) + StatusParser.getSharedOut_total() + " will be deducted from your registered card.");
                }

            }

            String image_path = StatusParser.getDriver_list_car_image();

            if (image_path == null || image_path.isEmpty()) {
                image_path = StatusParser.getSharedOut_list_car_image();
            }

            Glide.with(mContext)
                    .load(image_path)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .override(40, 40)
                    .dontAnimate()
                    .into(new GlideDrawableImageViewTarget(car) {
                        @Override
                        public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
                            super.onResourceReady(resource, animation);
                            Bitmap bitmap = ((GlideBitmapDrawable) resource).getBitmap();
                            car.setImageBitmap(bitmap);
                            int color = Color.parseColor("#cd0102"); //The color u want
                            car.setColorFilter(color);
                        }

                        @Override
                        public void onLoadFailed(Exception e, Drawable errorDrawable) {
                            super.onLoadFailed(e, errorDrawable);
                            //never called
                        }
                    });
//			Glide.with(RideReceipt.this).load(StatusParser.getDriver_list_car_image()).override(40, 40).into(car);

            String imgs = (StatusParser.getSharedOut_driver_profile_pic() != null) ? StatusParser.getSharedOut_driver_profile_pic() : "http://www.iconsdb.com/icons/download/gray/user-512.png";

            if (imgs.contains(".png") || imgs.contains(".jpeg") || imgs.contains(".jpg") || imgs.contains(".JPEG")) {

            } else {
                imgs = "http://www.iconsdb.com/icons/download/gray/user-512.png";
            }

            Glide.with(RideReceipt.this)
                    .load(imgs) // add your image url
                    .override(60, 60)
                    .transform(new CircleTransform(RideReceipt.this)) // applying the image transformer
                    .into(driver);


            if (StatusParser.getSharedOut_discount_applied() != null && StatusParser.getSharedOut_discount_applied().equalsIgnoreCase("true")) {
                discount_visibility.setVisibility(View.VISIBLE);
                discount.setText(mContext.getResources().getString(R.string.pound) + "" + StatusParser.getSharedOut_discount_total() + " discount applied");
            } else {
                discount_visibility.setVisibility(View.GONE);
            }

            if (StatusParser.getSharedOut_your_amount() != null) {
                amount.setText(mContext.getResources().getString(R.string.pound) + "" + StatusParser.getSharedOut_your_amount());
            }

            if (StatusParser.getSharedOut_vehicle_type() != null) {

                if (StatusParser.getSharedOut_no_of_seats() != null) {
                    String seatsText = "<font color=#B8B8B8>(" + StatusParser.getSharedOut_no_of_seats() + " people)</font>";
                    cartype.setText(Html.fromHtml("<b>" + StatusParser.getSharedOut_vehicle_type() + seatsText));
                } else {
                    cartype.setText(Html.fromHtml("<b>" + StatusParser.getSharedOut_vehicle_type() + "</b>"));
                }

            }

            if (StatusParser.getSharedOut_customer_pick_up_point_name() != null) {
                pickup.setText(StatusParser.getSharedOut_customer_pick_up_point_name());
            }

            if (StatusParser.getSharedOut_customer_destination_point_name() != null) {
                dropup.setText(StatusParser.getSharedOut_customer_destination_point_name());
            }

            if (StatusParser.getSharedOut_fare_multiLier() != null) {
                if (Double.parseDouble(StatusParser.getSharedOut_fare_multiLier()) > 1.0) {
                    surge.setVisibility(View.VISIBLE);
                    surge.setText(StatusParser.getSharedOut_fare_multiLier() + "x surge");
                } else {
                    surge.setVisibility(View.GONE);
                }
            } else {
                surge.setVisibility(View.GONE);
            }


        } else {
            String discount_status = mAppManager.App_getDriver_discount_applied();
            String total = mAppManager.App_getDriver_total();

            if (total == null || total.trim().length() == 0) {
                total = mAppManager.App_getDriver_total_cost();
            }

            String seats = (mAppManager.App_getDriver_no_of_seats() != null) ? mAppManager.App_getDriver_no_of_seats() : "";
            String type = (mAppManager.App_getDriver_vehicle_type() != null) ? mAppManager.App_getDriver_vehicle_type() : "";
            String pickuptxt = mAppManager.App_getDriver_customer_pick_up_point_name();
            String dropuptxt = mAppManager.App_getDriver_customer_destination_point_name();
            String driver_image = (mAppManager.App_getDriver_driver_profile_pic() != null) ? mAppManager.App_getDriver_driver_profile_pic() : "http://www.iconsdb.com/icons/download/gray/user-512.png";

            if (driver_image.contains(".png") || driver_image.contains(".jpeg") || driver_image.contains(".jpg") || driver_image.contains(".JPEG")) {

            } else {
                driver_image = "http://www.iconsdb.com/icons/download/gray/user-512.png";
            }

            String car_image = mAppManager.App_getDriver_list_car_image();

            if (car_image == null || car_image.trim().length() == 0) {
                car_image = mAppManager.App_getDriver_list_car_image();
            }

            String discoutn = mAppManager.App_getDriver_discount_total();
            String surgetxt = mAppManager.App_getDriver_fare_multiplier();

            String trip_duration = mAppManager.App_getDriver_trip_duration();
            String trip_distance = mAppManager.App_getDriver_trip_distance();
            String payment_mode = mAppManager.App_getDriver_payment_type();


            if (trip_duration != null) {
                durationShow.setText(trip_duration);
            }

            if (trip_distance != null) {
                distanceShow.setText(trip_distance + " miles");
            }

            if (payment_mode != null) {

                if (payment_mode.equalsIgnoreCase("1")) {
                    paymentMessage.setText(mContext.getResources().getString(R.string.pound) + total + " will be deducted from your registered card.");
                    paymentTypeShow.setText("Card");
                } else if (payment_mode.equalsIgnoreCase("3")) {
                    paymentMessage.setText("Please pay " + mContext.getResources().getString(R.string.pound) + total + " to driver");
                    paymentTypeShow.setText("Cash");
                } else {
                    paymentMessage.setText(mContext.getResources().getString(R.string.pound) + total + " will be deducted from your registered card.");
                    paymentTypeShow.setText("Card");
                }
            }
            Glide.with(mContext)
                    .load(car_image)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .override(40, 40)
                    .dontAnimate()
                    .into(new GlideDrawableImageViewTarget(car) {
                        @Override
                        public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
                            super.onResourceReady(resource, animation);
                            Bitmap bitmap = ((GlideBitmapDrawable) resource).getBitmap();
                            car.setImageBitmap(bitmap);
                            int color = Color.parseColor("#cd0102"); //The color u want
                            car.setColorFilter(color);
                        }

                        @Override
                        public void onLoadFailed(Exception e, Drawable errorDrawable) {
                            super.onLoadFailed(e, errorDrawable);
                            //never called
                        }
                    });
//			Glide.with(RideReceipt.this).load(car_image).override(40, 40).into(car);

            Glide.with(RideReceipt.this)
                    .load(driver_image) // add your image url
                    .override(60, 60)
                    .placeholder(R.drawable.holdersml)
                    .transform(new CircleTransform(RideReceipt.this)) // applying the image transformer
                    .into(driver);


            if (discount_status != null && discount_status.equalsIgnoreCase("true")) {
                discount_visibility.setVisibility(View.VISIBLE);
                discount.setText(mContext.getResources().getString(R.string.pound) + discoutn + " discount applied");
            } else {
                discount_visibility.setVisibility(View.GONE);
            }

            if (total != null) {
                amount.setText(mContext.getResources().getString(R.string.pound) + "" + total);
            }

            if (type != null) {

                if (seats != null) {
                    cartype.setText(Html.fromHtml("<b>" + type + " </b>(" + seats + " people)"));
                } else {
                    cartype.setText(Html.fromHtml("<b>" + type + "</b>"));
                }

            }

            if (pickuptxt != null) {
                pickup.setText(pickuptxt);
            }

            if (dropuptxt != null) {
                dropup.setText(dropuptxt);
            }

            if (surgetxt != null) {
                if (Double.parseDouble(surgetxt) > 1.0) {
                    surge.setVisibility(View.VISIBLE);
                    surge.setText(surgetxt + "x surge");
                } else {
                    surge.setVisibility(View.GONE);
                }
            } else {
                surge.setVisibility(View.GONE);
            }
        }

    }


    @OnClick(R.id.Ok_now_btn)
    public void okClicked() {
        Intent toHome = new Intent(getApplicationContext(), CustomerHomeActivity.class);
        startActivity(toHome);
        finish();
    }


    Dialog dialog2;

    private void Dialog() {
        TextView mDialogBody, mOK;


        dialog2 = new Dialog(RideReceipt.this, R.style.Theme_Dialog);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog2.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog2.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);
        dialog2.setContentView(R.layout.dialog_no_title);
        dialog2.setCancelable(false);
        dialog2.setCanceledOnTouchOutside(false);
        dialog2.show();
        mDialogBody = (TextView) dialog2.findViewById(R.id.txt_dialog_body);
        mOK = (TextView) dialog2.findViewById(R.id.txt_dialog_ok);
        mDialogBody.setText("Thank you for using our service.");
        mOK.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                dialog2.dismiss();
                if (MyFirebaseMessagingService.push_Status_Flag.equals("12") || mAppManager.getPrevActivity("REC").equals("REQ") || SuperBaseActivity.push_flag.equals("12") || mAppManager.getShared()) {

                    MyFirebaseMessagingService.push_Status_Flag = "";
                    mAppManager.setShared(false);
                    startActivity(new Intent(RideReceipt.this, SplashActivity.class));
                    finish();
                } else {
//					mAppManager.savePrevActivity("RATE", "REC");
//					Intent myintent = new Intent(RideReceipt.this,
//							RatingActivity.class);
//					startActivity(myintent);
//					finish();
                }
            }
        });

    }


    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activityResumed();
        if (myNManager != null) {
            myNManager.cancelAll();
        }
        registerReceiver(this.Payment_Done, new IntentFilter(RECEIVER_PAYMENT_DONE));

        if (!mSocket.connected()) {
            Log.d("NodeConnection", "RideReceipt-onResume");
            Intent start_Serv = new Intent(getApplicationContext(), ConnectorService.class);
            startService(start_Serv);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.activityPaused();
    }

    @Override
    protected void onStop() {

        if (rideRatingrequestQueue != null) {
            rideRatingrequestQueue.cancelAll(this);
        }
        if (dialog2 != null) {
            dialog2.dismiss();
        }
        mAppManager.savePrevActivity("REC", "");
        unregisterReceiver(Payment_Done);
        super.onStop();
    }


    private BroadcastReceiver Payment_Done = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {
            headertext.setText("Review");
            viewSwitcher.setDisplayedChild(1);
        }

    };


	/*
        Rating Api
	 */

    String parsresp;
    RequestQueue rideRatingrequestQueue;

    private void rideRating() {


        presenter.rideRating(mAppManager.getTokenCode(), rate_value, ride_id, this);


    }

    @Override
    public void customerCancellationChargeResponse(String response) {

    }

    @Override
    public void customerCancelRideResponse(String response) {

    }

    @Override
    public void userCheckIsRideEndResponse(String response) {

    }

    @Override
    public void showProgreass() {
        loader = new Dialog(RideReceipt.this);
        loader.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loader.setContentView(R.layout.dialog_progress);
        loader_wheel = (ProgressWheel) loader.findViewById(R.id.progress_wheel);
        loader_wheel.setCircleRadius(50);
        loader.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        loader.setCancelable(false);
        loader.show();
    }

    @Override
    public void hideProgress() {
        if (loader != null) {
            loader.dismiss();
        }

    }

    @Override
    public void onFail(Throwable t) {

    }

    @Override
    public void onFailRideEnd(Throwable t) {

    }

    @Override
    public void rideRatinResponse(String response) {

        hideProgress();

        System.out.println("RATING===" + response);
        parsresp = Parser.rateTip(response);

        if (!TextUtils.isEmpty(parsresp)) {

            if (parsresp.equals("TRUE")) {
                if (!activity.isFinishing()) {
                    try {
                        Intent in = new Intent(RideReceipt.this, CustomerHomeActivity.class);
                        startActivity(in);
                        finish();
                    } catch (Exception e) {
                        Log.d("Rating", "" + e.toString());
                    }
                }

            } else {

                Toast.makeText(getApplicationContext(), Parser.getRate_status_message(), Toast.LENGTH_LONG).show();
            }


        } else {

            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
            System.exit(0);
        }


    }

    @Override
    public void rideRatingFail(Throwable t) {

        hideProgress();

        Intent toSplash = new Intent(getApplicationContext(), SplashActivity.class);
        startActivity(toSplash);
        finish();


    }

    @Override
    public void setPresenter(TrackRideContract.Presenter presenter) {

        this.presenter = presenter;

    }
}

