package com.us.hoodruncustomer.authentication;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.commonwork.Injection;
import com.us.hoodruncustomer.payment.AddPaymentActivity;
import com.us.hoodruncustomer.registration.Verification;
import com.us.hoodruncustomer.splash.SplashActivity;
import com.us.hoodruncustomer.configure.Configure;
import com.us.hoodruncustomer.configure.EasyPermission;
import com.us.hoodruncustomer.custom.DrawableClickListener;
import com.us.hoodruncustomer.custom.MyEditText;
import com.us.hoodruncustomer.db.AppManager;
import com.us.hoodruncustomer.parser.ConnectionDetector;
import com.us.hoodruncustomer.parser.Parser;
import com.us.hoodruncustomer.parser.StatusParser;
import com.us.hoodruncustomer.home.CustomerHomeActivity;
import com.us.hoodruncustomer.trackride.RideReceipt;
import com.us.hoodruncustomer.trackride.TrackRide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class LoginActivity extends Activity implements LoginContract.View {
    private LoginContract.Presenter presenter;
    LinearLayout ImgBtnLogin;
    EditText mEdtUserName;
    MyEditText mEdtPassWord;
    AppManager mAppManager;
    Typeface tf;
    String font_path;
    String email, password, parsresp = "";
    Dialog dialog1, dialog2;
    ImageView back;
    ConnectionDetector cd;
    String message;
    boolean exit = false;
    private MyApplication app;
    Thread myThread;
    String parseresp = "";
    String id_type;

    private String regid = "";
    EasyPermission easyPermission;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        app = (MyApplication) getApplication();
        new LoginPresenter(Injection.remoteDataRepository(getApplicationContext()), this);
        setContentView(R.layout.activity_login);

        ImgBtnLogin = (LinearLayout) findViewById(R.id.img_btn_login);
        mEdtUserName = (EditText) findViewById(R.id.edt_username);
        mEdtPassWord = (MyEditText) findViewById(R.id.edt_password);
        back = (ImageView) findViewById(R.id.img_bck);
        mAppManager = AppManager.getInstance(this);
        mAppManager.savePrevActivity("ADD", "");
        mAppManager.savePrevActivity("VERIFY", "LOGIN");
        cd = new ConnectionDetector(LoginActivity.this);

        drawableClick(mEdtPassWord);

        font_path = "fonts/opensans.ttf";

        tf = Typeface.createFromAsset(this.getAssets(), font_path);
        mEdtUserName.setTypeface(tf);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        if (checkPlayServices()) {

            regid = FirebaseInstanceId.getInstance().getToken();

            if (regid == null) {
                regid = mAppManager.getDeviceId();
            }

            System.out.println("TOKEN " + regid);

            if (TextUtils.isEmpty(regid)) {
                myThread = new Thread(mRunId);
                myThread.start();
            }

        } else {

            Log.d("play services", "No valid Google Play Services APK found.");
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.clearFlags(LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(LoginActivity.this.getResources().getColor(R.color.black));
        }

        back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mAppManager.savePrevActivity("SPLASH", "LOGIN");
                Intent in = new Intent(LoginActivity.this, SplashActivity.class);
                startActivity(in);
                finish();
            }
        });

        ImgBtnLogin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (cd.isConnectingToInternet()) {


                    if (mEdtUserName.getText().toString().trim().length() == 0 || mEdtPassWord.getText().toString().trim().length() == 0) {

                        Toast.makeText(getApplicationContext(), "Enter email and password", Toast.LENGTH_LONG).show();
                    } else {
                        email = mEdtUserName.getText().toString();
                        password = mEdtPassWord.getText().toString();

                        // showProgress();
                        login();

                    }

                } else
                    Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_LONG).show();
            }
        });

    }


    private void drawableClick(final MyEditText editText) {

        editText.setDrawableClickListener(new DrawableClickListener() {


            public void onClick(DrawablePosition target) {
                switch (target) {
                    case RIGHT:
                        //Do something here
                        fpDialog();

                        break;

                    default:
                        break;
                }
            }


            private void fpDialog() {
                // TODO Auto-generated method stub


                dialog2 = new Dialog(LoginActivity.this, R.style.Theme_Dialog);
                dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
                Window window = dialog2.getWindow();
                window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);
                dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog2.setContentView(R.layout.forgot_password);
                dialog2.setCancelable(false);
                dialog2.setCanceledOnTouchOutside(false);
                dialog2.show();
                TextView mTitle = (TextView) dialog2.findViewById(R.id.txt_title);
                mTitle.setTypeface(tf, Typeface.BOLD);
                final EditText mEdtEmail = (EditText) dialog2.findViewById(R.id.edt_email);
                LinearLayout mLinCancel = (LinearLayout) dialog2.findViewById(R.id.lin_cancel);

                LinearLayout mLinProceed = (LinearLayout) dialog2.findViewById(R.id.lin_Ok);


                mLinCancel.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialog2.dismiss();


                    }
                });
                mLinProceed.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        email = mEdtEmail.getText().toString().trim();
                        if (cd.isConnectingToInternet()) {
                            if (email.length() == 0) {
                                Toast.makeText(getApplicationContext(), "Email field is required!", Toast.LENGTH_SHORT).show();
                            } else {

                                presenter.forgotPassword(email, LoginActivity.this);


                            }
                        } else
                            Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_LONG).show();
                    }
                });


            }

        });

    }


    private Runnable mRunId = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            if (cd.isConnectingToInternet()) {
                if (!exit) {

                    Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

                    new AsyncTask<Void, Void, String>() {
                        @Override
                        protected String doInBackground(Void... params) {
                            String msg = "";
                            try {
                                FirebaseMessaging.getInstance().subscribeToTopic("hoponn");
                                regid = FirebaseInstanceId.getInstance().getToken();
                                System.out.println("Current Device's Registration ID is: " + regid);

                            } catch (Exception ex) {
                                msg = "Error :" + ex.getMessage();

                            }
                            return msg;
                        }

                        @Override
                        protected void onPostExecute(String msg) {
                            //etRegId.setText(msg + "\n");
                            try {

                                if (TextUtils.isEmpty(regid))


                                {
                                    exit = true;
                                    myThread.stop();
                                    myThread.start();
                                } else exit = true;

                            } catch (Exception e) {
                                // TODO: handle exception

                                Toast.makeText(getApplicationContext(), "Something went wrong, Check your connection and try again", Toast.LENGTH_LONG).show();
                                System.exit(0);
                            }


                        }
                    }.execute(null, null, null);


                }
            } else {

            }
        }
    };

    Dialog loader;
    ProgressWheel loader_wheel;

    private void showProgress() {
        // TODO Auto-generated method stub
        loader = new Dialog(LoginActivity.this);
        loader.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loader.setContentView(R.layout.dialog_progress);
        loader_wheel = (ProgressWheel) loader.findViewById(R.id.progress_wheel);
        loader_wheel.setCircleRadius(50);
        loader.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        loader.setCancelable(false);
        loader.show();
    }


    @Override
    protected void onDestroy() {

        super.onDestroy();
    }


    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        checkPlayServices();
    }


    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        9000).show();
            } else {
                Log.d("PLAY SERVICES", "This device is not supported - Google Play Services.");
                finish();
            }
            return false;
        }
        return true;
    }


    private void login() {

        presenter.login(mEdtUserName.getText().toString().trim(), mEdtPassWord.getText().toString().trim(), LoginActivity.this);


    }


    private void Dialog(String message) {
        // TODO Auto-generated method stub


        dialog1 = new Dialog(LoginActivity.this, R.style.Theme_Dialog);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog1.getWindow();
        window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog1.setContentView(R.layout.dialog_onebutton);
        dialog1.setCancelable(false);
        dialog1.setCanceledOnTouchOutside(false);
        dialog1.show();

        TextView mTitle = (TextView) dialog1.findViewById(R.id.text_title);
        TextView mDialogBody = (TextView) dialog1.findViewById(R.id.txt_dialog_body);
        TextView mOK = (TextView) dialog1.findViewById(R.id.txt_dialog_ok);


        mTitle.setVisibility(View.GONE);

        mDialogBody.setText(message);

        mOK.setText("OK");
        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


                dialog1.dismiss();


            }
        });


    }


    Dialog retry;

    private void retryDialog(final int i) {

        retry = new Dialog(LoginActivity.this, R.style.Theme_Dialog);
        retry.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = retry.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        retry.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        retry.setContentView(R.layout.thaks_dialog_twobutton);
        retry.setCancelable(false);
        retry.setCanceledOnTouchOutside(false);
        retry.show();

        TextView mTitle = (TextView) retry.findViewById(R.id.txt_title);
        mTitle.setText("Error!");
        mTitle.setTypeface(tf, Typeface.BOLD);
        TextView mDialogBody = (TextView) retry.findViewById(R.id.txt_dialog_body);
        mDialogBody.setText("The server is taking too long to respond OR something is wrong with your internet connection.");
        LinearLayout mOK = (LinearLayout) retry.findViewById(R.id.lin_Ok);
        LinearLayout mCancel = (LinearLayout) retry.findViewById(R.id.lin_cancel);
        TextView mRetry = (TextView) retry.findViewById(R.id.txt_dialog_ok);
        TextView mExit = (TextView) retry.findViewById(R.id.txt_dialog_cancel);
        mRetry.setText("RETRY");
        mExit.setText("EXIT");

        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


                retry.dismiss();

                switch (i) {
                    case 1:
                        if (cd.isConnectingToInternet()) {

                            login();
                        } else {
                            Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_SHORT).show();
                            System.exit(0);
                        }
                        break;
                    case 2:
                        if (cd.isConnectingToInternet()) {

                            presenter.forgotPassword(email, LoginActivity.this);
                        } else {
                            Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_SHORT).show();
                            System.exit(0);
                        }
                        break;

                    default:
                        break;

                }


            }
        });

        mCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                retry.dismiss();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });

    }


    private void splashTask(String url) {
        String id = "";

        Map<String, String> params = new HashMap<String, String>();
        params.put("token_code", mAppManager.getTokenCode());
        if (mAppManager.getuser().equals("customer")) {
            id = mAppManager.getCustomerID();
            params.put(id_type, mAppManager.getCustomerID());
        }
        presenter.updateData(mAppManager.getTokenCode(), id, LoginActivity.this);


    }


    @Override
    public void nevigateToHeme(String response) {
        hideProgress();


        System.out.println("LOGIN===" + response);
        parsresp = Parser.login(response);

        if (!TextUtils.isEmpty(parsresp)) {

            switch (parsresp) {
                case "TRUE":

                    mAppManager.saveMobileNumber(Parser.getMob_no());
                    if (Parser.getUser_type().equals("1")) {


                        mAppManager.saveUser("customer");
                        mAppManager.saveCustomerName(Parser.getName());
                        switch (Parser.getProfileStatus()) {
                            case "1":
                                loader.dismiss();
                                mAppManager.saveCustomerID(Parser.customer_id);
                                mAppManager.saveTokenCode(Parser.getToken_code());
                                Toast.makeText(getApplicationContext(), "" + Parser.getLoginStatus(), Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(LoginActivity.this, AddPaymentActivity.class));

                                break;
                            case "2": {
                                loader.dismiss();
                                mAppManager.saveCustomerID(Parser.customer_id);
                                mAppManager.saveTokenCode(Parser.getToken_code());
                                mAppManager.saveOtpDestiantion("mobile");
                                // mAppManager.setLogin_status(true);
                                Toast.makeText(getApplicationContext(), "" + Parser.getLoginStatus(), Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(LoginActivity.this,
                                        Verification.class);
                                startActivity(i);

                                break;
                            }
                            case "3": {

                                mAppManager.saveCustomerID(Parser.customer_id);
                                mAppManager.saveTokenCode(Parser.getToken_code());
                                System.out.println("787tokencode787=== " + Parser.getToken_code() + "===" + Parser.customer_id);
                                mAppManager.setLogin_status(true);
//												if(mAppManager.getTokenCode() != null && !mAppManager.getTokenCode().isEmpty()) {
//													app.StartNode();
//												}
                                if (Parser.getIsride_status() == null) {
                                    loader.dismiss();
                                    Intent i = new Intent(LoginActivity.this, CustomerHomeActivity.class);
                                    i.putExtra("suspension", false);
                                    startActivity(i);
                                    finish();
                                } else {
                                    if (Parser.getIsride_status().equalsIgnoreCase("true")) {
                                        splashTask(Configure.URL_ride_completeness_customer);
                                    } else {
                                        loader.dismiss();
                                        Intent i = new Intent(LoginActivity.this, CustomerHomeActivity.class);
                                        i.putExtra("suspension", false);
                                        startActivity(i);
                                        finish();
                                    }
                                }
                                break;
                            }

                            case "4":
                                mAppManager.saveCustomerID(Parser.customer_id);
                                mAppManager.saveTokenCode(Parser.getToken_code());
                                System.out.println("787tokencode787=== " + Parser.getToken_code() + "===" + Parser.customer_id);
                                mAppManager.setLogin_status(true);
//												if(mAppManager.getTokenCode() != null && !mAppManager.getTokenCode().isEmpty()) {
//													app.StartNode();
//												}

                                if (Parser.getIsride_status() == null) {
                                    loader.dismiss();
                                    Intent i = new Intent(LoginActivity.this, CustomerHomeActivity.class);
                                    i.putExtra("suspension", true);
                                    i.putExtra("reason", Parser.getLoginStatus());
                                    startActivity(i);
                                    finish();
                                } else {
                                    if (Parser.getIsride_status().equalsIgnoreCase("true")) {
                                        splashTask(Configure.URL_ride_completeness_customer);
                                    } else {
                                        loader.dismiss();
                                        Intent i = new Intent(LoginActivity.this, CustomerHomeActivity.class);
                                        i.putExtra("suspension", true);
                                        i.putExtra("reason", Parser.getLoginStatus());
                                        startActivity(i);
                                        finish();
                                    }
                                }

                                break;

                        }

                        id_type = "customer_id";
                        //splashTask(Configure.URL_ride_completeness_customer);

                    }


                    break;
                case "FALSE":
                    String message = Parser.getLoginStatus();
                    iDialog(message);
                    break;
            }


        } else {
            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
            System.exit(0);
        }

    }

    @Override
    public void updateDataResponse(String response) {
        hideProgress();

        System.out.println("COMPLETENESS===" + response);

        parseresp = StatusParser.getRideStatus(response, id_type);

        if (!TextUtils.isEmpty(parseresp)) {
            if (parseresp.equals("TRUE")) {
                if (mAppManager.getuser().equals("customer")) {

                    boolean parseresponce = false;
                    try {
                        parseresponce = Parser.setDriverDataSplash(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (parseresponce) {

                        mAppManager.App_setDriver_ride_id(Parser.getDriver_ride_id());
                        mAppManager.App_setDriver_request_id(Parser.getDriver_request_id());
                        mAppManager.App_setDriver_ride_status(Parser.getDriver_ride_status());
                        mAppManager.App_setDriver_driver_id(Parser.getDriver_driver_id());
                        mAppManager.App_setDriver_driver_name(Parser.getDriver_driver_name());
                        mAppManager.App_setDriver_driver_phone(Parser.getDriver_driver_phone());
                        mAppManager.App_setDriver_driver_vehicle(Parser.getDriver_driver_vehicle());
                        mAppManager.App_setDriver_driver_profile_pic(Parser.getDriver_driver_profile_pic());
                        mAppManager.App_setDriver_driver_latitude(Parser.getDriver_driver_latitude());
                        mAppManager.App_setDriver_driver_longitude(Parser.getDriver_driver_longitude());

                        mAppManager.App_setDriver_push_Status_Flag(Parser.getDriver_push_Status_Flag());
                        mAppManager.App_setDriver_rating(Parser.getDriver_rating());

                        mAppManager.App_setDriver_customer_destination_point_lat(Parser.getDriver_customer_destination_point_lat());
                        mAppManager.App_setDriver_customer_destination_point_long(Parser.getDriver_customer_destination_point_long());
                        mAppManager.App_setDriver_customer_destination_point_name(Parser.getDriver_customer_destination_point_name());
                        mAppManager.App_setDriver_customer_pick_up_point_lat(Parser.getDriver_customer_pick_up_point_lat());
                        mAppManager.App_setDriver_customer_pick_up_point_long(Parser.getDriver_customer_pick_up_point_long());
                        mAppManager.App_setDriver_customer_pick_up_point_name(Parser.getDriver_customer_pick_up_point_name());

                        mAppManager.App_setDriver_vehicle_model(Parser.getDriver_vehicle_models());
                        mAppManager.App_setDriver_vehicle_number(Parser.getDriver_vehicle_numbers());
                        mAppManager.App_setDriver_vehicle_type(Parser.getDriver_vehicle_type());
                        mAppManager.App_setDriver_vehicle_image(Parser.getDriver_vehicle_image());

                        mAppManager.App_setDriver_fare_multiplier(Parser.getDriver_fare_multiplier());
                        mAppManager.App_setDriver_car_type(Parser.getDriver_car_type());
                        mAppManager.App_setDriver_map_car_image(Parser.getDriver_map_car_image());
                        mAppManager.App_setDriver_total(Parser.getDriver_total());
                        mAppManager.App_setDriver_trip_distance(Parser.getDriver_trip_distance());
                        mAppManager.App_setDriver_trip_duration(Parser.getDriver_trip_duration());
                        mAppManager.App_setDriver_tip_the_driver(Parser.getDriver_tip_the_driver());
                        mAppManager.App_setDriver_payment_type(Parser.getDriver_payment_type());
                        mAppManager.App_setDriver_total_cost(Parser.getDriver_total_cost());
                        mAppManager.App_setDriver_discount_applied(Parser.getDriver_discount_applied());
                        mAppManager.App_setDriver_discount_total(Parser.getDriver_discount_total());
                        mAppManager.App_setDriver_promo_code(Parser.getDriver_promo_code());
                        mAppManager.App_setDriver_payment_mode(Parser.getDriver_payment_mode());
                        mAppManager.App_setDriver_mobile_no(Parser.getDriver_mobile_no());
                    }

                    switch (StatusParser.getMessage()) {

                        case "Driver is not arrived":
                            mAppManager.App_setDriver_ride_status("2");
                            startActivity(new Intent(LoginActivity.this, TrackRide.class));
                            finish();
                            break;

                        case "Driver arrived at the the pick up location":
                            mAppManager.App_setDriver_ride_status("5");
                            startActivity(new Intent(LoginActivity.this, TrackRide.class));
                            finish();
                            break;

                        case "Journey started":
                            mAppManager.App_setDriver_ride_status("6");
                            startActivity(new Intent(LoginActivity.this, TrackRide.class));
                            finish();
                            break;

                        case "Tip is not given for this ride":
                            Intent toReceipt1 = new Intent(LoginActivity.this, RideReceipt.class);
                            toReceipt1.putExtra("payment_status", 1);
                            startActivity(toReceipt1);
                            finish();
                            break;

                        case "Tip is successfully added for this ride":

                            Intent toReceipt = new Intent(LoginActivity.this, RideReceipt.class);
                            toReceipt.putExtra("payment_status", 1);
                            startActivity(toReceipt);
                            finish();
                            break;


                        case "Journey completed and payment status updated":

                            startActivity(new Intent(LoginActivity.this, CustomerHomeActivity.class));
                            finish();
                            break;

                        case "Journey completed":

                            startActivity(new Intent(LoginActivity.this, CustomerHomeActivity.class));
                            finish();
                            break;

                        case "Incomplete ride rating":

                            startActivity(new Intent(LoginActivity.this, RideReceipt.class));
                            finish();
                            break;
                        case "Driver rejected the request":

                            startActivity(new Intent(LoginActivity.this, CustomerHomeActivity.class));
                            finish();
                            break;

                        case "No response from the driver":

                            mAppManager.savePrevActivity("TRACK", "SPLASHC_NR");
                            startActivity(new Intent(LoginActivity.this, TrackRide.class));
                            finish();

                            break;

                        case "The ride is cancelled by the driver":


                            mAppManager.savePrevActivity("TRACK", "SPLASHC_RC");
                            startActivity(new Intent(LoginActivity.this, CustomerHomeActivity.class));
                            finish();
                            break;
                        case "The ride is cancelled by the customer":


                            startActivity(new Intent(LoginActivity.this, CustomerHomeActivity.class));
                            finish();
                            break;

                        case "Request is expired":

                            startActivity(new Intent(LoginActivity.this, CustomerHomeActivity.class));
                            finish();
                            break;

                        default:
                            break;
                    }


                }
            }

        } else {
            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
            finish();
        }

    }


    @Override
    public void showProgreass() {

        loader = new Dialog(LoginActivity.this);
        loader.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loader.setContentView(R.layout.dialog_progress);
        loader_wheel = (ProgressWheel) loader.findViewById(R.id.progress_wheel);
        loader_wheel.setCircleRadius(50);
        loader.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        loader.setCancelable(false);
        loader.show();

    }

    Dialog login_dialog;

    private void iDialog(String message) {
        // TODO Auto-generated method stub

        login_dialog = new Dialog(LoginActivity.this, R.style.Theme_Dialog);
        login_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = login_dialog.getWindow();
        window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        login_dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        login_dialog.setContentView(R.layout.dialog_no_title);
        login_dialog.setCancelable(false);
        login_dialog.setCanceledOnTouchOutside(false);
        login_dialog.show();

        TextView mDialogBody = (TextView) login_dialog.findViewById(R.id.txt_dialog_body);
        TextView mOK = (TextView) login_dialog.findViewById(R.id.txt_dialog_ok);

        mDialogBody.setText(message);

        mOK.setText("OK");

        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                login_dialog.dismiss();

                if (loader != null && loader.isShowing())
                    loader.dismiss();
            }
        });


    }

    @Override
    public void hideProgress() {
        if (loader != null) {
            loader.dismiss();
        }

    }

    @Override
    public void onLoginFail(Throwable t) {

        retryDialog(1);


    }

    @Override
    public void onUpdateFail(Throwable t) {
        startActivity(new Intent(LoginActivity.this, SplashActivity.class));
        LoginActivity.this.finish();
    }

    @Override
    public void forgotResponseSuccess(String response) {

        hideProgress();
        System.out.println("FORGOTPW===" + response);
        JSONObject dataObject = null;
        try {
            dataObject = new JSONObject(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        parsresp = dataObject.optString("status");
        message = dataObject.optString("message");

        if (!TextUtils.isEmpty(parsresp)) {
            switch (parsresp) {
                case "TRUE":

                    dialog2.dismiss();

                    Dialog(message);


                    break;
                case "FALSE":

                    Toast.makeText(getApplicationContext(), "Please enter a valid email", Toast.LENGTH_SHORT).show();
                    break;
            }


            //loader.dismiss();
        } else {
            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
            System.exit(0);
        }


    }

    @Override
    public void forgotFail(Throwable t) {
        hideProgress();
        retryDialog(2);

    }


    @Override
    public void setPresenter(LoginContract.Presenter presenter) {
        this.presenter = presenter;

    }
}
