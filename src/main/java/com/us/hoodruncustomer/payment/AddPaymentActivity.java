package com.us.hoodruncustomer.payment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.Card;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.braintreepayments.api.interfaces.BraintreeErrorListener;
import com.braintreepayments.api.interfaces.PaymentMethodNonceCreatedListener;
import com.braintreepayments.api.models.CardBuilder;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Token;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.commonwork.Injection;
import com.us.hoodruncustomer.commonwork.SimpleActivity;
import com.us.hoodruncustomer.configure.Configure;
import com.us.hoodruncustomer.exceptionhandler.ExceptionHandler;
import com.us.hoodruncustomer.home.CustomerHomeActivity;
import com.us.hoodruncustomer.model.CardType;
import com.us.hoodruncustomer.parser.ConnectionDetector;
import com.us.hoodruncustomer.parser.Parser;
import com.us.hoodruncustomer.picker.SimpleDatePickerDialog.OnDateSetListener;
import com.us.hoodruncustomer.picker.SimpleDatePickerDialogFragment;
import com.us.hoodruncustomer.registration.Verification;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.security.AccessController.getContext;

public class AddPaymentActivity extends SimpleActivity implements OnDateSetListener, PaymentMethodNonceCreatedListener, BraintreeErrorListener, PaymentContract.View {

    private Toolbar toolbar;
    private TextView headertext;
    Typeface tf;
    String font_path;
    TextView txt_expiry, cardType;
    EditText edt_cardholder_name, edt_card_number, edt_cvv_number;
    String parsresp;
    Button skip;
    String brainparsresp, brainpMessage, braintree_token,stripe_token,payment_type;
    String expiry_date;
    RelativeLayout save_layout;
    protected BraintreeFragment mBraintreeFragment;
    ConnectionDetector cd;
    ArrayList<CardType> mCardtypeList;
    String MONTH;
    int YEAR;
    String card_holder_name, card_number, card_type, cvv_no;
    private String final_Url = null;
    private PaymentContract.Presenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_payment);
        new PaymentPresenter(Injection.remoteDataRepository(getApplicationContext()), this);

        /**
         * Important to catch the exceptions
         */

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        /*****************************************************************************************/

        cd = new ConnectionDetector(AddPaymentActivity.this);

        font_path = "fonts/opensans.ttf";

        tf = Typeface.createFromAsset(this.getAssets(), font_path);

        toolbar = (Toolbar) findViewById(R.id.toolbar6);

        if (toolbar != null) {
            int color = Color.parseColor("#ffffff");
            toolbar.setTitleTextColor(color);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

            headertext = (TextView) findViewById(android.R.id.text1);

            skip = (Button) findViewById(android.R.id.button1);

            if (mAppManager.getPrevActivity("ADD").equals("PAY")) {
                skip.setVisibility(View.GONE);
            } else {
                skip.setVisibility(View.VISIBLE);
                skip.setText("SKIP");
                skip.setTypeface(skip.getTypeface(), Typeface.BOLD);
            }

            headertext.setText("Add Payment Info");

            toolbar.setNavigationIcon(R.drawable.back_arrow);
            toolbar.setNavigationOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (mAppManager.getPrevActivity("ADD").equals("PAY")) {
                        startActivity(new Intent(AddPaymentActivity.this, PaymentActivity.class));
                        finish();
                    } else
                        Toast.makeText(getApplicationContext(), "Add your card details for complete registration.", Toast.LENGTH_LONG).show();

                }
            });
        }


        headertext.setTypeface(tf, Typeface.BOLD);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        setupView();

        edt_card_number.addTextChangedListener(new FourDigitCardFormatWatcher());

        txt_expiry.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                displaySimpleDatePickerDialogFragment();
            }


        });


        save_layout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                card_holder_name = edt_cardholder_name.getText().toString();
                card_number = edt_card_number.getText().toString().trim();
                cvv_no = edt_cvv_number.getText().toString();
                card_type = cardType.getText().toString().trim();
                if (cd.isConnectingToInternet()) {
                    if (checkValid()) {
                        if (mAppManager.getPrevActivity("ADD").equals("PAY")) {

                            if (cd.isConnectingToInternet()) {
                                //  showProgress();
                                GetTokenFromServer(Configure.URL_add_card);
                            } else {
                                Toast.makeText(getApplicationContext(), "No internet connection available.", Toast.LENGTH_LONG).show();
                                System.exit(0);
                            }
                        } else {

                            if (cd.isConnectingToInternet()) {
                                // showProgress();
                                GetTokenFromServer(Configure.URL_card_add_start);
                            } else {
                                Toast.makeText(getApplicationContext(), "No internet connection available.", Toast.LENGTH_LONG).show();
                                System.exit(0);
                            }

                        }
                    }
                } else
                    Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_SHORT).show();
            }
        });


        skip.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                sagePayResponse(Configure.URL_card_add_start, "", true);
            }
        });

    }

    private String getCardType(String cNum) {

        String cardtype = "";
        for (int i = 0; i < mCardtypeList.size(); i++) {
            Pattern pattern = Pattern.compile(mCardtypeList.get(i).getRegex());
            Matcher m = pattern.matcher(cNum);
            if (m.find()) {
                cardtype = mCardtypeList.get(i).getType();
                break;
            }
        }

        return cardtype;

    }

    private void setupView() {
        // TODO Auto-generated method stub

        txt_expiry = (TextView) findViewById(R.id.txt_expiry);
        edt_card_number = (EditText) findViewById(R.id.edt_card_number);
        edt_cardholder_name = (EditText) findViewById(R.id.edt_card_holder);
        edt_cvv_number = (EditText) findViewById(R.id.edt_cvv_number);
        cardType = (TextView) findViewById(R.id.txt_card_type);
        save_layout = (RelativeLayout) findViewById(R.id.lyt_btn);


        mCardtypeList = new ArrayList<CardType>();
        mCardtypeList.add(new CardType("VISA", "^4[0-9]{6,}$"));
        mCardtypeList.add(new CardType("MASTER CARD", "^5[1-5][0-9]{5,}$"));
        mCardtypeList.add(new CardType("AMEX", "^3[47][0-9]{5,}$"));
        mCardtypeList.add(new CardType("DINERS CLUB", "^3(?:0[0-5]|[68][0-9])[0-9]{4,}$"));
        mCardtypeList.add(new CardType("DISCOVER", "^6(?:011|5[0-9]{2})[0-9]{3,}$"));
        mCardtypeList.add(new CardType("JCB", "^(?:2131|1800|35[0-9]{3})[0-9]{3,}$"));
        mAppManager.savePrevActivity("HIS", "");

    }

    @Override
    public void onDateSet(int year, int monthOfYear) {
        // TODO Auto-generated method stub


        String month = "" + (monthOfYear + 1);

        if (monthOfYear + 1 < 10) {

            month = "0" + (monthOfYear + 1);
        }
        expiry_date = getExpiryDate(year, month);
        txt_expiry.setText(getTextView(year, monthOfYear));

        Log.d("DATE LISTENER", expiry_date);
        //Toast.makeText(getApplicationContext(), expiry_date, Toast.LENGTH_SHORT).show();
    }

    private String getTextView(int year, int monthOfYear) {
        // TODO Auto-generated method stub
        String[] months = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};


        return months[monthOfYear] + " " + year;
    }

    private String getExpiryDate(int year, String month) {

        MONTH = month;
        YEAR = year;

        List<Integer> digits = new ArrayList<Integer>();
        while (year > 0) {
            digits.add(year % 10);
            year /= 10;
        }
        String exp_dat = month + digits.get(1) + "" + digits.get(0);

        return exp_dat;


    }

    private void displaySimpleDatePickerDialogFragment() {
        SimpleDatePickerDialogFragment datePickerDialogFragment;
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        datePickerDialogFragment = SimpleDatePickerDialogFragment.getInstance(
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH));
        datePickerDialogFragment.setOnDateSetListener(this);
        datePickerDialogFragment.show(getSupportFragmentManager(), null);
    }


    public boolean checkValid() {
        boolean check = true;


        if (edt_cardholder_name.getText().length() == 0) {
            Toast.makeText(getApplicationContext(), "Please enter card holder's name.", Toast.LENGTH_SHORT).show();
            check = false;
        } else if (edt_card_number.getText().length() == 0) {
            Toast.makeText(getApplicationContext(), "Please enter card number.", Toast.LENGTH_SHORT).show();
            check = false;
        } else if (edt_card_number.getText().length() < 13) {
            Toast.makeText(getApplicationContext(), "Invalid card number.", Toast.LENGTH_SHORT).show();
            check = false;
        } else if (edt_cvv_number.getText().length() == 0) {
            Toast.makeText(getApplicationContext(), "Please enter CV2 number.", Toast.LENGTH_SHORT).show();
            check = false;
        } else if (cardType.getText().equals("")) {
            Toast.makeText(getApplicationContext(), "Invalid card type.", Toast.LENGTH_SHORT).show();
            check = false;
        } else if (txt_expiry.getText().equals("SELECT >")) {
            Toast.makeText(getApplicationContext(), "Please select an expiry date.", Toast.LENGTH_SHORT).show();
            check = false;
        }


        return check;

    }


    @Override
    public void onBackPressed() {

        if (mAppManager.getPrevActivity("ADD").equals("PAY")) {
            startActivity(new Intent(AddPaymentActivity.this, PaymentActivity.class));
            finish();
        } else
            Toast.makeText(getApplicationContext(), "Add your card details for complete registration.", Toast.LENGTH_LONG).show();
//	     		Toast.makeText(getApplicationContext(), "Add your card details for complete registration", Toast.LENGTH_LONG).show();

        //  moveTaskToBack(true);
    }

    @Override
    public void onError(Exception error) {

        Log.d("BraintreeError", "" + error.toString());
        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
        hideProgress();
    }

    @Override
    public void onPaymentMethodNonceCreated(PaymentMethodNonce paymentMethodNonce) {

        if (paymentMethodNonce.getNonce() != null) {
            // showProgress();
            sagePayResponse(final_Url, paymentMethodNonce.getNonce(), false);
        } else {
            //  hideProgress();
            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();

        }

    }

    @Override
    public void hideProgress() {

        if (loader != null) {
            loader.dismiss();
        }

    }

    @Override
    public void onFail(Throwable t) {

    }


    @Override
    public void getSaveCards(String response) {

    }

    @Override
    public void showProgreass() {
        loader = new Dialog(AddPaymentActivity.this);
        loader.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loader.setContentView(R.layout.dialog_progress);
        loader_wheel = (ProgressWheel) loader.findViewById(R.id.progress_wheel);
        loader_wheel.setCircleRadius(50);
        loader.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        loader.setCancelable(false);
        loader.show();

    }


    @Override
    public void getTokenResponse(String response) {
        if (loader != null) {
            loader.dismiss();
        }

        System.out.println("BrainTree_Token===" + response);
        JSONObject dataObject = null;
        JSONObject data_response = null;
        try {
            dataObject = new JSONObject(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        brainparsresp = dataObject.optString("status").toString();
        brainpMessage = dataObject.optString("message").toString();

        data_response = dataObject.optJSONObject("data");


        if (!TextUtils.isEmpty(brainparsresp)) {

            if (brainparsresp.equals("TRUE")) {


                payment_type = data_response.optString("payment_gateway").toString();

                if(payment_type.equalsIgnoreCase("stripe")){

                    stripe_token = data_response.optString("client_token").toString();

                    if (stripe_token != null) {

                        SetUpStripetoken(url);

                    } else {

                        Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();

                    }

                }else{

                    braintree_token = data_response.optString("client_token").toString();


                    if (braintree_token != null) {

                        SetUpNonce(url);

                    } else {

                        Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();

                    }
                }


            } else {

                Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();

            }

        } else {

            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
            System.exit(0);
        }


    }

    @Override
    public void onTokenFail(Throwable t) {

        System.out.println("volley time out");
        hideProgress();
        retryDialog(1);

    }

    @Override
    public void onPaymentResponse(String response) {
        hideProgress();

        System.out.println("ADDCARD===" + response);
        parsresp = Parser.addCard(response);

        if (!TextUtils.isEmpty(parsresp)) {

            if (parsresp.equals("TRUE")) {
                if (mAppManager.getPrevActivity("ADD").equals("PAY")) {
                    startActivity(new Intent(AddPaymentActivity.this, CustomerHomeActivity.class));
                    finish();
                } else {

                    mAppManager.saveCustomerID(Parser.customer_id);
                    mAppManager.savePrevActivity("VERIFY", "ADD");
                    mAppManager.saveProfileStatus(Parser.getProfileStatus());
                    mAppManager.saveOtpDestiantion("mobile");

                    startActivity(new Intent(AddPaymentActivity.this, Verification.class));

                    finish();
                }
            } else
                Toast.makeText(getApplicationContext(), Parser.getAdd_card_status_message(), Toast.LENGTH_SHORT).show();


        } else {
            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
            System.exit(0);
        }


    }


    @Override
    public void setPresenter(PaymentContract.Presenter presenter) {
        this.presenter = presenter;
    }


	 /*  Custom TextWatcher*/

    public class FourDigitCardFormatWatcher implements TextWatcher {

        // Change this to what you want... ' ', '-' etc..
        private static final char space = ' ';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            cardType.setText(getCardType(s.toString().trim().replace(" ", "")));

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            // Remove spacing char

            if (s.length() > 0 && (s.length() % 5) == 0) {
                final char c = s.charAt(s.length() - 1);
                if (space == c) {
                    s.delete(s.length() - 1, s.length());
                }

            }
            // Insert char where needed.
            if (s.length() > 0 && (s.length() % 5) == 0) {
                char c = s.charAt(s.length() - 1);
                // Only if its a digit where there should be a space we insert a space
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                    s.insert(s.length() - 1, String.valueOf(space));
                }
            }
        }
    }

    private void sagePayResponse(String url, final String nonce, final boolean skipped) {

        Map<String, String> params = new HashMap<String, String>();

        if (skipped) {
            params.put("skip", "true");
            params.put("card_holder", "");
            params.put("card_number", "");
            params.put("cv2", "");
            params.put("card_type", "");
            params.put("expire_date", "");
            params.put("customer_id", mAppManager.getCustomerID());
            params.put("nonce", "");
            params.put("token_code", "");
        } else {
            params.put("card_holder", card_holder_name);
            params.put("card_number", card_number.replace(" ", ""));
            params.put("cv2", cvv_no);
            params.put("card_type", card_type);
            params.put("expire_date", expiry_date);
            params.put("customer_id", mAppManager.getCustomerID());
            params.put("nonce", nonce);
            params.put("token_code", mAppManager.getTokenCode());
            params.put("token_code", mAppManager.getTokenCode());
        }

        presenter.savePaymentData(params, url, skipped, this);

        /*StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        hideProgress();

                        System.out.println("ADDCARD===" + response);
                        parsresp = Parser.addCard(response);

                        if (!TextUtils.isEmpty(parsresp)) {

                            if (parsresp.equals("TRUE")) {
                                if (mAppManager.getPrevActivity("ADD").equals("PAY")) {
                                    startActivity(new Intent(AddPaymentActivity.this, CustomerHomeActivity.class));
                                    finish();
                                } else {

                                    mAppManager.saveCustomerID(Parser.customer_id);
                                    mAppManager.savePrevActivity("VERIFY", "ADD");
                                    mAppManager.saveProfileStatus(Parser.getProfileStatus());
                                    mAppManager.saveOtpDestiantion("mobile");

                                    startActivity(new Intent(AddPaymentActivity.this, Verification.class));

                                    finish();
                                }
                            } else
                                Toast.makeText(getApplicationContext(), Parser.getAdd_card_status_message(), Toast.LENGTH_SHORT).show();


                        } else {
                            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
                            System.exit(0);
                        }


                    }
                },
                new Response.ErrorListener() {


                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        retryDialog(1);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {


                return params;
            }

//			@Override
//			public Map<String, String> getHeaders() throws AuthFailureError {
//				Map<String, String> mHeaders = new ArrayMap<String, String>();
//				mHeaders.put("Version","1");
//				return mHeaders;
//			}
        };


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.activityPaused();
    }


    Dialog retry;

    private void retryDialog(final int i) {

        retry = new Dialog(AddPaymentActivity.this, R.style.Theme_Dialog);
        retry.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = retry.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        retry.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        retry.setContentView(R.layout.thaks_dialog_twobutton);
        retry.setCancelable(false);
        retry.setCanceledOnTouchOutside(false);
        retry.show();

        TextView mTitle = (TextView) retry.findViewById(R.id.txt_title);
        mTitle.setText("Error!");
        mTitle.setTypeface(tf, Typeface.BOLD);
        TextView mDialogBody = (TextView) retry.findViewById(R.id.txt_dialog_body);
        mDialogBody.setText("Internet connection is slow or disconnected. Try connecting again.");
        LinearLayout mOK = (LinearLayout) retry.findViewById(R.id.lin_Ok);
        LinearLayout mCancel = (LinearLayout) retry.findViewById(R.id.lin_cancel);
        TextView mRetry = (TextView) retry.findViewById(R.id.txt_dialog_ok);
        TextView mExit = (TextView) retry.findViewById(R.id.txt_dialog_cancel);
        mRetry.setText("RETRY");
        mExit.setText("EXIT");

        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


                retry.dismiss();

                switch (i) {
                    case 1:
                        if (mAppManager.getPrevActivity("ADD").equals("PAY")) {

                            if (cd.isConnectingToInternet()) {

                                GetTokenFromServer(Configure.URL_add_card);

                            } else {
                                Toast.makeText(getApplicationContext(), "No internet connection available.", Toast.LENGTH_LONG).show();
                                System.exit(0);
                            }
                        } else {

                            if (cd.isConnectingToInternet()) {

                                GetTokenFromServer(Configure.URL_card_add_start);

                            } else {
                                Toast.makeText(getApplicationContext(), "No internet connection available.", Toast.LENGTH_LONG).show();
                                System.exit(0);
                            }

                        }
                        break;

                    default:
                        break;

                }


            }
        });

        mCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                retry.dismiss();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });

    }

    private String url = "";

    private void GetTokenFromServer(final String Url) {
        url = Url;

        presenter.getToken(mAppManager.getCustomerID(), "customer", this);


    }

    public void SetUpNonce(String url) {
        try {

            final_Url = url;

            mBraintreeFragment = BraintreeFragment.newInstance(this, braintree_token);

            /**
             * Split the date
             */

            CardBuilder cardBuilder = new CardBuilder()
                    .cardNumber(card_number.replace(" ", ""))
                    .expirationMonth(MONTH)
                    .expirationYear(Integer.toString(YEAR))
                    .cvv(cvv_no);

            Card.tokenize(mBraintreeFragment, cardBuilder);

            // mBraintreeFragment is ready to use!
        } catch (InvalidArgumentException e) {
            // There was an issue with your authorization string.
            hideProgress();
            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();

        }


    }

    public void SetUpStripetoken(String url) {

        if(stripe_token != null) {
                showProgreass();
                final_Url = url;

                com.stripe.android.model.Card card = new com.stripe.android.model.Card(card_number.replace(" ", ""), Integer.parseInt(MONTH), YEAR, cvv_no);

                // Remember to validate the card object before you use it to save time.

                if (card.validateCard()) {
                    Stripe stripe = new Stripe(mContext, stripe_token);
                    stripe.createToken(
                            card,
                            new TokenCallback() {
                                public void onSuccess(Token token) {
                                    hideProgress();
                                    if (token != null) {
                                        // showProgress();
                                        sagePayResponse(final_Url,token.getId(), false);
                                    } else {
                                          hideProgress();
                                        Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();

                                    }
                                }

                                public void onError(Exception error) {
                                    // Show localized error message
                                    Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
                                    hideProgress();
                                }
                            }
                    );
                } else {

                }

        }else{
            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();

        }


    }



}