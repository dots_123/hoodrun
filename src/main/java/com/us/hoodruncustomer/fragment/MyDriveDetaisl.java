package com.us.hoodruncustomer.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.us.hoodruncustomer.R;

import com.us.hoodruncustomer.authentication.LoginActivity;
import com.us.hoodruncustomer.db.AppManager;
import com.us.hoodruncustomer.parser.ConnectionDetector;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class MyDriveDetaisl extends Dialog implements
View.OnClickListener {

	public Activity c;
	public Dialog d;
	public RelativeLayout yes, no;
	  private AppManager mAppManager;

Typeface tf;

	  String parserResp="";
	  String url;
	  String user;
	  String message,font_path;


	public MyDriveDetaisl(Activity a, String url, String user) {
		super(a);
		// TODO Auto-generated constructor stub
		this.c = a;
		this.url=url;
		this.user=user;

		mAppManager=AppManager.getInstance(a);
	}



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawable(new ColorDrawable(0));
		setContentView(R.layout.fragment_customer_details);
		setCancelable(false);
		setCanceledOnTouchOutside(false);

		font_path = "fonts/opensans.ttf";

	    tf = Typeface.createFromAsset(c.getAssets(), font_path);
		yes = (RelativeLayout) findViewById(R.id.btn_yes);
		no = (RelativeLayout) findViewById(R.id.btn_no);
		yes.setOnClickListener(this);
		no.setOnClickListener(this);

	}




	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_yes:



			webService();




			break;
		case R.id.btn_no:

			dismiss();

			break;



		default:
			break;


		}
		dismiss();
	}




	public void webService()
	{


		ConnectionDetector cd = new ConnectionDetector(c);
		if (cd.isConnectingToInternet()) {

			String mLogoutURL=url;

			System.out.println("logoutUrl" + mLogoutURL);
			showProgress();
			attemptingToLogout(mLogoutURL);

		}else{

			alertMessage("No Network Connectivity","Network connection needed...");
		}

	}



    Dialog loader;
	ProgressWheel loader_wheel;
    private void showProgress() {

    	loader = new Dialog (c);
    	loader.requestWindowFeature (Window.FEATURE_NO_TITLE);
    	loader.setContentView (R.layout.dialog_progress);
    	loader_wheel=(ProgressWheel)loader.findViewById(R.id.progress_wheel);
    	loader_wheel.setCircleRadius(50);
    	loader.getWindow ().setBackgroundDrawableResource (android.R.color.transparent);
    	loader.setCancelable(false);
    	loader.show();
	}



	private void alertMessage(String title,String message) {


		AlertDialog.Builder builder = new AlertDialog.Builder(c);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setPositiveButton("Ok", new OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) { 
	        	dialog.dismiss();
	        }
	     });
	    
		AlertDialog alert = builder.create();
		alert.show();
}
	
	
	
	public void clearANDTerminate()
	{
		
	
		mAppManager.saveUser("");
		mAppManager.setLogin_status(false);
		
		c.finish();
	
		c.startActivity(new Intent(c,LoginActivity.class));
	}
	
	

	private void attemptingToLogout(String url){

		StringRequest stringRequest = new StringRequest(Request.Method.POST,url,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {

						System.out.println("LOGOUT==="+response);
						JSONObject mObject= null;
						try {
							mObject = new JSONObject(response);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						parserResp=mObject.optString("status");
						message=mObject.optString("message");

						if(TextUtils.isEmpty(parserResp))
						{

							alertMessage("No Network Connectivity","Invalid Response");

						}

						else if(parserResp.equals("TRUE")){

							clearANDTerminate();

						}


						else

						{

							alertMessage("Couldn't Log out",message);
							dismiss();
						}

						loader.dismiss();



					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {

						Toast.makeText(c,error.toString(),Toast.LENGTH_LONG).show();
						c.finish();
					}


				})
		{
			@Override
			protected Map<String,String> getParams(){

				Map<String,String> params = new HashMap<String, String>();
				if(user.equals("driver"))
				params.put("driver_id",mAppManager.getDriverID_Number());
				else

					params.put("customer_id",mAppManager.getCustomerID());;

				return params;
			}

		};

		RequestQueue requestQueue = Volley.newRequestQueue(c);
		int socketTimeout = 60000;//60 seconds - change to what you want
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		stringRequest.setRetryPolicy(policy);
		requestQueue.add(stringRequest);
	}

}

