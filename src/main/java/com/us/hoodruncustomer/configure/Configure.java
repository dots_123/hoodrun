package com.us.hoodruncustomer.configure;

public class Configure {

    //public static String base_URL="https://hoponn.co.uk/";
    //public static String base_URL="http://demo.hoponn.co.uk/";
    //public static String base_URL="http://ds412.projectstatus.co.uk/opon/";
    //public static String base_URL="http://beta.hoponn.co.uk/";
    // public static String base_URL = "http://ds412.projectstatus.co.uk/svnhoodrun/";
    public static String base_URL = "https://www.hoodrunnow.com/";
//    public static String base_URL = "http://192.168.4.125/hudrun/";
//    public static String base_URL = "http://192.168.4.139/hoodrun/";
    // public static String base_URL = "http://192.168.4.137/hoodrun/";
    //public static String base_URL="http://ds412.projectstatus.co.uk/local_hopon/";
    //public static String base_URL="http://192.168.4.139/hopon/";

    public static String URL_registration = base_URL + "api/customers/registration";
    public static String URL_login = base_URL + "api/customers/main_login";

    public static String URL_fb_login = base_URL + "api/Social/login";
    public static String URL_google_login = base_URL + "api/Social/login";

    public static String URL_otp_verification = base_URL + "api/customers/otp_verification";
    public static String URL_email_otp_verification = base_URL + "api/customers/otp_verification_email";
    public static String URL_driver = base_URL + "api/customers/request_driver";
    public static String URL_share_number_verification = base_URL + "api/customers/mobile_number_verfication";
    public static String URL_share_number = base_URL + "api/customers/send_push_shared_customer";
    public static String URL_share_the_fare = base_URL + "api/customers/share_the_fare";
    public static String URL_diver_accept = base_URL + "api/drivers/driver/driver_customer_waiting";
    public static String URL_view_profile_customer = base_URL + "api/customers/get_profile_details";
    public static String URL_driver_response = base_URL + "api/drivers/driver/driver_response_to_drive";
    public static String URL_ride_status = base_URL + "api/customers/ride_status";
    public static String URL_drver_location_update = base_URL + "api/drivers/driver/driver_location_update";
    public static String URL_user_check_drver_location = base_URL + "api/customers/search_driver_location";
    public static String URL_driver_update_ride_status = base_URL + "api/drivers/driver/driver_ride_status";
    public static String URL_driver_update_ride = base_URL + "api/drivers/driver/ride_location_update";
    public static String URL_user_check_end_ride = base_URL + "api/customers/search_end_ride";
    public static String URL_driver_end_ride = base_URL + "api/drivers/driver/end_journey";
    public static String URL_driver_redeeem_wallet = base_URL + "api/drivers/driver/wallet_request_to_admin";
    public static String URL_ride_rating = base_URL + "api/customers/ride_rating";
    public static String URL_wallet_balance = base_URL + "api/drivers/driver/wallet";
    public static String URL_payment_recieved = base_URL + "api/drivers/driver/paymentrecived";
    public static String URL_view_profile_driver = base_URL + "api/drivers/driver/get_profile_details";
    public static String URL_change_password_customer = base_URL + "api/customers/change_password";
    public static String URL_update_profile_customer = base_URL + "api/customers/update_profile";
    public static String URL_resend_otp = base_URL + "api/customers/resend_otp";
    public static String URL_resend_otp_email = base_URL + "api/customers/resend_email_otp";
    public static String URL_forgot_password = base_URL + "api/customers/forgot_password";
    public static String URL_customer_feedback = base_URL + "api/drivers/driver/search_customer_feedback";
    public static String URL_customer_logout = base_URL + "api/customers/logout";
    public static String URL_driver_logout = base_URL + "api/drivers/driver/logout";
    public static String URL_promocode = base_URL + "api/customers/promo_code_validation";
    public static String URL_promocode_cancel = base_URL + "api/customers/cancel_promotion";
    public static String URL_share_response = base_URL + "api/customers/share_information_to_participants";
    public static String URL_driver_cancel_ride = base_URL + "api/drivers/driver/driver_cancel_ride";
    public static String URL_customer_cancel = base_URL + "api/customers/customer_cancel";
    public static String URL_ride_completeness_customer = base_URL + "api/customers/ride_completeness";
    public static String URL_ride_completeness_driver = base_URL + "api/drivers/driver/ride_completeness";
    public static String URL_add_card = base_URL + "api/customers/add_card_details";
    public static String URL_sage_pay = base_URL + "sagepay_payment";
    public static String URL_saved_cards = base_URL + "api/customers/customer_card_list";
    public static String URL_delete_cards = base_URL + "api/customers/delete_card";
    public static String URL_default_card = base_URL + "api/customers/set_default_card";
    public static String URL_ride_history_customer = base_URL + "api/customers/ride_history";
    public static String URL_ride_analytics_customer = base_URL + "api/drivers/driver/earning_analytics";
    public static String URL_ride_history_driver = base_URL + "api/drivers/driver/ride_history";
    public static String URL_card_add_start = base_URL + "api/customers/account_details";
    public static String URL_terms_and_conditions = base_URL + "api/customers/terms_and_conditions";
    public static String URL_privacy_policy = base_URL + "privacy-policy?source=mobile";
    public static String URL_help = base_URL + "api/customers/help";
    public static String URL_driver_changepw = base_URL + "api/drivers/driver/change_password";
    public static String URL_check_request_status = base_URL + "api/drivers/driver/check_request_status";
    public static String URL_customer_cancellation_charge = base_URL + "api/customers/customer_cancellation_charge_alert";
    public static String URL_wallet_transaction_history = base_URL + "api/drivers/driver/earningInfo";
    public static String URL_wallet_Info = base_URL + "api/drivers/driver/walletInfo";
    public static String URL_driver_analytics = base_URL + "api/drivers/driver/driver_analytics";
    public static String URL_refer_cities = base_URL + "api/refers/getCities";
    public static String URL_refer_friends = base_URL + "api/refers/insert_referred_driver";
    public static String URL_refer_friends_list = base_URL + "api/refers/getReferredFriendCount";
    public static String URL_cities = base_URL + "api/common/get_cars";
    public static String URL_submit_complaint = base_URL + "api/complaints/submit_complaint";
    public static String URL_current_driver_offers = base_URL + "api/drivers/driver/currentDriverOffers";
    public static String URL_customer_offers = base_URL + "api/customers/customer_free_rides";
    public static String URL_current_driver_pending = base_URL + "api/refers/clearPendingAmount";
    public static String URL_invite_friends = base_URL + "api/invite/info";
    public static String URL_request_info = base_URL + "api/customers/request_information";
    public static String URL_request_heat_map = base_URL + "api/customers/heat_map";
    public static String URL_request_change_dropup = base_URL + "api/customers/update_dropoff_location";
    public static String URL_request_update_request = base_URL + "api/customers/update_drive_available_status";
    public static String URL_request_car_fares = base_URL + "api/common/getDriverCars";

    /**
     * Braintree Token From Server
     */

    public static String URL_braintree_token = base_URL + "api/customers/genrateBrainToken";

}
