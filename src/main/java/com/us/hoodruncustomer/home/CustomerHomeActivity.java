package com.us.hoodruncustomer.home;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;
import android.os.StrictMode;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.bumptech.glide.Glide;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.us.hoodruncustomer.BuildConfig;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.Constants;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.app.locationUtils;
import com.us.hoodruncustomer.commonwork.BaseActivity;
import com.us.hoodruncustomer.commonwork.CustomPlaces;
import com.us.hoodruncustomer.commonwork.Injection;
import com.us.hoodruncustomer.trackride.RideReceipt;
import com.us.hoodruncustomer.share.RideSharingActivity;
import com.us.hoodruncustomer.payment.RiderPaymentActivity;
import com.us.hoodruncustomer.splash.SplashActivity;
import com.us.hoodruncustomer.configure.SphericalUtil;
import com.us.hoodruncustomer.exceptionhandler.ExceptionHandler;
import com.us.hoodruncustomer.fcm.MyFirebaseMessagingService;
import com.us.hoodruncustomer.model.Cars;
import com.us.hoodruncustomer.model.Drivers;
import com.us.hoodruncustomer.nodeservice.ConnectorService;
import com.us.hoodruncustomer.nodeservice.NodeUtil;
import com.us.hoodruncustomer.parser.ConnectionDetector;
import com.us.hoodruncustomer.parser.DriverParser;
import com.us.hoodruncustomer.parser.StatusParser;
import com.us.hoodruncustomer.rider.adapters.TaxiListing;
import com.us.hoodruncustomer.rider.beans.DriverMark;
import com.us.hoodruncustomer.rider.beans.LocationData;
import com.us.hoodruncustomer.rider.events.AvailCars;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import io.socket.client.Socket;

public class CustomerHomeActivity extends BaseActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener,
        GoogleMap.OnCameraMoveStartedListener,
        GoogleMap.OnCameraMoveListener,
        GoogleMap.OnCameraMoveCanceledListener,
        GoogleMap.OnCameraIdleListener, TaxiListing.IupdateCars,
        CustomerContract.View {

    public static final String RECEIVER_SUSPEND_CUSTOMER = "com.uk.hoponn.receiver_customer";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final int REQUEST_CODE_DROP = 1;
    private static final int REQUEST_CODE_PICK = 2;
    public final static HashMap<String, Integer> timings = new HashMap<>();


    private FusedLocationProviderClient mFusedLocationClient;
    double tlat, tlng;
    /***************
     * Ui For Switcher
     ************/

    @BindView(R.id.locatorLabel)
    TextView locatorLabel;
    @BindView(R.id.primary_container)
    LinearLayout primaryContainer;
    @BindView(R.id.secondary_container)
    LinearLayout secondaryContainer;
    @BindView(R.id.primary_icon)
    ImageView primaryIcon;
    @BindView(R.id.primary_locator)
    TextView primaryLocator;
    @BindView(R.id.secondary_icon)
    ImageView secondaryIcon;
    @BindView(R.id.imageView9)
    ImageView imageView9;
    @BindView(R.id.bottom_view)
    LinearLayout bottomView;
    @BindView(R.id.fares_layout)
    LinearLayout faresLayout;
    @BindView(R.id.map_container)
    RelativeLayout mapContainer;
    @BindView(R.id.imageView10)
    ImageView imageView10;
    @BindView(R.id.imageMarker)
    ImageView imageMarker;
    @BindView(R.id.base_fare)
    TextView baseFare;
    @BindView(R.id.seperatorline)
    View seperatorLine;
    @BindView(R.id.min_fare)
    TextView minFare;
    @BindView(R.id.per_mile_fare)
    TextView perMileFare;
    @BindView(R.id.per_min_fare)
    TextView perMinFare;
    @BindView(R.id.secondary_locator)
    TextView secondaryLocator;
    @BindView(R.id.available_taxis)
    RecyclerView availableTaxisList;
    @BindView(R.id.surge_charge)
    LinearLayout surgeCharge;
    @BindView(R.id.surge)
    Button surgeMessage;
    @BindView(R.id.ride_now_btn)
    Button rideNowBtn;
    @BindView(R.id.infomessage)
    Button infoMessage;
    @BindView(R.id.shimmer_view_container)
    ShimmerFrameLayout shimmerViewcontainer;
    @BindView(R.id.cabs_switcher)
    ViewSwitcher cabSwitcher;
    @BindView(R.id.cabs_message)
    TextView cabsMessage;
    @BindView(R.id.marker_text)
    TextView markerText;
    View mapView;

    /**
     * Start Activity For getting location After Clicking Drop Location
     */
    boolean byclicked = true;
    boolean locationchnagedStatus = true;
    /**
     * Calling API : api/common/get_cars to get the cars corrosponding to that
     * current location.
     */

    boolean killMe = false;
    Handler CarsHandler;
    /**
     * Dialog
     */


    Dialog rate;
    /**
     * Dialog Suspension
     */
    private CustomerContract.Presenter presenter;
    Dialog dialogsuspend;
    Dialog push;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private int CurrentNode = 1;// PickUp Location
    private AddressResultReceiver mResultReceiver;
    private LatLng mCenterLatLong;
    private LinearLayoutManager linearLayoutManager;
    private boolean InitialPick = false;
    private TextView headertext;
    private Toolbar toolbar;
    private boolean mMapIsTouched;
    private Animation slide_down, slide_up;
    private TaxiListing taxiListingAdapter;
    public static double userLatitude = 51.5013673;
    public static double userLongitude = -0.1440787;
    private ArrayList<Drivers> drivers;
    private ArrayList<Cars> getCarList;
    private CountDownTimer countDownTimer;
    private Socket mSocket;
    private MyApplication app;
    private Handler MapHandler;
    private boolean byClicked;
    private RecyclerView.LayoutParams params;
    private String myCity = "";
    private Bitmap carImage;
    private int deviceWidth;
    private Resources resources;
    private float scale;
    private boolean OnActivityStatus;
    private boolean onetimeload;
    ConnectionDetector cd;
    private String parsresp;

    Runnable updateCarsHandler = new Runnable() {
        @Override
        public void run() {
            if (!killMe) {

                Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

                if (cd.isConnectingToInternet()) {
                    presenter.getCarsByLocation("" + userLatitude, "" + userLongitude, mAppManager.getTokenCode(), myCity, "true", mAppManager.getCustomerID(), CustomerHomeActivity.this);


                    //  availcarsPresenter.getCarsbyLocation(userLatitude, userLongitude, myCity);
                }

            }

        }
    };

    /* /////////////////////////// !BROADCAST SECTION /////////////////////////////////// */
    /**
     * Initialize LocationData Model to store PickUp & Drop Location.
     */
    private LocationData dataWorker;
    private boolean byAutoLocation;
    /**
     * Car types with their lat lon for real time car updates
     */

    private BroadcastReceiver refreshAfterMarkedNa = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {


            String message = in.getStringExtra("push_message");

            int suspend_type = in.getIntExtra("suspend_type", 0);

            DialogSuspend(message, suspend_type);

        }

    };


    private BroadcastReceiver Node_CUSTOMER_Location_Update = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {

            String request = in.getStringExtra("Object");

            JSONArray data = null;

            Log.d("Driver-Location-Single", request.toString());

            try {
                data = new JSONArray(request);
                if (data != null) {


                    for (int j = 0; j < data.length(); j++) {

                        JSONObject object = (JSONObject) data.get(j);

                        if (availCars.getStringHashMapHashMap().containsKey(object.getString("car_type"))) {

                            HashMap<String, Drivers> map = availCars.getStringHashMapHashMap().get(object.getString("car_type"));

                            if (map.containsKey(object.getString("driver_id"))) {

                                Drivers drivers = new Drivers();
                                drivers.setCar_type(object.getString("car_type"));
                                drivers.setDriver_id(object.getString("driver_id"));
                                drivers.setLat(object.getString("lat"));
                                drivers.setLng(object.getString("lng"));
                                drivers.setSurge_applied(object.optBoolean(

                                        "surge_applied"));
                                drivers.setSurge_message(object.optString("surge_message"));
                                drivers.setFare_multiplier(object.optString("fare_multiplier"));

                                map.put(drivers.getDriver_id(), drivers);

                                availCars.getStringHashMapHashMap().put(object.getString("car_type"), map);

                            } else {
                                Drivers drivers = new Drivers();
                                drivers.setCar_type(object.getString("car_type"));
                                drivers.setDriver_id(object.getString("driver_id"));
                                drivers.setLat(object.getString("lat"));
                                drivers.setLng(object.getString("lng"));
                                drivers.setSurge_applied(object.optBoolean("surge_applied"));
                                drivers.setSurge_message(object.optString("surge_message"));
                                drivers.setFare_multiplier(object.optString("fare_multiplier"));
                                map.put(drivers.getDriver_id(), drivers);

                                availCars.getStringHashMapHashMap().put(object.getString("car_type"), map);
                            }


                            // Time Updation

                            Location locationA = new Location("pickup");
                            locationA.setLatitude(userLatitude);
                            locationA.setLongitude(userLongitude);

                            Location locationb = new Location("drop");
                            locationb.setLatitude(object.optDouble("lat"));
                            locationb.setLongitude(object.optDouble("lng"));

                            int time = locationUtils.getTimebetweenLocation(locationA, locationb);

                            if (CustomerHomeActivity.timings.containsKey(object.getString("car_type"))) {
                                int currentTime = CustomerHomeActivity.timings.get(object.getString("car_type"));
                                if (currentTime != -1 && currentTime > time) {
                                    CustomerHomeActivity.timings.put(object.getString("car_type"), time);
                                } else {
                                    CustomerHomeActivity.timings.put(object.getString("car_type"), time);
                                }
                            } else {
                                CustomerHomeActivity.timings.put(object.getString("car_type"), time);
                            }


                        }
                    }

                    updateDrivers();
                    refreshSurge();

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    };


    public void updateDrivers() {
        try {

            if (timings.containsKey(specificCar.getId())) {
                if (timings.get(specificCar.getId()) != -1) {
                    if (CurrentNode == 1) {
                        markerText.setText(timings.get(specificCar.getId()) + " min");
                    }
                } else {
                    if (CurrentNode == 1) {
                        markerText.setText("no cars");
                    }
                }
            }


            HashMap<String, Drivers> stringDriversHashMap = availCars.getStringHashMapHashMap().get(specificCar.getId());

            ArrayList<Drivers> drivers = new ArrayList<>();

            drivers.addAll(stringDriversHashMap.values());

            plotCars(specificCar, drivers);


        } catch (Exception e) {

        }
    }


    private BroadcastReceiver Node_CUSTOMER_Location_Out_Of_Range = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {

            String request = in.getStringExtra("Object");

            JSONArray data = null;


            Log.d("Driver-Out-of-range", request.toString());

            try {
                data = new JSONArray(request);
                if (data != null) {

                    for (int j = 0; j < data.length(); j++) {

                        JSONObject object = (JSONObject) data.get(j);

                        String driverId = object.optString("driver_id");

                        String cartype = object.optString("car_type");

                        HashMap<String, Drivers> map = availCars.getStringHashMapHashMap().get(cartype);

                        if (map != null && map.containsKey(driverId)) {

                            availCars.getStringHashMapHashMap().get(cartype).remove(driverId);


                            if (availCars.getStringHashMapHashMap().get(cartype).size() > 0) {

                                CustomerHomeActivity.timings.put(cartype, -1);

                                if (availCars.getStringHashMapHashMap().get(cartype).size() > 0) {

                                    for (Map.Entry<String, Drivers> entry : availCars.getStringHashMapHashMap().get(cartype).entrySet()) {

                                        Drivers driver = (Drivers) entry.getValue();

                                        Location locationA = new Location("pickup");
                                        locationA.setLatitude(userLatitude);
                                        locationA.setLongitude(userLongitude);
                                        Location locationb = new Location("drop");
                                        locationb.setLatitude(Double.parseDouble(driver.getLat()));
                                        locationb.setLongitude(Double.parseDouble(driver.getLng()));


                                        int time = locationUtils.getTimebetweenLocation(locationA, locationb);

                                        if (CustomerHomeActivity.timings.containsKey(cartype)) {
                                            int currentTime = CustomerHomeActivity.timings.get(cartype);
                                            if (currentTime != -1 && currentTime > time) {
                                                CustomerHomeActivity.timings.put(cartype, time);
                                            } else {
                                                CustomerHomeActivity.timings.put(cartype, currentTime);
                                            }
                                        } else {
                                            CustomerHomeActivity.timings.put(cartype, time);
                                        }

                                    }
                                } else {
                                    CustomerHomeActivity.timings.put(cartype, -1);
                                }
                            } else {
                                CustomerHomeActivity.timings.put(cartype, -1);
                            }
                            updateDrivers();
                            refreshSurge();
                        }

                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    };

    /*/////////////////////////// !INTERFACE DYNAMIC CALLBACKS /////////////////////////////////// */


    private BroadcastReceiver Node_CUSTOMER_GET_CARS = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {

            String request = in.getStringExtra("Object");

            Log.d("Nodegetcars", "" + request.toString());
            AvailCars availCars = new AvailCars();

            try {

                if (request != null) {

                    availCars.getCars(request);
                    if (availCars.getCarList() != null && availCars.getCarList().size() > 0) {
                        taxiListingAdapter.updateTaxisListing(availCars.getCarList(), userLatitude, userLongitude);
                    } else {
                        if (CurrentNode == 1) {
                            markerText.setText("...");
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    };


    private Cars specificCar;
    /***************************************
     * !PlotCars
     *****************************************************/


    private HashMap<String, DriverMark> allMarkers = new HashMap<>();
    private String selectedCartype = "";
    private boolean cardsLoaded;

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    public static List<Address> getStringFromLocation(double lat, double lng)
            throws ClientProtocolException, IOException, JSONException {

        String address = String
                .format(Locale.ENGLISH, "http://maps.googleapis.com/maps/api/geocode/json?latlng=%1$f,%2$f&sensor=true&language="
                        + Locale.getDefault().getCountry(), lat, lng);
        HttpGet httpGet = new HttpGet(address);
        HttpClient client = new DefaultHttpClient();
        HttpResponse response;
        StringBuilder stringBuilder = new StringBuilder();

        List<Address> retList = null;

        response = client.execute(httpGet);
        HttpEntity entity = response.getEntity();
        InputStream stream = entity.getContent();
        int b;
        while ((b = stream.read()) != -1) {
            stringBuilder.append((char) b);
        }

        JSONObject jsonObject = new JSONObject(stringBuilder.toString());

        retList = new ArrayList<Address>();

        if ("OK".equalsIgnoreCase(jsonObject.getString("status"))) {
            JSONArray results = jsonObject.getJSONArray("results");
            for (int i = 0; i < results.length(); i++) {
                JSONObject result = results.getJSONObject(i);
                String indiStr = result.getString("formatted_address");
                Address addr = new Address(Locale.getDefault());
                addr.setAddressLine(0, indiStr);
                retList.add(addr);
                JSONArray addressComponents = results.optJSONObject(i).optJSONArray("address_components");
                for (int j = 0; j < addressComponents.length(); j++) {
                    JSONArray types = addressComponents.optJSONObject(j).optJSONArray("types");
                    for (int k = 0; k < types.length(); k++) {
                        String type = types.optString(k);
                        if (type.equals("locality")) {
                            addr.setLocality(addressComponents.optJSONObject(j).optString("long_name"));
                            return retList;
                        }
                    }
                }
            }
        }

        return retList;
    }

    RelativeLayout.LayoutParams paramsdrop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cd = new ConnectionDetector(CustomerHomeActivity.this);
        new CustomerPresenter(Injection.remoteDataRepository(getApplicationContext()), this);

        if (mAppManager.getuser().equals("customer")) {

            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        /* Setting current activity in application class. */
            setContentView(R.layout.activity_customer_home);
            ButterKnife.bind(this);
            paramsdrop = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT
            );

            /**
             * Important to catch the exceptions
             */

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

            /*****************************************************************************************/


            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            deviceWidth = size.x;
            app = (MyApplication) getApplication();
            mSocket = app.getSocket();
            /**Set Toolbar */
            setToolbar();

            /** Initialize the objects */
            resources = mContext.getResources();
            scale = resources.getDisplayMetrics().density;

            getCarList = new ArrayList<>();
            dataWorker = new LocationData(); // Location Storage
            mResultReceiver = new AddressResultReceiver(new Handler());
            linearLayoutManager = new LinearLayoutManager(this);
            linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            taxiListingAdapter = new TaxiListing(this, deviceWidth); // Taxi Listing Adapter
            CarsHandler = new Handler();
            MapHandler = new Handler();
            /** Set Horizontal Layout Manager for Taxis Listing & Setting Adapter*/

            availableTaxisList.setLayoutManager(linearLayoutManager);

            availableTaxisList.setAdapter(taxiListingAdapter);

            /** Initialize google map */

            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
            mapView = mapFragment.getView();

            /**
             * Check for play services availability if available then build google api client
             */

            if (checkPlayServices()) {
                // If this check succeeds, proceed with normal processing.
                // Otherwise, prompt user to get valid Play Services APK.
                if (!Constants.isLocationEnabled(this)) {
                    // notify user
                    AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                    dialog.setMessage("Location not enabled!");
                    dialog.setPositiveButton("Open location settings", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(myIntent);
                        }
                    });
                    dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        }
                    });
                    dialog.show();
                }
                buildGoogleApiClient();
            } else {
                Toast.makeText(this, "Location not supported in this device", Toast.LENGTH_SHORT).show();
            }

            if (MyFirebaseMessagingService.push_Status_Flag.equals("11")) {
                pushDialog(0);
            } else if (MyFirebaseMessagingService.push_Status_Flag.equals("12")) {
                pushDialog(1);
            }
        } else {
            Intent toSplash = new Intent(getApplicationContext(), SplashActivity.class);
            startActivity(toSplash);
            finish();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        locationchnagedStatus = true;
        mAppManager.savePrevActivity("TRACK", "CustomerHomeActivity");
        if (myNManager != null) {
            myNManager.cancelAll();
        }
        MyApplication.currentActivity = this.getClass().getSimpleName();
        MyApplication.activityResumed();

        killMe = false;


        if (CurrentNode == 1) {
            if (mMap != null) {
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            }
        }

        if (!app.isOfferVisible()) {
            customerOffers();
        }

        if (!mSocket.connected()) {
            Intent start_Serv = new Intent(getApplicationContext(), ConnectorService.class);
            startService(start_Serv);
        }

        String my_date = "31/08/2017";
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date strDate = null;
        try {
            strDate = sdf.parse(my_date);
            if (new Date().after(strDate)) {
                infoMessage.setVisibility(View.GONE);
            } else {
                infoMessage.setVisibility(View.VISIBLE);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }


     /*/////////////////////////// !INTERFACE LOCAL CALLBACKS /////////////////////////////////// */

    @Override
    public void onStart() {
        super.onStart();
        // Save the state of the animation
        if (!checkPermissions()) {
            requestPermissions();
        } else {
            getLastLocation();
        }
        registerReceiver(this.Node_CUSTOMER_Location_Update, new IntentFilter(NodeUtil.NODE_CUSTOMER_LOCATION_UPDATE));
        registerReceiver(this.Node_CUSTOMER_Location_Out_Of_Range, new IntentFilter(NodeUtil.NODE_CUSTOMER_LOCATION_OUT_OF_RANGE));
        registerReceiver(this.Node_CUSTOMER_GET_CARS, new IntentFilter(NodeUtil.NODE_CUSTOMER_GETCARS));
        registerReceiver(this.refreshAfterMarkedNa, new IntentFilter(RECEIVER_SUSPEND_CUSTOMER));

        try {
            if (mGoogleApiClient != null) {
                mGoogleApiClient.connect();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.activityPaused();
    }

    @Override
    public void onStop() {
        super.onStop();
        shimmerViewcontainer.stopShimmerAnimation();
        MyApplication.currentActivity = "";
        unregisterReceiver(Node_CUSTOMER_Location_Update);
        unregisterReceiver(Node_CUSTOMER_Location_Out_Of_Range);
        unregisterReceiver(Node_CUSTOMER_GET_CARS);
        unregisterReceiver(refreshAfterMarkedNa);

        killMe = true;


        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        try {

        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }

        if (rate != null) {
            rate.dismiss();
        }

        if (app.requestQueue != null) {
            app.requestQueue.cancelAll(CustomerHomeActivity.this);
        }
    }

    @Override
    protected void onDestroy() {

        if (dialogsuspend != null) {
            dialogsuspend.dismiss();
            dialogsuspend.cancel();
        }
        if (customerOffer != null) {
            customerOffer.dismiss();
            customerOffer.cancel();
        }
        try {
            if (requestQueueoffer != null) {
                requestQueueoffer.cancelAll(this);
            }
        } catch (Exception e) {

        }

        super.onDestroy();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

        if (mMap != null) {

            try {
                // Customise the styling of the base map using a JSON object defined
                // in a raw resource file.
                boolean success = googleMap.setMapStyle(
                        MapStyleOptions.loadRawResourceStyle(
                                this, R.raw.mapstyle));

                if (!success) {
                    Log.e("onMapReady", "Style parsing failed.");
                }
            } catch (Resources.NotFoundException e) {
                Log.e("onMapReady", "Can't find style. Error: ", e);
            }

            /** Setting up basic Google Map Properties */


            if (mapView != null &&
                    mapView.findViewById(Integer.parseInt("1")) != null) {
                // Get the button view
                View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                // and next place it, on bottom right (as Google Maps app)
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                        locationButton.getLayoutParams();
                // position on right bottom
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            }

            int margin = generateMargin(140);

            int margintop = generateMargin(200);

            mMap.setMyLocationEnabled(false);
            mMap.getUiSettings().setCompassEnabled(false);
            mMap.getUiSettings().setRotateGesturesEnabled(false);

            mMap.setPadding(0, margintop, 0, margin);

            /** Set up Various Camera Listeners */
            mMap.setOnCameraIdleListener(this);
            mMap.setOnCameraMoveStartedListener(this);
            mMap.setOnCameraMoveListener(this);
            mMap.setOnCameraMoveCanceledListener(this);

        }

    }

    /*/////////////////////////// !LOGIC SECTION /////////////////////////////////// */

    /**
     * Get Location to start the user location
     * Set Data Worker Pick Status = 1 & Pick Location to last location .
     */
    Location mLastLocation;

    @Override
    public void onConnected(Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        /**
         * Re initialize the Current Node in case of Activity Resume()
         */

        if (!OnActivityStatus) {

            CurrentNode = 1; // Represents Pick Up Location for the user

            if (mLastLocation == null) {
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            }
            if (mLastLocation != null) {

                dataWorker.setPickStatus(1);
                dataWorker.setPickLocation(mLastLocation);
                changeMap(REQUEST_CODE_PICK, 0);

                userLatitude = mLastLocation.getLatitude();
                userLongitude = mLastLocation.getLongitude();
                // getAddressFromAPI(0);
                /**
                 * Calling the api to get cars
                 */
                cabsMessage.setText("Getting cars near you...");
                shimmerViewcontainer.startShimmerAnimation();

                if (cd.isConnectingToInternet()) {
                    presenter.getCarsByLocation("" + userLatitude, "" + userLongitude, mAppManager.getTokenCode(), myCity, "true", mAppManager.getCustomerID(), CustomerHomeActivity.this);

                    //availcarsPresenter.getCarsbyLocation(userLatitude, userLongitude, myCity);
                } else {
                    DialogAlert("No Internet !",
                            "Check your connection and try again.", 0);
                }

                try {
                    LocationRequest mLocationRequest = new LocationRequest();
                    mLocationRequest.setInterval(10000);
                    mLocationRequest.setFastestInterval(5000);
                    mLocationRequest.setSmallestDisplacement(100);
                    mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationServices.FusedLocationApi.requestLocationUpdates(
                            mGoogleApiClient, mLocationRequest, this);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                showSnackbar(getString(R.string.no_location_detected));
            }

        } else {
            OnActivityStatus = false;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    /**
     * Update User Location
     *
     * @param location
     */

    @Override
    public void onLocationChanged(Location location) {
        try {
            if (location != null) {
                if (locationchnagedStatus) {
                    dataWorker.setPickLocation(location);
                    userLatitude = location.getLatitude();
                    userLongitude = location.getLongitude();
                    changeMap(REQUEST_CODE_PICK, 0);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // START Part 1

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    /**
     * Camera Movement Methods
     */


    @Override
    public void onCameraMoveCanceled() {
        Log.d("CameraFlow", "onCameraMoveCanceled");
    }

    @Override
    public void onCameraMove() {

        if (byclicked) {
            if (CurrentNode == 1) {
                if (dataWorker.getPickAddress() == null) {
                    primaryLocator.setText(R.string.gettingaddress);
                }
                markerText.setText("...");
            } else {
                if (dataWorker.getDropAddress() == null) {
                    secondaryLocator.setText(R.string.gettingaddress);
                }
            }
        }
    }

    // END PART 1


   /*/////////////////////////// !LISTENERS SECTIONS /////////////////////////////////// */

    @Override
    public void onCameraMoveStarted(int i) {
        Log.d("CameraFlow", "onCameraMoveStarted");
    }

    /**
     * This is where we have to call the service to get the address of the selected
     * latitude longitude .
     * **CONNECTED PARTS : 1
     */

    @Override
    public void onCameraIdle() {
        Log.d("CameraFlow", "onCameraIdle");

        if (byclicked) {

            secondaryContainer.setEnabled(true);
            primaryContainer.setEnabled(true);
            mCenterLatLong = mMap.getCameraPosition().target;

            try {

                Location mLocation = new Location("");
                mLocation.setLatitude(mCenterLatLong.latitude);
                mLocation.setLongitude(mCenterLatLong.longitude);

                if (CurrentNode == 1) {

                    userLatitude = mCenterLatLong.latitude;
                    userLongitude = mCenterLatLong.longitude;

                    //getAddressFromAPI(0);
                    if (CarsHandler != null) {
                        CarsHandler.removeCallbacks(updateCarsHandler);
                    }

                    CarsHandler.postDelayed(updateCarsHandler, 1000);

                    /** Disable Location Update service to update Pick Location by setting Initial Pick = true*/
                    InitialPick = true;
                    dataWorker.setPickLocation(mLocation);
                    if (!byAutoLocation) {
                        startIntentService(mLocation, REQUEST_CODE_PICK);
                    } else {
                        byAutoLocation = false;
                    }

                } else {
                    dataWorker.setDropLocation(mLocation);
                    if (!byAutoLocation) {
                        startIntentService(mLocation, REQUEST_CODE_DROP);
                    } else {
                        byAutoLocation = false;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            CarsHandler.postDelayed(updateCarsHandler, 1000);
            byclicked = true;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        OnActivityStatus = true;
        // Check that the result was from the autocomplete widget.
        if (requestCode == REQUEST_CODE_DROP) {

            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.

                markerText.setText("");
                if (data != null) {

                    double latitude = data.getDoubleExtra("latitude", 0);
                    double longitude = data.getDoubleExtra("longitude", 0);
                    String address = data.getStringExtra("address");
                    String primaryText = data.getStringExtra("primary");
                    String secondaryText = data.getStringExtra("secondary");

                    Location newDestination = new Location("Destination");
                    newDestination.setLatitude(latitude);
                    newDestination.setLongitude(longitude);

                    dataWorker.setDropStatus(1);
                    dataWorker.setDropAddress(primaryText + ", " + secondaryText.replace(",", ""));
                    dataWorker.setDropLocation(newDestination);
                    CurrentNode = 2;
                    changeMap(REQUEST_CODE_DROP, 1);

                }

            }

        } else if (requestCode == REQUEST_CODE_PICK) {

            if (resultCode == RESULT_OK) {

                locationchnagedStatus = false;

                if (data != null) {

                    double latitude = data.getDoubleExtra("latitude", 0);
                    double longitude = data.getDoubleExtra("longitude", 0);

                    String address = data.getStringExtra("address");
                    String primaryText = data.getStringExtra("primary");
                    String secondaryText = data.getStringExtra("secondary");
                    markerText.setText("...");
                    if (app.requestQueue != null) {
                        app.requestQueue.cancelAll(CustomerHomeActivity.this);
                    }
                    //   availcarsPresenter.cancelRequest();

                    userLatitude = latitude;
                    userLongitude = longitude;
                    if (cd.isConnectingToInternet()) {
                        presenter.getCarsByLocation("" + latitude, "" + longitude, mAppManager.getTokenCode(), "", "true", mAppManager.getCustomerID(), CustomerHomeActivity.this);

                        //availcarsPresenter.getCarsbyLocation(latitude, longitude, "");
                    } else {
                        DialogAlert("No Internet !",
                                "Check your connection and try again.", 0);
                    }

                    Location newDestination = new Location("PICKUP");
                    newDestination.setLatitude(latitude);
                    newDestination.setLongitude(longitude);

                    dataWorker.setPickStatus(1);
                    dataWorker.setPickAddress(primaryText + ", " + secondaryText.replace(",", ""));
                    dataWorker.setPickLocation(newDestination);
                    CurrentNode = 1;
                    changeMap(REQUEST_CODE_PICK, 1);

                }

            }
        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {

            Status status = PlaceAutocomplete.getStatus(this, data);
            //Toast.makeText(CustomerHomeActivity.this, "Error while getting location", Toast.LENGTH_SHORT).show();
        } else if (resultCode == RESULT_CANCELED) {

            // Indicates that the activity closed before a selection was made. For example if
            // the user pressed the back button.
        }
    }


    public void getcars(boolean status, String message, AvailCars availCars) {

        // stop the shimmer animation

        if (shimmerViewcontainer != null) {
            shimmerViewcontainer.stopShimmerAnimation();
        }

        if (status) {
            switch (availCars.getCar_status()) {
                case "true":

                    if (availCars.getCar_is_suspended().equalsIgnoreCase("FALSE")) {

                        getCarList.clear();
                        getCarList = availCars.getCarList();

                        if (getCarList != null && getCarList.size() > 0) {
                            cabSwitcher.setDisplayedChild(1);

                            if (!cardsLoaded) {
                                new downloadCars().execute(1);
                            } else {
                              /*  if(availCars.getCarList().size()>0)
                               availCars.getCarList().add(  availCars.getCarList().get(0));*/
                                taxiListingAdapter.updateTaxisListing(availCars.getCarList(), userLatitude, userLongitude);
                            }

                        }

                    } else if (availCars.getCar_is_suspended().equalsIgnoreCase("TRUE")) {

                        cabSwitcher.setDisplayedChild(1);
                        DialogSuspend(availCars.getCar_is_suspended_reason(), availCars.getCar_is_suspended_reason_type());

                        if (CurrentNode == 1) {
                            markerText.setText("no cars");
                        }

                    } else {
                        cabSwitcher.setDisplayedChild(0);
                        cabsMessage.setText(availCars.getCar_status_message());
                    }

                    break;

                case "false":
                    cabSwitcher.setDisplayedChild(0);
                    specificCar = null;
                    if (CurrentNode == 1) {
                        markerText.setText("...");
                    }
                    cabsMessage.setText(availCars.getCar_status_message());

                    if (availCars.getCar_status_message().equalsIgnoreCase("Invalid Token")) {

                    }

                    break;

                default:
                    break;
            }
        }

    }


    // API SECTION - MVP ONLY

    @Override
    public void specificCarClicked(Cars car, boolean userClicked) {

        specificCar = car;

        carImage = MyApplication.carImages.get(car.getId());

        /** If User Clicked on Cars , Clear the map first**/

        try {

            refreshSurge();

            HashMap<String, Drivers> stringDriversHashMap = availCars.getStringHashMapHashMap().get(specificCar.getId());

            ArrayList<Drivers> drivers = new ArrayList<>();

            drivers.addAll(stringDriversHashMap.values());


            plotCars(specificCar, drivers);

            //  plotCars(specificCar, availCars.getStringHashMapHashMap().get(specificCar.getId()));
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        if (timings.containsKey(car.getId())) {
            if (timings.get(car.getId()) != -1) {
                if (CurrentNode == 1) {
                    markerText.setText(timings.get(car.getId()) + " min");
                }
            } else {
                if (CurrentNode == 1) {
                    markerText.setText("no cars");
                }
            }
        }


//        if (!car.getAvg_time().equalsIgnoreCase("no cars")) {
//            if (CurrentNode == 1) {
//                markerText.setText(car.getAvg_time());
//            }
//        } else {
//            if (CurrentNode == 1) {
//                markerText.setText("no cars");
//
//            }
//        }
    }

    private void refreshSurge() {
        HashMap<String, Drivers> stringDriversHashMap = availCars.getStringHashMapHashMap().get(specificCar.getId());

        ArrayList<Drivers> drivers = new ArrayList<>();

        drivers.addAll(stringDriversHashMap.values());
        if (drivers.size() > 0) {
            if (drivers.get(0).isSurge_applied()) {
                int margin = generateMargin(180);
                int margintop = generateMargin(240);
                mMap.setPadding(0, margintop, 0, margin);
                seperatorLine.setVisibility(View.GONE);
                surgeCharge.setVisibility(View.VISIBLE);
                surgeMessage.setText(drivers.get(0).getSurge_message());
            }
        } else {
            int margin = generateMargin(140);
            int margintop = generateMargin(200);
            seperatorLine.setVisibility(View.VISIBLE);
            mMap.setPadding(0, margintop, 0, margin);
            surgeCharge.setVisibility(View.INVISIBLE);
            surgeMessage.setText("");
        }

    }

   /* /////////////////////////// !UTILITY SECTION /////////////////////////////////// */

    @Override
    public void specificCar(Cars car, boolean userClicked) {

        if (specificCar != null && specificCar.getId().equalsIgnoreCase(car.getId())) {
            flipFares(1);
        } else {
            flipFares(0);
            specificCar = car;

            carImage = MyApplication.carImages.get(car.getId());

            /** If User Clicked on Cars , Clear the map first**/

            try {

                HashMap<String, Drivers> stringDriversHashMap = availCars.getStringHashMapHashMap().get(specificCar.getId());

                ArrayList<Drivers> drivers = new ArrayList<>();

                drivers.addAll(stringDriversHashMap.values());

                plotCars(specificCar, drivers);


                //  plotCars(specificCar, availCars.getStringHashMapHashMap().get(car.getId()));
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            refreshSurge();
            if (timings.containsKey(car.getId())) {
                if (timings.get(car.getId()) != -1) {
                    if (CurrentNode == 1) {
                        markerText.setText(timings.get(car.getId()) + " min");
                    }
                } else {
                    if (CurrentNode == 1) {
                        markerText.setText("no cars");
                    }
                }
            }

//            if (!car.getAvg_time().equalsIgnoreCase("no cars")) {
//                if (CurrentNode == 1) {
//                    markerText.setText(car.getAvg_time());
//                }
//            } else {
//                if (CurrentNode == 1) {
//                    markerText.setText("no cars");
//
//                }
//            }
        }

    }

    private void flipFares(int wayForVisibilty) {

        if (faresLayout.getVisibility() == View.VISIBLE) {
            faresLayout.setVisibility(View.GONE);
            rideNowBtn.setVisibility(View.VISIBLE);
        } else if (wayForVisibilty == 1) {

            baseFare.setText((specificCar.getBase_fare() != null) ? mContext.getResources().getString(R.string.pound) + specificCar.getBase_fare() : "----");
            minFare.setText((specificCar.getMinimum_fare() != null) ? mContext.getResources().getString(R.string.pound) + specificCar.getMinimum_fare() : "----");
            perMileFare.setText((specificCar.getPer_mile_fare() != null) ? mContext.getResources().getString(R.string.pound) + specificCar.getPer_mile_fare() : "----");
            perMinFare.setText((specificCar.getPer_minute_fare() != null) ? mContext.getResources().getString(R.string.pound) + specificCar.getPer_minute_fare() : "----");
            faresLayout.setVisibility(View.VISIBLE);
            rideNowBtn.setVisibility(View.GONE);
        }

    }

    /**
     * @param status status  = 1 (PIck Up Location)
     *               status =  2 (Drop Up Loaction)
     */

    private Location changeMapMarkers(int status, boolean initialdrop) {

        Location location = null;

        if (status == 1) {

            location = dataWorker.getPickLocation();

            if (location != null) {

                imageMarker.setImageResource(R.drawable.timemarker);

                primaryLocator.setText(dataWorker.getPickAddress());
                locatorLabel.setText(R.string.pickup);
                markerText.setText("...");
                secondaryLocator.setTextColor(Color.parseColor("#010101"));

                mMap.getUiSettings().setZoomControlsEnabled(false);
                LatLng latLong;

                latLong = new LatLng(location.getLatitude(), location.getLongitude());

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(latLong).zoom(15f).tilt(10).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                imageView9.setVisibility(View.VISIBLE);
                primaryContainer.bringToFront();
                primaryContainer.setBackground(getResources().getDrawable(R.drawable.bubblelay));
                ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) primaryContainer.getLayoutParams();
                params.leftMargin = 0;
                params.rightMargin = 0;
                secondaryContainer.setBackground(getResources().getDrawable(R.drawable.borderlay));
                ViewGroup.MarginLayoutParams secondaryparams = (ViewGroup.MarginLayoutParams) secondaryContainer.getLayoutParams();
                secondaryparams.leftMargin = 22;
                secondaryparams.rightMargin = 22;
                secondaryContainer.invalidate();
            }
        } else {

            locatorLabel.setText(dataWorker.getPickAddress());
            imageMarker.setImageResource(R.drawable.map_pointer);

            if (initialdrop) {
                secondaryLocator.setText("Enter Drop Location");
            } else {

                location = dataWorker.getDropLocation();

                if (location != null) {

                    secondaryLocator.setText(Html.fromHtml(dataWorker.getDropAddress()));
                    LatLng latLong;
                    latLong = new LatLng(location.getLatitude(), location.getLongitude());

                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(latLong).zoom(15f).tilt(10).build();

                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                }

            }
            mMap.getUiSettings().setZoomControlsEnabled(false);
            imageView9.setVisibility(View.GONE);
            secondaryContainer.bringToFront();
            secondaryContainer.setBackground(getResources().getDrawable(R.drawable.bubblelay));
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) secondaryContainer.getLayoutParams();
            params.leftMargin = 0;
            params.rightMargin = 0;
            primaryContainer.setBackground(null);
            ViewGroup.MarginLayoutParams secondaryparams = (ViewGroup.MarginLayoutParams) primaryContainer.getLayoutParams();
            secondaryparams.leftMargin = 22;
            secondaryparams.rightMargin = 22;
            primaryContainer.setBackground(getResources().getDrawable(R.drawable.borderlay));
            primaryContainer.invalidate();
        }
        return location;
    }

    /**
     * Method to change user drop & pick location
     *
     * @param requestCode
     */
    private synchronized void changeMap(int requestCode, int way) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        // check if map is created successfully or not
        if (mMap != null) {

            if (requestCode == REQUEST_CODE_PICK) {

                if (way == 1) {
                    byAutoLocation = true;
                    changeMapMarkers(1, false);
                } else {
                    startIntentService(changeMapMarkers(1, false), REQUEST_CODE_PICK);
                }


            } else {
                if (way == 1) {
                    byAutoLocation = true;
                    changeMapMarkers(2, false);
                } else {
                    startIntentService(changeMapMarkers(2, false), REQUEST_CODE_DROP);
                }
            }

        } else {
            Toast.makeText(this, "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                    .show();
        }

    }


    /* //////////////////////////// !ANIMATIONS ///////////////////////////// */

    /**
     * When User changes the location of the pin point then we have to fetch the
     * address of that location from its latitude and longitude.
     * Methods Used:
     * 1) startIntentService ( To start a service to get the address)
     * 2) AddressResultReceiver (To get the result from the startIntentService)
     * 3) DisplayAddressOutput ( To display the output in the location holder)
     */

    protected void startIntentService(Location mLocation, int requestcode) {
        tlat = mLocation.getLatitude();
        tlng = mLocation.getLongitude();
        getAddressFromAPI(requestcode);

//        Intent intent = new Intent(this, FetchAddressIntentService.class);
//        intent.putExtra(Constants.RECEIVER, mResultReceiver);
//        intent.putExtra(Constants.VARIABLES, requestcode);
//        intent.putExtra(Constants.LOCATION_DATA_EXTRA, mLocation);
//        startService(intent);

    }

    /**
     * Updates the address in the UI.
     */
    protected void displayAddressOutput(String address, int requestcode) {
        //  mLocationAddressTextView.setText(mAddressOutput);
        try {
            if (primaryLocator != null) {
                if (requestcode == REQUEST_CODE_PICK) {
                    dataWorker.setPickAddress(address);
                    primaryLocator.setText(Html.fromHtml(address));
                } else {
                    dataWorker.setDropStatus(1);
                    dataWorker.setDropAddress(address);
                    secondaryLocator.setText(Html.fromHtml(address));
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.ride_now_btn)
    public void proceedToRequest() {
        rideNowBtn.setEnabled(false);
        showProgress();
        String pikupadress = primaryLocator.getText().toString().trim();
        String dropupadress = secondaryLocator.getText().toString().trim();

        if (pikupadress != null && !pikupadress.isEmpty()) {

            if (dropupadress != null && !dropupadress.isEmpty()) {

                if (dataWorker.getPickLocation() != null) {

                    if (dataWorker.getDropLocation() != null) {

                        if (specificCar != null) {

                            if (availCars.getStringHashMapHashMap().get(specificCar.getId()) != null && availCars.getStringHashMapHashMap().get(specificCar.getId()).size() > 0) {

                                if (dataWorker.getPickLocation().getLatitude() != 0.0) {

                                    if (dataWorker.getDropLocation().getLatitude() != 0.0) {

                                        if (dataWorker.getPickAddress() != null && !dataWorker.getPickAddress().isEmpty()) {

                                            if (dataWorker.getDropAddress() != null & !dataWorker.getDropAddress().isEmpty()) {
                                                hideProgress();
                                                Intent toConfirm = new Intent(this, ConfirmRideActivity.class);
                                                toConfirm.putExtra("picklat", dataWorker.getPickLocation().getLatitude());
                                                toConfirm.putExtra("picklng", dataWorker.getPickLocation().getLongitude());
                                                toConfirm.putExtra("droplat", dataWorker.getDropLocation().getLatitude());
                                                toConfirm.putExtra("droplng", dataWorker.getDropLocation().getLongitude());
                                                toConfirm.putExtra("pick", dataWorker.getPickAddress());
                                                toConfirm.putExtra("drop", dataWorker.getDropAddress());
                                                toConfirm.putExtra("type", specificCar.getId());
                                                toConfirm.putExtra("carname", specificCar.getName());
                                                toConfirm.putExtra("avgtime", specificCar.getAvg_time());
                                                toConfirm.putExtra("total_passengers", specificCar.getNo_of_seats());
                                                startActivity(toConfirm);
                                            } else {
                                                Toast.makeText(getApplicationContext(), "Enter your destination", Toast.LENGTH_LONG).show();
                                            }

                                        }

                                    } else {
                                        Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_LONG).show();
                                }

                            } else {
                                Toast.makeText(getApplicationContext(), "no cars available", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "no cars available", Toast.LENGTH_LONG).show();
                        }

                    } else {
                        mMap.getUiSettings().setMyLocationButtonEnabled(false);

                        openAutocompleteActivity(REQUEST_CODE_DROP);
                    }
                } else {
                    locationDialog(1, "Enter your pickup location.");
                }

            } else {
                locationDialog(2, "Enter your drop location.");
            }
        } else {
            locationDialog(1, "Enter your pickup location.");
        }
        rideNowBtn.setEnabled(true);
        hideProgress();
    }

    /**
     * ///////////////////////////////////////////  !Node ////////////////////////////////////////////
     */

    @OnClick(R.id.secondary_container)
    public void DropClicked() {
        mMap.getUiSettings().setMyLocationButtonEnabled(false);

        if (dataWorker.getDropStatus() == 0) { // No Drop Location
            openAutocompleteActivity(REQUEST_CODE_DROP);
        } else if (CurrentNode == 2) { // Current Node is Drop Up
            openAutocompleteActivity(REQUEST_CODE_DROP);
        } else {
            markerText.setText("");
            byclicked = false;
            CurrentNode = 2;  // Current Node is Pick Up , Change it to Dropup
            changeMapMarkers(2, false); // Change Marker Positions
        }

    }

    @OnClick(R.id.primary_container)
    public void primaryClick() {

        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        if (dataWorker.getPickStatus() == 0) { // No Pick Location
            openAutocompleteActivity(REQUEST_CODE_PICK);
        } else if (CurrentNode == 1) { // Current Node is PIck Up
            openAutocompleteActivity(REQUEST_CODE_PICK);
        } else {
            byclicked = false;
            CurrentNode = 1;// Current Node is Drop Up , Change it to Pickup
            changeMapMarkers(1, false);// Change Marker Positions
        }
    }

    /**
     * Calling API : api/common/get_cars to get the cars corrosponding to that
     * current location.
     */


    /**
     * Setting up the toolbar
     */


    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            int color = Color.parseColor("#ffffff");
            toolbar.setTitleTextColor(color);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            toolbar.setNavigationIcon(R.drawable.ic_menu_black);

            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            headertext = (TextView) findViewById(android.R.id.text1);
            headertext.setText("Request a Ride");

        }
        String[] navMenuTitles;
        TypedArray navMenuIcons;
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_user_items);
        navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons_user);
        set(navMenuTitles, navMenuIcons);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {

            }
            return false;
        }
        return true;
    }

    private void openAutocompleteActivity(int requestCode) {

        String hint = (requestCode == REQUEST_CODE_DROP) ? "Enter drop location" : "Enter pickup location";
        Intent toCustomPlaces = new Intent(getApplicationContext(), CustomPlaces.class);
        toCustomPlaces.putExtra("hint", hint);
        startActivityForResult(toCustomPlaces, requestCode);

//        try {
//            Util.hideKeyboard(this);
//            // The autocomplete activity requires Google Play Services to be available. The intent
//            // builder checks this and throws an exception if it is not the case.
//            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
//                    .build(this);
//            startActivityForResult(intent, requestCode);
//        } catch (GooglePlayServicesRepairableException e) {
//            // Indicates that Google Play Services is either not installed or not up to date. Prompt
//            // the user to correct the issue.
//            GoogleApiAvailability.getInstance().getErrorDialog(this, e.getConnectionStatusCode(),
//                    0 /* requestCode */).show();
//        } catch (GooglePlayServicesNotAvailableException e) {
//            // Indicates that Google Play Services is not available and the problem is not easily
//            // resolvable.
//            String message = "Google Play Services is not available: " +
//                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);
//
//            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
//        }

    }

    public void openUploadPopup(View view) {


        Animation animScale = AnimationUtils.loadAnimation(this, R.anim.slide_in_up);
        view.startAnimation(animScale);
        view.setVisibility(View.VISIBLE);


    }

    public void closeUploadPopup(View view) {

        Log.d("FromBase", "here");

        Animation animScale = AnimationUtils.loadAnimation(this, R.anim.slide_out_bottom);
        view.startAnimation(animScale);
        view.setVisibility(View.INVISIBLE);

    }


    private synchronized void plotCars(Cars car, ArrayList<Drivers> drivers) throws ExecutionException, InterruptedException {

        selectedCartype = car.getId();

        if (drivers != null && drivers.size() > 0) {

            for (int i = 0; i < drivers.size(); i++) {

                Drivers driver = drivers.get(i);

                if (allMarkers.containsKey(driver.getDriver_id())) {

                    DriverMark driverMark = (DriverMark) allMarkers.get(driver.getDriver_id());

                    Marker mark = driverMark.getMarker();

                    if (!driver.getCar_type().equalsIgnoreCase(selectedCartype)) {
                        mark.setVisible(false);
                    } else {
                        if (carImage != null) {
                            mark.setIcon(BitmapDescriptorFactory.fromBitmap(createcar(carImage)));
                            mark.setVisible(true);
                        } else {
                            drawDriverMarker(mark, car.getMap_car_image());
                        }
                    }

                    LatLng last = new LatLng(mark.getPosition().latitude, mark.getPosition().longitude);

                    LatLng newone = new LatLng(Double.parseDouble(driver.getLat()), Double.parseDouble(driver.getLng()));

                    double distance = SphericalUtil.computeDistanceBetween(last, newone);

                    float bearing = bearingBetweenLatLngs(last, newone);

                    if (distance > 15.0) {
                        if (bearing < 180.0) {
                            mark.setRotation(bearing);
                            mark.setAnchor(0.5f, 0.5f);
                            mark.setFlat(true);
                            animateMarker(mark, newone, false);
                        } else {
                            mark.setRotation(bearing);
                            mark.setAnchor(0.5f, 0.5f);
                            mark.setFlat(true);
                            animateMarker(mark, newone, false);
                        }

                    }
                    allMarkers.put(driver.getDriver_id(), new DriverMark(driver.getCar_type(), mark));

                } else {

                    Marker mark = drawMarker(new LatLng(Double.parseDouble(driver.getLat()), Double.parseDouble(driver.getLng())), carImage);
                    if (!driver.getCar_type().equalsIgnoreCase(selectedCartype)) {
                        mark.setVisible(false);
                    } else {
                        mark.setVisible(true);
                    }
                    LatLng newone = new LatLng(Double.parseDouble(driver.getLat()), Double.parseDouble(driver.getLng()));
                    mark.setAnchor(0.5f, 0.5f);
                    mark.setFlat(true);
                    mark.setPosition(newone);
                    allMarkers.put(driver.getDriver_id(), new DriverMark(driver.getCar_type(), mark));
                }

            }

        }

        removeOdds(car.getId(), drivers);
    }


    /*  *//*private synchronized void plotCars(Cars car, HashMap<String, Drivers> drivers) throws ExecutionException, InterruptedException {

        selectedCartype = car.getId();

        if (drivers != null && drivers.size() > 0) {


            for (Map.Entry<String, Drivers> entry : drivers.entrySet()) {


                Drivers driver = (Drivers) entry.getValue();

                if (allMarkers.containsKey(driver.getDriver_id())) {

                    DriverMark driverMark = (DriverMark) allMarkers.get(driver.getDriver_id());

                    Marker mark = driverMark.getMarker();

                    if (!driver.getCar_type().equalsIgnoreCase(selectedCartype)) {
                        mark.setVisible(false);
                    } else {
                        if (carImage != null) {
                            mark.setIcon(BitmapDescriptorFactory.fromBitmap(createcar(carImage)));
                            mark.setVisible(true);
                        } else {
                            drawDriverMarker(mark, car.getMap_car_image());
                        }
                    }

                    LatLng last = new LatLng(mark.getPosition().latitude, mark.getPosition().longitude);

                    LatLng newone = new LatLng(Double.parseDouble(driver.getLat()), Double.parseDouble(driver.getLng()));

                    double distance = SphericalUtil.computeDistanceBetween(last, newone);

                    float bearing = bearingBetweenLatLngs(last, newone);

                    if (distance > 15.0) {
                        if (bearing < 180.0) {
                            mark.setRotation(bearing);
                            mark.setAnchor(0.5f, 0.5f);
                            mark.setFlat(true);
                            animateMarker(mark, newone, false);
                        } else {
                            mark.setRotation(bearing);
                            mark.setAnchor(0.5f, 0.5f);
                            mark.setFlat(true);
                            animateMarker(mark, newone, false);
                        }

                    }
                    allMarkers.put(driver.getDriver_id(), new DriverMark(driver.getCar_type(), mark));

                } else {

                    Marker mark = drawMarker(new LatLng(Double.parseDouble(driver.getLat()), Double.parseDouble(driver.getLng())), carImage);
                    if (!driver.getCar_type().equalsIgnoreCase(selectedCartype)) {
                        mark.setVisible(false);
                    } else {
                        mark.setVisible(true);
                    }
                    LatLng newone = new LatLng(Double.parseDouble(driver.getLat()), Double.parseDouble(driver.getLng()));
                    mark.setAnchor(0.5f, 0.5f);
                    mark.setFlat(true);
                    mark.setPosition(newone);
                    allMarkers.put(driver.getDriver_id(), new DriverMark(driver.getCar_type(), mark));

                }

            }

        }

        removeOdds(car.getId(), drivers);
    }
*/
    private Bitmap theBitmap = null;

    private void drawDriverMarker(final Marker mark, final String image_path) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                if (Looper.myLooper() == null) {
                    Looper.prepare();
                }
                try {
                    theBitmap = Glide.
                            with(CustomerHomeActivity.this).
                            load(image_path).
                            asBitmap().
                            into(-1, -1).
                            get();
                } catch (final ExecutionException e) {
                    String err = (e.getMessage() == null) ? "SD Card failed" : e.getMessage();
                    Log.e("sdcard-err2:", err);
                } catch (final InterruptedException e) {
                    String err = (e.getMessage() == null) ? "SD Card failed" : e.getMessage();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void dummy) {
                if (null != theBitmap) {
                    // The full bitmap should be available here
                    if (mMap != null) {

                        if (mark != null) {
                            mark.setIcon(BitmapDescriptorFactory.fromBitmap(createcar(theBitmap)));
                        } else {
                            mark.setIcon(BitmapDescriptorFactory.fromBitmap(createcar(theBitmap)));
                        }
                    }
                }
            }
        }.execute();

    }

    private void removeOdds(String carType, ArrayList<Drivers> drivers) {

        if (drivers != null) {

            Iterator it = allMarkers.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                String driverId = (String) pair.getKey();
                boolean containsOrnot = false;
                for (Drivers driver : drivers) {
                    if (driver.getDriver_id().equalsIgnoreCase(driverId)) {
                        containsOrnot = true;
                    }
                }

                if (!containsOrnot) {

                    Marker mark = ((DriverMark) pair.getValue()).getMarker();
                    if (mark != null) {
                        mark.remove();
                    }

                    it.remove();
                }
            }
        } else {
            Iterator it = allMarkers.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();

                Marker mark = ((DriverMark) pair.getValue()).getMarker();


                mark.remove();
                it.remove();

            }
        }

    }


 /*   private void removeOdds(String carType, HashMap<String, Drivers> drivers) {

        if (drivers != null&&drivers.size()>0) {
            Iterator it = allMarkers.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                String driverId = (String) pair.getKey();
                boolean containsOrnot = false;


                for (Map.Entry<String, Drivers> entry : drivers.entrySet()) {

                    System.out.println(pair.getKey() + " = " + entry.getValue());
                    Drivers d = (Drivers) entry.getValue();
                    if (d.getDriver_id().equalsIgnoreCase(driverId)) {
                        containsOrnot = true;
                    }
                }


                if (!containsOrnot) {
                    Marker mark = ((DriverMark) pair.getValue()).getMarker();
                    if (mark != null) {
                        mark.remove();
                    }
                    it.remove();
                }
            }
        } else {
            Iterator it = allMarkers.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();

                Marker mark = ((DriverMark) pair.getValue()).getMarker();

                mark.remove();

            }
        }

    }*/

    private Marker drawMarker(LatLng point, Bitmap car) {
        // Creating an instance of MarkerOptions


        Marker marker = mMap.addMarker(new MarkerOptions().position(point).icon(BitmapDescriptorFactory.fromBitmap(createcar(carImage))));
        return marker;

    }

    private Location convertLatLngToLocation(LatLng latLng) {
        Location loc = new Location("someLoc");
        loc.setLatitude(latLng.latitude);
        loc.setLongitude(latLng.longitude);
        return loc;
    }

    private float bearingBetweenLatLngs(LatLng begin, LatLng end) {
        Location beginL = convertLatLngToLocation(begin);
        Location endL = convertLatLngToLocation(end);

        return beginL.bearingTo(endL);
    }

    public void animateMarker(final Marker m, final LatLng toPosition, final boolean hideMarke) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mMap.getProjection();
        Point startPoint = proj.toScreenLocation(m.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 1500;
        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                m.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarke) {
                        m.setVisible(false);
                    } else {
                        m.setVisible(true);
                    }
                }
            }
        });
    }

    private int generateMargin(int Value) {
        int dpValue = Value;
        float d = getResources().getDisplayMetrics().density;
        return (int) (dpValue * d); // margin in pixels
    }

    private void locationDialog(final int status, String message) {
        // TODO Auto-generated method stub


        rate = new Dialog(this, R.style.Theme_Dialog);
        rate.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = rate.getWindow();
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        rate.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        rate.setContentView(R.layout.dialog_no_title);
        rate.setCancelable(false);
        rate.setCanceledOnTouchOutside(false);


        TextView mDialogBody = (TextView) rate
                .findViewById(R.id.txt_dialog_body);
        TextView mOK = (TextView) rate.findViewById(R.id.txt_dialog_ok);


        mDialogBody.setText(message);

        mOK.setText("OK");

        mOK.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                rate.dismiss();
                if (status == 2) {
                    CurrentNode = 2;
                    openAutocompleteActivity(REQUEST_CODE_DROP);
                } else {
                    CurrentNode = 1;
                    openAutocompleteActivity(REQUEST_CODE_PICK);
                }

            }
        });


        try {
            if (rate != null) {
                if (rate.isShowing()) {
                    rate.dismiss();
                }
                rate.show();
            }
        } catch (Exception e) {

        }

    }

    private void DialogSuspend(String title, int type) {

        dialogsuspend = new Dialog(CustomerHomeActivity.this, R.style.Theme_Dialog);
        dialogsuspend.requestWindowFeature(Window.FEATURE_NO_TITLE);


        dialogsuspend.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogsuspend.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialogsuspend.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        dialogsuspend.setContentView(R.layout.dialog_suspension);
        dialogsuspend.setCancelable(false);
        dialogsuspend.setCanceledOnTouchOutside(false);


        TextView reason = (TextView) dialogsuspend.findViewById(R.id.reason);
        LinearLayout mDialogBody = (LinearLayout) dialogsuspend.findViewById(R.id.clear_view);
        TextView payment = (TextView) dialogsuspend.findViewById(R.id.txt_dialog_ok);

        reason.setText(title);

        if (type == 2) {
            mDialogBody.setVisibility(View.VISIBLE);
        } else {
            mDialogBody.setVisibility(View.GONE);
        }

        payment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogsuspend.cancel();

                Intent toPayment = new Intent(getApplicationContext(), RiderPaymentActivity.class);
                startActivity(toPayment);
                finish();

            }
        });


        if (dialogsuspend.isShowing()) {
            dialogsuspend.dismiss();
        }
        dialogsuspend.show();

    }

    private void pushDialog(final int f) {
        // TODO Auto-generated method stub


        push = new Dialog(CustomerHomeActivity.this, R.style.Theme_Dialog);
        push.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = push.getWindow();
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        push.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        push.setContentView(R.layout.dialog_no_title);
        push.setCancelable(false);
        push.setCanceledOnTouchOutside(false);
        push.show();


        TextView mDialogBody = (TextView) push
                .findViewById(R.id.txt_dialog_body);
        TextView mOK = (TextView) push.findViewById(R.id.txt_dialog_ok);
        if (f == 0) {

            mDialogBody.setText("You have a ride sharing invitation from " + StatusParser.getShared_customerName());

            mOK.setText("VIEW INVITATION");
        } else {

            mDialogBody.setText("Your ride has been ended successfully.");

            mOK.setText("View Details");
        }

        mOK.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                myNManager.cancelAll();

                if (f == 0) {
                    MyFirebaseMessagingService.push_Status_Flag = "";
                    mAppManager.savePaymentType(DriverParser.getPayment_type());
                    Intent myintent = new Intent(CustomerHomeActivity.this,
                            RideSharingActivity.class);
                    startActivity(myintent);
                    finish();
                } else {
                    MyFirebaseMessagingService.push_Status_Flag = "";
                    mAppManager.savePrevActivity("REC", "REQ");
                    Intent myintent = new Intent(CustomerHomeActivity.this,
                            RideReceipt.class);
                    startActivity(myintent);
                    finish();

                }
            }
        });

    }

    private Bitmap createcar(Bitmap car) {

        View view = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.car, null);
        ImageView carImage = (ImageView) view.findViewById(R.id.carimg);

        if (car != null && !car.equals("")) {
            carImage.setImageBitmap(car);
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    void updateLocationFields() {
        if (dataWorker.getPickAddress() != null) {
            primaryLocator.setText(dataWorker.getPickAddress());
        }
        if (dataWorker.getDropAddress() != null) {
            secondaryLocator.setText(dataWorker.getDropAddress());
        }
    }

    AvailCars availCars;

    @Override
    public void getCarsResponse(String response) {

        System.out.println("CAR_SELECT===" + response);

        if (response != null && response.trim().length() != 0) {

            availCars = new AvailCars();

            try {
                parsresp = availCars.getCars(response);

                if (!TextUtils.isEmpty(parsresp)) {


                    getcars(true, "Success", availCars);
                }

            } catch (Exception e) {


                getcars(false, "Error. Please try again.", availCars);
            }
        } else {
            getcars(false, "Error. Please try again.", null);
        }

    }

    @Override
    public void showProgreass() {

    }

    @Override
    public void onFail(Throwable t) {
        hideProgress();

        getcars(false, "Error. Please try again.", availCars);


    }

    @Override
    public void getEstimateResponse(String response) {

    }

    @Override
    public void driverSelectREsponse(String response) {

    }

    @Override
    public void driverSelectError(Throwable t) {

    }

    @Override
    public void getPromoCodeResponse(String response) {

    }

    @Override
    public void getPromocodeerror(Throwable t) {

    }

    @Override
    public void setPresenter(CustomerContract.Presenter presenter) {
        this.presenter = presenter;

    }

    /**
     * Receiver for data sent from FetchAddressIntentService.
     */
    class AddressResultReceiver extends android.os.ResultReceiver {

        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        /**
         * Receives data sent from FetchAddressIntentService and updates the UI in MainActivity.
         */
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Display the address string or an error message sent from the intent service.

            /**
             * Enable the Secondary Container For Further Work After Address Fetching
             */

            if (resultCode == Constants.SUCCESS_RESULT) {

                String address = "";

                address = address + ((resultData.getString(Constants.RESULT_DATA_KEY) != null) ? resultData.getString(Constants.RESULT_DATA_KEY) + " " : "");

                address = address + ((resultData.getString(Constants.LOCATION_DATA_AREA) != null) ? resultData.getString(Constants.LOCATION_DATA_AREA) + " " : "");

                address = address + ((resultData.getString(Constants.LOCATION_DATA_STREET) != null) ? resultData.getString(Constants.LOCATION_DATA_STREET) + " " : "");

                address = address + ((resultData.getString(Constants.LOCATION_DATA_CITY) != null) ? resultData.getString(Constants.LOCATION_DATA_CITY) + " " : "");

                if (resultData.getInt(Constants.VARIABLES) == REQUEST_CODE_PICK) {
                    displayAddressOutput(address, REQUEST_CODE_PICK);
                } else {
                    displayAddressOutput(address, REQUEST_CODE_DROP);
                }

            }

        }

    }


    void setAddressFields(int param, List<Address> addresses) {
        if (addresses != null) {
            if (param == REQUEST_CODE_PICK || param == 0) {
                myCity = "";
            }
            if (param == 0) {
                return;
            }
        }
        try {
            displayAddressOutput(addresses.get(0).getAddressLine(0), param);
        } catch (Exception e) {

        }

    }


    /**
     * ****************************************** !Webservices
     ****************************************/

    class downloadCars extends AsyncTask<Integer, Integer, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Integer... params) {

            for (int i = 0; i < getCarList.size(); i++) {

                if (!MyApplication.carImages.containsKey(getCarList.get(i).getId())) {

                    final String carId = getCarList.get(i).getId();
                    final String carUrl = getCarList.get(i).getMap_car_image();

                    Bitmap carImg = getBitmapFromURL(carUrl);
                    try {
                        MyApplication.carImages.put(carId, carImg);
                    } catch (Exception e) {

                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            taxiListingAdapter.updateTaxisListing(getCarList, userLatitude, userLongitude);
            cardsLoaded = true;
        }
    }

    void getAddressFromAPI(final int param) {

        double lat = param == 0 ? userLatitude : tlat;
        double lng = param == 0 ? userLongitude : tlng;

        String address = String
                .format(Locale.ENGLISH, "http://maps.googleapis.com/maps/api/geocode/json?latlng=%1$f,%2$f&sensor=true&language="
                        + Locale.getDefault().getCountry(), lat, lng);
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(getApplicationContext(), address, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    JSONObject jsonObject = new JSONObject(new String(responseBody));
                    ArrayList<Address> retList = new ArrayList<Address>();
                    if ("OK".equalsIgnoreCase(jsonObject.getString("status"))) {
                        JSONArray results = jsonObject.getJSONArray("results");
                        for (int i = 0; i < results.length(); i++) {
                            JSONObject result = results.getJSONObject(i);
                            String indiStr = result.getString("formatted_address");
                            Address addr = new Address(Locale.getDefault());
                            addr.setAddressLine(0, indiStr);
                            retList.add(addr);
                            JSONArray addressComponents = results.optJSONObject(i).optJSONArray("address_components");
                            for (int j = 0; j < addressComponents.length(); j++) {
                                JSONArray types = addressComponents.optJSONObject(j).optJSONArray("types");
                                for (int k = 0; k < types.length(); k++) {
                                    String type = types.optString(k);
                                    if (type.equals("locality")) {
                                        addr.setLocality(addressComponents.optJSONObject(j).optString("long_name"));
                                        if (retList != null) {
                                            setAddressFields(param, retList);
                                        }
                                        return;
                                    }
                                }
                            }
                        }
                    }

                    if (retList != null) {
                        setAddressFields(param, retList);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }


    /**
     * For Nokia *
     */

    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            if (!onetimeload) {
                                mLastLocation = task.getResult();
                                dataWorker.setPickLocation(mLastLocation);
                                userLatitude = mLastLocation.getLatitude();
                                userLongitude = mLastLocation.getLongitude();
                                onetimeload = true;
                            }
                            //changeMap(REQUEST_CODE_PICK, 0);
                        } else {
                            showSnackbar(getString(R.string.no_location_detected));
                        }
                    }
                });
    }

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    /**
     * Shows a {@link Snackbar} using {@code text}.
     *
     * @param text The Snackbar text.
     */
    private void showSnackbar(final String text) {
        View container = findViewById(R.id.main_activity_container);
        if (container != null) {
            Snackbar.make(container, text, Snackbar.LENGTH_LONG).show();
        }
    }

    /**
     * Shows a {@link Snackbar}.
     *
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * @param actionStringId   The text of the action item.
     * @param listener         The listener associated with the Snackbar action.
     */
    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(CustomerHomeActivity.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {


            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            startLocationPermissionRequest();
                        }
                    });

        } else {
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest();
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.

            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                getLastLocation();
                Intent toSplash = new Intent(getApplicationContext(), SplashActivity.class);
                startActivity(toSplash);
                finish();

            } else {

                showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }
    }


}

