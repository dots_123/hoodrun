package com.us.hoodruncustomer.commonwork;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.authentication.LoginActivity;
import com.us.hoodruncustomer.configure.Configure;
import com.us.hoodruncustomer.custom.CircularImageView;
import com.us.hoodruncustomer.custom.MyEditText;
import com.us.hoodruncustomer.exceptionhandler.ExceptionHandler;
import com.us.hoodruncustomer.parser.ConnectionDetector;
import com.us.hoodruncustomer.parser.Parser;
import com.us.hoodruncustomer.home.CustomerHomeActivity;
import com.us.hoodruncustomer.registration.Verification;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@SuppressWarnings("deprecation")
public class ProfileActivity extends SimpleActivity {

    private static int CameraRequestPermissionCode = 1331;
    MyEditText mEdtName, mEdtEmail,
            mEdtMobile_Number;
    ImageView mImgNext, mImgBack;
    CircularImageView mImageAdd;
    String token_code, parsresp, user, id_type, id;
    ConnectionDetector cd;
    Button change_btn, edit_btn;
    Toolbar toolbar;
    TextView headertext;
    Dialog dialog2;
    String image_code = "";
    String old_password, new_password, cpw_status, cpw_message;
    Drawable wrong;
    int flag;
    EditText oldPw;
    EditText newPw;
    EditText confirmPw;
    Activity context;
    Dialog dialog;
    Dialog dialogit;
    Dialog dialogA;
    Dialog retry;
    private Uri mCropImageUri, mUri = null;

    //validation input data
    public static boolean isValidWord(String w) {

        return w.matches("^[A-Za-z\\s]+$");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_view);

        MyApplication.activity = ProfileActivity.this;
        createView();
        context = this;

        edit_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                editProfile();
            }
        });

        mImageAdd.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                selectImage();
            }
        });


        mImgNext.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });


        mEdtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {

                Validation.hasText(mEdtName);

                if (!isValidWord(s.toString())) {
                    mEdtName.setError("Invalid name", wrong);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });


        mEdtEmail.addTextChangedListener(new TextWatcher() {
            // after every change has been made to this editText, we would like
            // to check validity
            @Override
            public void afterTextChanged(Editable s) {
                Validation.isEmailAddress(mEdtEmail, true);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }
        });


        mEdtMobile_Number.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                Validation.isPhoneNumber(mEdtMobile_Number, false);

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }
        });


        mImgNext.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                    /*
					 * Validation class will check the error and display the error
					 * on respective fields but it won't resist the form submission,
					 * so we need to check again before submit
					 */
                if (checkValidation()) {
                    //submitForm();

                    if (cd.isConnectingToInternet()) {

                        mEdtName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick, 0);
                        mEdtEmail.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick, 0);
                        mEdtMobile_Number.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick, 0);

                        mAppManager.saveCustomerName(mEdtName.getText().toString());
                        mAppManager.saveMobileNumber(mEdtMobile_Number.getText().toString().trim());
                        mAppManager.saveEmail(mEdtEmail.getText().toString().trim());

                        showProgress();
                        if (mUri != null) {

                            imageProcess();
                        }
                        updateProfile();

                    } else {
                        Toast.makeText(getApplicationContext(), "No internet connection available.", Toast.LENGTH_LONG).show();
                        System.exit(0);
                    }
                } else
                    Toast.makeText(ProfileActivity.this, "Please enter all fields.", Toast.LENGTH_LONG).show();
            }
        });

    }

    private void createView() {

        flag = 0;
        wrong = getResources().getDrawable(R.drawable.wrong);
        wrong.setBounds(new Rect(0, 0, wrong.getIntrinsicWidth(), wrong.getIntrinsicHeight()));


        cd = new ConnectionDetector(ProfileActivity.this);

        user = mAppManager.getuser();
        mAppManager.savePrevActivity("HIS", "");

        token_code = mAppManager.getTokenCode();

        font_path = "fonts/opensans.ttf";

        tf = Typeface.createFromAsset(this.getAssets(), font_path);


        toolbar = (Toolbar) findViewById(R.id.toolbar2);
        if (toolbar != null) {

            int color = Color.parseColor("#ffffff");
            toolbar.setTitleTextColor(color);
            toolbar.setNavigationIcon(R.drawable.back_arrow);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);


            headertext = (TextView) findViewById(android.R.id.text1);
            edit_btn = (Button) findViewById(android.R.id.button1);
            headertext.setTypeface(tf, Typeface.BOLD);

            edit_btn.setVisibility(View.INVISIBLE);

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(ProfileActivity.this));


            toolbar.setNavigationOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (flag == 1)
                        setFields();
                    else {


                            Intent in = new Intent(ProfileActivity.this, CustomerHomeActivity.class);
                            startActivity(in);
                            finish();

                    }
                }

            });

        }

        setupView();
    }

    private boolean checkValidation() {
        boolean ret = true;

        if (!Validation.hasText(mEdtName)) {
            ret = false;

        }
        if (!Validation.isEmailAddress(mEdtEmail, true)) {

            ret = false;
        }
        if (!Validation.isPhoneNumber(mEdtMobile_Number, false)) {

            ret = false;
        }

        return ret;
    }

    private void setupView() {

        mEdtEmail = (MyEditText) findViewById(R.id.edt_email);
        mEdtName = (MyEditText) findViewById(R.id.edt_name);
        mEdtMobile_Number = (MyEditText) findViewById(R.id.edt_mobile_number);
        mEdtEmail.setTypeface(tf, Typeface.BOLD);
        mEdtName.setTypeface(tf, Typeface.BOLD);
        mEdtMobile_Number.setTypeface(tf, Typeface.BOLD);
        mImgNext = (ImageView) findViewById(R.id.img_update_btn);
        mImgBack = (ImageView) findViewById(R.id.img_back_btn);
        mImageAdd = (CircularImageView) findViewById(R.id.img_add);
        change_btn = (Button) findViewById(R.id.btn_change_pw);
        change_btn.setVisibility(View.GONE);
        mImgBack.setVisibility(View.GONE);
        mImgNext.setVisibility(View.GONE);

        viewProfile();


    }

    private void viewProfile() {

        if (cd.isConnectingToInternet()) {


            if (user.equals("driver")) {

                id_type = "driver_id";
                id = mAppManager.getDriverID_Number();
                showProgress();
                attemptToGetProfile(Configure.URL_view_profile_driver);

            } else {

                id_type = "customer_id";
                id = mAppManager.getCustomerID();
                showProgress();
                attemptToGetProfile(Configure.URL_view_profile_customer);

            }

        } else DialogAlert("No Internet !", "Check your connection and try again");

    }

    private void imageProcess() {
        // TODO Auto-generated method stub


        InputStream iStream = null;
        try {
            iStream = getContentResolver().openInputStream(mUri);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            byte[] inputData = getBytes(iStream);
            image_code = Base64.encodeToString(inputData, Base64.NO_WRAP);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void setFields() {

        flag = 0;
        if (user.equals("driver")) {

            headertext.setText("Profile");
            change_btn.setVisibility(View.VISIBLE);

            change_btn.setClickable(true);

            edit_btn.setVisibility(View.GONE);

            edit_btn.setClickable(false);
            mImgBack.setVisibility(View.GONE);
            mImgNext.setVisibility(View.GONE);

            mImgBack.setClickable(false);
            mImgNext.setClickable(false);

        } else {

            headertext.setText("Profile");
            change_btn.setVisibility(View.VISIBLE);

            change_btn.setClickable(true);

            edit_btn.setVisibility(View.VISIBLE);

            edit_btn.setClickable(true);
            mImgBack.setVisibility(View.GONE);
            mImgNext.setVisibility(View.GONE);

            mImgBack.setClickable(false);
            mImgNext.setClickable(false);
        }

        Log.d("iiiiiiiii", "" + Parser.getCustomer_image());
        mEdtName.setText(nameInput(Parser.getCustomer_name()));
        mEdtName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        //mEdtName.setKeyListener(null);
        editTextDisabler(mEdtName);
        mEdtName.setTypeface(tf, Typeface.BOLD);
        mEdtEmail.setText(Parser.getCustomer_email());
        //mEdtEmail.setKeyListener(null);
        editTextDisabler(mEdtEmail);
        mEdtEmail.setTypeface(tf, Typeface.BOLD);
        mEdtEmail.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        mEdtMobile_Number.setText(Parser.getCustomer_mob_no());
        //mEdtMobile_Number.setKeyListener(null);
        editTextDisabler(mEdtMobile_Number);
        mEdtMobile_Number.setTypeface(tf, Typeface.BOLD);
        mEdtMobile_Number.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);


        if (Parser.getCustomer_image() != null && !Parser.getCustomer_image().isEmpty()) {

            Glide.with(ProfileActivity.this).load(Uri.parse(Parser.getCustomer_image()))
//						 .centerCrop()
                    .into(mImageAdd);
        } else mImageAdd.setImageResource(R.drawable.placeholder);

        mImageAdd.setClickable(false);


        change_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialogChangePw();
            }

        });

    }

    private void editProfile() {

        flag = 1;
        edit_btn.setVisibility(View.GONE);
        edit_btn.setClickable(false);
        mImgBack.setVisibility(View.VISIBLE);
        mImgNext.setVisibility(View.VISIBLE);
        change_btn.setVisibility(View.GONE);
        change_btn.setClickable(false);

        mImgBack.setClickable(true);
        mImgNext.setClickable(true);
        mImageAdd.setClickable(true);

        headertext.setText("Edit Profile");
        //getSupportActionBar().setTitle("EDIT");


        editTextEnabler(mEdtName);
        editTextEnabler(mEdtMobile_Number);
        editTextEnabler(mEdtEmail);
        mEdtName.requestFocus();
        mEdtName.setFocusable(true);
        mEdtName.setSelection(mEdtName.getText().length());


        mEdtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                Validation.hasText(mEdtName);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });


        mEdtEmail.addTextChangedListener(new TextWatcher() {
            // after every change has been made to this editText, we would like
            // to check validity
            @Override
            public void afterTextChanged(Editable s) {
                Validation.isEmailAddress(mEdtEmail, true);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }
        });


        mEdtMobile_Number.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                Validation.isPhoneNumber(mEdtMobile_Number, false);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }
        });


        mImgBack.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                setFields();
            }
        });

    }

    private void editTextEnabler(EditText mEditText) {
        mEditText.setFocusable(true);
        mEditText.setFocusableInTouchMode(true);

    }

    private void editTextDisabler(EditText mEditText) {

        mEditText.setFocusable(false);
        mEditText.setFocusableInTouchMode(false);

    }

    private void dialogChangePw() {


        dialog2 = new Dialog(ProfileActivity.this, R.style.Theme_Dialog);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog2.getWindow();
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog2.setContentView(R.layout.change_password);
        dialog2.setCancelable(false);
        dialog2.setCanceledOnTouchOutside(false);
        dialog2.show();
        LinearLayout mLinCancel = (LinearLayout) dialog2.findViewById(R.id.lay_btn_Cancel);

        LinearLayout mLinProceed = (LinearLayout) dialog2.findViewById(R.id.lay_btn_Proceed);

        oldPw = (EditText) dialog2.findViewById(R.id.edt_old_password);

        newPw = (EditText) dialog2.findViewById(R.id.edt_new_password);

        confirmPw = (EditText) dialog2.findViewById(R.id.edt_confirm_password);


        mLinCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                dialog2.dismiss();


            }
        });
        mLinProceed.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (nilCheck(oldPw, newPw, confirmPw) && checkPw(oldPw, newPw, confirmPw)) {
                    old_password = oldPw.getText().toString().trim();
                    new_password = newPw.getText().toString().trim();
                    changePw();
                }

            }
        });

    }

    private void selectImage() {

        if (checkPermission())
            CropImage.startPickImageActivity(this);
        else
            requestPermission();

    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(ProfileActivity.this, new String[]
                {
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                }, CameraRequestPermissionCode);

    }

    public boolean checkPermission() {

        int readPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int writePermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        return readPermissionResult == PackageManager.PERMISSION_GRANTED &&
                writePermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);
            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            boolean requirePermissions = false;
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                requirePermissions = true;
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);

            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                // ((ImageView) findViewById(R.id.quick_start_cropped_image)).setImageURI(result.getUri());
                mImageAdd.setImageURI(result.getUri());
                mUri = result.getUri();

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }

        if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                // ((ImageView) findViewById(R.id.quick_start_cropped_image)).setImageURI(result.getUri());

                if (result.getUri() != null) {
                    mImageAdd.setImageURI(result.getUri());
                    mUri = result.getUri();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if(requestCode==0) {
            if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // required permissions granted, start crop image activity
                startCropImageActivity(mCropImageUri);
            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
        else if(requestCode==CameraRequestPermissionCode) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                // required permissions granted, open camera activity
                CropImage.startPickImageActivity(this);
            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    protected void changePw() {

        if (cd.isConnectingToInternet()) {

            if (mAppManager.getuser().equals("customer")) {
                showProgress();
                changePassword(Configure.URL_change_password_customer);
            } else {
                showProgress();
                changePassword(Configure.URL_driver_changepw);
            }
        } else
            Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_LONG).show();
    }

    private boolean nilCheck(EditText oldPwd, EditText newPwd, EditText changePwd) {

        boolean nil_stat = false;
        if (oldPwd.getText().toString().length() == 0 || newPwd.getText().toString().length() == 0 || changePwd.getText().toString().length() == 0) {

            if (oldPwd.getText().toString().length() == 0)
                Toast.makeText(getApplicationContext(), "Please enter your old password", Toast.LENGTH_SHORT).show();
            else if (newPwd.getText().toString().length() == 0)
                Toast.makeText(getApplicationContext(), "Please enter new password", Toast.LENGTH_SHORT).show();
            else if (changePwd.getText().toString().length() == 0)
                Toast.makeText(getApplicationContext(), "Please confirm your new password", Toast.LENGTH_SHORT).show();

            nil_stat = false;

        } else if (newPwd.getText().toString().length() < 6 || changePwd.getText().toString().length() < 6) {

            if (newPwd.getText().toString().length() < 6)
                Toast.makeText(getApplicationContext(), "Password should contain more than 6 characters", Toast.LENGTH_SHORT).show();

            nil_stat = false;

        } else nil_stat = true;

        return nil_stat;

    }

    private boolean checkPw(EditText oldPwd, EditText newPwd, EditText changePwd) {

        boolean nil_stat = false;
        if (newPwd.getText().toString().equals(oldPwd.getText().toString()) || !newPwd.getText().toString().equals(changePwd.getText().toString())) {
            if (!newPwd.getText().toString().matches("^[a-zA-Z0-9!@#$%^&*()]*$"))
                Toast.makeText(getApplicationContext(), "Only !@#$%^&*() are allowed", Toast.LENGTH_SHORT).show();
            else if (newPwd.getText().toString().equals(oldPwd.getText().toString()))
                Toast.makeText(getApplicationContext(), "Your old password should not be your new password", Toast.LENGTH_SHORT).show();
            else if (!newPwd.getText().toString().equals(changePwd.getText().toString())) {
                Toast.makeText(getApplicationContext(), "Password missmatch, Please confirm again", Toast.LENGTH_SHORT).show();
                changePwd.setText("");
            }

            nil_stat = false;

        } else nil_stat = true;

        return nil_stat;
    }

    private void setEmpty() {
        oldPw.setText("");
        newPw.setText("");
        confirmPw.setText("");
    }

    private void Dialog() {

        dialog = new Dialog(ProfileActivity.this, R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.dialog_onebutton);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView mTitle = (TextView) dialog.findViewById(R.id.text_title);
        TextView mDialogBody = (TextView) dialog.findViewById(R.id.txt_dialog_body);
        TextView mOK = (TextView) dialog.findViewById(R.id.txt_dialog_ok);

        mTitle.setText("VERIFICATION");
        if (mAppManager.getOtpDestiantion().equals("mobile"))
            mDialogBody.setText("The verification code has been sent to your registered mobile number");
        else
            mDialogBody.setText("The verification code has been sent to your registered email address");
        mOK.setText("OK");
        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                dialog.dismiss();

                mAppManager.savePrevActivity("VERIFY", "PROFILE");
                Intent myintent = new Intent(ProfileActivity.this,
                        Verification.class);
                startActivity(myintent);
                //finish();

            }
        });

    }

    @Override
    public void onBackPressed() {
        if (flag == 1)
            setFields();
        else {


                Intent in = new Intent(ProfileActivity.this, CustomerHomeActivity.class);
                startActivity(in);
                finish();

        }
    }

    String nameInput(String name) {

        String fName = name.substring(0, 1).toUpperCase(Locale.getDefault()) + name.substring(1);
        return fName;

    }

    private void DialogIT() {

        dialogit = new Dialog(ProfileActivity.this, R.style.Theme_Dialog);
        dialogit.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialogit.getWindow();
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialogit.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogit.setContentView(R.layout.dialog_onebutton);
        dialogit.setCancelable(false);
        dialogit.setCanceledOnTouchOutside(false);
        dialogit.show();

        TextView mTitle = (TextView) dialogit.findViewById(R.id.text_title);
        TextView mDialogBody = (TextView) dialogit.findViewById(R.id.txt_dialog_body);
        TextView mOK = (TextView) dialogit.findViewById(R.id.txt_dialog_ok);

        mTitle.setText("ALERT");

        mDialogBody.setText("You have already logged in from other device" + "\n" + "Please login to continue..");

        mOK.setText("OK");
        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                dialogit.dismiss();
                Intent myintent = new Intent(ProfileActivity.this, LoginActivity.class);
                startActivity(myintent);
                finish();

            }
        });

    }

    private void DialogAlert(String title, String body) {

        dialogA = new Dialog(ProfileActivity.this, R.style.Theme_Dialog);
        dialogA.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialogA.getWindow();
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialogA.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        dialogA.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogA.setContentView(R.layout.dialog_onebutton);
        dialogA.setCancelable(false);
        dialogA.setCanceledOnTouchOutside(false);
        dialogA.show();

        TextView mTitle = (TextView) dialogA.findViewById(R.id.text_title);
        TextView mDialogBody = (TextView) dialogA.findViewById(R.id.txt_dialog_body);
        TextView mOK = (TextView) dialogA.findViewById(R.id.txt_dialog_ok);

        mTitle.setText(title);
        mDialogBody.setText(body);
        mOK.setText("OK");


        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogA.cancel();

                finish();
            }
        });

    }

    private void attemptToGetProfile(String url) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        hideProgress();

                        System.out.println("PROFILE===" + response);

                        parsresp = Parser.getProfileView(response);

                        if (!TextUtils.isEmpty(parsresp)) {
                            if (parsresp.equals("TRUE")) {

                                setFields();
                            } else if (parsresp.equals("FALSE")) {

                                if (Parser.getProfileViewStatus().equals("Invalid token code"))
                                    DialogIT();
                                //keText(getApplicationContext(), ""+Parser.getProfileViewStatus(),Toast.LENGTH_LONG).show();

                            }

                        } else {

                            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
                            System.exit(0);
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        retryDialog(1);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put(id_type, id);
                params.put("token_code", token_code);


                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void updateProfile() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_update_profile_customer,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        hideProgress();

                        System.out.println("UPDATEPROFILE===" + response);
                        JSONObject dataObject = null;
                        try {
                            dataObject = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        cpw_status = dataObject.optString("status");
                        cpw_message = dataObject.optString("message");

                        if (!TextUtils.isEmpty(cpw_status)) {


                            if (cpw_status.equals("TRUE")) {

                                //Toast.makeText(getApplicationContext(), ""+cpw_message,Toast.LENGTH_LONG).show();
                                switch (cpw_message) {
                                    case "OTP successfully sent to your new mobile number":
                                        mAppManager.saveOtpDestiantion("mobile");
                                        mAppManager.saveMobileNumber(mEdtMobile_Number.getText().toString());
                                        Dialog();
                                        break;
                                    case "OTP successfully sent to your new email":
                                        mAppManager.saveOtpDestiantion("email");
                                        mAppManager.saveEmail(mEdtEmail.getText().toString());
                                        Dialog();
                                        break;
                                    default:
                                        Toast.makeText(getApplicationContext(), "" + cpw_message, Toast.LENGTH_LONG).show();


                                        flag = 0;
                                        createView();
                                        break;
                                }


                            } else {
                                Toast.makeText(getApplicationContext(), "" + cpw_message, Toast.LENGTH_LONG).show();
                            }
                        } else {


                            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
                            System.exit(0);
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        retryDialog(2);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("name", mEdtName.getText().toString());
                params.put("email", mEdtEmail.getText().toString());
                params.put("mobile_number", mEdtMobile_Number.getText().toString());
                params.put("profile_picture", image_code);
                params.put("customer_id", mAppManager.getCustomerID());
                params.put("token_code", mAppManager.getTokenCode());


                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);

    }

    private void changePassword(String url) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        hideProgress();

                        System.out.println("CHANGEPASSWORD===" + response);
                        JSONObject dataObject = null;
                        try {
                            dataObject = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        cpw_status = dataObject.optString("status");
                        cpw_message = dataObject.optString("message");

                        if (!TextUtils.isEmpty(cpw_status)) {
                            if (cpw_status.equals("TRUE")) {


                                dialog2.dismiss();
                                edit_btn.setVisibility(View.VISIBLE);
                                edit_btn.setClickable(true);
                                headertext.setText("Profile");

                                mEdtName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                                mEdtEmail.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                                mEdtMobile_Number.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

                                Toast.makeText(getApplicationContext(), "" + cpw_message, Toast.LENGTH_LONG).show();
                                setEmpty();
                                if (mAppManager.getuser().equals("driver")) {
                                    edit_btn.setClickable(false);
                                    edit_btn.setVisibility(View.INVISIBLE);
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "" + cpw_message, Toast.LENGTH_LONG).show();
                                setEmpty();
                            }


                        } else {

                            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
                            System.exit(0);
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgress();
                        retryDialog(3);
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("token_code", token_code);
                params.put(id_type, id);
                params.put("old_password", old_password);
                params.put("new_password", new_password);

                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.activityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.activityPaused();
    }

    private void retryDialog(final int i) {

        retry = new Dialog(ProfileActivity.this, R.style.Theme_Dialog);
        retry.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = retry.getWindow();
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        retry.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        retry.setContentView(R.layout.thaks_dialog_twobutton);
        retry.setCancelable(false);
        retry.setCanceledOnTouchOutside(false);
        retry.show();

        TextView mTitle = (TextView) retry.findViewById(R.id.txt_title);
        mTitle.setText("Error!");
        mTitle.setTypeface(tf, Typeface.BOLD);
        TextView mDialogBody = (TextView) retry.findViewById(R.id.txt_dialog_body);
        mDialogBody.setText("The server is taking too long to respond OR something is wrong with your internet connection.");
        LinearLayout mOK = (LinearLayout) retry.findViewById(R.id.lin_Ok);
        LinearLayout mCancel = (LinearLayout) retry.findViewById(R.id.lin_cancel);
        TextView mRetry = (TextView) retry.findViewById(R.id.txt_dialog_ok);
        TextView mExit = (TextView) retry.findViewById(R.id.txt_dialog_cancel);
        mRetry.setText("RETRY");
        mExit.setText("EXIT");

        mOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


                retry.dismiss();

                switch (i) {
                    case 1:
                        viewProfile();
                        break;
                    case 2:
                        if (cd.isConnectingToInternet()) {
                            showProgress();
                            updateProfile();
                        } else {
                            Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_SHORT).show();
                            System.exit(0);
                        }
                        break;
                    case 3:
                        changePw();
                        break;
                    default:
                        break;

                }


            }
        });

        mCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                retry.dismiss();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });

    }


}
