package com.us.hoodruncustomer.parser;


import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.us.hoodruncustomer.model.Analytics;
import com.us.hoodruncustomer.model.AnalyticsData;
import com.us.hoodruncustomer.model.Card;
import com.us.hoodruncustomer.model.Cities;
import com.us.hoodruncustomer.model.Driver;
import com.us.hoodruncustomer.model.History;
import com.us.hoodruncustomer.model.ReferredDrivers;
import com.us.hoodruncustomer.model.TransactionsHistoryDao;
import com.us.hoodruncustomer.model.WalletInfoDao;

import android.util.Log;


public class Parser {

    static String reg_status_message = null;


    static String registerstatus;

    public static String Registration(String data) {
        try {

            JSONObject dataObject = new JSONObject(data);

            registerstatus = dataObject.optString("status");

            if (registerstatus.equals("TRUE")) {

                reg_status_message = dataObject.optString("message").toString();
                customer_id = dataObject.optString("customer_id").toString();
                profile_status = dataObject.optString("profile_status").toString();
                Log.d("Inside true", "" + reg_status_message);
            } else {
                registerstatus = "FALSE";
                reg_status_message = dataObject.optString("message").toString();
                profile_status = dataObject.optString("profile_status").toString();
                Log.d("Inside false", "" + reg_status_message);

            }
        } catch (Exception e) {

        }
        return registerstatus;
    }





    public static String getRegStatus() {
        return reg_status_message;
    }


    public static String getCities_status() {
        return cities_status;
    }

    public static void setCities_status(String cities_status) {
        Parser.cities_status = cities_status;
    }

    public static String getCities_status_message() {
        return cities_status_message;
    }

    public static void setCities_status_message(String cities_status_message) {
        Parser.cities_status_message = cities_status_message;
    }

    public static ArrayList<Cities> getCities() {
        return cities;
    }

    public static void setCities(ArrayList<Cities> cities) {
        Parser.cities = cities;
    }



    static String refer_status;
    static String refer_status_message;

    public static String getRefer_status() {
        return refer_status;
    }

    public static void setRefer_status(String refer_status) {
        Parser.refer_status = refer_status;
    }

    public static String getRefer_status_message() {
        return refer_status_message;
    }

    public static void setRefer_status_message(String refer_status_message) {
        Parser.refer_status_message = refer_status_message;
    }

    public static String referFriends(String data) {
        try {

            JSONObject cobj = new JSONObject(data);
            refer_status = cobj.getString("status");

            if (refer_status.equals("true")) {

                refer_status_message = cobj.getString("message");

            } else if (refer_status.equals("false")) {
                JSONObject error = new JSONObject(data);
                refer_status_message = error.getString("message");
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return refer_status;
    }




    static String cities_status;
    static String cities_status_message;
    static ArrayList<Cities> cities ;



    static Cities city;
    public static String Cities(String data) {
        try {

            JSONObject cobj = new JSONObject(data);
            cities_status = cobj.getString("status");

            if (cities_status.equals("true")) {

                JSONArray jArray = cobj.optJSONArray("data");

                System.out.println("LLLLLLL " + jArray.length());
                cities = new ArrayList<Cities>();
                for (int i = 0; i < jArray.length(); i++) {
                    city = new Cities();
                    System.out.println("inside for loop ");
                    JSONObject obj = jArray.getJSONObject(i);

                    city.setId(obj.optInt("id"));
                    city.setCity(obj.optString("city"));
                    city.setLatitude(obj.optString("latitude"));
                    city.setLongitude(obj.optString("longitude"));
                    city.setMap_name(obj.optString("map_name"));

                    cities.add(city);

                }

                System.out.println("CardList:" + cities.size());
            } else if (cities_status.equals("false")) {
                JSONObject error = new JSONObject(data);
                cities_status_message = error.getString("message");
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return cities_status;
    }



    private static String login_status;
    private static String login_status_message;
    private static String profile_status;
    public static  String customer_id;
    private static String user_type;
    private static String driver_id;
    private static String driver_status;
    private static String token_code;
    private static String mob_no;
    private static String name;
    private static String isride_message;
    private static String isride_status;


    public static String getMob_no() {
        return mob_no;
    }


    public static String getName() {
        return name;
    }


    public static String login(String data) {


        try {

            JSONObject dataObject = new JSONObject(data);

            login_status = dataObject.optString("status");

            if (login_status.equalsIgnoreCase("TRUE")) {

                user_type = dataObject.optString("user_type").toString();
                if (user_type.equals("1")) {
                    customer_id = dataObject.optString("customer_id").toString();
                    profile_status = dataObject.optString("profile_status").toString();
                } else {
                    driver_id = dataObject.optString("driver_id").toString();

                    driver_status = dataObject.optString("driver_status").toString();
                }
                name = dataObject.getString("name");
                mob_no = dataObject.optString("mobile_number");
                token_code = dataObject.optString("token_code").toString();

                JSONObject rideObject = dataObject.optJSONObject("ride_status");

                if(rideObject != null){
                    isride_message = rideObject.optString("message");
                    isride_status = rideObject.optString("status");
                }

                System.out.println("111111===== " + customer_id);
                login_status_message = dataObject.optString("message").toString();
                Log.d("LOGIN-Inside true", "" + login_status_message);
            } else {
                login_status = "FALSE";
                profile_status = dataObject.optString("profile_status").toString();
                login_status_message = dataObject.optString("message").toString();
                Log.d("LOGIN-Inside false", "" + login_status_message);

            }
        } catch (Exception e) {

        }


        return login_status;

    }

    public static String getDriver_status() {
        return driver_status;
    }


    public static String getUser_type() {
        return user_type;
    }


    public static String getDriver_id() {
        return driver_id;
    }


    public static String getGetProfile_status_message() {
        return getProfile_status_message;
    }


    private static String verification_status;
    private static String verification_status_message;

    public static String verification(String data) {


        try {

            JSONObject dataObject = new JSONObject(data);
            verification_status = dataObject.optString("status").toString();
            verification_status_message = dataObject.optString("message").toString();
            token_code = dataObject.optString("token_code").toString();
            customer_id = dataObject.optString("customer_id").toString();
            mob_no = dataObject.optString("mobile_number");
            name = dataObject.getString("name");

        } catch (Exception e) {

        }

        return verification_status;

    }


    private static String resend_status;

    public static String getResend_status() {
        return resend_status;
    }


    public static String getResend_status_message() {
        return resend_status_message;
    }


    private static String resend_status_message;

    public static String resendOTP(String data) {


        try {

            JSONObject dataObject = new JSONObject(data);

            resend_status = dataObject.optString("status").toString();
            resend_status_message = dataObject.optString("message").toString();

        } catch (Exception e) {

        }

        return resend_status;

    }


    public static String getAdd_card_status() {
        return add_card_status;
    }


    public static String getAdd_card_status_message() {
        return add_card_status_message;
    }


    public static String getCard_no() {
        return card_no;
    }


    private static String add_card_status;
    private static String add_card_status_message;
    private static String card_no;

    public static String addCard(String data) {


        try {

            JSONObject dataObject = new JSONObject(data);

            add_card_status = dataObject.optString("status").toString();
            add_card_status_message = dataObject.optString("message").toString();
            if (add_card_status.equals("TRUE")) {
                card_no = dataObject.optString("card_no");
                profile_status = dataObject.optString("Profile_status");
                customer_id = dataObject.optString("customer_id");
            }

        } catch (Exception e) {

        }

        return add_card_status;

    }

    private static String getProfile_status;
    private static String getProfile_status_message;
    private static String customer_name;
    private static String customer_email;
    private static String customer_mob_no;
    private static String customer_image;

    public static String getProfileView(String data) {


        try {

            JSONObject dataObject = new JSONObject(data);

            getProfile_status = dataObject.optString("status");
            getProfile_status_message = dataObject.optString("message");
            customer_name = dataObject.optString("name");
            customer_email = dataObject.optString("email");
            customer_mob_no = dataObject.optString("mobile_number");
            customer_image = dataObject.optString("profile_picture");
        } catch (Exception e) {

        }

        return getProfile_status;

    }

    private static String mob_no_status;
    private static String mob_no_status_message;
    private static String person_name;

    public static String getShare_id() {
        return share_id;
    }

    public static void setShare_id(String share_id) {
        Parser.share_id = share_id;
    }

    private static String share_id;
    private static String person_mob_no;


    public static String shareNumber(String data) {


        try {

            JSONObject dataObject = new JSONObject(data);

            mob_no_status = dataObject.optString("status");

            mob_no_status_message = dataObject.optString("message");

            if (mob_no_status.equalsIgnoreCase("TRUE")) {

                JSONObject obj = dataObject.getJSONObject("data");

                share_id = obj.getString("share_id");

                person_mob_no = obj.getString("mobile_number");

                System.out.println("inside trueeeeee person " + person_name);

            } else {
                mob_no_status = dataObject.optString("status");

                mob_no_status_message = dataObject.optString("message");
            }
        } catch (Exception e) {

        }

        return mob_no_status;

    }



    private static boolean share_status;
    private static int share_status_code;
    private static String share_status_message;
    private static String share_driver_latitude;
    private static String share_driver_longitude;
    private static String customer_cancel_fee;
    private static String request_id;
    private static String driver_name;
    private static String current_driver_id;
    private static String current_driver_rating;
    private static String driver_profile_pic;
    private static String driver_vehicle_model;
    private static String driver_vehicle_number;

    public static String getRequest_id() {
        return request_id;
    }


    public static String getCustomer_cancel_fee() {
        return customer_cancel_fee;
    }


    private static ArrayList<String> invalid_numbers;


    /**
     * ========================== Node Implemented shareSubmit ====================================//
     * @param data
     * @return
     */

    public static void shareSubmitPUSH(String data) {

        try {
            JSONObject dataObjectmain = new JSONObject(data);

            share_driver_latitude = dataObjectmain.getString("driver_latitude");
            share_driver_longitude = dataObjectmain.getString("driver_longitude");
            customer_cancel_fee = dataObjectmain.getString("customer_cancel_fee");
            request_id = dataObjectmain.getString("request_id");

            driver_name = dataObjectmain.getString("driver_name");
            current_driver_id = dataObjectmain.getString("driver_id");
            driver_profile_pic = dataObjectmain.getString("driver_profile_pic");
            driver_vehicle_model = dataObjectmain.getString("driver_vehicle_model");
            driver_vehicle_number = dataObjectmain.getString("driver_vehicle_number");

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }










    public static boolean shareSubmit(String data) {
//{"status":true,"message":"success","code":-1,
// "data":{"driver_longitude":"75.8172148","request_id":"133","customer_cancel_fee":"15.00"}}





        try {

            JSONObject dataObject = new JSONObject(data);

            share_status = dataObject.optBoolean("status");

            share_status_message = dataObject.optString("message");

            share_status_code = dataObject.optInt("code");

            if (share_status) {

                JSONObject dataObjectmain = dataObject.getJSONObject("data");

                share_driver_latitude = dataObjectmain.getString("driver_latitude");
                share_driver_longitude = dataObjectmain.getString("driver_longitude");
                customer_cancel_fee = dataObjectmain.getString("customer_cancel_fee");
                request_id = dataObjectmain.getString("request_id");
                driver_name = dataObjectmain.getString("driver_name");
                current_driver_id = dataObjectmain.getString("driver_id");
                driver_profile_pic = dataObjectmain.getString("driver_profile_pic");
                driver_vehicle_model = dataObjectmain.getString("driver_vehicle_model");
                driver_vehicle_number = dataObjectmain.getString("driver_vehicle_number");
                current_driver_rating = dataObjectmain.getString("rating");
                System.out.println("inside trueeeeee lat long " + share_driver_latitude + share_driver_longitude);

            } else {
//
//                share_status_message = dataObject.optString("message");
//
//                JSONArray jArray = dataObject.optJSONArray("invalid_number");
//
//                for (int i = 0; i < jArray.length(); i++) {
//                    JSONObject obj = jArray.getJSONObject(i);
//                    invalid_numbers = new ArrayList<String>();
//                    invalid_numbers.add(obj.getString("mobile"));
//
//                    //System.out.println("array "+invalid_numbers[i]);
//                }
//
//                share_driver_latitude = dataObject.getString("driver_latitude");
//                share_driver_longitude = dataObject.getString("driver_longitude");
//                customer_cancel_fee = dataObject.getString("customer_cancel_fee");
//                request_id = dataObject.getString("request_id");
//                System.out.println("false share status");
            }
        } catch (Exception e) {

        }
        return share_status;

    }


    public static boolean isDriverRide_status() {
        return DriverRide_status;
    }

    public static void setDriverRide_status(boolean driverRide_status) {
        DriverRide_status = driverRide_status;
    }

    public static String getDriver_message() {
        return Driver_message;
    }

    public static void setDriver_message(String driver_message) {
        Driver_message = driver_message;
    }

    public static int getDriver_code() {
        return Driver_code;
    }

    public static void setDriver_code(int driver_code) {
        Driver_code = driver_code;
    }

    public static String getDriver_ride_id() {
        return Driver_ride_id;
    }

    public static void setDriver_ride_id(String driver_ride_id) {
        Driver_ride_id = driver_ride_id;
    }

    public static String getDriver_request_id() {
        return Driver_request_id;
    }

    public static void setDriver_request_id(String driver_request_id) {
        Driver_request_id = driver_request_id;
    }

    public static String getDriver_ride_status() {
        return Driver_ride_status;
    }

    public static void setDriver_ride_status(String driver_ride_status) {
        Driver_ride_status = driver_ride_status;
    }

    public static String getDriver_driver_id() {
        return Driver_driver_id;
    }

    public static void setDriver_driver_id(String driver_driver_id) {
        Driver_driver_id = driver_driver_id;
    }

    public static String getDriver_driver_name() {
        return Driver_driver_name;
    }

    public static void setDriver_driver_name(String driver_driver_name) {
        Driver_driver_name = driver_driver_name;
    }

    public static String getDriver_driver_phone() {
        return Driver_driver_phone;
    }

    public static void setDriver_driver_phone(String driver_driver_phone) {
        Driver_driver_phone = driver_driver_phone;
    }

    public static String getDriver_driver_vehicle() {
        return Driver_driver_vehicle;
    }

    public static void setDriver_driver_vehicle(String driver_driver_vehicle) {
        Driver_driver_vehicle = driver_driver_vehicle;
    }

    public static String getDriver_driver_profile_pic() {
        return Driver_driver_profile_pic;
    }

    public static void setDriver_driver_profile_pic(String driver_driver_profile_pic) {
        Driver_driver_profile_pic = driver_driver_profile_pic;
    }

    public static String getDriver_push_Status_Flag() {
        return Driver_push_Status_Flag;
    }

    public static void setDriver_push_Status_Flag(String driver_push_Status_Flag) {
        Driver_push_Status_Flag = driver_push_Status_Flag;
    }

    public static String getDriver_rating() {
        return Driver_rating;
    }

    public static void setDriver_rating(String driver_rating) {
        Driver_rating = driver_rating;
    }

    public static String getDriver_customer_pick_up_point_lat() {
        return Driver_customer_pick_up_point_lat;
    }

    public static void setDriver_customer_pick_up_point_lat(String driver_customer_pick_up_point_lat) {
        Driver_customer_pick_up_point_lat = driver_customer_pick_up_point_lat;
    }

    public static String getDriver_customer_pick_up_point_long() {
        return Driver_customer_pick_up_point_long;
    }

    public static void setDriver_customer_pick_up_point_long(String driver_customer_pick_up_point_long) {
        Driver_customer_pick_up_point_long = driver_customer_pick_up_point_long;
    }

    public static String getDriver_customer_pick_up_point_name() {
        return Driver_customer_pick_up_point_name;
    }

    public static void setDriver_customer_pick_up_point_name(String driver_customer_pick_up_point_name) {
        Driver_customer_pick_up_point_name = driver_customer_pick_up_point_name;
    }

    public static String getDriver_customer_destination_point_lat() {
        return Driver_customer_destination_point_lat;
    }

    public static void setDriver_customer_destination_point_lat(String driver_customer_destination_point_lat) {
        Driver_customer_destination_point_lat = driver_customer_destination_point_lat;
    }

    public static String getDriver_customer_destination_point_long() {
        return Driver_customer_destination_point_long;
    }

    public static void setDriver_customer_destination_point_long(String driver_customer_destination_point_long) {
        Driver_customer_destination_point_long = driver_customer_destination_point_long;
    }

    public static String getDriver_customer_destination_point_name() {
        return Driver_customer_destination_point_name;
    }

    public static void setDriver_customer_destination_point_name(String driver_customer_destination_point_name) {
        Driver_customer_destination_point_name = driver_customer_destination_point_name;
    }

    public static String getDriver_vehicle_type() {
        return Driver_vehicle_type;
    }

    public static void setDriver_vehicle_type(String driver_vehicle_type) {
        Driver_vehicle_type = driver_vehicle_type;
    }

    public static String getDriver_fare_multiplier() {
        return Driver_fare_multiplier;
    }

    public static void setDriver_fare_multiplier(String driver_fare_multiplier) {
        Driver_fare_multiplier = driver_fare_multiplier;
    }

    public static String getDriver_car_type() {
        return Driver_car_type;
    }

    public static void setDriver_car_type(String driver_car_type) {
        Driver_car_type = driver_car_type;
    }

    public static String getDriver_map_car_image() {
        return Driver_map_car_image;
    }

    public static void setDriver_map_car_image(String driver_map_car_image) {
        Driver_map_car_image = driver_map_car_image;
    }

    public static String getDriver_driver_latitude() {
        return Driver_driver_latitude;
    }

    public static void setDriver_driver_latitude(String driver_driver_latitude) {
        Driver_driver_latitude = driver_driver_latitude;
    }

    public static String getDriver_driver_longitude() {
        return Driver_driver_longitude;
    }

    public static void setDriver_driver_longitude(String driver_driver_longitude) {
        Driver_driver_longitude = driver_driver_longitude;
    }

    /**
     * Driver Accepted the Ride
     *
     * "status":"TRUE",
     "message":"Driver has accepted the ride",
     "code":2,
     "data":{
     "ride_id":32,
     "request_id":"142",
     "ride_status":2,
     "driver_id":"178",
     "driver_name":"sobia shaheen",
     "driver_phone":"07459658082",
     "driver_vehicle":"1",
     "driver_profile_pic":"http:\/\/ds412.projectstatus.co.uk\/opon\/uploads\/drivers\/images\/1489821858.jpg",
     "push_Status_Flag":"3",
     "rating":0,
     "customer_pick_up_point_lat":"26.871573046673948",
     "customer_pick_up_point_long":"75.81721466034651",
     "customer_pick_up_point_name":"Jhalana Quary Road\nJhalana Institutional Area, Jhalana Doongri\nJaipur, Rajasthan 302004 null null nu",
     "customer_destination_point_lat":"26.858011038196814",
     "customer_destination_point_long":"75.76966848224401",
     "customer_destination_point_name":"73\/39-40, Veer Path\nMansarovar Sector 7, Shipra Path, Barh Devariya, Mansarovar\nJaipur, Rajasthan 30",
     "vehicle_type":"prime",
     "vehicle_model":"1",
     "vehicle_number":"1",
     "vehicle_image":"",
     "fare_multiplier":"1.5",
     "car_type":1,
     "map_car_image":"http:\/\/ds412.projectstatus.co.uk\/opon\/uploads\/managers\/images\/1495197312.png",
     "driver_latitude":26.8716025,
     "driver_longitude":75.8172148
     *
     */

    private static boolean DriverRide_status;
    private static String Driver_message;
    private static int    Driver_code;
    private static boolean Driver_Is_Payment_Done;

    public static boolean isDriver_Is_Payment_Done() {
        return Driver_Is_Payment_Done;
    }

    public static void setDriver_Is_Payment_Done(boolean driver_Is_Payment_Done) {
        Driver_Is_Payment_Done = driver_Is_Payment_Done;
    }

    private static String Driver_ride_id;
    private static String Driver_request_id;
    private static String Driver_ride_status;
    private static String Driver_driver_id;
    private static String Driver_driver_name;
    private static String Driver_driver_phone;
    private static String Driver_driver_vehicle;
    private static String Driver_driver_profile_pic;
    private static String Driver_push_Status_Flag;
    private static String Driver_rating;
    private static String Driver_customer_pick_up_point_lat;
    private static String Driver_customer_pick_up_point_long;
    private static String Driver_customer_pick_up_point_name;
    private static String Driver_customer_destination_point_lat;
    private static String Driver_customer_destination_point_long;
    private static String Driver_customer_destination_point_name;
    private static String Driver_vehicle_type;
    private static String Driver_vehicle_image;
    private static String Driver_vehicle_model;
    private static String Driver_vehicle_number;
    private static String Driver_fare_multiplier;
    private static String Driver_car_type;
    private static String Driver_map_car_image;
    private static String Driver_driver_latitude;
    private static String Driver_driver_longitude;

    private static String Driver_total_cost;
    private static String Driver_trip_distance;
    private static String Driver_trip_duration;
    private static String Driver_tip_the_driver;
    private static String Driver_payment_type;

    private static String Driver_total;
    private static String Driver_discount_total;
    private static String Driver_discount_applied;
    private static String Driver_promo_code;
    private static String Driver_payment_mode;
    private static String Driver_mobile_no;
    private static String Driver_no_of_seats;
    private static String Driver_list_car_image;
    private static String Driver_Customer_id;



    public static boolean setDriverDataSplash(String data) throws JSONException {

        JSONObject dataObject = new JSONObject(data);

        DriverRide_status = dataObject.optBoolean("status");

        Driver_message = dataObject.optString("message");

        Driver_code = dataObject.optInt("code");

        if (DriverRide_status) {
            Driver_Is_Payment_Done = dataObject.optBoolean("is_payment_done");
            Driver_ride_id = dataObject.optString("ride_id");
            Driver_request_id= dataObject.optString("request_id");
            Driver_ride_status= dataObject.optString("ride_status");
            Driver_driver_id= dataObject.optString("driver_id");
            Driver_Customer_id = dataObject.optString("customer_id");
            Driver_driver_name= dataObject.optString("driver_name");
            Driver_driver_phone= dataObject.optString("driver_phone");
            Driver_driver_vehicle= dataObject.optString("driver_vehicle");
            Driver_driver_profile_pic= dataObject.optString("driver_image");
            Driver_push_Status_Flag= dataObject.optString("push_Status_Flag");
            Driver_rating= dataObject.optString("rating");
            Driver_customer_pick_up_point_lat= dataObject.optString("customer_pick_up_point_lat");
            Driver_customer_pick_up_point_long= dataObject.optString("customer_pick_up_point_long");
            Driver_customer_pick_up_point_name= dataObject.optString("customer_pick_up_point_name");
            Driver_customer_destination_point_lat= dataObject.optString("customer_destination_point_lat");
            Driver_customer_destination_point_long= dataObject.optString("customer_destination_point_long");
            Driver_customer_destination_point_name= dataObject.optString("customer_destination_point_name");
            Driver_vehicle_type= dataObject.optString("vehicle_type");
            Driver_vehicle_model= dataObject.optString("vehicle_model");
            Driver_vehicle_number= dataObject.optString("vehicle_number");
            Driver_vehicle_image= dataObject.optString("vehicle_image");
            Driver_fare_multiplier= dataObject.optString("fare_multiplier");
            Driver_car_type= dataObject.optString("car_type");
            Driver_map_car_image= dataObject.optString("map_car_image");
            Driver_driver_latitude= dataObject.optString("driver_latitude");
            Driver_driver_longitude= dataObject.optString("driver_longitude");

            Driver_total_cost = dataObject.optString("total_cost");
            Driver_trip_distance= dataObject.optString("trip_distance");
            Driver_trip_duration= dataObject.optString("trip_duration");
            Driver_tip_the_driver= dataObject.optString("tip_the_driver");
            Driver_payment_type= dataObject.optString("payment_type");

            Driver_total= dataObject.optString("total");
            Driver_discount_total= dataObject.optString("discount_total");
            Driver_discount_applied= dataObject.optString("discount_applied");
            Driver_promo_code= dataObject.optString("promo_code");
            Driver_payment_mode= dataObject.optString("payment_mode");
            Driver_mobile_no= dataObject.optString("mobile_no");

            Driver_no_of_seats = dataObject.optString("no_of_seats");
            Driver_list_car_image =  dataObject.optString("list_car_image");

        }
        return DriverRide_status;
    }


    public static String getDriver_total() {
        return Driver_total;
    }

    public static void setDriver_total(String driver_total) {
        Driver_total = driver_total;
    }

    public static String getDriver_vehicle_models() {
        return Driver_vehicle_model;
    }

    public static void setDriver_vehicle_models(String driver_vehicle_model) {
        Driver_vehicle_model = driver_vehicle_model;
    }

    public static String getDriver_discount_total() {
        return Driver_discount_total;
    }  public static void setDriver_vehicle_numbers(String driver_vehicle_number) {
        Driver_vehicle_number = driver_vehicle_number;
    }

    public static String getDriver_vehicle_numbers() {
        return Driver_vehicle_number;
    }

    public static void setDriver_discount_total(String driver_discount_total) {
        Driver_discount_total = driver_discount_total;
    }

    public static String getDriver_discount_applied() {
        return Driver_discount_applied;
    }

    public static void setDriver_discount_applied(String driver_discount_applied) {
        Driver_discount_applied = driver_discount_applied;
    }

    public static String getDriver_promo_code() {
        return Driver_promo_code;
    }

    public static void setDriver_promo_code(String driver_promo_code) {
        Driver_promo_code = driver_promo_code;
    }

    public static String getDriver_payment_mode() {
        return Driver_payment_mode;
    }

    public static void setDriver_payment_mode(String driver_payment_mode) {
        Driver_payment_mode = driver_payment_mode;
    }

    public static String getDriver_mobile_no() {
        return Driver_mobile_no;
    }

    public static void setDriver_mobile_no(String driver_mobile_no) {
        Driver_mobile_no = driver_mobile_no;
    }

    public static String getReg_status_message() {
        return reg_status_message;
    }

    public static void setReg_status_message(String reg_status_message) {
        Parser.reg_status_message = reg_status_message;
    }

    public static String getDriver_total_cost() {
        return Driver_total_cost;
    }

    public static void setDriver_total_cost(String driver_total_cost) {
        Driver_total_cost = driver_total_cost;
    }

    public static String getDriver_trip_distance() {
        return Driver_trip_distance;
    }

    public static void setDriver_trip_distance(String driver_trip_distance) {
        Driver_trip_distance = driver_trip_distance;
    }

    public static String getDriver_trip_duration() {
        return Driver_trip_duration;
    }

    public static void setDriver_trip_duration(String driver_trip_duration) {
        Driver_trip_duration = driver_trip_duration;
    }

    public static String getDriver_tip_the_driver() {
        return Driver_tip_the_driver;
    }

    public static void setDriver_tip_the_driver(String driver_tip_the_driver) {
        Driver_tip_the_driver = driver_tip_the_driver;
    }

    public static String getDriver_payment_type() {
        return Driver_payment_type;
    }

    public static void setDriver_payment_type(String driver_payment_type) {
        Driver_payment_type = driver_payment_type;
    }

    public static boolean setDriverData(String data,int way) throws JSONException {

        if(way == 1) {

            JSONObject dataObject = new JSONObject(data);

            DriverRide_status = dataObject.optBoolean("status");

            Driver_message = dataObject.optString("message");

            Driver_code = dataObject.optInt("code");

            if (DriverRide_status) {
                JSONObject mainObj = dataObject.getJSONObject("data");
                Driver_Is_Payment_Done = dataObject.optBoolean("is_payment_done");
                Driver_ride_id = mainObj.optString("ride_id");
                Driver_request_id = mainObj.optString("request_id");
                Driver_ride_status = mainObj.optString("ride_status");
                Driver_driver_id = mainObj.optString("driver_id");
                Driver_driver_name = mainObj.optString("driver_name");
                Driver_driver_phone = mainObj.optString("driver_phone");
                Driver_driver_vehicle = mainObj.optString("driver_vehicle");
                Driver_driver_profile_pic = mainObj.optString("driver_profile_pic");
                Driver_push_Status_Flag = mainObj.optString("push_Status_Flag");
                Driver_rating = mainObj.optString("rating");
                Driver_customer_pick_up_point_lat = mainObj.optString("customer_pick_up_point_lat");
                Driver_customer_pick_up_point_long = mainObj.optString("customer_pick_up_point_long");
                Driver_customer_pick_up_point_name = mainObj.optString("customer_pick_up_point_name");
                Driver_customer_destination_point_lat = mainObj.optString("customer_destination_point_lat");
                Driver_customer_destination_point_long = mainObj.optString("customer_destination_point_long");
                Driver_customer_destination_point_name = mainObj.optString("customer_destination_point_name");
                Driver_vehicle_type = mainObj.optString("vehicle_type");
                Driver_vehicle_model = mainObj.optString("vehicle_model");
                Driver_vehicle_number = mainObj.optString("vehicle_number");
                Driver_vehicle_image = mainObj.optString("vehicle_image");
                Driver_fare_multiplier = mainObj.optString("fare_multiplier");
                Driver_car_type = mainObj.optString("car_type");
                Driver_map_car_image = mainObj.optString("map_car_image");
                Driver_driver_latitude = mainObj.optString("driver_latitude");
                Driver_driver_longitude = mainObj.optString("driver_longitude");
            }
            return DriverRide_status;
        }else{
            if (data != null) {
                JSONObject mainObj = new JSONObject(data);
                Driver_Is_Payment_Done = mainObj.optBoolean("is_payment_done");
                Driver_ride_id = mainObj.optString("ride_id");
                Driver_request_id = mainObj.optString("request_id");
                Driver_ride_status = mainObj.optString("ride_status");
                Driver_driver_id = mainObj.optString("driver_id");
                Driver_driver_name = mainObj.optString("driver_name");
                Driver_driver_phone = mainObj.optString("driver_phone");
                Driver_driver_vehicle = mainObj.optString("driver_vehicle");
                Driver_driver_profile_pic = mainObj.optString("driver_profile_pic");
                Driver_push_Status_Flag = mainObj.optString("push_Status_Flag");
                Driver_rating = mainObj.optString("rating");
                Driver_customer_pick_up_point_lat = mainObj.optString("customer_pick_up_point_lat");
                Driver_customer_pick_up_point_long = mainObj.optString("customer_pick_up_point_long");
                Driver_customer_pick_up_point_name = mainObj.optString("customer_pick_up_point_name");
                Driver_customer_destination_point_lat = mainObj.optString("customer_destination_point_lat");
                Driver_customer_destination_point_long = mainObj.optString("customer_destination_point_long");
                Driver_customer_destination_point_name = mainObj.optString("customer_destination_point_name");
                Driver_vehicle_type = mainObj.optString("vehicle_type");
                Driver_vehicle_model = mainObj.optString("vehicle_model");
                Driver_vehicle_number = mainObj.optString("vehicle_number");
                Driver_vehicle_image = mainObj.optString("vehicle_image");
                Driver_fare_multiplier = mainObj.optString("fare_multiplier");
                Driver_car_type = mainObj.optString("car_type");
                Driver_map_car_image = mainObj.optString("map_car_image");
                Driver_driver_latitude = mainObj.optString("driver_latitude");
                Driver_driver_longitude = mainObj.optString("driver_longitude");
            }
            return true;
        }
    }



    /**
     * ============================= Node Implemented customerRideStatus ========================
     */


    private static boolean bool_ride_status;
    private static int bool_ride_status_code;

    private static String ride_status_message;

    private static String ride_id;

    private static String customer_ride_status;

    private static Driver selectedDriver;

    private static String customer_driver_id;

    private static String customer_request_id;


    private static String driver;


    public static boolean customerRideStatus(String data,int way) {


        if(way == 1) {

            try {

                JSONObject dataObject = new JSONObject(data);

                bool_ride_status = dataObject.optBoolean("status");

                ride_status_message = dataObject.optString("message");

                bool_ride_status_code = dataObject.optInt("code");

                JSONObject mainObj = dataObject.getJSONObject("data");

                if (bool_ride_status) {

                    customer_ride_status = mainObj.getString("ride_status");
                    ride_id = mainObj.optString("ride_id");
                    request_id = mainObj.optString("request_id");
                    System.out.println("inside trueeeeee person ");

                } else {

                    customer_ride_status = mainObj.getString("ride_status");

                }


            } catch (Exception e) {

            }

            return bool_ride_status;
        }else{
            try {
                    JSONObject dataObject = new JSONObject(data);
                    ride_status_message = dataObject.optString("message");

                    if(dataObject.optInt("ride_status") == 3){
                        bool_ride_status_code = 5;
                    }else if(dataObject.optInt("ride_status") == 4){
                        bool_ride_status_code = 6;
                    }else if(dataObject.optInt("ride_status") == 8){
                        bool_ride_status_code = 7;
                    }
                    customer_ride_status = dataObject.getString("ride_status");
                    ride_id = dataObject.optString("ride_id");
                    request_id = dataObject.optString("request_id");
                    System.out.println("inside trueeeeee person ");

            } catch (Exception e) {

            }

            return true;
        }

    }


    private static String endstatus;
    private static String endstatus_message;
    private static String updated_latitude;
    private static String updated_longitude;


    public static String EndRide(String data) {
        try {

            JSONObject dataObject = new JSONObject(data);

            endstatus = dataObject.optString("status");

            if (endstatus.equals("TRUE")) {

                endstatus_message = dataObject.optString("message").toString();


            } else {
                endstatus_message = dataObject.optString("message").toString();
                updated_latitude = dataObject.optString("latitude").toString();
                updated_longitude = dataObject.optString("longitude").toString();
            }
        } catch (Exception e) {

        }
        return endstatus;
    }


    private static String rate_status;


    public static String getRate_status_message() {
        return rate_status_message;
    }


    public static String getRide_status() {
        return ride_status;
    }


    public static String getTrip_distance_cust() {
        return trip_distance_cust;
    }


    public static String getTrip_duration_cust() {
        return trip_duration_cust;
    }


    public static String getTotal_cust() {
        return total_cust;
    }


    private static String rate_status_message;
    private static String ride_status;
    private static String trip_distance_cust;
    private static String trip_duration_cust;
    private static String total_cust;

    public static String rateTip(String data) {


        try {

            JSONObject dataObject = new JSONObject(data);

            rate_status = dataObject.optString("status");

            if (rate_status.equals("TRUE")) {


                rate_status_message = dataObject.optString("message").toString();
                trip_distance_cust = dataObject.optString("trip_distance").toString();
                trip_duration_cust = dataObject.optString("trip_duration").toString();
                total_cust = dataObject.optString("total").toString();

            } else {
                rate_status_message = dataObject.optString("message").toString();
            }

        } catch (Exception e) {

        }

        return rate_status;

    }


    public static String getRide_id() {
        return ride_id;
    }


    public static String getRide_status_message() {
        return ride_status_message;
    }

    public static String getCustomer_ride_status() {
        return customer_ride_status;
    }

    public static String getCustomer_driver_id() {
        return customer_driver_id;
    }

    public static String getCustomer_requestId() {
        return customer_request_id;
    }


    public static String getShare_status_message() {
        return share_status_message;
    }


    public static String getShare_driver_latitude() {
        return share_driver_latitude;
    }


    public static String getShare_driver_longitude() {
        return share_driver_longitude;
    }


    public static ArrayList<String> getInvalid_numbers() {
        return invalid_numbers;
    }


    public static String getCustomer_name() {
        return customer_name;
    }


    public static String getCustomer_email() {
        return customer_email;
    }


    public static String getCustomer_mob_no() {
        return customer_mob_no;
    }


    public static String getCustomer_image() {
        return customer_image;
    }


    public static String getLoginStatus() {

        return login_status_message;
    }

    public static String getProfileStatus() {

        return profile_status;
    }

    public static String getCustomer_id() {

        return customer_id;
    }

    public static String getVerification_Status() {

        return verification_status_message;
    }

    public static String getToken_code() {

        return token_code;
    }

    public static String getProfileViewStatus() {

        return getProfile_status_message;
    }

    public static String getVerification_status() {
        return verification_status;
    }


    public static String getVerification_status_message() {
        return verification_status_message;
    }


    public static String getMob_no_status() {
        return mob_no_status;
    }


    public static String getMob_no_status_message() {
        return mob_no_status_message;
    }


    public static String getPerson_name() {
        return person_name;
    }


    public static String getPerson_mob_no() {
        return person_mob_no;
    }

    public static String getEndstatus_message() {
        return endstatus_message;
    }


    public static String getUpdated_latitude() {
        return updated_latitude;
    }


    public static String getUpdated_longitude() {
        return updated_longitude;
    }


    public static String getCard_status() {
        return card_status;
    }


    public static String getCard_status_message() {
        return card_status_message;
    }


    private static String card_status;
    private static String card_status_message;

    private static ArrayList<Card> cardList;

    public static ArrayList<Card> getCardList() {
        return cardList;
    }


    private static Card mCard;

    public static String getCardDetails(String data) throws Exception {

        try {

            JSONObject cobj = new JSONObject(data);
            card_status = cobj.getString("status");

            if (card_status.equals("TRUE")) {

                JSONArray jArray = cobj.optJSONArray("card_list");

                System.out.println("LLLLLLL " + jArray.length());
                cardList = new ArrayList<Card>();
                for (int i = 0; i < jArray.length(); i++) {
                    mCard = new Card();
                    System.out.println("inside for loop ");
                    JSONObject obj = jArray.getJSONObject(i);
                    //mDriver.setDriver_id(obj.getString("driver_id"));
                    mCard.setCard_id(obj.optString("card_id"));
                    mCard.setCustomer_id(obj.optString("customer_id"));
                    mCard.setCard_type(obj.optString("card_type"));
                    mCard.setPayment_token(obj.optString("payment_token"));
                    mCard.setCard_no(obj.optString("card_no"));

                    cardList.add(mCard);

                }

                System.out.println("CardList:" + cardList.size());
            } else if (card_status.equals("FALSE")) {
                JSONObject error = new JSONObject(data);
                card_status_message = error.getString("message");
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return card_status;
    }


    private static String history_status;

    private static String history_status_message;
    private static ArrayList<History> historyList = null;


    public static String getHistory_status() {
        return history_status;
    }

    public static String getHistory_status_message() {
        return history_status_message;
    }

    public static ArrayList<History> getHistoryList() {
        return historyList;
    }


    public static String getHistory(String data, String user) throws Exception {

        try {

            JSONObject cobj = new JSONObject(data);
            history_status = cobj.getString("status");

            if (history_status.equals("TRUE")) {

                JSONArray jArray = cobj.optJSONArray("ride_details");

                System.out.println("HISTList " + jArray.length());
                historyList = new ArrayList<History>();
                for (int i = 0; i < jArray.length(); i++) {
                    History mHistory = new History();

                    JSONObject obj = jArray.getJSONObject(i);
                    mHistory.setSurge_applied(obj.optString("surge_applied"));
                    mHistory.setSurge(obj.optString("surge"));

                    mHistory.setRide_id(obj.optString("ride_id"));
                    mHistory.setName(obj.optString("name"));
                    mHistory.setTrip_distance(obj.optString("trip_distance"));
                    mHistory.setTrip_duration(obj.optString("trip_duration"));
                    mHistory.setTotal(obj.optString("total"));
                    mHistory.setPromo_code(obj.optString("promo_code"));
                    mHistory.setPromotion_discount(obj.optString("promotion_discount"));
                    mHistory.setDriver_cancel_status(obj.optString("driver_cancel_status"));
                    mHistory.setCustomer_cancel_status(obj.optString("customer_cancel_status"));
                    mHistory.setPickup_location_name(obj.optString("pickup_location_name"));
                    mHistory.setDestination_name(obj.optString("destination_name"));
                    mHistory.setPayment_type(obj.optString("payment_type"));
                    mHistory.setRide_map(obj.optString("ride_map"));
                    mHistory.setImage(obj.optString("image"));
                    mHistory.setRide_time(obj.optString("ride_time"));
                    mHistory.setVehicle_model(obj.optString("vehicle_model"));
                    mHistory.setVehicle_type(obj.optString("vehicle_type"));
                    mHistory.setDetailed_ride_map(obj.optString("detail_map"));
                    if (!obj.optString("customer_rating").equals(""))
                        mHistory.setCustomer_rating(obj.optString("customer_rating"));
                    else
                        mHistory.setCustomer_rating("0");

                    if (user.equals("customer")) {
                        mHistory.setShare_status(obj.optString("share_status"));
                        mHistory.setShare_amt(obj.optString("share_amt"));
                        mHistory.setShare_participant(obj.optString("share_participant"));
                    }

                    historyList.add(mHistory);

                }

                System.out.println("HistoryList:" + historyList.size());
            } else if (history_status.equals("FALSE")) {
                JSONObject falseObj = new JSONObject(data);
                history_status_message = falseObj.getString("message");
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return history_status;
    }


    private static String invite_status;
    private static String invite_message;
    private static String invite_referral_code;
    private static String invite_referral_link;
    private static String invite_description;
    private static String invite_tc;

    public static String getInvites(String data) {


        try {

            JSONObject dataObject = new JSONObject(data);

            invite_status = dataObject.optString("status");

            if (invite_status.equals("true")) {

                JSONObject mainObj = dataObject.optJSONObject("data");

                invite_referral_code = mainObj.optString("referral_code").toString();
                invite_referral_link = mainObj.optString("referral_link").toString();
                invite_tc = mainObj.optString("terms").toString();
                invite_description = mainObj.optString("description").toString();


            } else {
                invite_message = dataObject.optString("message").toString();
            }

        } catch (Exception e) {

        }

        return invite_status;

    }






    private static String referd_status;
    private static String referd_terms;

    private static String referral_description;

    private static String referd_status_message;


    public static ArrayList<ReferredDrivers> getDriversList() {
        return driversList;
    }

    public static void setDriversList(ArrayList<ReferredDrivers> driversList) {
        Parser.driversList = driversList;
    }

    private static ArrayList<ReferredDrivers> driversList = null;



    public static String getReferredDrivers(String data) throws Exception {

        try {

            JSONObject cobj = new JSONObject(data);
            referd_status = cobj.getString("status");

            if (referd_status.equals("true")) {


                JSONObject objref = cobj.getJSONObject("data");
                referral_description = objref.getString("referral_description");
                referd_terms = objref.getString("terms");
                JSONArray jArray = objref.optJSONArray("drivers");

                System.out.println("HISTList " + jArray.length());
                driversList = new ArrayList<ReferredDrivers>();
                for (int i = 0; i < jArray.length(); i++) {
                    ReferredDrivers mHistory = new ReferredDrivers();

                    JSONObject obj = jArray.getJSONObject(i);

                    mHistory.setId(obj.optString("id"));
                    mHistory.setFirst_name(obj.optString("first_name"));
                    mHistory.setSecond_name(obj.optString("second_name"));
                    mHistory.setEmail(obj.optString("email"));
                    mHistory.setPhone(obj.optString("phone"));
                    mHistory.setAddress(obj.optString("address"));
                    mHistory.setRefer_status(obj.optString("refer_status"));
                    mHistory.setReferral_amount_alloted(obj.optString("referral_amount_alloted"));
                    driversList.add(mHistory);

                }

                System.out.println("HistoryList:" + driversList.size());
            } else if (referd_status.equals("false")) {
                JSONObject falseObj = new JSONObject(data);
                referd_status_message = falseObj.getString("message");
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return referd_status;
    }






    public static String getAnalytics_status() {
        return analytics_status;
    }

    public static void setAnalytics_status(String analytics_status) {
        Parser.analytics_status = analytics_status;
    }

    /**
     * Analytics Records
     */

    private static String analytics_status;

    private static String total_earning;

    private static String heading;

    private static String prev_next_date;

    public static String getHeading() {
        return heading;
    }

    public static void setHeading(String heading) {
        Parser.heading = heading;
    }

    public static String getPrev_next_date() {
        return prev_next_date;
    }

    public static void setPrev_next_date(String prev_next_date) {
        Parser.prev_next_date = prev_next_date;
    }

    public static String getAnalytics_status_message() {
        return analytics_status_message;
    }

    public static void setAnalytics_status_message(String analytics_status_message) {
        Parser.analytics_status_message = analytics_status_message;
    }

    private static String analytics_status_message;

    public static ArrayList<Analytics> getAnalyticsList() {
        return analyticsList;
    }

    public static void setAnalyticsList(ArrayList<Analytics> analyticsList) {
        Parser.analyticsList = analyticsList;
    }

    private static ArrayList<Analytics> analyticsList = null;


    public static String getAnalytics(String data) throws Exception {

        try {

            JSONObject cobj = new JSONObject(data);
            analytics_status = cobj.getString("status");

            analytics_status_message = cobj.getString("message");

            if (analytics_status.equals("TRUE")) {

                JSONObject jresult = cobj.optJSONObject("result");

                total_earning = jresult.getString("total_earning");
                heading = jresult.getString("heading");
                prev_next_date = jresult.getString("prev_next_date");

                analyticsList = new ArrayList<Analytics>();

                JSONArray jArray = jresult.optJSONArray("analytics");

                for (int i = 0; i < jArray.length(); i++) {

                    JSONObject Analyticsobj = jArray.getJSONObject(i);
                    Analytics mAnalytics = new Analytics(Analyticsobj.optString("time_input"), Analyticsobj.optString("time_map"), Analyticsobj.optString("time_of_data"), Analyticsobj.optString("canceled_ride"), Analyticsobj.optString("accepted_ride"),
                            Analyticsobj.optString("rejected_ride"), Analyticsobj.optString("ignored_ride"), Analyticsobj.optString("working_hours"),
                            Analyticsobj.optString("earning"));

                    analyticsList.add(mAnalytics);
                }
                Log.d("AnalyticsList", "" + analyticsList.size());
            } else if (analytics_status.equals("FALSE")) {
                JSONObject falseObj = new JSONObject(data);
                analytics_status_message = falseObj.getString("message");

                JSONObject jfalseresult = cobj.optJSONObject("result");

                total_earning = jfalseresult.getString("total_earning");
                heading = jfalseresult.getString("heading");
                prev_next_date = jfalseresult.getString("prev_next_date");
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return analytics_status;
    }


    public static String getTotal_earning() {
        return total_earning;
    }

    public static void setTotal_earning(String total_earning) {
        Parser.total_earning = total_earning;
    }


    /**
     * Transaction history Records
     */

    private static String transaction_status;

    private static String transaction_message;

    public static String getTransaction_status() {
        return transaction_status;
    }

    public static void setTransaction_status(String transaction_status) {
        Parser.transaction_status = transaction_status;
    }

    public static String getTransaction_message() {
        return transaction_message;
    }

    public static void setTransaction_message(String transaction_message) {
        Parser.transaction_message = transaction_message;
    }

    public static ArrayList<TransactionsHistoryDao> getTransactionList() {
        return transactionList;
    }

    public static void setTransactionList(ArrayList<TransactionsHistoryDao> analyticsList) {
        Parser.transactionList = analyticsList;
    }

    private static ArrayList<TransactionsHistoryDao> transactionList = null;


    public static String getTransactionsHistory(String data) throws Exception {

        try {

            JSONObject cobj = new JSONObject(data);
            transaction_status = cobj.getString("status");

            transaction_message = cobj.getString("message");

            if (transaction_status.equals("TRUE")) {

                JSONArray jArray = cobj.getJSONArray("result");

                // total_earning = jresult.getString("total_earning")

                transactionList = new ArrayList<TransactionsHistoryDao>();

                //	JSONArray jArray = jresult.optJSONArray("analytics");

                for (int i = 0; i < jArray.length(); i++) {


                    JSONObject Analyticsobj = jArray.getJSONObject(i);
                    TransactionsHistoryDao mAnalytics = new TransactionsHistoryDao(Analyticsobj.optString("debit_account"), Analyticsobj.optString("credit_account"), Analyticsobj.optString("amount"), Analyticsobj.optString("transaction_date"), Analyticsobj.optString("transaction_time"), Analyticsobj.optString("description"), Analyticsobj.optString("driver_name"), Analyticsobj.optString("color"));

                    transactionList.add(mAnalytics);
                }
                Log.d("TransactionList", "" + transactionList.size());
            } else if (transaction_status.equals("FALSE")) {
                JSONObject falseObj = new JSONObject(data);
                transaction_message = falseObj.getString("message");
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return transaction_status;
    }


    /**
     * Wallet Info Records
     */

    private static String wallet_info_status;

    private static String wallet_info_message;

    public static String getWalletinfo_status() {
        return wallet_info_status;
    }

    public static void setWalletinfo_status(String wallet_info_status) {
        Parser.wallet_info_status = transaction_status;
    }

    public static String getWalletinfo_message() {
        return wallet_info_message;
    }

    public static void setWalletinfo_message(String wallet_info_message) {
        Parser.wallet_info_message = transaction_message;
    }

    public static ArrayList<WalletInfoDao> getWalletinfoList() {
        return walletinfoList;
    }

    public static void setWalletinfoList(ArrayList<WalletInfoDao> analyticsList) {
        Parser.walletinfoList = analyticsList;
    }

    private static ArrayList<WalletInfoDao> walletinfoList = null;


    public static String getWalletinfo(String data) throws Exception {

        try {

            JSONObject cobj = new JSONObject(data);
            wallet_info_status = cobj.getString("status");

            wallet_info_message = cobj.getString("message");

            if (wallet_info_status.equals("TRUE")) {

                JSONArray jarray = cobj.getJSONArray("result");
                JSONObject jobj = jarray.getJSONObject(0);

                // total_earning = jresult.getString("total_earning")
                WalletInfoDao model = new WalletInfoDao();
                model.setBase_fare(jobj.getString("base_fare"));
                model.setDriver_earning(jobj.getString("driver_earning"));
                model.setHoponn_earning(jobj.getString("hoponn_earning"));
                model.setTotal_cost(jobj.getString("total_cost"));

                walletinfoList = new ArrayList<WalletInfoDao>();

                //	JSONArray jArray = jresult.optJSONArray("analytics");

                walletinfoList.add(model);
                Log.d("TransactionList", "" + walletinfoList.size());
            } else if (wallet_info_status.equals("FALSE")) {
                JSONObject falseObj = new JSONObject(data);
                wallet_info_message = falseObj.getString("message");
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return wallet_info_status;
    }

    /**
     * Driver Analytics Records
     */

    private static String driver_analytics_status;

    private static String driver_analytics_message;

    private static String analyticsDate;

    public static String getAnalyticsDate() {
        return analyticsDate;
    }

    public static void setAnalyticsDate(String date) {
        Parser.analyticsDate = date;
    }

    public static String getdriver_analytics__status() {
        return driver_analytics_status;
    }

    public static void setdriver_analytics__status(String driver_analytics_status) {
        Parser.driver_analytics_status = driver_analytics_status;
    }

    public static String getdriver_analytics_message() {
        return driver_analytics_message;
    }

    public static void setdriver_analytics_message(String driver_analytics_message) {
        Parser.driver_analytics_message = driver_analytics_message;
    }

    public static ArrayList<AnalyticsData> getDriverAnalyticsList() {
        return driverAnalyticsList;
    }

    public static void setDriverAnalyticsList(ArrayList<AnalyticsData> driverAnalyticsList) {
        Parser.driverAnalyticsList = driverAnalyticsList;
    }

    private static ArrayList<AnalyticsData> driverAnalyticsList = null;


    public static String getDriverAnalytics(String data) throws Exception {

        try {

            JSONObject cobj = new JSONObject(data);
            driver_analytics_status = cobj.getString("status");

            driver_analytics_message = cobj.getString("message");

            if (driver_analytics_status.equals("TRUE")) {

                JSONArray jresult = cobj.optJSONArray("result");
                JSONArray analyticsArray = null;

                for (int i = 0; i < jresult.length() ; i++){
                    JSONObject jsonObject = (JSONObject) jresult.get(i);
                    analyticsDate = jsonObject.optString("date");
                    analyticsArray = jsonObject.optJSONArray("analytics");
                }

                driverAnalyticsList = new ArrayList<AnalyticsData>();

                for (int i = 0; i < analyticsArray.length(); i++) {

                    JSONObject Analyticsobj = analyticsArray.getJSONObject(i);
                    AnalyticsData mAnalytics = new AnalyticsData(Analyticsobj.optString("id"), Analyticsobj.optString("driver_id"), Analyticsobj.optString("name"), Analyticsobj.optString("city"),Analyticsobj.optString("date"),Analyticsobj.optString("shift_in"),Analyticsobj.optString("shift_out"),
                            Analyticsobj.optString("canceled_ride"),Analyticsobj.optString("accepted_ride"),Analyticsobj.optString("rejected_ride"), Analyticsobj.optString("ignored_ride"));

                    driverAnalyticsList.add(mAnalytics);
                }
                Log.d("DriverAnalyticsList", "" + driverAnalyticsList.size());
            } else if (driver_analytics_status.equals("FALSE")) {
                JSONObject falseObj = new JSONObject(data);
                driver_analytics_message = falseObj.getString("message");
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return driver_analytics_status;
    }

    public static String getReferral_description() {
        return referral_description;
    }

    public static void setReferral_description(String referral_description) {
        Parser.referral_description = referral_description;
    }

    public static String getInvite_message() {
        return invite_message;
    }

    public static void setInvite_message(String invite_message) {
        Parser.invite_message = invite_message;
    }

    public static String getInvite_referral_code() {
        return invite_referral_code;
    }

    public static void setInvite_referral_code(String invite_referral_code) {
        Parser.invite_referral_code = invite_referral_code;
    }

    public static String getInvite_referral_link() {
        return invite_referral_link;
    }

    public static void setInvite_referral_link(String invite_referral_link) {
        Parser.invite_referral_link = invite_referral_link;
    }

    public static String getInvite_description() {
        return invite_description;
    }

    public static void setInvite_description(String invite_description) {
        Parser.invite_description = invite_description;
    }

    public static int getShare_status_code() {
        return share_status_code;
    }

    public static void setShare_status_code(int share_status_code) {
        Parser.share_status_code = share_status_code;
    }

    public static Driver getSelectedDriver() {
        return selectedDriver;
    }

    public static void setSelectedDriver(Driver selectedDriver) {
        Parser.selectedDriver = selectedDriver;
    }

    public static int getBool_ride_status_code() {
        return bool_ride_status_code;
    }

    public static void setBool_ride_status_code(int bool_ride_status_code) {
        Parser.bool_ride_status_code = bool_ride_status_code;
    }

    public static String getDriver_name() {
        return driver_name;
    }

    public static void setDriver_name(String driver_name) {
        Parser.driver_name = driver_name;
    }

    public static String getDriver_profile_pic() {
        return driver_profile_pic;
    }

    public static void setDriver_profile_pic(String driver_profile_pic) {
        Parser.driver_profile_pic = driver_profile_pic;
    }

    public static String getDriver_vehicle_model() {
        return driver_vehicle_model;
    }

    public static void setDriver_vehicle_model(String driver_vehicle_model) {
        Parser.driver_vehicle_model = driver_vehicle_model;
    }

    public static String getDriver_vehicle_number() {
        return driver_vehicle_number;
    }

    public static void setDriver_vehicle_number(String driver_vehicle_number) {
        Parser.driver_vehicle_number = driver_vehicle_number;
    }

    public static String getCurrent_driver_id() {
        return current_driver_id;
    }

    public static void setCurrent_driver_id(String current_driver_id) {
        Parser.current_driver_id = current_driver_id;
    }

    public static String getCurrent_driver_rating() {
        return current_driver_rating;
    }

    public static void setCurrent_driver_rating(String current_driver_rating) {
        Parser.current_driver_rating = current_driver_rating;
    }

    public static String getInvite_tc() {
        return invite_tc;
    }

    public static void setInvite_tc(String invite_tc) {
        Parser.invite_tc = invite_tc;
    }

    public static String getReferd_terms() {
        return referd_terms;
    }

    public static void setReferd_terms(String referd_terms) {
        Parser.referd_terms = referd_terms;
    }

    public static String getDriver_vehicle_image() {
        return Driver_vehicle_image;
    }

    public static void setDriver_vehicle_image(String driver_vehicle_image) {
        Driver_vehicle_image = driver_vehicle_image;
    }

    public static String getDriver_no_of_seats() {
        return Driver_no_of_seats;
    }

    public static void setDriver_no_of_seats(String driver_no_of_seats) {
        Driver_no_of_seats = driver_no_of_seats;
    }

    public static String getDriver_list_car_image() {
        return Driver_list_car_image;
    }

    public static void setDriver_list_car_image(String driver_list_car_image) {
        Driver_list_car_image = driver_list_car_image;
    }

    public static String getDriver_Customer_id() {
        return Driver_Customer_id;
    }

    public static void setDriver_Customer_id(String driver_Customer_id) {
        Driver_Customer_id = driver_Customer_id;
    }

    public static String getIsride_message() {
        return isride_message;
    }

    public static void setIsride_message(String isride_message) {
        Parser.isride_message = isride_message;
    }

    public static String getIsride_status() {
        return isride_status;
    }

    public static void setIsride_status(String isride_status) {
        Parser.isride_status = isride_status;
    }
}







				
	








	



