package com.us.hoodruncustomer.share;

import android.content.Context;

import com.us.hoodruncustomer.commonwork.BasePresenter;
import com.us.hoodruncustomer.commonwork.BaseView;

import java.util.Map;

/**
 * Created by admin on 11/16/2017.
 */

public interface ShareContract {

    interface View extends BaseView<Presenter> {
        void getShareInfoResponse(String response);

        void showProgreass();

        void hideProgress();

        void onFail(Throwable t);

        void mobileVerifyResponse(String response);

        void shareFareResponse(String response);

        void shareFareFail(Throwable t);


    }


    interface Presenter extends BasePresenter {

        void shareInfo(String customerId, String tokenCode, String shatreStatus, String requestId, Context context);


        void mobileVerification(Map<String, String> params, Context context);

        void shareFare(String tokencode, String requestId, Context context);


    }


}
