package com.us.hoodruncustomer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.us.hoodruncustomer.configure.TwitterRestClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class TestMultipart extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_multipart);
        complainRequest();
    }

    private void complainRequest(){


        RequestParams params = new RequestParams();

        params.put("first_name", "test");
        params.put("last_name", "last");
        params.put("email", "testpatient56@yopmail.com");
        params.put("mobile", "667599999");
        params.put("dob", "1900-02-23");
        params.put("zipcode", "768897");
        params.put("city", "jaipur");
        params.put("country", "India");
        params.put("address_line_1", "jaipur1");
        params.put("profile_photo_file","");
        params.put("address_line_2", "jaipur2");
        params.put("disabilities", "1");
        params.put("gender", "male");
        params.put("description","cool");

        TwitterRestClient.post("api/users/profile", params, new BaseJsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String rawJsonResponse, Object response) {

                System.out.println("Complain==="+rawJsonResponse);
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, String rawJsonData, Object errorResponse) {
                System.out.println("Complain==="+rawJsonData);
            }

            @Override
            protected Object parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });

    }



}
