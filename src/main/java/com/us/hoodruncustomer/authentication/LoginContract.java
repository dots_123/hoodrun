package com.us.hoodruncustomer.authentication;


import android.content.Context;

import com.us.hoodruncustomer.commonwork.BasePresenter;
import com.us.hoodruncustomer.commonwork.BaseView;


/**
 * Created by admin
 * Created on 10/25/2016.
 * Modified on 25,October,2016
 */
public interface LoginContract {

    interface View extends BaseView<Presenter> {
        void nevigateToHeme(String response);

        void updateDataResponse(String response);

        void showProgreass();

        void hideProgress();

        void onLoginFail(Throwable t);

        void onUpdateFail(Throwable t);

        void forgotResponseSuccess(String response);

        void forgotFail(Throwable t);
    }

    interface Presenter extends BasePresenter {
        /**
         * Login.
         *
         * @param username the username
         * @param password the password
         * @param context
         */
        void login(String username, String password, Context context);

//        {"facebook_id" =>""1234567", "device_id" => "123", "os" => "1", "name" => "test", "email" => "test@yopmail.com"}
        void loginFb(String facebookId, String device_id, String os, String name, String email, Context context);

        //        {"google_id" =>""1234567", "device_id" => "123", "os" => "1", "name" => "test", "email" => "test@yopmail.com"}
        void loginGoogle(String googleId, String device_id, String os, String name, String email, Context context);

        void updateData(String tokencode, String id_type, Context context);

        void forgotPassword(String email, Context context);

    }
}
