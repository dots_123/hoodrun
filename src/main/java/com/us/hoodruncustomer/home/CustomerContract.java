package com.us.hoodruncustomer.home;

import android.content.Context;

import com.us.hoodruncustomer.commonwork.BasePresenter;
import com.us.hoodruncustomer.commonwork.BaseView;

import java.util.Map;

/**
 * Created by admin on 11/14/2017.
 */

public interface CustomerContract {

    interface View extends BaseView<CustomerContract.Presenter> {
        void getCarsResponse(String response);

        void showProgreass();

        void hideProgress();

        void onFail(Throwable t);





        void getEstimateResponse(String response);

        void driverSelectREsponse(String response);
        void driverSelectError(Throwable t);

        void getPromoCodeResponse(String response);
        void getPromocodeerror(Throwable t);
    }




    interface Presenter extends BasePresenter {

        void getCarsByLocation(String lat, String lng, String token_code, String city, String checkCity, String customerId, Context context);

        void getEstimate(Map<String, String> params, Context context);
        void driverSelect(Map<String, String> params, Context context);
        void checkPromoCode(Map<String, String> params, Context context);

    }

}
