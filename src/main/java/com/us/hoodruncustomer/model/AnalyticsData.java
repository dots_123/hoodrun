package com.us.hoodruncustomer.model;

public class AnalyticsData {


	public String id;
	public String driver_id;
	public String name;


	public String city;
	public String date;
	public String shift_in;
	public String shift_out;

	public String getCanceled_ride() {
		return canceled_ride;
	}

	public void setCanceled_ride(String canceled_ride) {
		this.canceled_ride = canceled_ride;
	}

	public String canceled_ride;
	public String accepted_ride;
	public String rejected_ride;
	public String ignored_ride;

	public AnalyticsData(String id, String driver_id, String name, String city, String date, String shift_in, String shift_out, String canceled_ride,String accepted_ride, String rejected_ride, String ignored_ride) {
		this.id = id;
		this.driver_id = driver_id;
		this.name = name;
		this.city = city;
		this.date = date;
		this.shift_in = shift_in;
		this.shift_out = shift_out;
		this.accepted_ride = accepted_ride;
		this.canceled_ride = canceled_ride;
		this.rejected_ride = rejected_ride;
		this.ignored_ride = ignored_ride;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDriver_id() {
		return driver_id;
	}

	public void setDriver_id(String driver_id) {
		this.driver_id = driver_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getShift_in() {
		return shift_in;
	}

	public void setShift_in(String shift_in) {
		this.shift_in = shift_in;
	}

	public String getShift_out() {
		return shift_out;
	}

	public void setShift_out(String shift_out) {
		this.shift_out = shift_out;
	}

	public String getAccepted_ride() {
		return accepted_ride;
	}

	public void setAccepted_ride(String accepted_ride) {
		this.accepted_ride = accepted_ride;
	}

	public String getRejected_ride() {
		return rejected_ride;
	}

	public void setRejected_ride(String rejected_ride) {
		this.rejected_ride = rejected_ride;
	}

	public String getIgnored_ride() {
		return ignored_ride;
	}

	public void setIgnored_ride(String ignored_ride) {
		this.ignored_ride = ignored_ride;
	}
}
