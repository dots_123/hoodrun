package com.us.hoodruncustomer.payment;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.commonwork.BaseActivity;
import com.us.hoodruncustomer.commonwork.Injection;
import com.us.hoodruncustomer.exceptionhandler.ExceptionHandler;
import com.us.hoodruncustomer.home.CustomerHomeActivity;
import com.us.hoodruncustomer.model.Card;
import com.us.hoodruncustomer.model.PaymentListAdapter;
import com.us.hoodruncustomer.parser.ConnectionDetector;
import com.us.hoodruncustomer.parser.Parser;

import java.util.ArrayList;


public class PaymentActivity extends BaseActivity implements PaymentContract.View{

	private PaymentContract.Presenter presenter;
	private Toolbar toolbar;
	private TextView headertext;
	private ArrayList<Card> CardList=new ArrayList<Card>();
	private PaymentListAdapter lisAdapter;
	Typeface tf;
	String font_path;
	String parsresp;
	ListView pymenttype;
	LinearLayout mAddcard;

	ConnectionDetector cd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_payment);
		new PaymentPresenter(Injection.remoteDataRepository(getApplicationContext()), this);

		/**
		 * Important to catch the exceptions
		 */

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

		/*****************************************************************************************/


		font_path="fonts/opensans.ttf";
		tf = Typeface.createFromAsset(this.getAssets(), font_path);

		toolbar = (Toolbar) findViewById(R.id.toolbar);
		if (toolbar != null) {
			int color = Color.parseColor("#ffffff");
			toolbar.setTitleTextColor(color);
			setSupportActionBar(toolbar);
			getSupportActionBar().setDisplayShowTitleEnabled(false);

			getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
			headertext=(TextView) findViewById(android.R.id.text1);
			headertext.setText("Saved Cards");
			headertext.setTypeface(tf, Typeface.BOLD);

		}
		String[] navMenuTitles;
		TypedArray navMenuIcons;

		if(mAppManager.getuser().equals("customer")){

			navMenuTitles = getResources().getStringArray(R.array.nav_drawer_user_items);
			navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons_user);
		}else{

			navMenuTitles = getResources().getStringArray(R.array.nav_drawer_driver_items);
			navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);
		}



		set(navMenuTitles, navMenuIcons);

		cd=new ConnectionDetector(PaymentActivity.this);

		pymenttype=(ListView)findViewById(R.id.pymenttype);
		mAddcard=(LinearLayout)findViewById(R.id.lin_addcard);

		mAppManager.savePrevActivity("HIS", "");

		callAsync();




		mAddcard.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mAppManager.savePrevActivity("ADD", "PAY");
				startActivity(new Intent(PaymentActivity.this,AddPaymentActivity.class));

				finish();
			}
		});

	}

	private void callAsync() {
		// TODO Auto-generated method stub


		if(cd.isConnectingToInternet()) {
		//	showProgress();
			savedCards();
		}
		else {
			Toast.makeText(getApplicationContext(), "No internet connection available.", Toast.LENGTH_LONG).show();
			System.exit(0);
		}
	}


	@Override
	public void onBackPressed()
	{
		// code here to show dialog
		super.onBackPressed();  // optional depending on your needs
		startActivity(new Intent(PaymentActivity.this,CustomerHomeActivity.class));


	}


	private void savedCards(){


		presenter.getSavecards(mAppManager.getTokenCode(),mAppManager.getCustomerID(),this);
		/*StringRequest stringRequest = new StringRequest(Request.Method.POST, Configure.URL_saved_cards,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						hideProgress();

						System.out.println("SAVEDCARDS==="+response);
						try {
							parsresp=Parser.getCardDetails(response);
						} catch (Exception e) {
							e.printStackTrace();
						}
						if (!TextUtils.isEmpty(parsresp)) {


							if(parsresp.equals("TRUE")){
								CardList=Parser.getCardList();
								lisAdapter=new PaymentListAdapter(PaymentActivity.this,CardList);
								pymenttype.setAdapter(lisAdapter);
							}
							else{

								Toast.makeText(getApplicationContext(), ""+Parser.getCard_status_message(), Toast.LENGTH_SHORT).show();
							}

						}else {

							Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
							System.exit(0);
						}



					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						hideProgress();
						retryDialog(1);
					}


				})
		{
			@Override
			protected Map<String,String> getParams(){

				Map<String,String> params = new HashMap<String, String>();
				params.put("token_code", mAppManager.getTokenCode());
				params.put("customer_id",mAppManager.getCustomerID());


				return params;
			}

		};

		RequestQueue requestQueue = Volley.newRequestQueue(this);
		int socketTimeout = 60000;//60 seconds - change to what you want
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		stringRequest.setRetryPolicy(policy);
		requestQueue.add(stringRequest);*/
	}

	@Override
	protected void onResume() {
		super.onResume();
		MyApplication.activityResumed();
	}

	@Override
	protected void onPause() {
		super.onPause();
		MyApplication.activityPaused();
	}


	Dialog retry;
	private void retryDialog(final int i){

		retry = new Dialog(PaymentActivity.this, R.style.Theme_Dialog);
		retry.requestWindowFeature(Window.FEATURE_NO_TITLE);
		Window window = retry.getWindow();
		window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);
		retry.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		retry.setContentView(R.layout.thaks_dialog_twobutton);
		retry.setCancelable(false);
		retry.setCanceledOnTouchOutside(false);
		retry.show();

		TextView mTitle=(TextView)retry.findViewById(R.id.txt_title);
		mTitle.setText("Error!");
		mTitle.setTypeface(tf, Typeface.BOLD);
		TextView mDialogBody=(TextView)retry.findViewById(R.id.txt_dialog_body);
		mDialogBody.setText("Internet connection is slow or disconnected. Try connecting again.");
		LinearLayout mOK=(LinearLayout)retry.findViewById(R.id.lin_Ok);
		LinearLayout mCancel=(LinearLayout)retry.findViewById(R.id.lin_cancel);
		TextView mRetry=(TextView)retry.findViewById(R.id.txt_dialog_ok);
		TextView mExit=(TextView)retry.findViewById(R.id.txt_dialog_cancel);
		mRetry.setText("RETRY");
		mExit.setText("EXIT");

		mOK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				retry.dismiss();

				switch (i)
				{
					case 1:
						callAsync();
						break;

					default:
						break;

				}
			}
		});

		mCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				retry.dismiss();
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);

			}
		});

	}

	@Override
	public void getSaveCards(String response) {

		hideProgress();

		System.out.println("SAVEDCARDS==="+response);
		try {
			parsresp=Parser.getCardDetails(response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (!TextUtils.isEmpty(parsresp)) {


			if(parsresp.equals("TRUE")){
				CardList=Parser.getCardList();
				lisAdapter=new PaymentListAdapter(PaymentActivity.this,CardList);
				pymenttype.setAdapter(lisAdapter);
			}
			else{

				Toast.makeText(getApplicationContext(), ""+Parser.getCard_status_message(), Toast.LENGTH_SHORT).show();
			}

		}else {

			Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
			System.exit(0);
		}

	}

	@Override
	public void showProgreass() {
		loader = new Dialog(PaymentActivity.this);
		loader.requestWindowFeature(Window.FEATURE_NO_TITLE);
		loader.setContentView(R.layout.dialog_progress);
		loader_wheel = (ProgressWheel) loader.findViewById(R.id.progress_wheel);
		loader_wheel.setCircleRadius(50);
		loader.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		loader.setCancelable(false);
		loader.show();
	}

	@Override
	public void hideProgress() {
		if (loader != null) {
			loader.dismiss();
		}

	}

	@Override
	public void onFail(Throwable t) {
		hideProgress();
		retryDialog(1);

	}

	@Override
	public void getTokenResponse(String response) {

	}

	@Override
	public void onTokenFail(Throwable t) {

	}

	@Override
	public void onPaymentResponse(String response) {

	}

	@Override
	public void setPresenter(PaymentContract.Presenter presenter) {

		this.presenter = presenter;

	}
}


