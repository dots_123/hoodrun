package com.us.hoodruncustomer.payment;

import android.content.Context;

import com.us.hoodruncustomer.data.DataSource;
import com.us.hoodruncustomer.data.remote.RemoteDataSource;

import java.util.Map;

/**
 * Created by admin on 11/15/2017.
 */

public class PaymentPresenter implements PaymentContract.Presenter, DataSource.PaymentI ,DataSource.AddPaymentI{

    private DataSource dataSource;
    private PaymentContract.View mpView;
    private Context context;
    private boolean isSkip;
    public PaymentPresenter(RemoteDataSource remoteDataSource, PaymentContract.View mLoginView) {

        dataSource = remoteDataSource;
        this.mpView = mLoginView;
        this.mpView.setPresenter(this);
    }

    @Override
    public void getSavecards(String tokencode, String customerId, Context context) {
        mpView.showProgreass();
        dataSource.getCards(tokencode, customerId, this, context);

    }

    @Override
    public void getToken(String customerId, String userType, Context context) {
        mpView.showProgreass();
        dataSource.getTokenService(customerId, userType, this, context);
    }

    @Override
    public void savePaymentData(Map<String, String> params, String url, boolean skipped, Context context) {
        isSkip = skipped;

        if(!isSkip)
        {
            mpView.showProgreass();
        }


        dataSource.savePaymentData(params, url, this, context);
    }

    @Override
    public void drop() {
        mpView = null;
    }


    @Override
    public void getCardsResponse(String baseResponse) {
        mpView.hideProgress();
        mpView.getSaveCards(baseResponse);

    }

    @Override
    public void onFail(Throwable t) {
        mpView.hideProgress();
        mpView.onFail(t);

    }

    @Override
    public void getTokenResponse(String baseResponse) {
        mpView.hideProgress();
        mpView.getTokenResponse(baseResponse);

    }

    @Override
    public void onTokenFail(Throwable t) {
        if (isSkip) {

            mpView.onTokenFail(t);

        } else {
            mpView.hideProgress();

            mpView.onTokenFail(t);
        }

    }

    @Override
    public void onPaymentResponse(String respnse) {
        mpView.hideProgress();
        mpView.onPaymentResponse(respnse);

    }
}
