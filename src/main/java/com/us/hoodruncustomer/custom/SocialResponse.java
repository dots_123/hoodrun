package com.us.hoodruncustomer.custom;

/**
 * Created by admin on 5/14/2018.
 */

public class SocialResponse {

    /**
     * status : true
     * code : 200
     * message : Login successful
     * data : {"token_code":"29xJ5Q13Ye","customer_id":1441}
     */

    private boolean status;
    private int code;
    private String message;
    private DataBean data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * token_code : 29xJ5Q13Ye
         * customer_id : 1441
         */

        private String token_code;
        private int customer_id;

        public String getToken_code() {
            return token_code;
        }

        public void setToken_code(String token_code) {
            this.token_code = token_code;
        }

        public int getCustomer_id() {
            return customer_id;
        }

        public void setCustomer_id(int customer_id) {
            this.customer_id = customer_id;
        }
    }
}
