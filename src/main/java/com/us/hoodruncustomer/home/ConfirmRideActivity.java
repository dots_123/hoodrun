package com.us.hoodruncustomer.home;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.bumptech.glide.Glide;
import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraIdleListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.Constants;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.app.locationUtils;
import com.us.hoodruncustomer.commonwork.CustomPlaces;
import com.us.hoodruncustomer.commonwork.Injection;
import com.us.hoodruncustomer.commonwork.SimpleActivity;
import com.us.hoodruncustomer.payment.PaymentActivity;
import com.us.hoodruncustomer.splash.SplashActivity;
import com.us.hoodruncustomer.configure.SphericalUtil;
import com.us.hoodruncustomer.configure.Util;
import com.us.hoodruncustomer.exceptionhandler.ExceptionHandler;
import com.us.hoodruncustomer.model.Cars;
import com.us.hoodruncustomer.model.Drivers;
import com.us.hoodruncustomer.nodeservice.ConnectorService;
import com.us.hoodruncustomer.nodeservice.NodeUtil;
import com.us.hoodruncustomer.parser.ConnectionDetector;
import com.us.hoodruncustomer.parser.Parser;
import com.us.hoodruncustomer.rider.beans.DriverMark;
import com.us.hoodruncustomer.rider.beans.Estimation;
import com.us.hoodruncustomer.rider.events.AvailCars;
import com.us.hoodruncustomer.trackride.TrackRide;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.us.hoodruncustomer.R.id.cardview;

public class ConfirmRideActivity extends SimpleActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        OnCameraIdleListener,
        GoogleMap.OnMapLoadedCallback,
        GoogleApiClient.OnConnectionFailedListener,
        RoutingListener,
        com.google.android.gms.location.LocationListener, CustomerContract.View {

    private GoogleMap mMap;
    private CustomerContract.Presenter presenter;
    private Toolbar toolbar;
    private TextView headertext, awaytext;
    private int driverReject, driverAccept;
    Marker markerpick, markerdrop;
    Typeface tf;
    String font_path;
    ConnectionDetector cd;
    int mapWidth = 300;
    int mapHeight = 300;

    @BindView(R.id.pickedup)
    TextView pickedUp;

    @BindView(R.id.dropup)
    TextView dropUp;

    @BindView(R.id.persons)
    TextView noOfpersons;

    @BindView(R.id.estimation)
    TextView estimationvalue;

    @BindView(R.id.llayout_promocode)
    LinearLayout layout_promocode;

    @BindView(R.id.txt_code)
    EditText mPromoCode;

    @BindView(R.id.txt_apply)
    TextView mApply;

    @BindView(R.id.payment_type)
    TextView paymentTypevalue;

    @BindView(R.id.imageView8)
    ImageView imagePaymentType;

    @BindView(R.id.type_choose)
    LinearLayout typeChoose;

    @BindView(R.id.estimation_progress)
    ProgressBar estimationProgress;

    @BindView(R.id.confirm_lays)
    RelativeLayout confirmLays;

    @BindView(R.id.textView16)
    TextView progressText;

    @BindView(R.id.mapContainerride)
    LinearLayout mapContainer;

    @BindView(cardview)
    LinearLayout changeLaytype;

    @BindView(R.id.chamge_destination)
    RelativeLayout chamgeDestination;

    @BindView(R.id.locationMarker)
    RelativeLayout locationMarkerLayout;

    @BindView(R.id.payment_chnage)
    LinearLayout payment_chnage;

    private Bundle locationdata;
    private boolean IsRideOpen;

    private int paymentType = 1;
    public static final String RECEIVER_CONFIRM_RIDE = "com.uk.hoponn.confirm_ride_get";
    private double userlat, userlng, destlat, destlng;
    private String pickupaddress, dropupaddress, carname;
    private String avgtime, total_passengers;
    private String carType, promo_code = "";
    private LatLng driverstart, destinationend;
    private Estimation estimation;
    private CountDownTimer countDownTimer;
    private ArrayList<Cars> cars1 = new ArrayList<>();
    private Cars specifiedCars;
    private boolean activityinpause = false;
    private boolean makeRoute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_ride);
        ButterKnife.bind(this);
        makeRoute = true;
        new CustomerPresenter(Injection.remoteDataRepository(getApplicationContext()), this);
        setlayoutWidth();
        locationdata = new Bundle();
        mAppManager.saveCurrentActivity("ConfirmRideActivity");
        userlat = getIntent().getDoubleExtra("picklat", 0);
        userlng = getIntent().getDoubleExtra("picklng", 0);
        destlat = getIntent().getDoubleExtra("droplat", 0);
        destlng = getIntent().getDoubleExtra("droplng", 0);
        pickupaddress = getIntent().getStringExtra("pick");
        dropupaddress = getIntent().getStringExtra("drop");
        carname = getIntent().getStringExtra("carname");

        carType = getIntent().getStringExtra("type");
        avgtime = getIntent().getStringExtra("avgtime");
        total_passengers = getIntent().getStringExtra("total_passengers");
        driverstart = new LatLng(userlat, userlng);
        destinationend = new LatLng(destlat, destlng);
        pickedUp.setText(pickupaddress);
        dropUp.setText(dropupaddress);
        CarsHandler = new Handler();
        mAppManager.saveCustomerLatitude(userlat);
        mAppManager.saveCustomerLongitude(userlng);
        noOfpersons.setText(" 1 - " + total_passengers);

        /**
         * Important to catch the exceptions
         */

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        /*****************************************************************************************/

        cd = new ConnectionDetector(ConfirmRideActivity.this);

        font_path = "fonts/opensans.ttf";

        tf = Typeface.createFromAsset(this.getAssets(), font_path);

        toolbar = (Toolbar) findViewById(R.id.toolbar6);

        if (toolbar != null) {
            int color = Color.parseColor("#ffffff");
            toolbar.setTitleTextColor(color);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            headertext = (TextView) findViewById(android.R.id.text1);
            awaytext = (TextView) findViewById(R.id.awaytext);

            headertext.setText(carname);
            awaytext.setText(" " + avgtime + " away");

            toolbar.setNavigationIcon(R.drawable.back_arrow);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (changeLaytype.getVisibility() == View.VISIBLE) {
                        closeUploadPopup();
                    } else {
                        finish();
                    }
                }
            });
        }


        headertext.setTypeface(tf, Typeface.BOLD);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        /**
         * Initialize google map
         */

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        /**
         * Check for play services availability if available then build google api client
         */

        if (checkPlayServices()) {
            // If this check succeeds, proceed with normal processing.
            // Otherwise, prompt user to get valid Play Services APK.
            if (!Constants.isLocationEnabled(this)) {
                // notify user
                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                dialog.setMessage("Location not enabled!");
                dialog.setPositiveButton("Open location settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                });
                dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        // TODO Auto-generated method stub

                    }
                });
                dialog.show();
            }
            buildGoogleApiClient();
        } else {
            Toast.makeText(this, "Location not supported in this device", Toast.LENGTH_SHORT).show();
        }

        if (mAppManager.getPrevActivity("TRACK").equals("SPLASHC_NR")) {
            confirmLays.setVisibility(View.VISIBLE);
            confirmLays.setBackgroundColor(Color.parseColor("#000000"));
            progressText.setText("Waiting for driver response");
            setCountDown();
        } else {
            getCity();
        }

    }


//    @OnClick(R.id.type_choose)
//    public void changeType(){
//
//        openUploadPopup();
//    }

    @OnClick(R.id.carddefault)
    public void carddefault() {
        closeUploadPopup();
        paymentTypevalue.setText("Card");
        imagePaymentType.setImageResource(R.drawable.creditcard);
        paymentType = 1;
    }

    @OnClick(R.id.cashdefault)
    public void cashdefault() {
        closeUploadPopup();
        paymentTypevalue.setText("Cash");
        imagePaymentType.setImageResource(R.drawable.cash);
        paymentType = 3;
    }

    @OnClick(cardview)
    public void hidePaymentOptions() {
        closeUploadPopup();
    }


    MarkerOptions marker;
    float mZoom = 20;

    private void setMarkers() throws ExecutionException, InterruptedException {

        createMarker(destlat, destlng);

        route(driverstart, destinationend);

        getEstimate();

    }

    @Override
    protected void onPause() {
        super.onPause();
        activityinpause = true;
        MyApplication.activityPaused();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (myNManager != null) {
            myNManager.cancelAll();
        }
        activityinpause = false;
        MyApplication.currentActivity = this.getClass().getSimpleName();
        MyApplication.activityResumed();
        killMe = false;

        if (CarsHandler != null) {
            CarsHandler.removeCallbacks(updateCarsHandler);
        }
        CarsHandler.post(updateCarsHandler);

        if (!mSocket.connected()) {
            Intent start_Serv = new Intent(getApplicationContext(), ConnectorService.class);
            startService(start_Serv);
        }

    }

    boolean killMe = false;
    Handler CarsHandler;

    Runnable updateCarsHandler = new Runnable() {
        @Override
        public void run() {
            if (!killMe) {


                presenter.getCarsByLocation("" + userlat, "" + userlng, mAppManager.getTokenCode(), myCity, "true", mAppManager.getCustomerID(), ConfirmRideActivity.this);


            }

        }
    };


    protected void createMarker(double latitude, double longitude) {

        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                // Getting view from the layout file info_window_layout
                if (!marker.equals(markerdrop))
                    return null;
                View v = getLayoutInflater().inflate(R.layout.window_layout, null);

                TextView snippet = (TextView) v.findViewById(R.id.tv_lat);

                snippet.setText(marker.getSnippet());

                return v;
            }
        });
        if (mMap != null) {
            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

                @Override
                public void onMapClick(LatLng arg0) {
                    if (markerdrop != null)
                        markerdrop.showInfoWindow();
                    // TODO Auto-generated method stub
                    Log.d("arg0", arg0.latitude + "-" + arg0.longitude);
                }
            });
        }
    }


    private GoogleApiClient mGoogleApiClient;

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {

            }
            return false;
        }
        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

        if (mMap != null) {
            try {
                // Customise the styling of the base map using a JSON object defined
                // in a raw resource file.
                boolean success = googleMap.setMapStyle(
                        MapStyleOptions.loadRawResourceStyle(
                                this, R.raw.mapstyle));

                if (!success) {
                    Log.e("onMapReady", "Style parsing failed.");
                }
            } catch (Resources.NotFoundException e) {
                Log.e("onMapReady", "Can't find style. Error: ", e);
            }
            mMap.setMyLocationEnabled(false);
            mMap.setOnMapLoadedCallback(this);
            mMap.getUiSettings().setCompassEnabled(false);
            mMap.getUiSettings().setRotateGesturesEnabled(true);
            mMap.setOnCameraIdleListener(this);

            //mMap.setPadding(20,dpToPx(190),20,20);
            markerpick = mMap.addMarker(new MarkerOptions().position(new LatLng(userlat, userlng)).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pointer_yellow)));
            markerdrop = mMap.addMarker(new MarkerOptions().position(new LatLng(destlat, destlng)).snippet("").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pointer)));

        }
    }


    public void zoomRoute(GoogleMap googleMap, List<LatLng> lstLatLngRoute, int time) {

        ArrayList<LatLng> allBounds = new ArrayList<>();
        allBounds.add(markerpick.getPosition());
        allBounds.addAll(lstLatLngRoute);
        allBounds.add(markerdrop.getPosition());

        if (googleMap == null || allBounds == null || allBounds.isEmpty()) return;
        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
        for (LatLng latLngPoint : allBounds)
            boundsBuilder.include(latLngPoint);
        LatLngBounds latLngBounds = boundsBuilder.build();
        int px = Util.pxToDp(getApplicationContext(), 20);
        int top = (int) (locationMarkerLayout.getHeight() + locationMarkerLayout.getY()) + px * 4;
        googleMap.setPadding(px, top, px, px * 2);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 0));

        try {

            timeFromDriver = time;
            markerdrop.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.map_pointer));
            markerdrop.setSnippet("" + timeFromDriver + " min");
            if (markerdrop.getSnippet() != null && markerdrop.getSnippet().length() > 0) {
                markerdrop.showInfoWindow();
            }

        } catch (Exception e) {

        }

    }


    /**
     * Draw Route Between Locations
     */
    Routing routing;

    public void route(LatLng start, LatLng end) {

        if (start != null && end != null) {

            routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(this)
                    .alternativeRoutes(false)
                    .key(getString(R.string.google_direction_api_key))
                    .waypoints(start, end)
                    .build();
            routing.execute();

        }
    }

    /**
     * Calculate the Route and time estimations
     *
     * @param e
     */

    @Override
    public void onRoutingFailure(RouteException e) {

        Log.d("Routing", "" + e.toString());
    }

    @Override
    public void onRoutingStart() {

    }

    private List<Polyline> polylines;
    private int timeFromDriver;
    private static final int[] COLORS = new int[]{R.color.black, R.color.black, R.color.black, R.color.black, R.color.black};

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
        polylines = new ArrayList<>();
        if (polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }

        polylines = new ArrayList<>();

        if (route.get(0).getPoints().size() > 800) {

            //In case of more than 5 alternative routes
            int colorIndex = 0 % COLORS.length;
            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(getResources().getColor(COLORS[colorIndex]));
            polyOptions.width(10 + 0 * 3);
            polyOptions.addAll(route.get(0).getPoints());
            Polyline polyline = mMap.addPolyline(polyOptions);
            polylines.add(polyline);

        } else {

            if (route.size() > 0) {
                final float red = 205.0f;
                final float green = 1.0f;
                final float yellow = 2.0f;
                float redSteps = (red / route.get(0).getPoints().size());
                float greenSteps = (green / route.get(0).getPoints().size());
                float yellowSteps = (yellow / route.get(0).getPoints().size());
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(route.get(0).getPoints().get(0));
                for (int i = 1; i < route.get(0).getPoints().size(); i++) {
                    builder.include(route.get(0).getPoints().get(i));
                    int redColor = (int) (red - (redSteps * i));
                    int greenColor = (int) (green - (greenSteps * i));
                    int yellowColor = (int) (yellow - (yellowSteps * i));
                    Log.e("Color", "" + redColor);
                    int color = Color.rgb(redColor, greenColor, yellowColor);

                    PolylineOptions options = new PolylineOptions().width(8).color(color).geodesic(true);
                    options.add(route.get(0).getPoints().get(i - 1));
                    options.add(route.get(0).getPoints().get(i));
                    Polyline line = mMap.addPolyline(options);
                    line.setEndCap(new RoundCap());
                }
                // SetZoomlevel(route.get(0).getPoints());


            }
        }
        zoomRoute(mMap, route.get(0).getPoints(), route.get(0).getDurationValue() / 60);

    }


    @Override
    public void onRoutingCancelled() {

    }

    RequestQueue getEstimate_requestQueue;

    private void getEstimate() {


        Map<String, String> params = new HashMap<String, String>();
        params.put("picking_point_lat", "" + userlat);
        params.put("picking_point_long", "" + userlng);
        params.put("destination_point_lat", "" + destlat);
        params.put("destination_point_long", "" + destlng);
        params.put("picking_Location_Name", pickupaddress);
        params.put("destination_Location_Name", dropupaddress);
        params.put("total_passengers", total_passengers);
        params.put("token_code", mAppManager.getTokenCode());
        params.put("customer_id", mAppManager.getCustomerID());
        params.put("car_type", carType);

        presenter.getEstimate(params, this);

    }


    @OnClick(R.id.ride_now_btn)
    public void getDrivers() {
        String pickuptext = pickedUp.getText().toString().trim();
        String dropattext = dropUp.getText().toString().trim();

        if (pickuptext != null && pickuptext.length() > 0) {
            confirmLays.setVisibility(View.VISIBLE);
            progressText.setText("Sending your request to nearest driver");
            IsRideOpen = true;
            if (cd.isConnectingToInternet()) {

                driverSelect();

            } else {
                if (dialogA != null) {
                    dialogA.dismiss();
                    dialogA = null;
                }
                DialogAlert("No Internet !",
                        "Check your connection and try again.");
            }

        } else {
            Toast.makeText(ConfirmRideActivity.this, "Pickup address not found. please try again later", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Confirm-Booking
     */


    private void driverSelect() {

        Map<String, String> params = new HashMap<String, String>();
        params.put("picking_point_lat", "" + userlat);
        params.put("picking_point_long", "" + userlng);
        params.put("destination_point_lat", "" + destlat);
        params.put("destination_point_long", "" + destlng);
        params.put("picking_Location_Name", pickupaddress);
        params.put("destination_Location_Name", dropupaddress);
        params.put("total_passengers", total_passengers);
        params.put("token_code", mAppManager.getTokenCode());
        params.put("customer_id", mAppManager.getCustomerID());
        params.put("car_type", carType);
        params.put("promo_code", promo_code);
        params.put("promotion_id", promotion_id);
        params.put("payment_type", "" + paymentType);

        presenter.driverSelect(params, this);

    }


    /**
     * Node Actions
     */

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(this.Node_CUSTOMER_Location_Update, new IntentFilter(NodeUtil.NODE_CUSTOMER_LOCATION_UPDATE));
        registerReceiver(this.Node_OnConnect, new IntentFilter(NodeUtil.NODE_CONNECTION));
        registerReceiver(this.Node_OnDisConnect, new IntentFilter(NodeUtil.NODE_DISCONNECTION));
        registerReceiver(this.Node_CUSTOMER_TRACK, new IntentFilter(NodeUtil.NODE_CUSTOMER_TRACKRIDE));
        registerReceiver(this.Notify_CUSTOMER_TRACK, new IntentFilter(RECEIVER_CONFIRM_RIDE));
        registerReceiver(this.Node_CUSTOMER_GET_CARS, new IntentFilter(NodeUtil.NODE_CUSTOMER_GETCARS));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(Notify_CUSTOMER_TRACK);
        unregisterReceiver(Node_CUSTOMER_TRACK);
        unregisterReceiver(Node_CUSTOMER_GET_CARS);
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }


    private BroadcastReceiver Node_OnConnect = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {

            String connctionStatus = in.getStringExtra("Object");
            hideProgress();
        }
    };

    private BroadcastReceiver Node_OnDisConnect = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {

            String connctionStatus = in.getStringExtra("Object");
            hideProgress();
        }

    };


    /**
     * Handle node and notification part to get the status of the job
     */


    private BroadcastReceiver Notify_CUSTOMER_TRACK = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {

            String status = in.getStringExtra("status");

            if (status.equalsIgnoreCase("accepted")) {

                if (driverAccept != 1) {

                    driverAccept = 1;

                    String requestData = in.getStringExtra("data");

                    try {
                        Parser.setDriverData(requestData, 2);

                        mAppManager.App_setDriver_ride_id(Parser.getDriver_ride_id());
                        mAppManager.App_setDriver_request_id(Parser.getDriver_request_id());
                        mAppManager.App_setDriver_ride_status(Parser.getDriver_ride_status());
                        mAppManager.App_setDriver_driver_id(Parser.getDriver_driver_id());
                        mAppManager.App_setDriver_driver_name(Parser.getDriver_driver_name());
                        mAppManager.App_setDriver_driver_phone(Parser.getDriver_driver_phone());
                        mAppManager.App_setDriver_driver_vehicle(Parser.getDriver_driver_vehicle());
                        mAppManager.App_setDriver_driver_profile_pic(Parser.getDriver_driver_profile_pic());
                        mAppManager.App_setDriver_driver_latitude(Parser.getDriver_driver_latitude());
                        mAppManager.App_setDriver_driver_longitude(Parser.getDriver_driver_longitude());

                        mAppManager.App_setDriver_push_Status_Flag(Parser.getDriver_push_Status_Flag());
                        mAppManager.App_setDriver_rating(Parser.getDriver_rating());

                        mAppManager.App_setDriver_customer_destination_point_lat(Parser.getDriver_customer_destination_point_lat());
                        mAppManager.App_setDriver_customer_destination_point_long(Parser.getDriver_customer_destination_point_long());
                        mAppManager.App_setDriver_customer_destination_point_name(Parser.getDriver_customer_destination_point_name());
                        mAppManager.App_setDriver_customer_pick_up_point_lat(Parser.getDriver_customer_pick_up_point_lat());
                        mAppManager.App_setDriver_customer_pick_up_point_long(Parser.getDriver_customer_pick_up_point_long());
                        mAppManager.App_setDriver_customer_pick_up_point_name(Parser.getDriver_customer_pick_up_point_name());

                        mAppManager.App_setDriver_vehicle_model(Parser.getDriver_vehicle_models());
                        mAppManager.App_setDriver_vehicle_number(Parser.getDriver_vehicle_numbers());
                        mAppManager.App_setDriver_vehicle_type(Parser.getDriver_vehicle_type());
                        mAppManager.App_setDriver_vehicle_image(Parser.getDriver_vehicle_image());


                        mAppManager.App_setDriver_fare_multiplier(Parser.getDriver_fare_multiplier());
                        mAppManager.App_setDriver_car_type(Parser.getDriver_car_type());
                        mAppManager.App_setDriver_map_car_image(Parser.getDriver_map_car_image());

                        /**
                         * Driver Accepted the Ride
                         */

                        if (activityinpause) {
                            sendNotifications("Driver accepted the ride request");
                        }

                        if (Parser.getDriver_ride_status().equalsIgnoreCase("2")) {
                            Intent toTrackRide = new Intent(getApplicationContext(), TrackRide.class);
                            startActivity(toTrackRide);
                            finish();
                        }

                    } catch (JSONException e) {
                        IsRideOpen = false;
                        e.printStackTrace();
                    }

                }
            } else {
                IsRideOpen = false;
                if (driverReject != 1) {

                    driverReject = 1;

                    if (pdialog != null && !pdialog.isShowing()) {
                        if (pdialog != null) {
                            pdialog.dismiss();
                            pdialog = null;
                        }
                        Dialog(1, "No driver is currently available");
                    } else {
                        if (pdialog != null) {
                            pdialog.dismiss();
                            pdialog = null;
                        }
                        Dialog(1, "No driver is currently available");
                    }
                }
            }
        }
    };


    boolean parseresponce;

    private BroadcastReceiver Node_CUSTOMER_TRACK = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {

            if (driverAccept != 1) {

                driverAccept = 1;

                String request = in.getStringExtra("Object");

                Log.d("Node_CUSTOMER_TRACK", "" + request);

                hideProgress();

                try {

                    parseresponce = Parser.setDriverData(request, 1);

                    if (parseresponce) {

                        mAppManager.App_setDriver_ride_id(Parser.getDriver_ride_id());
                        mAppManager.App_setDriver_request_id(Parser.getDriver_request_id());
                        mAppManager.App_setDriver_ride_status(Parser.getDriver_ride_status());
                        mAppManager.App_setDriver_driver_id(Parser.getDriver_driver_id());
                        mAppManager.App_setDriver_driver_name(Parser.getDriver_driver_name());
                        mAppManager.App_setDriver_driver_phone(Parser.getDriver_driver_phone());
                        mAppManager.App_setDriver_driver_vehicle(Parser.getDriver_driver_vehicle());
                        mAppManager.App_setDriver_driver_profile_pic(Parser.getDriver_driver_profile_pic());
                        mAppManager.App_setDriver_driver_latitude(Parser.getDriver_driver_latitude());
                        mAppManager.App_setDriver_driver_longitude(Parser.getDriver_driver_longitude());

                        mAppManager.App_setDriver_push_Status_Flag(Parser.getDriver_push_Status_Flag());
                        mAppManager.App_setDriver_rating(Parser.getDriver_rating());

                        mAppManager.App_setDriver_customer_destination_point_lat(Parser.getDriver_customer_destination_point_lat());
                        mAppManager.App_setDriver_customer_destination_point_long(Parser.getDriver_customer_destination_point_long());
                        mAppManager.App_setDriver_customer_destination_point_name(Parser.getDriver_customer_destination_point_name());
                        mAppManager.App_setDriver_customer_pick_up_point_lat(Parser.getDriver_customer_pick_up_point_lat());
                        mAppManager.App_setDriver_customer_pick_up_point_long(Parser.getDriver_customer_pick_up_point_long());
                        mAppManager.App_setDriver_customer_pick_up_point_name(Parser.getDriver_customer_pick_up_point_name());

                        mAppManager.App_setDriver_vehicle_model(Parser.getDriver_vehicle_models());
                        mAppManager.App_setDriver_vehicle_number(Parser.getDriver_vehicle_numbers());
                        mAppManager.App_setDriver_vehicle_type(Parser.getDriver_vehicle_type());
                        mAppManager.App_setDriver_vehicle_image(Parser.getDriver_vehicle_image());


                        mAppManager.App_setDriver_fare_multiplier(Parser.getDriver_fare_multiplier());
                        mAppManager.App_setDriver_car_type(Parser.getDriver_car_type());
                        mAppManager.App_setDriver_map_car_image(Parser.getDriver_map_car_image());

                        /**
                         * Driver Accepted the Ride
                         */

                        if (activityinpause) {
                            sendNotifications(Parser.getDriver_message());
                        }

                        if (Parser.getDriver_ride_status().equalsIgnoreCase("2")) {

                            Intent toTrackRide = new Intent(getApplicationContext(), TrackRide.class);
                            startActivity(toTrackRide);
                            finish();

                        }

                    } else {
                        IsRideOpen = false;
                        if (driverReject != 1) {

                            driverReject = 1;

                            if (pdialog != null && !pdialog.isShowing()) {
                                if (pdialog != null) {
                                    pdialog.dismiss();
                                    pdialog = null;
                                }
                                Dialog(1, "No driver is currently available");
                            } else {
                                if (pdialog != null) {
                                    pdialog.dismiss();
                                    pdialog = null;
                                }
                                Dialog(1, "No driver is currently available");
                            }
                        }
                    }

                } catch (Exception e) {
                    IsRideOpen = false;
                    e.printStackTrace();
                }

            }
        }

    };


    /**
     * Promocode Work
     */

    @OnClick(R.id.txt_apply)
    public void applypromo() {
        if (!promostatus) {

            if (mPromoCode.getText().toString().equals("")) {
                Toast.makeText(getApplicationContext(), "Promocode field is empty.", Toast.LENGTH_SHORT).show();
            } else {
                if (cd.isConnectingToInternet()) {
                    promo_code = mPromoCode.getText().toString().trim();

                    checkPromoCode();
                } else {
                    Toast.makeText(getApplicationContext(), "No internet connection available.", Toast.LENGTH_LONG).show();
                    System.exit(0);
                }
            }
        } else {
            promostatus = false;
            promotion_id = "";
            editTextModifierAgain(mPromoCode);
        }
    }


    private String parsresp;
    private String promotion_id = "";
    private boolean promostatus = false;
    RequestQueue checkPromoCode_requestQueue;

    private void checkPromoCode() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("token_code", mAppManager.getTokenCode());
        params.put("promo_code", promo_code);
        params.put("customer_id", mAppManager.getCustomerID());


        presenter.checkPromoCode(params, this);

    }

    private void editTextModifier(EditText mEditText) {

        mEditText.setFocusable(false);
        mEditText.setFocusableInTouchMode(false);
        mEditText.setTextColor(getResources().getColor(R.color.payment_green));

        mApply.setText("Remove");


    }

    private void editTextModifierAgain(EditText mEditText) {

        mEditText.setText("");
        mEditText.setFocusable(true);
        mEditText.setFocusableInTouchMode(true);
        mEditText.setTextColor(getResources().getColor(R.color.black));
        mApply.setText("Apply");

    }

    /**
     * Dialogs
     */


    Dialog pdialog;

    private void Dialog(final int f, final String message) {
        TextView mTitle, mDialogBody, mOK;

        pdialog = new Dialog(ConfirmRideActivity.this, R.style.Theme_Dialog);
        pdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = pdialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        pdialog.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);
        pdialog.setContentView(R.layout.dialog_onebutton);
        pdialog.setCancelable(false);
        pdialog.setCanceledOnTouchOutside(false);

        mTitle = (TextView) pdialog.findViewById(R.id.text_title);
        mTitle.setTypeface(tf, Typeface.BOLD);

        mDialogBody = (TextView) pdialog.findViewById(R.id.txt_dialog_body);
        mOK = (TextView) pdialog.findViewById(R.id.txt_dialog_ok);
        if (f == 1) {
            mTitle.setVisibility(View.GONE);
            mTitle.setText("Alert");
            mDialogBody.setText(message);
        } else if (f == 2) {
            mTitle.setText("Alert");
            mTitle.setVisibility(View.GONE);
            mDialogBody.setText(message);

        } else if (f == 3) {

            mTitle.setText("Alert");
            mTitle.setVisibility(View.GONE);
            mDialogBody.setText(message);

        } else {
            mTitle.setText("Alert");
            mTitle.setVisibility(View.GONE);
            mDialogBody.setText(message);
        }

        mOK.setText("OK");

        mOK.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                pdialog.dismiss();
                if (mAppManager.getPrevActivity("TRACK").equals("SPLASHC_NR")) {
                    Intent toSplash = new Intent(getApplicationContext(), SplashActivity.class);
                    startActivity(toSplash);
                } else {
                    if (f == 1) {
                        finish();
                    }
                    if (f == 5) {
                        Intent intent2 = new Intent(getApplicationContext(), PaymentActivity.class);
                        startActivity(intent2);
                        finish();// finishes the current activity
                    }
                }
            }
        });

        if (pdialog != null) {
            pdialog.dismiss();
        }
        pdialog.show();

    }

    private void setCountDown() {

        countDownTimer = new CountDownTimer(40000, 1000) {

            public void onTick(long millisUntilFinished) {
                Log.d("CountDownTimer", "" + millisUntilFinished / 1000);
            }

            public void onFinish() {

               /* if (cd.isConnectingToInternet()) {

                    splashTask(Configure.URL_ride_completeness_customer);

                } else {
                    if (dialogA != null) {
                        dialogA.dismiss();
                        dialogA = null;
                    }
                    DialogAlert("No Internet !",
                            "Check your connection and try again.");
                }*/
            }
        }.start();

    }


    /****************************************** !LISTNERS *****************************************/

    private static final int REQUEST_CODE_DROP = 1;

    @OnClick(R.id.chamge_destination)
    public void dropClick() {
        openAutocompleteActivity(REQUEST_CODE_DROP);
    }


    private void openAutocompleteActivity(int requestCode) {

        String hint = (requestCode == REQUEST_CODE_DROP) ? "Enter drop location" : "Enter pickup location";
        Intent toCustomPlaces = new Intent(getApplicationContext(), CustomPlaces.class);
        toCustomPlaces.putExtra("hint", hint);
        startActivityForResult(toCustomPlaces, requestCode);

       /* try {
            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .build(this);
            startActivityForResult(intent, requestCode);
        } catch (GooglePlayServicesRepairableException e) {
            // Indicates that Google Play Services is either not installed or not up to date. Prompt
            // the user to correct the issue.
            GoogleApiAvailability.getInstance().getErrorDialog(this, e.getConnectionStatusCode(),
                    0 *//* requestCode *//*).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check that the result was from the autocomplete widget.
        if (requestCode == REQUEST_CODE_DROP) {
            if (resultCode == RESULT_OK) {

                double latitude = data.getDoubleExtra("latitude", 0);
                double longitude = data.getDoubleExtra("longitude", 0);
                String address = data.getStringExtra("address");
                String primaryText = data.getStringExtra("primary");
                String secondaryText = data.getStringExtra("secondary");

                Location newDestination = new Location("Destination");
                newDestination.setLatitude(latitude);
                newDestination.setLongitude(longitude);

                dropupaddress = primaryText + ", " + secondaryText.replace(",", "");
                dropUp.setText(dropupaddress);

                LatLng latLong;
                latLong = new LatLng(latitude, longitude);

                destlat = latLong.latitude;
                destlng = latLong.longitude;
                destinationend = new LatLng(destlat, destlng);

                // Get the user's selected place from the Intent.
               /* Place place = PlaceAutocomplete.getPlace(this, data);

                // TODO call location based filter

                LatLng latLong;

                latLong = place.getLatLng();

                dropUp.setText(place.getAddress().toString());

                destlat = latLong.latitude;
                destlng =latLong.longitude;
                destinationend = new LatLng(destlat, destlng);

                dropupaddress = place.getAddress().toString();*/

                if (polylines != null) {
                    polylines.clear();
                }

                mMap.clear();
                allMarkers.clear();
                markerpick = mMap.addMarker(new MarkerOptions().position(new LatLng(userlat, userlng)).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pointer_yellow)));
                markerdrop = mMap.addMarker(new MarkerOptions().position(new LatLng(destlat, destlng)).snippet("").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pointer)));
                ArrayList<LatLng> allBounds = new ArrayList<>();
                allBounds.add(markerpick.getPosition());
                allBounds.add(markerdrop.getPosition());

                if (mMap == null || allBounds == null || allBounds.isEmpty()) return;
                LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
                for (LatLng latLngPoint : allBounds)
                    boundsBuilder.include(latLngPoint);

                int routePadding = 100;
                LatLngBounds latLngBounds = boundsBuilder.build();
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, routePadding));

                try {
                    setMarkers();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
            Status status = PlaceAutocomplete.getStatus(this, data);
        } else if (resultCode == RESULT_CANCELED) {

            // Indicates that the activity closed before a selection was made. For example if
            // the user pressed the back button.
        }
    }

    @Override
    protected void onStop() {
        MyApplication.currentActivity = "";
        unregisterReceiver(Node_OnConnect);
        unregisterReceiver(Node_CUSTOMER_Location_Update);
        unregisterReceiver(Node_OnDisConnect);

        if (routing != null) {
            routing.cancel(true);
        }

        if (pdialog != null) {
            pdialog.dismiss();
        }
        if (dialogA != null) {
            dialogA.dismiss();
        }
        killMe = true;

        if (CarsHandler != null) {
            CarsHandler.removeCallbacks(updateCarsHandler);
        }

        if (getEstimate_requestQueue != null) {
            getEstimate_requestQueue.cancelAll(this);
            Log.d("Volley-Service-Cancel", "getEstimate");
        }
       /* if (splashTask_requestQueue != null) {
            splashTask_requestQueue.cancelAll(this);
            Log.d("Volley-Service-Cancel", "splashTask");
        }*/
        if (checkPromoCode_requestQueue != null) {
            checkPromoCode_requestQueue.cancelAll(this);
            Log.d("Volley-Service-Cancel", "checkPromoCode");
        }
     /*   if (driverSelectrequestQueue != null) {
            driverSelectrequestQueue.cancelAll(this);
            Log.d("Volley-Service-Cancel", "driverSelect");
        }*/
        super.onStop();
    }

    private BroadcastReceiver Node_CUSTOMER_GET_CARS = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {

            String request = in.getStringExtra("Object");

            AvailCars availCars = new AvailCars();

            try {

                if (request != null) {
                    availCars.getCars(request);

                    if (availCars.getCarList() != null && availCars.getCarList().size() > 0) {


                        for (Cars cars : availCars.getCarList()) {
                            if (cars.getId().equalsIgnoreCase(carType)) {
                                specifiedCars = cars;
                                break;
                            }
                        }
                        carImage = MyApplication.carImages.get(specifiedCars.getId());

                        HashMap<String, Drivers> stringDriversHashMap = availCars.getStringHashMapHashMap().get(specifiedCars.getId());

                        ArrayList<Drivers> drivers = new ArrayList<>();

                        drivers.addAll(stringDriversHashMap.values());


                        plotCars(specifiedCars, drivers);
                    } else {

                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    };


    @Override
    public void onBackPressed() {

        if (changeLaytype.getVisibility() == View.VISIBLE) {
            closeUploadPopup();
        } else {
           /* if (stringRequest != null) {
                stringRequest.cancel();
            }*/
            if (countDownTimer != null) {
                countDownTimer.cancel();
            }
            if (!IsRideOpen) {
                super.onBackPressed();
            }

        }

    }

    /************************* ANIMATION ************************/

    public void openUploadPopup() {

        Log.d("FromBase", "here");
        changeLaytype.setVisibility(View.VISIBLE);
        Animation animScale = AnimationUtils.loadAnimation(this, R.anim.slide_in_up);
        payment_chnage.startAnimation(animScale);
        payment_chnage.setVisibility(View.VISIBLE);

    }

    public void closeUploadPopup() {

        Log.d("FromBase", "here");
        Animation animScale = AnimationUtils.loadAnimation(this, R.anim.slide_out_bottom);
        payment_chnage.startAnimation(animScale);
        payment_chnage.setVisibility(View.INVISIBLE);
        changeLaytype.setVisibility(View.GONE);

    }


/* Last Call TO RIde Completeness for verfocation
 */


   /* String parseresp;
    StringRequest stringRequestsplashTask;
    RequestQueue splashTask_requestQueue;
*/
    /*private void splashTask(String url) {

        stringRequestsplashTask = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        System.out.println("COMPLETENESS===" + response);

                        parseresp = StatusParser.getRideStatus(response, "customer_id");

                        if (!TextUtils.isEmpty(parseresp)) {

                            if (parseresp.equals("TRUE")) {
                                if (mAppManager.getuser().equals("customer")) {

                                    try {

                                        boolean parseresponce = Parser.setDriverDataSplash(response);

                                        if (parseresponce) {

                                            mAppManager.App_setDriver_ride_id(Parser.getDriver_ride_id());
                                            mAppManager.App_setDriver_request_id(Parser.getDriver_request_id());
                                            mAppManager.App_setDriver_ride_status(Parser.getDriver_ride_status());
                                            mAppManager.App_setDriver_driver_id(Parser.getDriver_driver_id());
                                            mAppManager.App_setDriver_driver_name(Parser.getDriver_driver_name());
                                            mAppManager.App_setDriver_driver_phone(Parser.getDriver_driver_phone());
                                            mAppManager.App_setDriver_driver_vehicle(Parser.getDriver_driver_vehicle());
                                            mAppManager.App_setDriver_driver_profile_pic(Parser.getDriver_driver_profile_pic());
                                            mAppManager.App_setDriver_driver_latitude(Parser.getDriver_driver_latitude());
                                            mAppManager.App_setDriver_driver_longitude(Parser.getDriver_driver_longitude());

                                            mAppManager.App_setDriver_push_Status_Flag(Parser.getDriver_push_Status_Flag());
                                            mAppManager.App_setDriver_rating(Parser.getDriver_rating());

                                            mAppManager.App_setDriver_customer_destination_point_lat(Parser.getDriver_customer_destination_point_lat());
                                            mAppManager.App_setDriver_customer_destination_point_long(Parser.getDriver_customer_destination_point_long());
                                            mAppManager.App_setDriver_customer_destination_point_name(Parser.getDriver_customer_destination_point_name());
                                            mAppManager.App_setDriver_customer_pick_up_point_lat(Parser.getDriver_customer_pick_up_point_lat());
                                            mAppManager.App_setDriver_customer_pick_up_point_long(Parser.getDriver_customer_pick_up_point_long());
                                            mAppManager.App_setDriver_customer_pick_up_point_name(Parser.getDriver_customer_pick_up_point_name());

                                            mAppManager.App_setDriver_vehicle_model(Parser.getDriver_vehicle_models());
                                            mAppManager.App_setDriver_vehicle_number(Parser.getDriver_vehicle_numbers());
                                            mAppManager.App_setDriver_vehicle_type(Parser.getDriver_vehicle_type());
                                            mAppManager.App_setDriver_vehicle_image(Parser.getDriver_vehicle_image());

                                            mAppManager.App_setDriver_fare_multiplier(Parser.getDriver_fare_multiplier());
                                            mAppManager.App_setDriver_car_type(Parser.getDriver_car_type());
                                            mAppManager.App_setDriver_map_car_image(Parser.getDriver_map_car_image());
                                            mAppManager.App_setDriver_total(Parser.getDriver_total());
                                            mAppManager.App_setDriver_trip_distance(Parser.getDriver_trip_distance());
                                            mAppManager.App_setDriver_trip_duration(Parser.getDriver_trip_duration());
                                            mAppManager.App_setDriver_tip_the_driver(Parser.getDriver_tip_the_driver());
                                            mAppManager.App_setDriver_payment_type(Parser.getDriver_payment_type());
                                            mAppManager.App_setDriver_total_cost(Parser.getDriver_total_cost());
                                            mAppManager.App_setDriver_discount_applied(Parser.getDriver_discount_applied());
                                            mAppManager.App_setDriver_discount_total(Parser.getDriver_discount_total());
                                            mAppManager.App_setDriver_promo_code(Parser.getDriver_promo_code());
                                            mAppManager.App_setDriver_payment_mode(Parser.getDriver_payment_mode());
                                            mAppManager.App_setDriver_mobile_no(Parser.getDriver_mobile_no());

                                        }


                                    } catch (JSONException e) {
                                        IsRideOpen = false;
                                        e.printStackTrace();
                                    }


                                    switch (StatusParser.getMessage()) {

                                        case "Driver is not arrived":
                                            mAppManager.App_setDriver_ride_status("2");
                                            startActivity(new Intent(ConfirmRideActivity.this, TrackRide.class));
                                            finish();
                                            break;

                                        case "Driver arrived at the the pick up location":
                                            mAppManager.App_setDriver_ride_status("5");
                                            startActivity(new Intent(ConfirmRideActivity.this, TrackRide.class));
                                            finish();
                                            break;

                                        case "Journey started":
                                            mAppManager.App_setDriver_ride_status("6");
                                            startActivity(new Intent(ConfirmRideActivity.this, TrackRide.class));
                                            finish();
                                            break;


                                        default:
                                            if (pdialog != null && !pdialog.isShowing()) {
                                                if (pdialog != null) {
                                                    pdialog.dismiss();
                                                    pdialog = null;
                                                }
                                                Dialog(1, "No driver is currently available");
                                            } else {
                                                if (pdialog != null) {
                                                    pdialog.dismiss();
                                                    pdialog = null;
                                                }
                                                Dialog(1, "No driver is currently available");
                                            }
                                            break;
                                    }
                                }

                            } else {
                                IsRideOpen = false;
                                if (pdialog != null && !pdialog.isShowing()) {
                                    if (pdialog != null) {
                                        pdialog.dismiss();
                                        pdialog = null;
                                    }
                                    Dialog(1, "No driver is currently available");
                                } else {
                                    if (pdialog != null) {
                                        pdialog.dismiss();
                                        pdialog = null;
                                    }
                                    Dialog(1, "No driver is currently available");
                                }
                            }
                        } else {
                            IsRideOpen = false;
                            if (pdialog != null && !pdialog.isShowing()) {
                                if (pdialog != null) {
                                    pdialog.dismiss();
                                    pdialog = null;
                                }
                                Dialog(1, "No driver is currently available");
                            } else {
                                if (pdialog != null) {
                                    pdialog.dismiss();
                                    pdialog = null;
                                }
                                Dialog(1, "No driver is currently available");
                            }
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        IsRideOpen = false;
                    }


                }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("token_code", mAppManager.getTokenCode());
                params.put("customer_id", mAppManager.getCustomerID());
                return params;
            }

        };

        splashTask_requestQueue = Volley.newRequestQueue(this);
        int socketTimeout = 60000;//60 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequestsplashTask.setRetryPolicy(policy);
        splashTask_requestQueue.add(stringRequestsplashTask);
    }*/

    Dialog dialogA;

    private void DialogAlert(String title, String body) {

        dialogA = new Dialog(ConfirmRideActivity.this, R.style.Theme_Dialog);
        dialogA.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialogA.getWindow();
        window.setLayout(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialogA.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);

        dialogA.getWindow().setBackgroundDrawableResource(
                android.R.color.transparent);
        dialogA.setContentView(R.layout.dialog_onebutton);
        dialogA.setCancelable(false);
        dialogA.setCanceledOnTouchOutside(false);
        dialogA.show();

        TextView mTitle = (TextView) dialogA.findViewById(R.id.text_title);
        mTitle.setTypeface(tf, Typeface.BOLD);
        TextView mDialogBody = (TextView) dialogA
                .findViewById(R.id.txt_dialog_body);
        TextView mOK = (TextView) dialogA.findViewById(R.id.txt_dialog_ok);

        mTitle.setText(title);
        mDialogBody.setText(body);
        mOK.setText("OK");

        mOK.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialogA.cancel();

                finish();
            }
        });

    }

    private HashMap<String, DriverMark> allMarkers = new HashMap<>();
    private String selectedCartype = "";
    Bitmap carImage;

  /*  private synchronized void plotCars(Cars car, HashMap<String, Drivers> drivers) throws ExecutionException, InterruptedException {

        selectedCartype = car.getId();

        if (drivers != null && drivers.size() > 0) {

            for (Map.Entry<String, Drivers> entry : drivers.entrySet()) {


                Drivers driver = (Drivers) entry.getValue();

                if (allMarkers.containsKey(driver.getDriver_id())) {

                    DriverMark driverMark = (DriverMark) allMarkers.get(driver.getDriver_id());

                    Marker mark = driverMark.getMarker();

                    if (!driver.getCar_type().equalsIgnoreCase(selectedCartype)) {
                        mark.setVisible(false);
                    } else {
                        if (carImage != null) {
                            mark.setIcon(BitmapDescriptorFactory.fromBitmap(createcar(carImage)));
                        } else {
                            drawDriverMarker(mark, car.getMap_car_image());
                        }
                        mark.setVisible(true);
                    }

                    LatLng last = new LatLng(mark.getPosition().latitude, mark.getPosition().longitude);

                    LatLng newone = new LatLng(Double.parseDouble(driver.getLat()), Double.parseDouble(driver.getLng()));

                    double distance = SphericalUtil.computeDistanceBetween(last, newone);

                    float bearing = bearingBetweenLatLngs(last, newone);

                    if (distance > 15.0) {

                        if (distance < 400) {
                            mark.setRotation(bearing);
                            mark.setAnchor(0.5f, 0.5f);
                            mark.setFlat(true);
                            animateMarker(mark, newone, false);
                        } else {
                            mark.setRotation(bearing);
                            mark.setAnchor(0.5f, 0.5f);
                            mark.setFlat(true);
                            mark.setPosition(newone);
                        }

                    }
                    allMarkers.put(driver.getDriver_id(), new DriverMark(driver.getCar_type(), mark));

                } else {

                    Marker mark = drawMarker(new LatLng(Double.parseDouble(driver.getLat()), Double.parseDouble(driver.getLng())), carImage);
                    if (!driver.getCar_type().equalsIgnoreCase(selectedCartype)) {
                        mark.setVisible(false);
                    } else {
                        mark.setVisible(true);
                    }
                    LatLng newone = new LatLng(Double.parseDouble(driver.getLat()), Double.parseDouble(driver.getLng()));
                    mark.setAnchor(0.5f, 0.5f);
                    mark.setFlat(true);
                    mark.setPosition(newone);
                    allMarkers.put(driver.getDriver_id(), new DriverMark(driver.getCar_type(), mark));

                }

            }

        }

        removeOdds(car.getId(), drivers);
    }
*/
    private Bitmap createcar(Bitmap car) {

        View view = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.car, null);
        ImageView carImage = (ImageView) view.findViewById(R.id.carimg);

        if (car != null) {
            carImage.setImageBitmap(car);
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    private synchronized void plotCars(Cars car, ArrayList<Drivers> drivers) throws ExecutionException, InterruptedException {

        selectedCartype = car.getId();

        if (drivers != null && drivers.size() > 0) {

            for (int i = 0; i < drivers.size(); i++) {

                Drivers driver = drivers.get(i);

                if (allMarkers.containsKey(driver.getDriver_id())) {

                    DriverMark driverMark = (DriverMark) allMarkers.get(driver.getDriver_id());

                    Marker mark = driverMark.getMarker();

                    if (!driver.getCar_type().equalsIgnoreCase(selectedCartype)) {
                        mark.setVisible(false);
                    } else {
                        if (carImage != null) {
                            mark.setIcon(BitmapDescriptorFactory.fromBitmap(createcar(carImage)));
                            mark.setVisible(true);
                        } else {
                            drawDriverMarker(mark, car.getMap_car_image());
                        }
                    }

                    LatLng last = new LatLng(mark.getPosition().latitude, mark.getPosition().longitude);

                    LatLng newone = new LatLng(Double.parseDouble(driver.getLat()), Double.parseDouble(driver.getLng()));

                    double distance = SphericalUtil.computeDistanceBetween(last, newone);

                    float bearing = bearingBetweenLatLngs(last, newone);

                    if (distance > 15.0) {
                        if (bearing < 180.0) {
                            mark.setRotation(bearing);
                            mark.setAnchor(0.5f, 0.5f);
                            mark.setFlat(true);
                            animateMarker(mark, newone, false);
                        } else {
                            mark.setRotation(bearing);
                            mark.setAnchor(0.5f, 0.5f);
                            mark.setFlat(true);
                            animateMarker(mark, newone, false);
                        }

                    }
                    allMarkers.put(driver.getDriver_id(), new DriverMark(driver.getCar_type(), mark));

                } else {

                    Marker mark = drawMarker(new LatLng(Double.parseDouble(driver.getLat()), Double.parseDouble(driver.getLng())), carImage);
                    if (!driver.getCar_type().equalsIgnoreCase(selectedCartype)) {
                        mark.setVisible(false);
                    } else {
                        mark.setVisible(true);
                    }
                    LatLng newone = new LatLng(Double.parseDouble(driver.getLat()), Double.parseDouble(driver.getLng()));
                    mark.setAnchor(0.5f, 0.5f);
                    mark.setFlat(true);
                    mark.setPosition(newone);
                    allMarkers.put(driver.getDriver_id(), new DriverMark(driver.getCar_type(), mark));

                }

            }

        }

        removeOdds(car.getId(), drivers);
    }

    private void removeOdds(String carType, ArrayList<Drivers> drivers) {

        if (drivers != null) {

            Iterator it = allMarkers.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                String driverId = (String) pair.getKey();
                boolean containsOrnot = false;
                for (Drivers driver : drivers) {
                    if (driver.getDriver_id().equalsIgnoreCase(driverId)) {
                        containsOrnot = true;
                    }
                }

                if (!containsOrnot) {

                    Marker mark = ((DriverMark) pair.getValue()).getMarker();
                    if (mark != null) {
                        mark.remove();
                    }

                    it.remove();
                }
            }
        } else {
            Iterator it = allMarkers.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();

                Marker mark = ((DriverMark) pair.getValue()).getMarker();


                mark.remove();
                it.remove();

            }
        }

    }


    /*private void removeOdds(String carType, HashMap<String, Drivers> drivers) {

        if (drivers != null) {

            Iterator it = allMarkers.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                String driverId = (String) pair.getKey();
                boolean containsOrnot = false;
                for (Map.Entry<String, Drivers> entry : drivers.entrySet()) {

                    System.out.println(pair.getKey() + " = " + entry.getValue());
                    Drivers d = (Drivers) entry.getValue();
                    if (d.getDriver_id().equalsIgnoreCase(driverId)) {
                        containsOrnot = true;
                    }
                }

                if (!containsOrnot) {

                    Marker mark = ((DriverMark) pair.getValue()).getMarker();
                    if (mark != null) {
                        mark.remove();
                    }

                    it.remove();
                }
            }
        } else {
            Iterator it = allMarkers.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();

                Marker mark = ((DriverMark) pair.getValue()).getMarker();


                mark.remove();
                it.remove();

            }
        }
    }*/


    /*private BroadcastReceiver Node_CUSTOMER_Location_Update = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {

            String request = in.getStringExtra("Object");

            JSONArray data = null;

            Log.d("Driver-Location-Single", request.toString());

            try {
                data = new JSONArray(request);
                if (data != null) {


                    for (int j = 0; j < data.length(); j++) {


                        JSONObject object = (JSONObject) data.get(j);
                        if (availCars.getStringHashMapHashMap().containsKey(object.getString("car_type"))) {

                            HashMap<String, Drivers> map = availCars.getStringHashMapHashMap().get(object.getString("car_type"));

                            if (map.containsKey(object.getString("driver_id"))) {
                                Drivers drivers = new Drivers();
                                drivers.setCar_type(object.getString("car_type"));
                                drivers.setDriver_id(object.getString("driver_id"));
                                drivers.setLat(object.getString("lat"));
                                drivers.setLng(object.getString("lng"));
                                map.put(drivers.getDriver_id(), drivers);
                                availCars.getStringHashMapHashMap().put(object.getString("car_type"), map);

                            } else {
                                Drivers drivers = new Drivers();
                                drivers.setCar_type(object.getString("car_type"));
                                drivers.setDriver_id(object.getString("driver_id"));
                                drivers.setLat(object.getString("lat"));
                                drivers.setLng(object.getString("lng"));
                                map.put(drivers.getDriver_id(), drivers);
                                availCars.getStringHashMapHashMap().put(object.getString("car_type"), map);
                            }

                        }


                    }
                    plotCars(specifiedCars, availCars.getStringHashMapHashMap().get(specifiedCars.getId()));


                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    };
*/


    private BroadcastReceiver Node_CUSTOMER_Location_Update = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent in) {

            String request = in.getStringExtra("Object");

            JSONArray data = null;

            Log.d("Driver-Location-Single", request.toString());

            try {
                data = new JSONArray(request);
                if (data != null) {


                    for (int j = 0; j < data.length(); j++) {

                        JSONObject object = (JSONObject) data.get(j);

                        if (availCars.getStringHashMapHashMap().containsKey(object.getString("car_type"))) {

                            HashMap<String, Drivers> map = availCars.getStringHashMapHashMap().get(object.getString("car_type"));

                            if (map.containsKey(object.getString("driver_id"))) {

                                Drivers drivers = new Drivers();
                                drivers.setCar_type(object.getString("car_type"));
                                drivers.setDriver_id(object.getString("driver_id"));
                                drivers.setLat(object.getString("lat"));
                                drivers.setLng(object.getString("lng"));
                                drivers.setSurge_applied(object.optBoolean(

                                        "surge_applied"));
                                drivers.setSurge_message(object.optString("surge_message"));
                                drivers.setFare_multiplier(object.optString("fare_multiplier"));

                                map.put(drivers.getDriver_id(), drivers);

                                availCars.getStringHashMapHashMap().put(object.getString("car_type"), map);

                            } else {
                                Drivers drivers = new Drivers();
                                drivers.setCar_type(object.getString("car_type"));
                                drivers.setDriver_id(object.getString("driver_id"));
                                drivers.setLat(object.getString("lat"));
                                drivers.setLng(object.getString("lng"));
                                drivers.setSurge_applied(object.optBoolean("surge_applied"));
                                drivers.setSurge_message(object.optString("surge_message"));
                                drivers.setFare_multiplier(object.optString("fare_multiplier"));
                                map.put(drivers.getDriver_id(), drivers);

                                availCars.getStringHashMapHashMap().put(object.getString("car_type"), map);
                            }


                            // Time Updation

                            Location locationA = new Location("pickup");
                            locationA.setLatitude(userlat);
                            locationA.setLongitude(userlng);

                            Location locationb = new Location("drop");
                            locationb.setLatitude(object.optDouble("lat"));
                            locationb.setLongitude(object.optDouble("lng"));

                            int time = locationUtils.getTimebetweenLocation(locationA, locationb);

                            if (CustomerHomeActivity.timings.containsKey(object.getString("car_type"))) {
                                int currentTime = CustomerHomeActivity.timings.get(object.getString("car_type"));
                                if (currentTime != -1 && currentTime > time) {
                                    CustomerHomeActivity.timings.put(object.getString("car_type"), time);
                                } else {
                                    CustomerHomeActivity.timings.put(object.getString("car_type"), time);
                                }
                            } else {
                                CustomerHomeActivity.timings.put(object.getString("car_type"), time);
                            }


                        }
                    }

                    updateDrivers();


                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    };

    public void updateDrivers() {
        try {




            HashMap<String, Drivers> stringDriversHashMap = availCars.getStringHashMapHashMap().get(specifiedCars.getId());

            ArrayList<Drivers> drivers = new ArrayList<>();

            drivers.addAll(stringDriversHashMap.values());

            plotCars(specifiedCars, drivers);


        } catch (Exception e) {

        }
    }


    public void animateMarker(final Marker m, final LatLng toPosition, final boolean hideMarke) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = mMap.getProjection();
        Point startPoint = proj.toScreenLocation(m.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 1500;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                m.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarke) {
                        m.setVisible(false);
                    } else {
                        m.setVisible(true);
                    }
                }
            }
        });
    }

    private Marker drawMarker(LatLng point, Bitmap car) {
        // Creating an instance of MarkerOptions
        Marker marker = mMap.addMarker(new MarkerOptions().position(point).icon(BitmapDescriptorFactory.fromBitmap(createcar(car))));
        return marker;


    }

    private float bearingBetweenLatLngs(LatLng begin, LatLng end) {
        Location beginL = convertLatLngToLocation(begin);
        Location endL = convertLatLngToLocation(end);

        return beginL.bearingTo(endL);
    }

    private Location convertLatLngToLocation(LatLng latLng) {
        Location loc = new Location("someLoc");
        loc.setLatitude(latLng.latitude);
        loc.setLongitude(latLng.longitude);
        return loc;
    }


    /**
     * Get City
     **/
    private String myCity = "";

    private void getCity() {
        Geocoder gcd = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(userlat, userlng, 1);

            if (addresses != null) {
                if (addresses.get(0).getSubAdminArea() != null) {
                    myCity = addresses.get(0).getSubAdminArea();
                } else {
                    myCity = addresses.get(0).getLocality();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    private void setlayoutWidth() {

        ViewTreeObserver observer = mapContainer.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                // TODO Auto-generated method stub
                initMapConatiner();
                mapContainer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

    }

    protected void initMapConatiner() {
        mapHeight = mapContainer.getHeight() - 200;
        mapWidth = mapContainer.getWidth();
    }

    @Override
    public void onCameraIdle() {
        if (makeRoute) {
            try {
                setMarkers();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                makeRoute = false;
            }
        }
    }

    NotificationManager mNotificationManager;
    PendingIntent contentIntent;
    public static final int NOTIFICATION_ID = 1000;

    private void sendNotifications(String msg) {

        Intent newone = new Intent(this, TrackRide.class);
        newone.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        contentIntent = PendingIntent.getActivity(this, 0, newone, 0);
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this).setSmallIcon(R.drawable.centered)
                .setContentTitle("Hoponn")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setContentText(msg);
        mBuilder.setAutoCancel(true);

        mBuilder.setContentIntent(contentIntent);
        AssetFileDescriptor afd = null;

        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

    }


    /**
     * Draw car if application dont have it
     */

    private Bitmap theBitmap = null;

    private void drawDriverMarker(final Marker mark, final String image_path) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                if (Looper.myLooper() == null) {
                    Looper.prepare();
                }
                try {
                    theBitmap = Glide.
                            with(ConfirmRideActivity.this).
                            load(image_path).
                            asBitmap().
                            into(-1, -1).
                            get();
                } catch (final ExecutionException e) {
                    String err = (e.getMessage() == null) ? "SD Card failed" : e.getMessage();
                    Log.e("sdcard-err2:", err);
                } catch (final InterruptedException e) {
                    String err = (e.getMessage() == null) ? "SD Card failed" : e.getMessage();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void dummy) {
                if (null != theBitmap) {
                    // The full bitmap should be available here
                    if (mMap != null) {

                        if (mark != null) {
                            mark.setIcon(BitmapDescriptorFactory.fromBitmap(createcar(theBitmap)));
                        } else {
                            mark.setIcon(BitmapDescriptorFactory.fromBitmap(createcar(theBitmap)));
                        }
                    }
                }
            }
        }.execute();

    }

    @Override
    public void onMapLoaded() {

        ArrayList<LatLng> allBounds = new ArrayList<>();
        allBounds.add(markerpick.getPosition());
        allBounds.add(markerdrop.getPosition());

        if (mMap != null && allBounds != null && !allBounds.isEmpty()) {
            LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
            for (LatLng latLngPoint : allBounds)
                boundsBuilder.include(latLngPoint);

            int routePadding = 100;
            LatLngBounds latLngBounds = boundsBuilder.build();
            try {
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, routePadding));
            } catch (Exception e) {

            }
        }
    }

    AvailCars availCars;

    @Override
    public void getCarsResponse(String response) {


        availCars = new AvailCars();

        try {

            if (response != null) {
                availCars.getCars(response);

                if (availCars.getCarList() != null && availCars.getCarList().size() > 0) {


                    for (Cars cars : availCars.getCarList()) {
                        if (cars.getId().equalsIgnoreCase(carType)) {
                            specifiedCars = cars;
                            break;
                        }
                    }
                    carImage = MyApplication.carImages.get(specifiedCars.getId());


                    HashMap<String, Drivers> stringDriversHashMap = availCars.getStringHashMapHashMap().get(specifiedCars.getId());

                    ArrayList<Drivers> drivers = new ArrayList<>();

                    drivers.addAll(stringDriversHashMap.values());


                    plotCars(specifiedCars, drivers);
                } else {

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void showProgreass() {

        loader = new Dialog(ConfirmRideActivity.this);
        loader.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loader.setContentView(R.layout.dialog_progress);
        loader_wheel = (ProgressWheel) loader.findViewById(R.id.progress_wheel);
        loader_wheel.setCircleRadius(50);
        loader.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        loader.setCancelable(false);
        loader.show();


    }


    @Override
    public void hideProgress() {
        if (loader != null) {
            loader.dismiss();
        }

    }

    @Override
    public void onFail(Throwable t) {
        estimationProgress.setVisibility(View.GONE);
        estimationvalue.setText("--");

    }

    @Override
    public void getEstimateResponse(String response) {

        try {

            JSONObject cobj = new JSONObject(response);
            estimationProgress.setVisibility(View.GONE);
            boolean status = cobj.getBoolean("status");

            if (status) {

                JSONObject data = cobj.getJSONObject("data");

                if (data != null) {

                    estimation = new Estimation();
                    estimation.setEstimated_fare(data.getString("estimated_fare"));
                    estimation.setFare_expiration_time(data.getString("surge_applied"));
                    estimation.setFare_multiplier(data.getString("fare_multiplier"));
                    estimation.setSurge_applied(data.getString("fare_expiration_time"));
                    estimation.setTotal_passengers(data.getString("total_passengers"));

                    estimationvalue.setText(Html.fromHtml(estimation.getEstimated_fare()));
                } else {
                    estimationvalue.setText("--");
                }

            }


        } catch (JSONException e) {
            e.printStackTrace();
            estimationProgress.setVisibility(View.GONE);
            estimationvalue.setText("--");
        }

    }

    @Override
    public void driverSelectREsponse(String response) {

        hideProgress();
        System.out.println("DRIVERSELECT===" + response);

        try {
            JSONObject obj = new JSONObject(response);

            boolean status = obj.getBoolean("status");
            String message = obj.getString("message");
            int code = obj.getInt("code");
            if (status) {
                if (countDownTimer != null) {
                    countDownTimer.cancel();
                    setCountDown();
                } else {
                    setCountDown();
                }
                confirmLays.setVisibility(View.VISIBLE);

                progressText.setText("Waiting for driver response");
            } else {
                IsRideOpen = false;
                if (pdialog != null) {
                    pdialog.dismiss();
                    pdialog = null;
                }
                if (message.equalsIgnoreCase("Your card has been deemed invalid, Please update your payment settings.")) {
                    Dialog(1, message);
                } else if (code == 417) {
                    Dialog(5, message);
                }
            }

        } catch (JSONException e) {
            IsRideOpen = false;
            e.printStackTrace();
            if (pdialog != null) {
                pdialog.dismiss();
                pdialog = null;
            }
            Dialog(1, "Please Try Again");
        }


    }

    @Override
    public void driverSelectError(Throwable t) {
        hideProgress();
        IsRideOpen = false;
        hideProgress();
        if (pdialog != null) {
            pdialog.dismiss();
            pdialog = null;
        }
        Dialog(1, "Please Try Again");


    }

    @Override
    public void getPromoCodeResponse(String response) {
        hideProgress();

        System.out.println("PROMOCODE===" + response);

        String message = "";
        int return_code;

        JSONObject dataObject = null;
        try {
            dataObject = new JSONObject(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        parsresp = dataObject.optString("status").toString();
        message = dataObject.optString("message").toString();
        promotion_id = dataObject.optString("promotion_id").toString();
        return_code = dataObject.optInt("code");

        if (!TextUtils.isEmpty(parsresp)) {

            if (parsresp.equals("TRUE")) {

                promostatus = true;

                editTextModifier(mPromoCode);
                if (pdialog != null) {
                    pdialog.dismiss();
                    pdialog = null;
                }
                Dialog(2, message);

            } else if (parsresp.equals("FALSE")) {
                if (pdialog != null) {
                    pdialog.dismiss();
                    pdialog = null;
                }
                Dialog(2, message);
            } else {
                if (pdialog != null) {
                    pdialog.dismiss();
                    pdialog = null;
                }
                Dialog(2, message);
            }

        } else {

            Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
            System.exit(0);
        }


    }

    @Override
    public void getPromocodeerror(Throwable t) {
        hideProgress();

    }

    @Override
    public void setPresenter(CustomerContract.Presenter presenter) {
        this.presenter = presenter;

    }
}
