package com.us.hoodruncustomer.share;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.pnikosis.materialishprogress.ProgressWheel;
import com.us.hoodruncustomer.R;
import com.us.hoodruncustomer.app.MyApplication;
import com.us.hoodruncustomer.commonwork.Injection;
import com.us.hoodruncustomer.commonwork.SimpleActivity;
import com.us.hoodruncustomer.custom.DrawableClickListener;
import com.us.hoodruncustomer.custom.MyEditText;
import com.us.hoodruncustomer.exceptionhandler.ExceptionHandler;
import com.us.hoodruncustomer.parser.ConnectionDetector;
import com.us.hoodruncustomer.parser.Parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ShareTheFare extends SimpleActivity  implements  ShareContract.View{
	private ShareContract.Presenter presenter;
	MyEditText mEdtNumber;
	LinearLayout mLayout;
	TextView mFinddriver;
	private boolean parseresponce;
	String parsresp,shareresp;

	Dialog dialog2;
	AlertDialog.Builder builder;
	private int driverResponse = 0;
	int k = 0;
	int flag;
	int shareflag=0;
	String customer_id,token,mob_no,payment_type,driver_id,share_status,ride_id,request_id;
	public static MyEditText mEditText[] = new MyEditText[6];

	public static RelativeLayout mRalativeArray[]=new RelativeLayout[6];
	boolean checked = true;
	boolean isNumberExist;
	ConnectionDetector cd;
	Drawable wrong;
	String font_path;
	Typeface tf;
	TextView headertext;
	JSONObject shareObj = new JSONObject();
	JSONArray jsonArr = new JSONArray();


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.share_fare);
		new SharePresenter(Injection.remoteDataRepository(getApplicationContext()), this);

		/**
		 * Important to catch the exceptions
		 */

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

		/*****************************************************************************************/


		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		if (toolbar != null) {
			int color = Color.parseColor("#ffffff");
			toolbar.setTitleTextColor(color);
			setSupportActionBar(toolbar);
			getSupportActionBar().setDisplayShowTitleEnabled(false);

			getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
			toolbar.setNavigationIcon(R.drawable.back_arrow);
			toolbar.setNavigationOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onBackPressed();
					finish();

				}
			});
			headertext = (TextView) findViewById(android.R.id.text1);

			headertext.setText("Share the Fare");

		}

		cd=new ConnectionDetector(ShareTheFare.this);


		font_path = "fonts/opensans.ttf";

		tf = Typeface.createFromAsset(this.getAssets(), font_path);
		headertext.setTypeface(tf, Typeface.BOLD);
		driver_id=mAppManager.getDriverID_Number();
		share_status="1";
		payment_type="card";
		wrong=getResources().getDrawable(R.drawable.wrong);
		wrong.setBounds(new Rect(0, 0, wrong.getIntrinsicWidth(), wrong.getIntrinsicHeight()));
		//set(navMenuTitles, navMenuIcons);
		mLayout=(LinearLayout)findViewById(R.id.linearLayout);
		//mRelativeLayout=(RelativeLayout)findViewById(R.id.innerlinearLayout);


		mEdtNumber=(MyEditText)findViewById(R.id.EditText01);

		mEdtNumber.setImeActionLabel("CHECK", EditorInfo.IME_ACTION_DONE);

		mFinddriver=(TextView) findViewById(R.id.imageButton1);
		token=mAppManager.getTokenCode();
		customer_id=mAppManager.getCustomerID();

		ride_id=mAppManager.App_getDriver_ride_id();
		request_id=mAppManager.App_getDriver_request_id();


		mEditText[0]=mEdtNumber;


		editTextGenerator(mEdtNumber);
		checkLength(mEditText[k]);


		mFinddriver.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int c=0,e=0;
				int i;
				for(i = 0;i<=k;i++)
				{
					System.out.println("edit values......"+ mEditText[i].getText().toString());
					JSONObject pnObj = new JSONObject();
					try {
						pnObj.put("mobile", mEditText[i].getText().toString());
						jsonArr.put(pnObj);
					} catch (JSONException ex) {
						// TODO Auto-generated catch block
						ex.printStackTrace();
					}

					if(mEditText[i].getText().toString().length()!=0)
					{
						c++;
					}
					else if(mEditText[i].getText().toString().length()<14 && mEditText[i].getText().toString().length()>0){
						e++;
						Toast.makeText(getApplicationContext(), "Please check the mobile number/s you've entered.", Toast.LENGTH_SHORT).show();
					}
				}


				if(c==0){

					Toast.makeText(getApplicationContext(), "Enter phone numbers to share the fare", Toast.LENGTH_SHORT).show();
				}else if(shareflag > 0 && e==0 && checked)
				{
					if(cd.isConnectingToInternet()){

						mAppManager.savePaymentType("1");
						payment_type="1";
						try {
							shareObj.put("share_mobile", jsonArr);

							//return shareObj.toString();
						} catch (JSONException je) {
							// TODO Auto-generated catch block
							je.printStackTrace();
						}

						System.out.println("RESPONSE:"+shareObj.toString());


						shareTheFare();
					}else {
						Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_LONG).show();
						System.exit(0);
					}
				}
				else Toast.makeText(getApplicationContext(), "Please enter correct phone number", Toast.LENGTH_SHORT).show();

			}
		});


	}
	private void editTextGenerator(final MyEditText edtTxt){

		drawableClick(edtTxt);

		//mLayout.addView(createNewEditText());
	}
	private RelativeLayout createNewEditText() {
		final LayoutParams lparamsrelative = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

		final LayoutParams lparamsedttxt = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

		if(k<5)
		{
			k++;
			flag=k;
			System.out.println("flag "+flag);
			mRalativeArray[flag]=new RelativeLayout(ShareTheFare.this);
			mRalativeArray[flag].setId(flag+20);
			mRalativeArray[flag].setLayoutParams(lparamsrelative);
			mEditText[flag] = new MyEditText(ShareTheFare.this);
			mEditText[flag].setLayoutParams(lparamsedttxt);
			mEditText[flag].setHint("MOBILE NUMBER");
			mEditText[flag].setSelection(0);
			mEditText[flag].setImeActionLabel("CHECK", EditorInfo.IME_ACTION_DONE);
			mEditText[flag].setHintTextColor(Color.parseColor("#222222"));
			mEditText[flag].setTextColor(Color.parseColor("#222222"));
			mEditText[flag].setId(flag);
			mEditText[flag].setInputType(InputType.TYPE_CLASS_NUMBER);
			mEditText[flag].setSingleLine(true);



			setEditTextMaxLength(  mEditText[flag],10);

			mEditText[flag].setBackgroundResource(R.drawable.line);
			mEditText[flag].setCompoundDrawablesWithIntrinsicBounds(0, 0,
					R.drawable.share_plus, 0);
			if(k==(5)){

				mEditText[flag].setCompoundDrawablesWithIntrinsicBounds(0, 0,
						0, 0);
			}
			if(mEditText[flag].getParent()!=null)
				((ViewGroup)mEditText[flag].getParent()).removeView(mEditText[flag]);


			mRalativeArray[flag].addView( mEditText[flag]);

			checkLength(mEditText[flag]);

			drawableClick(mEditText[flag]);
			return mRalativeArray[flag];
		} else {

			Toast.makeText(getApplicationContext(), "Maximum "+5+" people are allowed ", Toast.LENGTH_SHORT).show();

			return null;
		}

	}

	private void drawableClick(final MyEditText editText){

		editText.setDrawableClickListener(new DrawableClickListener() {


			@Override
			public void onClick(DrawablePosition target) {
				switch (target) {
					case RIGHT:
						//Do something here
						if(!mEditText[k].getText().toString().equals(null)&& mEditText[k].getText().toString().trim().length()>0 && checked && isNumberExist){
							if(createNewEditText()!=null)
							{

								//	k--;
								//k++;
								mLayout.addView(createNewEditText());


								editText.setCompoundDrawablesWithIntrinsicBounds(0, 0,
										0, 0);






							}
						}

						break;

					default:
						break;
				}
			}

		});

	}

	private void checkLength(final MyEditText editText){
		editText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(s.length()>9){

					mob_no=editText.getText().toString();
					if(!checkNumberRepeatition(mob_no)){
						Toast.makeText(getApplicationContext(), "The given number is already entered", Toast.LENGTH_LONG).show();
					}
				}
				else
				if(s.length()==0)
				{
					editText.setTextColor(Color.BLACK);
					checked=true;
					editText.setCompoundDrawablesWithIntrinsicBounds(0, 0,
							R.drawable.share_plus, 0);
				}
			}



			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if(s.length()<10&&s.length()>0){
					editText.setError(null,wrong);
				}else if(s.length()==0){
					editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.share_plus, 0);
					editText.setTextColor(Color.BLACK);
				}
			}
		});


		editText.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				// TODO Auto-generated method stub

				if(actionId == EditorInfo.IME_ACTION_SEARCH ||
						actionId == EditorInfo.IME_ACTION_DONE ||
						actionId == EditorInfo.IME_ACTION_GO ||
						actionId == EditorInfo.IME_ACTION_NEXT||
						event.getAction()==KeyEvent.ACTION_DOWN
						)
				{

					if(cd.isConnectingToInternet()){
						if(checked){


							mobilenumberVerification();
						}else Toast.makeText(getApplicationContext(), "The given number is already entered", Toast.LENGTH_LONG).show();
					}else {
						Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_LONG).show();
						System.exit(0);
					}
				}
				return false;
			}
		});
	}

	private boolean checkNumberRepeatition(String mob_no) {

		if(k>0){
			for(int i=1;i<=k;i++){
				if(mob_no.equals(mEditText[k-i].getText().toString()))

					checked=false;

				else checked=true;
			}
		}

		return checked;
	}



	Dialog dialog3;
	private void mDialog(String message) {

		dialog3 = new Dialog(ShareTheFare.this, R.style.Theme_Dialog);
		dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
		Window window = dialog3.getWindow();
		window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);
		dialog3.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		dialog3.setContentView(R.layout.dialog_onebutton);
		dialog3.setCancelable(false);
		dialog3.setCanceledOnTouchOutside(false);
		dialog3.show();

		TextView mTitle=(TextView)dialog3.findViewById(R.id.text_title);
		TextView mDialogBody=(TextView)dialog3.findViewById(R.id.txt_dialog_body);
		TextView mOK=(TextView)dialog3.findViewById(R.id.txt_dialog_ok);

		mTitle.setTypeface(tf, Typeface.BOLD);
		mTitle.setVisibility(View.GONE);
		mDialogBody.setText(message);

		mOK.setText("OK");
		mOK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog3.dismiss();
			}
		});

	}



	Dialog dialog1;
	private void alertDialog(final EditText editText) {

		dialog1 = new Dialog(ShareTheFare.this, R.style.Theme_Dialog);
		dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
		Window window = dialog1.getWindow();
		window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);
		dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		dialog1.setContentView(R.layout.dialog_onebutton);
		dialog1.setCancelable(false);
		dialog1.setCanceledOnTouchOutside(false);
		dialog1.show();

		TextView mTitle=(TextView)dialog1.findViewById(R.id.text_title);

		mTitle.setTypeface(tf, Typeface.BOLD);
		TextView mDialogBody=(TextView)dialog1.findViewById(R.id.txt_dialog_body);
		TextView mOK=(TextView)dialog1.findViewById(R.id.txt_dialog_ok);

		mTitle.setVisibility(View.GONE);

		mDialogBody.setText("No accounts are  available with this mobile number for sharing the ride.");

		mOK.setText("OK");
		mOK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				dialog1.dismiss();
				editText.getText().clear();
				editText.setHint("MOBILE NUMBER");
				editText.setHintTextColor(Color.parseColor("#222222"));

			}
		});

	}

	Dialog dialogmobile;
	private void MobileVerificationAlert(final EditText editText,String msg) {

		dialogmobile = new Dialog(ShareTheFare.this, R.style.Theme_Dialog);
		dialogmobile.requestWindowFeature(Window.FEATURE_NO_TITLE);
		Window window = dialogmobile.getWindow();
		window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);
		dialogmobile.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		dialogmobile.setContentView(R.layout.dialog_onebutton);
		dialogmobile.setCancelable(false);
		dialogmobile.setCanceledOnTouchOutside(false);
		dialogmobile.show();

		TextView mTitle=(TextView)dialogmobile.findViewById(R.id.text_title);

		mTitle.setTypeface(tf, Typeface.BOLD);
		TextView mDialogBody=(TextView)dialogmobile.findViewById(R.id.txt_dialog_body);
		TextView mOK=(TextView)dialogmobile.findViewById(R.id.txt_dialog_ok);

		mTitle.setVisibility(View.GONE);
		mDialogBody.setText(msg);

		mOK.setText("OK");
		mOK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				dialogmobile.dismiss();
				editText.getText().clear();
				editText.setHint("MOBILE NUMBER");
				editText.setHintTextColor(Color.parseColor("#222222"));

			}
		});

	}


	public void setEditTextMaxLength(final EditText editText, int length) {
		InputFilter[] FilterArray = new InputFilter[1];
		FilterArray[0] = new InputFilter.LengthFilter(length);
		editText.setFilters(FilterArray);
	}

	public String parsrespsh;
	private void mobilenumberVerification(){

		Map<String,String> params = new HashMap<String, String>();
		params.put("token_code", mAppManager.getTokenCode());
		params.put("customer_id", customer_id);
		params.put("mobile_number", mob_no);
		params.put("request_id", request_id);

		presenter.mobileVerification(params,this);

	}

	private void shareTheFare() {

		presenter.shareFare(mAppManager.getTokenCode(), request_id, this);

	}








	@Override
	protected void onResume() {
		super.onResume();
		MyApplication.activityResumed();
	}

	@Override
	protected void onPause() {
		super.onPause();
		MyApplication.activityPaused();
	}


	Dialog retry;
	private void retryDialog(final int i){

		retry = new Dialog(ShareTheFare.this, R.style.Theme_Dialog);
		retry.requestWindowFeature(Window.FEATURE_NO_TITLE);
		Window window = retry.getWindow();
		window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);
		retry.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		retry.setContentView(R.layout.thaks_dialog_twobutton);
		retry.setCancelable(false);
		retry.setCanceledOnTouchOutside(false);
		retry.show();

		TextView mTitle=(TextView)retry.findViewById(R.id.txt_title);
		mTitle.setText("Error!");
		mTitle.setTypeface(tf, Typeface.BOLD);
		TextView mDialogBody=(TextView)retry.findViewById(R.id.txt_dialog_body);
		mDialogBody.setText("Internet connection is slow or disconnected. Try connecting again.");
		LinearLayout mOK=(LinearLayout)retry.findViewById(R.id.lin_Ok);
		LinearLayout mCancel=(LinearLayout)retry.findViewById(R.id.lin_cancel);
		TextView mRetry=(TextView)retry.findViewById(R.id.txt_dialog_ok);
		TextView mExit=(TextView)retry.findViewById(R.id.txt_dialog_cancel);
		mRetry.setText("RETRY");
		mExit.setText("EXIT");

		mOK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {


				retry.dismiss();

				switch (i)
				{
					case 1:
						if(cd.isConnectingToInternet())
						{
							shareTheFare();
						}

						else{
							Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_SHORT).show();
							System.exit(0);
						}
						break;
					case 2:
						if(cd.isConnectingToInternet())
						{
							mobilenumberVerification();
						}
						else {
							Toast.makeText(getApplicationContext(), "No internet connection available", Toast.LENGTH_SHORT).show();
							System.exit(0);
						}
						break;
					default:
						break;

				}



			}
		});

		mCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				retry.dismiss();
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);

			}
		});

	}


	@Override
	protected void onStart() {
		super.onStart();
			}

	@Override
	protected void onDestroy() {
		super.onDestroy();

	}


	@Override
	public void getShareInfoResponse(String response) {

	}

	@Override
	public void showProgreass() {
		loader = new Dialog(ShareTheFare.this);
		loader.requestWindowFeature(Window.FEATURE_NO_TITLE);
		loader.setContentView(R.layout.dialog_progress);
		loader_wheel = (ProgressWheel) loader.findViewById(R.id.progress_wheel);
		loader_wheel.setCircleRadius(50);
		loader.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		loader.setCancelable(false);
		loader.show();
	}

	@Override
	public void hideProgress() {
		if (loader != null) {
			loader.dismiss();
		}

	}
	@Override
	public void onFail(Throwable t) {

		hideProgress();
		retryDialog(2);

	}

	@Override
	public void mobileVerifyResponse(String response) {

		hideProgress();
		System.out.println("MOBILEVERIFICATION==="+response);

		parsrespsh=Parser.shareNumber(response);

		if (!TextUtils.isEmpty(parsrespsh)) {
			if(parsrespsh.equalsIgnoreCase("TRUE")){

				System.out.println("inside trueeeeee "+Parser.getPerson_name());

				shareflag++;
				isNumberExist=true;
				mEditText[k].setFocusable(false);
				mEditText[k].setFocusableInTouchMode(false);
				mDialog(Parser.getMob_no_status_message());

				if(k==(5)){

					mEditText[k].setCompoundDrawablesWithIntrinsicBounds(0, 0,
							0, 0);
				}

				else mEditText[k].setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.share_plus, 0);

			}

			else if(parsrespsh.equalsIgnoreCase("FALSE"))
			{

				isNumberExist=false;
				MobileVerificationAlert(mEditText[k],""+Parser.getMob_no_status_message());

			}



		}else {

			Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
			System.exit(0);
		}

	}

	@Override
	public void shareFareResponse(String response) {

		hideProgress();

		System.out.println("SHARE==="+response);

		parsresp=Parser.shareNumber(response);

		if (!TextUtils.isEmpty(parsresp)) {

			if(parsresp.equalsIgnoreCase("TRUE")){

				finish();
			}

			else if(parsresp.equals("FALSE"))
			{
				Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
				System.exit(0);
			}



		}else {

			Toast.makeText(getApplicationContext(), "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
			System.exit(0);
		}


	}

	@Override
	public void shareFareFail(Throwable t) {

		hideProgress();
		retryDialog(2);

	}

	@Override
	public void setPresenter(ShareContract.Presenter presenter) {
		this.presenter = presenter;

	}
}
